<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body class="bg-primary">
    <div class="container">
        <div class="row d-flex justify-content-center p-5">
            <div class="col-12 col-md-8 col-lg-6 col-xl-6">
                <div class="card shadow-2-strong" style="border-radius: 1rem;">
                    <div class="card-body p-5 text-center">

                        <h3 class="mb-5">Admin Panel</h3>
                        <?php if($this->session->flashdata('status')=='error'): ?>
                        <div class="mb-3">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <?=$this->session->flashdata('msg');
                                ?>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        </div>
                        <?php endif; ?>
                        <form action="<?= base_url() ?>home/login" method="post">
                            <div class="form-outline mb-4">
                                <input type="text" name="username" id="typeEmailX-2" placeholder="Username" class="form-control form-control-lg">

                            </div>

                            <div class="form-outline mb-4">
                                <input type="password" name="password" id="typePasswordX-2" placeholder="Password" class="form-control form-control-lg">

                            </div>

                            <!-- Checkbox -->
                            <div class="form-check d-flex d-none justify-content-start mb-4">
                                <input class="form-check-input" type="checkbox" value="" id="form1Example3">
                                <label class="form-check-label" for="form1Example3"> Remember password </label>
                            </div>

                            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
                        </form>
                        <hr class="my-4">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>