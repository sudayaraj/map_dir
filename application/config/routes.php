<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = "home/login";
$route['register'] = "home/register";
$route['logout'] = "home/logout";
$route['post_ads'] = "home/post_ads";
