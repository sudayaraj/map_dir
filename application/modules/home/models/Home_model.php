<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home_Model extends CI_Model
{
    public function getCategories()
    {
        $this->db->where('isactive', 1);
        $this->db->order_by('categoryname', 'asc');
        $q = $this->db->get('category');
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function getrecentpost()
    {
        $this->db->select('*');
        $this->db->from('adposts');
        $this->db->join('adpost_img', 'adpost_img.adposts_id = adposts.id', 'left');
        $this->db->join('category', 'category.id = adposts.category_id', 'left');
        $this->db->where('adposts.isactive', 1);
        $this->db->limit(10);
        $this->db->order_by('adposts.created_datetime', 'desc');

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function getrandompost()
    {
        $this->db->select('*');
        $this->db->from('adposts');
        $this->db->join('adpost_img', 'adpost_img.adposts_id = adposts.id', 'left');
        $this->db->join('category', 'category.id = adposts.category_id', 'left');
        $this->db->where('adposts.isactive', 1);
        $this->db->limit(10);
        $this->db->order_by('adposts.id', 'RANDOM');

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function check_login($uname, $pwd)
    {
        $this->db->where(array('username' => $uname, 'password' => $pwd));
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            return $q->row_array();
        } else {
            return false;
        }
    }

    public function check_posts($category, $keyword, $type, $location)
    {
        if ($category > 0) {
            $this->db->where('category_id', $category);
            $c = true;
        }
        if (!empty($keyword)) {
            $this->db->like('product_name', $keyword);
            $k = true;
        }
        if ($type > 0) {
            $this->db->where('type', $type);
            $t = true;
        }
        if (!empty($location)) {
            $loc = explode(',', $location);
            $this->db->like('city', $loc[0]);
            $ci = true;
        }
        if (!empty($c) || !empty($k) || !empty($t) || !empty($ci)) {
            $q = $this->db->get('adposts');
            if ($q->num_rows() > 0) {
                return true;
            } else {
                if (!empty($keyword)) {
                    $this->db->like('category', $keyword);
                    $q2 = $this->db->get('adposts');
                    if ($q2->num_rows() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }

    public function check_user_data($uname)
    {
        $this->db->where(array('username' => $uname));
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            return $q->row_array();
        } else {
            return false;
        }
    }

    public function insert_userdata($data)
    {
        $this->db->insert('users', $data);
    }

    public function get_myads($userid)
    {
        $this->db->select('adposts.*,adpost_img.*,category.*,adposts.isactive as adposts_isactive,adposts.created_datetime as adposts_created_datetime');
        $this->db->from('adposts');
        $this->db->join('adpost_img', 'adpost_img.adposts_id = adposts.id', 'left');
        $this->db->join('category', 'category.id = adposts.category_id', 'left');
        // $this->db->where('adposts.isactive',1);
        $this->db->where('adposts.userid', $userid);
        $this->db->limit(10);
        $this->db->order_by('adposts.created_datetime', 'desc');

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function remove_myad($id, $userid, $setactive)
    {
        if ($setactive == 0) {
            $expiry_date = date("Y-m-d");
        } else {
            $expiry_date = NULL;
        }
        $this->db->where('id', $id);
        $this->db->where('userid', $userid);
        $this->db->update('adposts', array('isactive' => $setactive, 'expiry_date' => $expiry_date));
        return true;
    }

    public function get_keywords($term)
    {
        $this->db->like('keyword', $term);
        $this->db->limit(10);
        $q = $this->db->get('keyword');

        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function get_forgot_password($loginEmailId) {
        $this->db->select('*');
        $this->db->from('users');
        if(is_numeric($loginEmailId)){
            $this->db->where('mobile_no', $loginEmailId);
        } else {
            $this->db->where('username', $loginEmailId);
        }
        // $this->db->where('user_email', $loginEmailId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return '';
        }
    }

    public function update_user_activation_key($activation_key,$user_no) {
        $data = array('otp' => $activation_key,);
        // $data = array('activation_key' => $activation_key,);
        $this->db->where('username',$user_no);
        if ($this->db->update('users', $data)) {
            return true;
        }
        return false;
    }

    public function set_newpassword($confirmpassword, $email){
        $this->db->where('username', $email);
        $this->db->update('users', array('password'=>$confirmpassword));
        return "success";
    }

    public function get_mapdata(){
        $this->db->select('adposts.*, category.categoryname as category_name, type.typename as type_name, adpost_img.images as img_src');
        $this->db->from('adposts');
        $this->db->join('category', 'adposts.category_id = category.id', 'left');
        $this->db->join('type', 'adposts.type = type.id', 'left');
        $this->db->join('adpost_img', 'adposts.id = adpost_img.id', 'left');        
        $q=$this->db->get();
        if($q->num_rows() > 0){
            return $q->result_array();
        }else{
            return false;
        }
        
    }
}
