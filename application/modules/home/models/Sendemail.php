<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sendemail extends MY_Controller {

    private $from;
	private $to;
	private $config;
	private $subject;
	private $message;
	private $head;
	private $weburl;
	private $footer;
	private $support;
	private $admin, $query_email;

    public function __construct() {
		parent::__construct();
		// $this->prasanna = 'prasanna@travelpd.com';
		// $this->abhishek = 'abhishek@travelpd.com';
		$this->footer = '<br><br><b>Virtual2Live,<br>dgsd<br>ffsd, India<br>Tel: 1-234-567-890, Email: support@virtual2live.com</b>'; 
		$this->head = 'Virtual2live';
		$this->support = 'support@virtual2live.com';
		$this->query_email = 'support@virtual2live.com';
		$this->admin = 'support@virtual2live.com';
		$this->weburl = 'https://www.virtual2live.com';
	
		$this->from = 'it@virtual2live.com';
		$this->config = Array(
        'protocol' => 'telnet',
        'smtp_host' => 'mail.virtual2live.com',
        'smtp_port' => '25',
        'smtp_user' => 'it@virtual2live.com',
        'smtp_pass' => 'virtual2live@2023',
        'mailtype' => 'html',
        'starttls' => true,
        'newline' => "\r\n"
		); 
	}
	
	public function send($to, $subject, $message) {
		$this->load->library('email', $this->config);
		//$this->load->library('email');
		$this->email->initialize($this->config);
		$this->email->from($this->from, $this->head);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();
		// echo $this->email->print_debugger();
		// exit();
	}

    public function forgot_password($data) {
		$message = $data['otp'].' is the OTP for your email verification on '.$this->head.'. This OTP will be valid for 15 minutes';
		$msgbody = 'Dear User,<br>
		<p>Greetings From '.$this->head.',</p>
		<div>'.$message.'</div>
		<div>
			<p>Please do not hesitate to contact us on '.$this->query_email.' for all your Urgent Queries or Requirements.</p></div>
		<div>'.$this->footer.'</div>';
		$subject = 'Forgot Password';
		$this->send($data['email'], $subject, $msgbody);
		// return true;
	}

	public function password_change_email($data) {
		$msgbody = '<p>Greetings From '.$this->head.',</p>
		<div>Recently, you updated the password for this account :<br>
			<p>UserName : '.$data['email'].'</p>
		</div>
		<div>
			<p>If you do not requested to change the password, please inform us immediately.</p>
			<p>Please do not hesitate to contact us on '.$this->query_email.' for all your Urgent Queries / Requirements.</p>
		</div>
		<div>'.$this->footer.'</div>';
		$subject = 'Password Update';
		$this->send($data['email'], $subject, $msgbody);
	}

}

/* End of file Sendemail.php */
