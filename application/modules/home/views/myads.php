<?php $this->load->view('template/header'); ?>
<style>
    .prod_name {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    .inactivemode {
        opacity: .36;
    }

    .mt-5 {
        margin-top: 15px;
    }

    .btn-transp {
        border: 1px solid black;
        background-color: transparent;
        border-radius: 5px;
        font-size: 20px;
    }

    .btn-transp:hover {
        border: 2px solid black;
    }

    .right {
        float: right;
    }

    .txt-caps {
        text-transform: uppercase;
    }
</style>
<div class="container mt-5">
    <div class="row m-5">
        <div class="col-md-12 text-center">
            <h2>My ADS</h2>
        </div>
        <?php if (!empty($myads)) {
            foreach ($myads as $myad) { ?>
                <div class="col-md-12 mt-5">
                    <div class="panel panel-default shadow">
                        <div class="panel-body">
                            <div class="<?php if($myad->adposts_isactive==0){echo "inactivemode";}else{echo "activemode";} ?>">
                                <div class="col-md-2 txt-caps">
                                    <span>FROM: </span>
                                    <label><?php $fromDate = explode(' ', $myad->adposts_created_datetime);
                                            echo date("d M y", strtotime($fromDate[0])); ?></label>
                                    <?php if (!empty($myad->expiry_date)) { ?>
                                        <br>
                                        <span>TO: </span>
                                        <label><?= date("d M y", strtotime($myad->expiry_date)); ?></label>
                                    <?php } ?>
                                </div>
                                <div class="col-md-2">
                                    <?php $imageslide = explode(',', $myad->images); ?>
                                    <img src="<?= base_url() . 'adpostimages/' . $myad->adposts_id . '/images/' . $imageslide[0] ?>" alt="" width="50">
                                </div>
                                <div class="col-md-3">
                                    <span class="prod_name"><?= $myad->product_name ?></span>
                                </div>
                                <div class="col-md-2">
                                    <span>₹ <?= $myad->price ?></span>
                                </div>
                                <div class="col-md-2">
                                    <span class="label label-primary"><?php if ($myad->adposts_isactive == 1) {
                                                                            echo "Active";
                                                                        } else {
                                                                            echo "Inactive";
                                                                        } ?></span>
                                </div>
                                <!-- <div class="col-md-2">
                                <label style="font-size: larger;"><i class="fa fa-ellipsis-h"></i></label>
                                </div> -->
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="right">
                                    <?php if ($myad->adposts_isactive == 1) { ?>
                                        <button onclick="remove_myad(this)" data-setactive="0" data-activeid="<?= $myad->adposts_isactive ?>" data-userid="<?= $this->session->userdata('id'); ?>" data-refid="<?= $myad->adposts_id ?>" class="btn-transp">Remove</button>
                                    <?php } elseif ($myad->adposts_isactive == 0) { ?>
                                        <button onclick="remove_myad(this)" data-setactive="1" data-activeid="<?= $myad->adposts_isactive ?>" data-userid="<?= $this->session->userdata('id'); ?>" data-refid="<?= $myad->adposts_id ?>" class="btn-transp">Republish</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
        } else { ?>
            <div class="col-md-12 mt-5">
                <h3>No ADS Post is Found</h3>
            </div>
        <?php } ?>
    </div>
</div>
<?php $this->load->view('template/footer'); ?>
<script>
    function remove_myad(vl) {
        var refid = $(vl).data('refid');
        var userid = $(vl).data('userid');
        var setactive = $(vl).data('setactive');
        if (setactive == 0) {
            var setactivetext = 'Yes, delete it!';
            var status_text = 'Deleted!';
            var status_msg = 'Your AD has been deleted.';
        } else {
            var setactivetext = 'Yes, republish it!';
            var status_text = 'Republished!';
            var status_msg = 'Your AD has been republished.';
        }
        Swal.fire({
            title: 'Are you sure?',
            // text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: setactivetext
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: site_url + "home/remove_myad",
                    type: "POST",
                    data: {
                        refid: refid,
                        userid: userid,
                        setactive: setactive
                    },
                    success: function(data) {
                        // console.log(data);
                        // location.reload();
                    }
                });
                Swal.fire(
                    status_text,
                    status_msg,
                    'success'
                ).then((res) => {
                    if (res.isConfirmed) {
                        location.reload();
                    }
                })

            }
        })


    };
</script>