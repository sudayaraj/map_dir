<?php $this->load->view('template/header'); ?>
<style>
    .mt-5 {
        margin-top: 10px;
    }

    .border {
        border: 1px solid #7f9799;
    }

    .panel-body a {
        font-size: 20px;
    }

    .panel-body li {

        list-style: none;
    }
    @media (min-width: 992px){
    .ads_dropdown_menu {
        width: 50% !important;
        margin-left: 275px !important;
    }
}
</style>
<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5 text-center">
                <h1>POST YOUR AD</h1>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 ads_dropdown_menu">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Cars
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <li>
                                    <a href="<?=base_url()?>attributes/index/cars">Cars</a>
                                </li>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Properties
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <ul>
                                    <li><a href="#">For Sale: Houses & Apartments</a></li>
                                    <li><a href="#">For Rent: Houses & Apartments</a></li>
                                    <li><a href="#">Lands & Plots</a></li>
                                    <li><a href="#">For Rent: Shops & Offices</a></li>
                                    <li><a href="#">For Sale: Shops & Offices</a></li>
                                    <li><a href="#">PG & Guest Houses</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Mobiles
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <ul>
                                    <li><a href="#">Mobile Phones</a></li>
                                    <li><a href="#">Accessories</a></li>
                                    <li><a href="#">Tablets</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div></div>
            </div>
        </div>
    </div>
</main>


<?php $this->load->view('template/footer'); ?>