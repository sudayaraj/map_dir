<?php $this->load->view('template_home/header'); ?>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">


<div id="page-content">
    <div class="hero-section full-screen has-map">
        <div class="map-wrapper">
            <div class="geo-location">
                <i class="fa fa-map-marker"></i>
            </div>
            <div class="map" id="map-homepage"></div>
        </div>
        <!--end map-wrapper-->

        <div class="form search-form horizontal position-bottom inputs-dark">
            <div class="container">
                <form id="searchform">
                    <div class="row">
                        <div class="col-md-1 col-sm-3">
                            <div class="form-group">
                                <select class="form-control selectpicker" name="category">
                                    <option value="0" selected> All </option>
                                    <?php foreach ($categories as $category) { ?>
                                        <option value="<?= $category->id ?>"> <?= $category->categoryname ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-4-->
                        <div class="col-md-5 col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Find Cars, Mobile Phones and more...">
                                <input type="hidden" name="keyword_id">
                                <input type="hidden" name="keyword_categoryid">
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-4-->

                        <div class="col-md-3 col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control selectpicker" id="location" name="location">
                                <input type="hidden" name="manual_latitude" id="manual_latitude">
                                <input type="hidden" name="manual_longitude" id="manual_longitude">
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-4-->
                        <div class="col-md-2 col-sm-4">
                            <div class="form-group">
                                <select class="form-control selectpicker" name="type">
                                    <option value="0">All</option>
                                    <option value="1">Owner / Self</option>
                                    <option value="2">Broker</option>
                                    <option value="3">Agency</option>

                                </select>
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-4-->
                        <div class="col-md-1 col-sm-4">
                            <div class="form-group">
                                <!-- <button type="submit" data-ajax-response="map" data-ajax-data-file="assets/external/data_2.php" data-ajax-auto-zoom="1" class="btn btn-primary pull-right darker"><i class="fa fa-search"></i></button> -->
                                <button type="submit" class="btn btn-primary pull-right darker"><i class="fa fa-search"></i></button>
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-4-->
                    </div>
                    <!--end row-->

                </form>
                <!--end form-hero-->
            </div>
            <!--end container-->
        </div>
        <!--end search-form-->

    </div>
    <!--end hero-section-->
    <?php if(!empty($recentpost)){?>
    <div class="block">
        <div class="container">
            <div class="center">
                <div class="section-title">
                    <div class="center">
                        <h2>Recent Updates</h2>
                        <h3 class="subtitle">Fresh recommendations</h3>
                    </div>
                </div>
                <!--end section-title-->
            </div>
            <!--end center-->
            
            <div class="row">
                <?php $a = 1;
                $i = 1;
                foreach ($recentpost as $key => $recentposts) { ?>
                    <div class="col-md-3 col-sm-3">
                        <div class="item" data-id="<?= $recentposts->adposts_id ?>">
                            <a href="#" onclick="openModal(<?=$recentposts->adposts_id?>, 'home/modal_item')">
                                <div class="description">
                                    <figure>Price: <?= $recentposts->price ?></figure>
                                    <div class="label label-default"><?= $recentposts->categoryname ?></div>
                                    <h3><?= $recentposts->product_name ?></h3>
                                    <h4><?= $recentposts->city ?></h4>
                                </div>
                                <!--end description-->
                                <div class="image bg-transfer">
                                    <?php $imageslide = explode(',', $recentposts->images); ?>
                                    <img src="<?= base_url() . 'adpostimages/' . $recentposts->adposts_id . '/images/' . $imageslide[0] ?>" alt="">
                                </div>
                                <!--end image-->
                            </a>
                            <div class="additional-info">
                                <div class="rating-passive" data-rating="4">
                                    <span class="stars"></span>
                                    <span class="reviews d-none">6</span>
                                </div>
                                <div class="controls-more d-none">
                                    <ul>
                                        <!-- <li><a href="#">Add to favorites</a></li>
                                        <li><a href="#">Add to watchlist</a></li> -->
                                        <li><a href="#" class="quick-detail">Quick detail</a></li>
                                    </ul>
                                </div>
                                <!--end controls-more-->
                            </div>
                            <!--end additional-info-->
                        </div>
                        <!--end item-->
                    </div>
                    <!--<end col-md-3-->
                <?php if ($i++ == 4) break;
                } ?>
                <div id="show_viewmorepost" class="d-none">
                    <?php $r = 1;
                    foreach ($recentpost as $recentposts2) {
                        if ($r++ > 4) { ?>
                            <div class="col-md-3 col-sm-3">
                                <div class="item" data-id="<?= $recentposts2->adposts_id ?>">
                                    <a href="#">
                                        <div class="description">
                                            <figure>Price: <?= $recentposts2->price ?></figure>
                                            <div class="label label-default"><?= $recentposts2->categoryname ?></div>
                                            <h3><?= $recentposts2->product_name ?></h3>
                                            <h4><?= $recentposts2->city ?></h4>
                                        </div>
                                        <!--end description-->
                                        <div class="image bg-transfer">
                                            <?php $imageslide = explode(',', $recentposts2->images); ?>
                                            <img src="<?= base_url() . 'adpostimages/' . $recentposts2->adposts_id . '/images/' . $imageslide[0] ?>" alt="">
                                        </div>
                                        <!--end image-->
                                    </a>
                                    <div class="additional-info">
                                        <div class="rating-passive" data-rating="4">
                                            <span class="stars"></span>
                                            <span class="reviews d-none">6</span>
                                        </div>
                                        <div class="controls-more d-none">
                                            <ul>
                                                <!-- <li><a href="#">Add to favorites</a></li>
                                                <li><a href="#">Add to watchlist</a></li> -->
                                                <li><a href="#" class="quick-detail">Quick detail</a></li>
                                            </ul>
                                        </div>
                                        <!--end controls-more-->
                                    </div>
                                    <!--end additional-info-->
                                </div>
                                <!--end item-->
                            </div>
                            <!--<end col-md-3-->
                    <?php }
                    } ?>
                </div>
            </div>
            <!--end row-->
           <?php if(count($recentpost) > 4){?>
            <div class="center">
                <button id="view_morerecentpost" class="btn btn-primary btn-light-frame btn-rounded btn-framed arrow">View
                    More</button>
            </div>
            <?php } ?>
            <!--end center-->
            
        </div>
        <!--end container-->
    </div>
    <?php } ?>
    <!--end block-->
    <div class="container">
        <hr>
    </div>
    <div class="block">
        <div class="container">
            <div class="section-title">
                <div class="center">
                    <h2>Browse Our Listings Services</h2>
                </div>
            </div>
            <!--end section-title-->
            <div class="categories-list">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <div class="list-item">
                            <div class="title">
                                <div class="icon"><i class="fa fa-paint-brush"></i></div>
                                <h3><a href="#">Home Services</a></h3>
                            </div>
                            <!--end title-->
                            <ul>
                                <li><a href="">Home Cleaning & Maintenance</a>
                                    <figure class="count d-none">3</figure>
                                </li>
                                <li><a href="">Home Cooks</a>
                                    <figure class="count d-none">2</figure>
                                </li>
                                <li><a href="">AC Repair & Installation Services</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Electrical & Wood Works</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Painting</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Plumbing Repair Service</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Home Nurse</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                            </ul>
                        </div>
                        <!--end list-item-->
                    </div>
                    <!--end col-md-3-->
                    <div class="col-md-3 col-sm-3">
                        <div class="list-item">
                            <div class="title">
                                <div class="icon"><i class="fa fa-suitcase"></i></div>
                                <h3><a href="#">Wedding Services</a></h3>
                            </div>
                            <!--end title-->
                            <ul>
                                <li><a href="">Assembler</a>
                                    <figure class="count d-none">6</figure>
                                </li>
                                <li><a href="">Decorator</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Astro & Numaralogy</a>
                                    <figure class="count d-none">3</figure>
                                </li>
                                <li><a href="">Pandit</a>
                                    <figure class="count d-none">5</figure>
                                </li>
                                <li><a href="">Catering Services</a>
                                    <figure class="count d-none">5</figure>
                                </li>
                            </ul>
                        </div>
                        <!--end list-item-->
                    </div>
                    <!--end col-md-3-->
                    <div class="col-md-3 col-sm-3">
                        <div class="list-item">
                            <div class="title">
                                <div class="icon"><i class="fa fa-desktop"></i></div>
                                <h3><a href="#">Man Power Supplyer</a></h3>
                            </div>
                            <!--end title-->
                            <ul>
                                <li><a href="">Packers & Movers</a>
                                    <figure class="count d-none">10</figure>
                                </li>
                                <li><a href="">Securty Service</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Housekeeping and Maintenance</a>
                                    <figure class="count d-none">6</figure>
                                </li>
                                <li><a href="">Contract Labours</a>
                                    <figure class="count d-none">7</figure>
                                </li>
                            </ul>
                        </div>
                        <!--end list-item-->
                    </div>
                    <!--end col-md-3-->
                    <div class="col-md-3 col-sm-3">
                        <div class="list-item">
                            <div class="title">
                                <div class="icon"><i class="fa fa-graduation-cap"></i></div>
                                <h3><a href="#">Computer Services</a></h3>
                            </div>
                            <!--end title-->
                            <ul>
                                <li><a href="">Laptop & Computer rentals</a>
                                    <figure class="count d-none">8</figure>
                                </li>
                                <li><a href="">Laptop & PC Services</a>
                                    <figure class="count d-none">7</figure>
                                </li>
                                <li><a href="">Custom PC Builders </a>
                                    <figure class="count d-none">2</figure>
                                </li>
                                <li><a href="">CCTV Installation & Services </a>
                                    <figure class="count d-none">2</figure>
                                </li>
                                <li><a href="">Mobile Phone Service Center </a>
                                    <figure class="count d-none">2</figure>
                                </li>
                            </ul>
                        </div>
                        <!--end list-item-->
                    </div>
                    <!--end col-md-3-->
                </div>
                <!--end row-->
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <div class="list-item">
                            <div class="title">
                                <div class="icon"><i class="fa fa-television"></i></div>
                                <h3><a href="#">Booking Services</a></h3>
                            </div>
                            <!--end title-->
                            <ul>
                                <li><a href="">Hotel & Resorts</a>
                                    <figure class="count d-none">6</figure>
                                </li>
                                <li><a href="">Party Hall</a>
                                    <figure class="count d-none">9</figure>
                                </li>
                                <li><a href="">Hostel & Manson</a>
                                    <figure class="count d-none">1</figure>
                                </li>
                                <li><a href="">Trekking </a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Toursit Guide</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Tourism </a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Train Bus Flight Booking </a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">PG & Guest Houses </a>
                                    <figure class="count d-none">4</figure>
                                </li>
                            </ul>
                        </div>
                        <!--end list-item-->
                    </div>
                    <!--end col-md-3-->
                    <div class="col-md-3 col-sm-3">
                        <div class="list-item">
                            <div class="title">
                                <div class="icon"><i class="fa fa-university"></i></div>
                                <h3><a href="#">Property Buy/Rent/Lease</a></h3>
                            </div>
                            <!--end title-->
                            <ul>
                                <li><a href="">Home/ Office For Rental</a>
                                    <figure class="count d-none">3</figure>
                                </li>
                                <li><a href="">Land For Sale / Lease</a>
                                    <figure class="count d-none">2</figure>
                                </li>
                                <li><a href="">Land Property Brokers</a>
                                    <figure class="count d-none">6</figure>
                                </li>
                                <li><a href="">Document Writer</a>
                                    <figure class="count d-none">7</figure>
                                </li>
                                <li><a href="">Auditor</a>
                                    <figure class="count d-none">7</figure>
                                </li>
                                <li><a href="">Lawyer</a>
                                    <figure class="count d-none">7</figure>
                                </li>
                            </ul>
                        </div>
                        <!--end list-item-->
                    </div>
                    <!--end col-md-3-->
                    <div class="col-md-3 col-sm-3">
                        <div class="list-item">
                            <div class="title">
                                <div class="icon"><i class="fa fa-heart"></i></div>
                                <h3><a href="#">Second Hand Sales</a></h3>
                            </div>
                            <!--end title-->
                            <ul>
                                <li><a href="">Laptop & Computer</a>
                                    <figure class="count d-none">1</figure>
                                </li>
                                <li><a href="">Car and Bike</a>
                                    <figure class="count d-none">5</figure>
                                </li>
                                <li><a href="">Mobile</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Home Appliances </a>
                                    <figure class="count d-none">8</figure>
                                </li>
                            </ul>
                        </div>
                        <!--end list-item-->
                    </div>
                    <!--end col-md-3-->
                    <div class="col-md-3 col-sm-3">
                        <div class="list-item">
                            <div class="title">
                                <div class="icon"><i class="fa fa-newspaper-o"></i></div>
                                <h3><a href="#">Others</a></h3>
                            </div>
                            <!--end title-->
                            <ul>
                                <li><a href="">Car Mechanic</a>
                                    <figure class="count d-none">5</figure>
                                </li>
                                <li><a href="">Taxi & Drivers</a>
                                    <figure class="count d-none">9</figure>
                                </li>
                                <li><a href="">Yoga Teacher and Trainer</a>
                                    <figure class="count d-none">3</figure>
                                </li>
                                <li><a href="">Beauty & Spa Center</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                                <li><a href="">Chit Fund</a>
                                    <figure class="count d-none">4</figure>
                                </li>
                            </ul>
                        </div>
                        <!--end list-item-->
                    </div>
                    <!--end col-md-3-->
                </div>
                <!--end row-->
            </div>
            <!--end categories-list-->
        </div>
        <!--end container-->
    </div>
    <!--end block-->
    <div class="block big-padding">
        <div class="container">
            <div class="vertical-aligned-elements">
                <div class="element width-50">
                    <h3>Subscribe and be notified about new locations</h3>
                </div>
                <!--end element-->
                <div class="element width-50">
                    <form class="form form-email inputs-underline" id="form-subscribe">
                        <div class="input-group">
                            <input type="text" class="form-control" name="email" placeholder="Your email" required="">
                            <span class="input-group-btn">
                                <button class="btn" type="submit"><i class="arrow_right"></i></button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                    <!--end form-->
                </div>
                <!--end element-->
            </div>
            <!--end vertical-aligned-elements-->
        </div>
        <!--end container-->
        <div class="background-wrapper">
            <div class="background-color background-color-black opacity-5"></div>
        </div>
        <!--end background-wrapper-->
    </div>
    <!--end block-->
    <div class="block background-is-dark">
        <div class="container">
            <div class="section-title vertical-aligned-elements">
                <div class="element">
                    <h2>Vendor Announcement</h2>
                </div>
                <div class="element text-align-right">
                    <a href="#" class="btn btn-framed btn-rounded btn-default invisible-on-mobile">Promote
                        yours</a>
                    <div id="gallery-nav"></div>
                </div>
            </div>
            <!--end section-title-->
        </div>
        <div class="gallery featured">
            <div class="owl-carousel" data-owl-items="6" data-owl-loop="1" data-owl-auto-width="1" data-owl-nav="1" data-owl-dots="1" data-owl-nav-container="#gallery-nav">
                <?php foreach ($randomposts as $randompost) { ?>
                    <div class="item featured" data-id="<?= $randompost->adposts_id ?>">
                        <a href="#">
                            <div class="description">
                                <figure>Price: <?= $randompost->price ?></figure>
                                <div class="label label-default"><?= $randompost->categoryname ?></div>
                                <h3><?= $randompost->product_name ?></h3>
                                <h4><?= $randompost->city ?></h4>
                            </div>
                            <!--end description-->
                            <div class="image bg-transfer">
                                <?php $imageslide = explode(',', $randompost->images); ?>
                                <img src="<?= base_url() . 'adpostimages/' . $randompost->adposts_id . '/images/' . $imageslide[0] ?>" alt="">
                            </div>
                            <!--end image-->
                        </a>
                        <div class="additional-info">
                            <div class="rating-passive" data-rating="4">
                                <span class="stars"></span>
                                <span class="reviews">6</span>
                            </div>
                            <div class="controls-more">
                                <ul>
                                    <!-- <li><a href="#">Add to favorites</a></li>
                                    <li><a href="#">Add to watchlist</a></li> -->
                                    <li><a href="#" class="quick-detail">Quick detail</a></li>
                                </ul>
                            </div>
                            <!--end controls-more-->
                        </div>
                        <!--end additional-info-->
                    </div>
                    <!--end item-->
                <?php } ?>
            </div>
        </div>
        <!--end gallery-->
        <div class="background-wrapper">
            <div class="background-color background-color-default"></div>
        </div>
        <!--end background-wrapper-->
    </div>
    <!--end block-->

    <!--end block-->

    <div class="block d-none">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <div class="section-title">
                        <h2>Recently Rated Items</h2>
                    </div>
                    <!--end section-title-->
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="item" data-id="2">
                                <a href="detail.html">
                                    <div class="description">
                                        <div class="label label-default">Restaurant</div>
                                        <h3>Ironapple</h3>
                                        <h4>4209 Glenview Drive</h4>
                                    </div>
                                    <!--end description-->
                                    <div class="image bg-transfer">
                                        <img src="assets/img/items/2.jpg" alt="">
                                    </div>
                                    <!--end image-->
                                </a>
                                <div class="additional-info">
                                    <div class="rating-passive" data-rating="3">
                                        <span class="stars"></span>
                                        <span class="reviews">13</span>
                                    </div>
                                    <div class="controls-more">
                                        <ul>
                                            <li><a href="#">Add to favorites</a></li>
                                            <li><a href="#">Add to watchlist</a></li>
                                            <li><a href="#" class="quick-detail">Quick detail</a></li>
                                        </ul>
                                    </div>
                                    <!--end controls-more-->
                                </div>
                                <!--end additional-info-->
                            </div>
                            <!--end item-->
                        </div>
                        <!--<end col-md-4-->
                        <div class="col-md-5 col-sm-5">
                            <div class="item" data-id="3">
                                <figure class="ribbon">Top</figure>
                                <a href="detail.html">
                                    <div class="description">
                                        <figure>Starts at: 19:00</figure>
                                        <div class="label label-default">Event</div>
                                        <h3>Food Festival</h3>
                                        <h4>840 My Drive</h4>
                                    </div>
                                    <!--end description-->
                                    <div class="image bg-transfer">
                                        <img src="assets/img/items/4.jpg" alt="">
                                    </div>
                                    <!--end image-->
                                </a>
                                <div class="additional-info">
                                    <figure class="circle" title="Featured"><i class="fa fa-check"></i></figure>
                                    <div class="rating-passive" data-rating="5">
                                        <span class="stars"></span>
                                        <span class="reviews">12</span>
                                    </div>
                                    <div class="controls-more">
                                        <ul>
                                            <li><a href="#">Add to favorites</a></li>
                                            <li><a href="#">Add to watchlist</a></li>
                                            <li><a href="#" class="quick-detail">Quick detail</a></li>
                                        </ul>
                                    </div>
                                    <!--end controls-more-->
                                </div>
                                <!--end additional-info-->
                            </div>
                            <!--end item-->
                        </div>
                        <!--<end col-md-5-->
                        <div class="col-md-3 col-sm-3">
                            <div class="item" data-id="4">
                                <a href="detail.html">
                                    <div class="description">
                                        <div class="label label-default">Lounge</div>
                                        <h3>Cosmopolit</h3>
                                        <h4>2896 Ripple Street</h4>
                                    </div>
                                    <!--end description-->
                                    <div class="image bg-transfer">
                                        <img src="assets/img/items/5.jpg" alt="">
                                    </div>
                                    <!--end image-->
                                </a>
                                <div class="additional-info">
                                    <div class="rating-passive" data-rating="5">
                                        <span class="stars"></span>
                                        <span class="reviews">43</span>
                                    </div>
                                    <div class="controls-more">
                                        <ul>
                                            <li><a href="#">Add to favorites</a></li>
                                            <li><a href="#">Add to watchlist</a></li>
                                            <li><a href="#" class="quick-detail">Quick detail</a></li>
                                        </ul>
                                    </div>
                                    <!--end controls-more-->
                                </div>
                                <!--end additional-info-->
                            </div>
                            <!--end item-->
                        </div>
                        <!--<end col-md-3-->
                    </div>
                    <!--end row-->
                </div>
                <!--end col-md-9-->
                <div class="col-md-3 col-sm-3">
                    <div class="section-title">
                        <h2>Client’s Word</h2>
                    </div>
                    <div class="testimonials center box">
                        <div class="owl-carousel" data-owl-items="1" data-owl-nav="0" data-owl-dots="1">
                            <blockquote>
                                <div class="image">
                                    <div class="bg-transfer">
                                        <img src="assets/img/person-01.jpg" alt="">
                                    </div>
                                </div>
                                <h3>Jane Woodstock</h3>
                                <h4>CEO at ArtBrands</h4>
                                <p>Ut nec vulputate enim. Nulla faucibus convallis dui. Donec arcu enim,
                                    scelerisque gravida lacus vel.</p>
                            </blockquote>
                            <blockquote>
                                <div class="image">
                                    <div class="bg-transfer">
                                        <img src="assets/img/person-04.jpg" alt="">
                                    </div>
                                </div>
                                <h3>Peter Doe</h3>
                                <h4>CEO at ArtBrands</h4>
                                <p>Donec arcu enim, scelerisque gravida lacus vel, dignissim cursus lectus.
                                    Aliquam laoreet purus in iaculis sodales.</p>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <!--end col-md-3-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </div>
    <!--end block-->
    <div class="block d-none">
        <div class="container">
            <div class="section-title">
                <h2>User Review & Comments</h2>
            </div>
            <!--end section-title-->
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="text-element">
                        <h4><a href="blog-detail.html">Lorem ipsum dolor sit amet</a></h4>
                        <figure class="date">21.06.2015</figure>
                        <p>Ut nec vulputate enim. Nulla faucibus convallis dui. Donec arcu enim, scelerisque
                            gravida lacus vel, dignissim cursus</p>
                        <a href="blog-detail.html"><i class="arrow_right"></i></a>
                    </div>
                    <!--end text-element-->
                </div>
                <!--end col-md-4-->
                <div class="col-md-4 col-sm-4">
                    <div class="text-element">
                        <h4><a href="blog-detail.html">Sed et justo ut nibh condimentum lacinia</a></h4>
                        <figure class="date">13.06.2015</figure>
                        <p>Donec arcu enim, scelerisque gravida lacus vel, dignissim cursus lectus. Aliquam
                            laoreet purus in iaculis sodales. </p>
                        <a href="blog-detail.html"><i class="arrow_right"></i></a>
                    </div>
                    <!--end text-element-->
                </div>
                <!--end col-md-4-->
                <div class="col-md-4 col-sm-4">
                    <div class="text-element">
                        <h4><a href="blog-detail.html">Suspendisse varius eros id enim </a></h4>
                        <figure class="date">03.04.2015</figure>
                        <p>Nullam nec turpis blandit, sodales risus vitae, tincidunt velit. Vestibulum ac ipsum
                            tincidunt, vestibulum leo eget, </p>
                        <a href="blog-detail.html"><i class="arrow_right"></i></a>
                    </div>
                    <!--end text-element-->
                </div>
                <!--end col-md-4-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </div>
    <!--end block-->

    <div class="container">
        <div class="section-title text-center">
            <h2 style="font-family: Georgia, 'Times New Roman', Times, serif; font-weight:bold;">Sponsors</h2>
        </div>
        <hr>
    </div>
    <!--end container-->

    <div class="block">
        <div class="container">
            <div class="logos">
                <div class="logo">
                    <a href="#"><img src="assets/img/logo-1.png" alt=""></a>
                </div>
                <div class="logo">
                    <a href="#"><img src="assets/img/logo-2.png" alt=""></a>
                </div>
                <div class="logo">
                    <a href="#"><img src="assets/img/logo-3.png" alt=""></a>
                </div>
                <div class="logo">
                    <a href="#"><img src="assets/img/logo-4.png" alt=""></a>
                </div>
                <div class="logo">
                    <a href="#"><img src="assets/img/logo-5.png" alt=""></a>
                </div>
            </div>
            <!--/ .logos-->
        </div>
        <!--end container-->
    </div>
    <!--end block-->
</div>
<!--end page-content-->
<script>
    var site_url = "<?= site_url() ?>";
</script>
<?php $this->load->view('template/footer'); ?>
<script>
    window.onload = getlivelocate;

    function getlivelocate() {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;

                var infoWindowHTML = "Latitude: " + currentLatitude + "<br>Longitude: " + currentLongitude;
                var infoWindow = new google.maps.InfoWindow({
                    map: map,
                    content: infoWindowHTML
                });
                var currentLocation = {
                    lat: currentLatitude,
                    lng: currentLongitude
                };
                infoWindow.setPosition(currentLocation);
                getmap(currentLatitude, currentLongitude);
            });
        }
    }
</script>

<script>
    function getmap(currentLatitude, currentLongitude) {
        var optimizedDatabaseLoading = 0;
        // var _latitude = 40.7344458;
        // var _longitude = -73.86704922;
        var _latitude = currentLatitude;
        var _longitude = currentLongitude;
        var element = "map-homepage";
        var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
        var sidebarResultTarget = "sidebar"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
        var showMarkerLabels = true; // next to every marker will be a bubble with title
        var mapDefaultZoom = 14; // default zoom
        heroMap(_latitude, _longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);
    }
</script>

<script>
    $("#searchform").on("submit", function(event) {
        
        // console.log(5566);
        var form = $(this);
        var forms = form.serialize();
        // console.log(forms);
        event.preventDefault();

        const params = Object.fromEntries(new URLSearchParams(forms));
        // console.log(params.manual_longitude);




        $.ajax({
            type: "POST",
            url: site_url + "home/check_posts",
            data: form.serialize(), // serializes the form's elements.
            dataType: "JSON",
            success: function(data) {
                if (data.status == 'success') {
                    var _latitude = params.manual_latitude;
                    var _longitude = params.manual_longitude;
                    var element = "map-homepage";
                    var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
                    var sidebarResultTarget = "sidebar"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
                    var showMarkerLabels = true; // next to every marker will be a bubble with title
                    var mapDefaultZoom = 14; // default zoom
                    heroMap(_latitude, _longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);
                } else {
                    var _latitude = 'params.manual_latitude';
                    var _longitude = params.manual_longitude;
                    var element = "map-homepage";
                    var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
                    var sidebarResultTarget = "sidebar"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
                    var showMarkerLabels = false; // next to every marker will be a bubble with title
                    var mapDefaultZoom = 14; // default zoom
                    // heroMap(_latitude, _longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);
                    getlivelocate();
                    alert('No AD Found, Please try diffrent products');
                }
            }
        });
    });

    function unserialize(data) {
        data = data.toString().split('&');
        var response = {};
        for (var k in data) {
            var newData = data[k].split('=');
            response[newData[0]] = newData[1];
        }
        return response;
    }
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    $(function() {
        $("#keyword").autocomplete({
            source: "<?php echo base_url('home/search_keyword'); ?>",
            select: function(event, ui) {
                event.preventDefault();
                $("#keyword").val(ui.item.value);
                $("input[name=keyword_id]").val(ui.item.id);
                $("input[name=keyword_categoryid]").val(ui.item.categoryId);
            }
        });
    });
</script>