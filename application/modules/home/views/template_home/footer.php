<footer id="page-footer">
    <div class="footer-wrapper">
        <div class="block">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="element width-50">
                        <p data-toggle="modal" data-target="#myModal"><a href="#">Terms of
                                Use</a> and <a href="#">Privacy Policy</a>.</p>
                    </div>
                    <div class="element width-50 text-align-right">
                        <a href="#" class="circle-icon"><i class="social_twitter"></i></a>
                        <a href="#" class="circle-icon"><i class="social_facebook"></i></a>
                        <a href="#" class="circle-icon"><i class="social_youtube"></i></a>
                    </div>
                </div>
                <div class="background-wrapper">
                    <div class="bg-transfer opacity-50">
                        <img src="<?= base_url() ?>assets/img/footer-bg.png" alt="">
                    </div>
                </div>
                <!--end background-wrapper-->
            </div>
        </div>
        <div class="footer-navigation">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="element width-50">(C) 2023 Your Company, All right reserved</div>
                    <div class="element width-50 text-align-right">
                        <a href="<?= base_url() ?>">Home</a>
                        <!-- <a href="listing-grid-right-sidebar.html">Listings</a>
                                <a href="submit.html">Submit Item</a> -->
                        <a href="#">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--end page-footer-->

<!--signin modal start  -->
<div class="modal fade" id="sigin" tabindex="-1" role="dialog" aria-labelledby="siginLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="siginLabel"></h4>
            </div>
            <div class="modal-body">
                <form action="<?= site_url() ?>login" autocomplete="off" method="post">
                    <div style="margin: 16px auto;">
                        <label for="uname"><b>Username</b></label>
                        <input type="text" placeholder="Enter Username" name="uname" required>
                    </div>
                    <div style="margin: 16px auto;">
                        <label for="psw"><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="pwd" required>
                    </div>
                    <div class="form-group" style="margin: 16px auto;">
                        <button class="btn btn-primary" type="submit" style="margin: 16px auto;">Login</button>
                    </div>

                    <div class="my-3" style="background-color:#f1f1f1">

                        <span class="psw"> <a href="#" data-target="#Forgot_password" data-toggle="modal">Forgot Password?</a></span>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!--signin modal end -->
<!--Register modal start  -->
<div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="RegisterLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="RegisterLabel"> </h4>
            </div>
            <div class="modal-body">
                <form action="<?= site_url() ?>register" method="POST">
                    <div class="">
                        <h1>Register</h1>
                        <p>Please fill in this form to create an account.</p>

                        <div style="margin: 16px auto;">
                            <label for="email"><b>Email</b></label>
                            <input type="text" placeholder="Enter Email" name="email" id="email" required>

                        </div>
                        <div style="margin: 16px auto;">
                            <label for="psw"><b>Password</b></label>
                            <input type="password" placeholder="Enter Password" name="pwd" id="pwd_reg" required>
                        </div>
                        <div style="margin: 16px auto;">
                            <label for="psw-repeat"><b>Repeat Password</b></label>
                            <small class="d-none" id="pwd_mismatch" style="color:red;">Those passwords didn’t match. Try again.</small>
                            <input type="password" placeholder="Repeat Password" name="pwd_repeat" id="pwd_repeat" required>
                        </div>

                        <button class="btn btn-primary" id="registerbtn" type="submit" class="registerbtn">Register</button>
                    </div>

                    <div class=" signin" style="margin: 16px auto;">
                        <p>Already have an account? <a href="#" data-toggle="modal" data-target="#sigin">Sign
                                in</a>.</p>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!--Register modal end -->
<!--Forgot password modal start  -->
<div class="modal fade" id="Forgot_password" tabindex="-1" role="dialog" aria-labelledby="Forgot_passwordLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="Forgot_passwordLabel"> </h4>
            </div>
            <div class="modal-body">
                <form action="<?= site_url() ?>home/forgot_password" method="POST">
                    <div class="">
                        <h1>Forgot Password</h1>
                        <!-- <p>Please fill in this form to create an account.</p> -->

                        <div style="margin: 16px auto;">
                            <label for="email"><b>Email</b></label>
                            <input type="text" placeholder="Enter Email" name="email" id="email" required>

                        </div>
                        

                        <button class="btn btn-primary" id="registerbtn" type="submit" class="registerbtn">Reset Password</button>
                    </div>

                    
                </form>
            </div>

        </div>
    </div>
</div>
<!--Forgot password modal end -->
</div>
<!--end page-wrapper-->
<a href="#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>

<script>
    var site_url = "<?= site_url() ?>";
</script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap-select.min.js"></script>
<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&libraries=places"></script> -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAJXHqnGy3iKO5P7veP9_vo51A6zuP4ZiE&libraries=places"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/richmarker-compiled.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/markerclusterer_packed.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/infobox.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/moment.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/icheck.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.nouislider.all.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/custom.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/maps.js"></script>


<script>
    $('#pwd_repeat').keyup(function() {
        var pwd = $('#pwd_reg').val();
        var pwd_repeat = $('#pwd_repeat').val();
        if (pwd === pwd_repeat) {
            $('#registerbtn').prop("disabled", false);
            $('#pwd_mismatch').addClass('d-none');
        } else {
            $('#registerbtn').prop("disabled", true);
            $('#pwd_mismatch').removeClass('d-none');
        }
    });
</script>
<script src=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js "></script>
<?php if ($this->session->flashdata('success_msg')) { ?>
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: '<?= $this->session->flashdata('success_msg') ?>',
            showConfirmButton: false,
            timer: 1500
        })
    </script>
<?php } ?>
<script>
    $(document).ready(function() {
        var autocomplete;
        var id = 'location';

        autocomplete = new google.maps.places.Autocomplete((document.getElementById(id)), {
            types: ['geocode'],
        })

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            // alert(place);
            $("#manual_latitude").val(place.geometry.location.lat());
            $("#manual_longitude").val(place.geometry.location.lng());
        })
    });
</script>
</body>

</html>