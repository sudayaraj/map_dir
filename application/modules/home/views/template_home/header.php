<!DOCTYPE html>

<html lang="en-US">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="udaya">

    <link href="<?= base_url() ?>assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/zabuto_calendar.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.css" type="text/css">

    <link rel="stylesheet" href="<?= base_url() ?>assets/css/trackpad-scroll-emulator.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.nouislider.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css" type="text/css">
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css " rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>vrmall_assets/css/vr_mall.css">
    <title>Virtual2live</title>

</head>
<style>
    .customizer {
        display: none;
    }

    .page-wrapper {
        padding-top: 4rem !important;
    }


    .nav1 {
        position: absolute;
        z-index: 200;
        background-color: #322d2dd9 !important;
        width: 100%;
    }

    .vr_linknavbar.navbar:before,
    .vr_linknavbar.navbar:after {
        content: none !important;
    }

    .d-none {
        display: none !important;
    }

    @media only screen and (max-width: 1310px) {
        .page-wrapper {
            padding-top: 5rem !important;
        }
    }
</style>

<body class="homepage">



    <!-- nav1 -->
    <div class="nav1">
        <div class="container-fluid text-center">
            <div class="vr_linknavbar navbar ">
                <img src="" alt="Logo" class="logo">
                <!-- toggle img start -->
                <img src="<?= base_url() ?>assets/icons/toggle.jpg" style="width:34px;" alt="toggle" class="res_toggle" id="res_toggle">
                <!-- toggle img end -->
                <ul class="navul1 " id="navul1">
                    <li class="navli1"><a href="<?= base_url() ?>" class="nav_active">Virtual Link</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_mall">Virtual Mall</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_property">Virtual Property</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Virtual Education</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_studio">VR Studio</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_tour">VR Tour</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_dharisnam">VR Dharisanam</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Buy Sharing</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">CP Hub</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Live Stream</a></li>
                    <li class="navli1 navrmborder"><a href="#">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>


    <div class="page-wrapper">

        <header id="page-header">

            <!-- nav2 -->
            <nav>
                <div class="left">
                    <a href="<?= base_url() ?>" class="brand"><img src="<?= base_url() ?>assets/img/logo.png" alt=""></a>
                </div>
                <!--end left-->
                <div class="right">
                    <div class="primary-nav has-mega-menu">
                        <ul class="navigation">
                            <li class=""><a href="<?= base_url() ?>">Home</a>
                            </li>
                            <li><a href="#">Contact</a></li>
                            <?php if ($this->session->userdata('user_logged_in') == 1) { ?>
                                <li class="active"><a href="<?= base_url() ?>attributes/">SELL</a></li>
                            <?php } else { ?>
                                <li class="active"><a href="#" data-toggle="modal" data-target="#sigin">SELL</a></li>
                            <?php } ?>

                            </li>
                        </ul>
                        <!--end navigation-->
                    </div>
                    <!--end primary-nav-->
                    <div class="secondary-nav">
                        <?php
                        if ($this->session->userdata('user_logged_in') == 1) {
                        ?>

                            <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $this->session->userdata('username'); ?>
                                <i class="fa fa-chevron-down"></i>
                            </a>
                            <!-- <a href="<?= base_url() ?>logout">Sign Out</a> -->
                            <ul class="dropdown-menu">
                                <li><a href="<?= base_url() ?>home/myads">MY ADS</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?= base_url() ?>logout?callbackid=<?=base64_encode('home')?>">Sign Out</a></li>
                            </ul>
                        <?php } else { ?>
                            <a href="#" data-toggle="modal" data-target="#sigin">Sign In</a>
                            <a href="#" data-toggle="modal" data-target="#Register">Register</a>
                        <?php } ?>

                    </div>
                    <!--end secondary-nav-->

                    <div class="nav-btn">
                        <i></i>
                        <i></i>
                        <i></i>
                    </div>
                    <!--end nav-btn-->
                </div>
                <!--end right-->
            </nav>
            <!--end nav-->
        </header>
        <script>
            document.getElementById('res_toggle').addEventListener('click', () => {

                document.getElementById('navul1').classList.toggle('toggleshow')
                document.getElementById('page-header').classList.toggle('d-none');
            })
        </script>
        <!--end page-header-->
        <?php
        if ($this->session->flashdata('status') == "success") { ?>
            <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> <?= $this->session->flashdata('message');   ?>
            </div> <?php
                } elseif ($this->session->flashdata('status') == "error") {
                    ?>
            <div class="alert alert-danger fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> <?= $this->session->flashdata('message'); ?>
            </div>
        <?php } ?>