<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Home_Model');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: *");
		$this->load->library('session');
	}

	public function mapdata()
	{
		$mapdata = $this->Home_Model->get_mapdata();
		if ($mapdata != false) {
			$data = array();
			foreach ($mapdata as $row) {
				$img_src = explode(',', $row['img_src']);
				$data[] =
					[
						'id' => $row['id'],
						'latitude' => $row['current_latitude'],
						'longitude' => $row['current_longitude'],
						'featured' => 1,
						'title' => $row['product_name'],
						'location' => $row['city'],
						'city' => 1,
						'type_name' => $row['type_name'],
						'phone' => $row['mobile'],
						'category' => $row['category_name'],
						'rating' => "",
						'reviews_number' => "",
						'marker_image' => "adpostimages/" . $row['id'] . "/images/" . $img_src[0],
						'gallery' => array(
							"adpostimages/" . $row['id'] . "/images/" . $row['img_src']
						),
						'price' => $row['price'],
						'tags' => array(
							"Diesel",
							"First Owner",
							"4x4",
							"Air Conditioning"
						),
						'additional_info' => "",
						'url' => "detail.html",
						'description' => $row['description'],
						'description_list' => array(
							[
								'title' => "Engine",
								'value' => "Diesel",
							],
							[
								'title' => "Mileage",
								'value' => 14500,
							],
							[
								'title' => "Max Speed",
								'value' => "220 Mph",
							],
							[
								'title' => "marker_color",
								'value' => "Dark Brown",
							],
							[
								'title' => "Status",
								'value' => "Sale",
							],
						),
						'marker_color' => "#45ad00"
					];
			}
			echo json_encode($data);
		}
	}

	public function modal_item()
	{
		$mapdata = $this->Home_Model->get_mapdata();
		$data = array();
		foreach ($mapdata as $row) {
			$img = explode(',', $row['img_src']);
			$gallery = [];
			for ($im = 0; $im < count($img); $im++) {
				$gallery[] = "adpostimages/" . $row['id'] . "/images/" . $img[$im];
			}
			if (empty($gallery)) {
				$gallery = base_url() . "assets/img/no-image.png";
			}
			$data[] =
				[
					'id' => $row['id'],
					'latitude' => $row['current_latitude'],
					'longitude' => $row['current_longitude'],
					'featured' => 1,
					'title' => $row['product_name'],
					'location' => $row['city'],
					'city' => 1,
					'type_name' => $row['type_name'],
					'phone' => $row['mobile'],
					'category' => $row['category_name'],
					'rating' => "",
					'reviews_number' => "",
					'marker_image' => $gallery,
					'gallery' => $gallery,
					'price' => $row['price'],
					'tags' => array(
						"Diesel",
						"First Owner",
						"4x4",
						"Air Conditioning"
					),
					'additional_info' => "",
					'url' => "detail.html",
					'description' => $row['description'],
					'description_list' => array(
						[
							'title' => "Engine",
							'value' => "Diesel",
						],
						[
							'title' => "Mileage",
							'value' => 14500,
						],
						[
							'title' => "Max Speed",
							'value' => "220 Mph",
						],
						[
							'title' => "marker_color",
							'value' => "Dark Brown",
						],
						[
							'title' => "Status",
							'value' => "Sale",
						],
					),
					'marker_color' => "#45ad00"
				];
		}

		for ($i = 0; $i < count($data); $i++) {
			if ($data[$i]['id'] == $_POST['id']) {
				$currentLocation = $data[$i]; // Loaded data must be stored in the "$currentLocation" variable
			}
		}

		// End of example //////////////////////////////////////////////////////////////////////////////////////////////////////

		// Modal HTML code

		$latitude = "";
		$longitude = "";
		$address = "";
		$output = "";

		if (!empty($currentLocation['latitude'])) {
			$latitude = $currentLocation['latitude'];
		}

		if (!empty($currentLocation['longitude'])) {
			$longitude = $currentLocation['longitude'];
		}

		if (!empty($currentLocation['address'])) {
			$address = $currentLocation['address'];
		}

		$output .=

			'<div class="modal-item-detail modal-dialog" role="document" data-latitude="' . $latitude . '" data-longitude="' . $longitude . '" data-address="' . $address . '">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<div class="section-title">
						<h2>' . $currentLocation['title'] . '</h2>
						<div class="label label-default">' . $currentLocation['category'] . '</div><label>' . $currentLocation['type_name'] . '</label>';

		// Ribbon ------------------------------------------------------------------------------------------

		if (!empty($currentLocation['ribbon'])) {
			$output .=
				'<figure class="ribbon">' . $currentLocation['ribbon'] . '</figure>';
		}

		// Rating ------------------------------------------------------------------------------------------

		if (!empty($currentLocation['rating'])) {
			$output .=
				'<div class="rating-passive" data-rating="' . $currentLocation['rating'] . '">
								<span class="stars"></span>
								<span class="reviews">' . $currentLocation['reviews_number'] . '</span>
							</div>';
		}

		$output .=
			'<div class="controls-more">
							<ul>
								<li><a href="#">Add to favorites</a></li>
								<li><a href="#">Add to watchlist</a></li>
							</ul>
						</div>
						<!--end controls-more-->
					</div>
					<!--end section-title-->
				</div>
				<!--end modal-header-->
				<div class="modal-body">
					<div class="left">';

		// Gallery -----------------------------------------------------------------------------------------

		if (!empty($currentLocation['gallery'])) {
			$gallery = "";
			$noimg = base_url() . "assets/img/no-image.png";
			for ($i = 0; $i < count($currentLocation['gallery']); $i++) {
				$gallery .= '<img src="' . $currentLocation['gallery'][$i] . '" alt="" onerror="this.src="' . $noimg . '">';
			}
			$output .=
				'<div class="gallery owl-carousel" data-owl-nav="1" data-owl-dots="0">' . $gallery . '</div>
							<!--end gallery-->';
		}

		$output .=
			'<div class="map" id="map-modal"></div>
						<!--end map-->
		
						<section>
						<h3>Contact</h3>';
		// Contact -----------------------------------------------------------------------------------------

		if (!empty($currentLocation['title'])) {
			$output .=
				'<h5><i class="fa fa-user"></i>' . $currentLocation['title'] . '</h5>';
		}

		if (!empty($currentLocation['location'])) {
			$output .=
				'<a href="http://maps.google.com?q=' . $latitude . ',' . $longitude . '"><h5><i class="fa fa-map-marker"></i>' . $currentLocation['location'] . '</h5></a>';
		}

		// Phone -------------------------------------------------------------------------------------------

		if (!empty($currentLocation['phone'])) {
			$output .=
				'<h5><i class="fa fa-phone"></i>' . $currentLocation['phone'] . '</h5>';
		}

		// Email -------------------------------------------------------------------------------------------

		if (!empty($currentLocation['email'])) {
			$output .=
				'<h5><i class="fa fa-envelope"></i>' . $currentLocation['email'] . '</h5>';
		}

		$output .=
			'</section>
						<section>
							<h3>Social Share</h3>
							<div class="social-share"></div>
						</section>
					</div>
					<!--end left -->
					<div class="right">
						<section>
							<h3>Description</h3>
							<div class="read-more"><p>' . $currentLocation['description'] . '</p></div>
						</section>
						<!--end about-->';
		$output .= '<div class="right"><section style="display: flex;">
		<h3>Price : </h3><h4 style="margin-bottom: 15px;margin-top: 0px;margin-left: 5px;
		font-size: 20px;">' . $currentLocation['price'] . '</h4>
							
		</section></div>';
		// Tags ----------------------------------------------------------------------------------------------------------------

		if (!empty($currentLocation['tags'])) {
			$tags = "";
			for ($i = 0; $i < count($currentLocation['tags']); $i++) {
				$tags .= '<li>' . $currentLocation['tags'][$i] . '</li>';
			}
			$output .=
				'<section class="d-none">
									<h3>Features</h3>
									<ul class="tags">' .  $tags . '</ul>
							</section>
							<!--end tags-->';
		}

		// Today Menu --------------------------------------------------------------------------------------

		if (!empty($currentLocation['today_menu'])) {
			$output .=
				'<section>
								<h3>Today menu</h3>';
			for ($i = 0; $i < count($currentLocation['today_menu']); $i++) {
				$output .=
					'<ul class="list-unstyled list-descriptive icon">
										<li>
											<i class="fa fa-cutlery"></i>
											<div class="description">
												<strong>' . $currentLocation['today_menu'][$i]['meal_type'] . '</strong>
												<p>' . $currentLocation['today_menu'][$i]['meal'] . '</p>
											</div>
										</li>
									</ul>
									<!--end list-descriptive-->';
			}
			$output .=
				'</section>
							<!--end today-menu-->';
		}

		// Schedule ----------------------------------------------------------------------------------------

		if (!empty($currentLocation['schedule'])) {
			$output .=
				'<section>
								<h3>Schedule</h3>';
			for ($i = 0; $i < count($currentLocation['schedule']); $i++) {
				$output .=
					'<ul class="list-unstyled list-schedule">
										<li>
											<div class="left">
												<strong class="promoted">' . $currentLocation['schedule'][$i]['date'] . '</strong>
												<figure>' . $currentLocation['schedule'][$i]['time'] . '</figure>
											</div>
											<div class="right">
												<strong>' . $currentLocation['schedule'][$i]['location_title'] . '</strong>
												<figure>' . $currentLocation['schedule'][$i]['location_address'] . '</figure>
											</div>
										</li>
									</ul>
									<!--end list-schedule-->';
			}
			$output .=
				'</section>
							<!--end schedule-->';
		}

		// Video -------------------------------------------------------------------------------------------

		if (!empty($currentLocation['video'])) {
			$output .=
				'<section>
								<h3>Video presentation</h3>
								<div class="video">' . $currentLocation['video'] . '</div>
							</section>
							<!--end video-->';
		}

		// Description list --------------------------------------------------------------------------------

		if (!empty($currentLocation['description_list'])) {
			$output .=
				'<section class="d-none">
								<h3>Listing Details</h3>';
			for ($i = 0; $i < count($currentLocation['description_list']); $i++) {
				$output .=
					'<dl>
											<dt>' . $currentLocation['description_list'][$i]['title'] . '</dt>
											<dd>' . $currentLocation['description_list'][$i]['value'] . '</dd>
										</dl>
										<!--end property-details-->';
			}
			$output .=
				'</section>
							<!--end description-list-->';
		}

		// Reviews -----------------------------------------------------------------------------------------

		if (!empty($currentLocation['reviews'])) {
			$output .=
				'<section>
								<h3>Latest reviews</h3>';
			for ($i = 0; $i < 2; $i++) {
				$output .=
					'<div class="review">
										<div class="image">
											<div class="bg-transfer" style="background-image: url(' . $currentLocation['reviews'][$i]['author_image'] . ')"></div>
										</div>
										<div class="description">
											<figure>
												<div class="rating-passive" data-rating="' . $currentLocation['reviews'][$i]['rating'] . '">
													<span class="stars"></span>
												</div>
												<span class="date">' . $currentLocation['reviews'][$i]['date'] . '</span>
											</figure>
											<p>' . $currentLocation['reviews'][$i]['review_text'] . '</p>
										</div>
									</div>
									<!--end review-->';
			}
			$output .=
				'</section>
							<!--end reviews-->';
		}
		$output .=
			'</div>
					<!--end right-->
				</div>
				<!--end modal-body-->
			</div>
			<!--end modal-content-->
		</div>
		<!--end modal-dialog-->
		';

		echo $output;
	}

	public function index()
	{
		$data['categories'] = $this->Home_Model->getCategories();
		$data['recentpost'] = $this->Home_Model->getrecentpost();
		$data['randomposts'] = $this->Home_Model->getrandompost();
		$this->load->view('index', $data);
	}

	public function login()
	{
		$uname = $this->input->post('uname');
		$pwd = $this->input->post('pwd');
		$callback = $this->input->post('callback');

		$chk_login = $this->Home_Model->check_login($uname, $pwd);

		if ($chk_login != false) {
			$this->session->set_userdata($chk_login);

			$this->session->set_userdata(array('user_logged_in' => 1));
			// echo $this->session->userdata('user_logged_in');
		} else {
			$this->session->set_flashdata('status', 'error');
			$this->session->set_flashdata('message', 'Username / Password Mismatch');
		}
		$logged_in = $this->session->userdata('user_logged_in');
		if(!empty($callback)){
			redirect($callback, 'refresh');
		}else{
			redirect('home', 'refresh');
		}
		
	}

	public function logout()
	{
		$callback = $_GET['callbackid'];
		$this->session->unset_userdata('user_logged_in');
		if(!empty($callback)){
			$callback_url = base64_decode($callback);
			redirect($callback_url, 'refresh');
		}else{
			redirect('home', 'refresh');
		}
	}

	public function post_ads()
	{
		$this->load->view('post_ads');
	}

	public function check_posts()
	{

		$category = $this->input->post('category');
		$keyword = $this->input->post('keyword');
		$keyword_id = $this->input->post('keyword_id');
		$keyword_categoryid = $this->input->post('keyword_categoryid');
		$location = $this->input->post('location');
		$manual_latitude = $this->input->post('manual_latitude');
		$manual_longitude = $this->input->post('manual_longitude');
		$type = $this->input->post('type');
		$check = $this->Home_Model->check_posts($category, $keyword, $type, $location);
		// echo $this->db->last_query();

		if ($check == true) {
			echo json_encode(array('status' => 'success'));
		} else {
			echo json_encode(array('status' => 'failed'));
		}
	}

	public function register()
	{

		$uname = $this->input->post('email');
		$pwd = $this->input->post('pwd');
		$pwd_repeat = $this->input->post('pwd_repeat');
		$check_user_record = $this->Home_Model->check_user_data($uname);

		if ($check_user_record == false) {
			$data = array(
				"username" => $uname,
				"password" => $pwd_repeat,
				"level" => 2,
				"status" => 1
			);


			$insert_data = $this->Home_Model->insert_userdata($data);
			$this->session->set_flashdata('status', 'success');
			$this->session->set_flashdata('message', 'Successfully Registered!');

			redirect('home', 'refresh');
		} else {
			$this->session->set_flashdata('status', 'error');
			$this->session->set_flashdata('message', 'Registeration Failed!');

			redirect('home', 'refresh');
		}
	}

	public function myads()
	{
		$user_id = $this->session->userdata('id');
		$data['myads'] = $this->Home_Model->get_myads($user_id);
		$this->load->view('myads', $data);
	}

	public function remove_myad()
	{
		$id = $this->input->post('refid');
		$userid = $this->input->post('userid');
		$setactive = $this->input->post('setactive');
		$resp = $this->Home_Model->remove_myad($id, $userid, $setactive);
		if ($resp == true) {
			echo json_encode(array('status' => 'success'));
		} else {
			echo json_encode(array('status' => 'Failed'));
		}
	}

	public function search_keyword()
	{

		$response = array();

		$term = $this->input->get('term');

		$result = $this->Home_Model->get_keywords($term);

		if ($result != false) {
			foreach ($result as $row) {
				$id = $row->id;
				$keyword = $row->keyword;
				$category_id = $row->category_id;
				$response[] = array(
					"value" => $keyword,
					"label" => $keyword,
					"id" => $id,
					"categoryId" => $category_id
				);
			}
		} else {
			$response[] = array(
				"label" => "No Result Found",
				"value" => "",
				"id" => "",
				"categoryId" => ""
			);
		}

		echo json_encode($response);
	}

	public function forgot_password()
	{
		// echo '<pre/>';print_r($_POST);exit;
		$loginEmailId = $this->input->post('email_id');
		$status = '';
		if (is_numeric($loginEmailId)) {
			$this->form_validation->set_rules('email_id', 'Mobile No', 'trim|required|integer|max_length[10]|min_length[10]');
		} else {
			$this->form_validation->set_rules('email_id', 'Email', 'trim|required|valid_email');
		}

		if ($this->form_validation->run() !== FALSE) {
			// $data = base64_encode($loginEmailId);
			$getpassword = $this->Home_Model->get_forgot_password($loginEmailId);
			// echo $this->db->last_query();
			// echo '<pre/>';print_r($getpassword);exit;
			if ($getpassword == '') {
				$status = 'fail';
				$message = "Sorry !!! " . $loginEmailId . " is not registered";
			} else {
				$user_no = $getpassword->username;
				// $activation_key = sha1($loginEmailId . 'Mytrippatner');
				if (is_numeric($loginEmailId)) {
					// echo 1;exit;
					$this->load->model('home/smsgateway');
					$otp = $this->smsgateway->sendOtp($loginEmailId);
					$type = 'mobile';
				} else {
					// echo 2;exit;
					$otp = $this->getOtp(6);
					$type = 'email';
				}
				// $this->B2c_Model->update_user_activation_key($otp, $user_no);
				// echo $this->db->last_query();exit;

				if ($this->Home_Model->update_user_activation_key($otp, $user_no)) {
					// echo $this->db->last_query();exit;
					$message = "An OTP has been sent to your email address to reset the password.";
					$status = 'success';
					if ($type = 'email') {
						$data_email = array(
							'email' => $loginEmailId,
							// 'user_no' => $user_no,
							'otp' => $otp,
						);
						$this->load->model('home/sendemail');
						$this->sendemail->forgot_password($data_email);
					}
				} else {
					$message = "Something went wrong please try again!";
					$status = 'fail';
				}
			}
		} else {
			$message = validation_errors();
			$status = 'fail';
		}
		// echo json_encode(array(
		//     'status' => $status,
		//     'message' => $message,
		//     'otp_user' => $loginEmailId,
		// ));

		if ($status == 'fail') {
			// redirect('home/error_page/' . base64_encode($message));
			echo json_encode(array("status" => "failed"));
		} else {
			// redirect('home/success_page/' . base64_encode($message));
			echo json_encode(array("status" => "success"));
		}
	}

	function getOtp($n)
	{
		$generator = "1357902468";
		$result = "";
		for ($i = 1; $i <= $n; $i++) {
			$result .= substr($generator, (rand() % (strlen($generator))), 1);
		}
		// Return result 
		return $result;
	}

	function success_page($success)
	{
		$data['success'] = base64_decode($success);
		$this->load->view('success', $data);
	}

	function error_page($error)
	{
		$data['error'] = base64_decode($error);
		$this->load->view('error', $data);
	}

	function warning_page($error)
	{
		$data['error'] = base64_decode($error);
		$this->load->view('error', $data);
	}

	function verify_otp()
	{
		$otp = $this->input->post('otp');
		$email = $this->input->post('email');
		$this->db->where(array('username' => $email, 'otp' => $otp));
		$q = $this->db->get('users');
		if ($q->num_rows() > 0) {
			echo json_encode(array("status" => "success"));
		} else {
			echo json_encode(array("status" => "failed"));
		}
	}

	function setpassword()
	{
		$confirmpassword = $this->input->post('confirmpassword');
		$email = $this->input->post('email');
		$status = $this->Home_Model->set_newpassword($confirmpassword, $email);
		if ($status == 'fail') {
			$message = "Something went wrong please try again!";
			// redirect('home/error_page/' . base64_encode($message));
			echo json_encode(array("status" => "failed", "msg" => $message));
		} else {
			$message = "You have successfully changed your account password. you can now login to start using the application!";
			// redirect('home/success_page/' . base64_encode($message));
			echo json_encode(array("status" => "success", "msg" => $message));
		}
	}

	public function close()
	{
		$this->session->unset_userdata('status');
		$this->session->unset_userdata('message');
	}
}
