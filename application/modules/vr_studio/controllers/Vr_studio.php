<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vr_studio extends MY_Controller{

    //keep controller name same as filename
    public function index()
    {
        $this->load->view('index');        
    }
    public function work_adjusted()
    {
        $this->load->view('work_adjusted');        
    }
    public function works_slider_parallax()
    {
        $this->load->view('works_slider_parallax');        
    }
    public function gallery_bricks()
    {
        $this->load->view('gallery_bricks');        
    }
    public function contacts()
    {
        $this->load->view('contacts');        
    }
}