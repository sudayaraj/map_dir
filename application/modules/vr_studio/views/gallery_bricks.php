<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ashade | New Family</title>

    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat%3A500%2C700%7CRoboto+Condensed:700%7CRoboto%3A700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url() ?>vl_assets/css/line-awesome.css">
	<link rel="stylesheet" href="<?= base_url() ?>vl_assets/css/photoswipe.css">
	<link rel="stylesheet" href="<?= base_url() ?>vl_assets/css/default-skin/default-skin.css">
	<link rel="stylesheet" href="<?= base_url() ?>vl_assets/css/style.css">
	<link rel="stylesheet" href="<?= base_url() ?>vl_assets/css/responsive.css">

	<!-- Icon -->
	<link rel="icon" href="img/favicon.jpg" sizes="32x32" />
    <style>
      
    .navbar {
        display: flex;
        align-items: baseline;
        padding: 7px 0px 0px;
        justify-content: space-between;
    }

    .logo {
        width: 90px;
        height: 40px;
        object-fit: cover;
    }

    .nav1 {
        position: absolute;
        /* z-index: 200; */
        z-index: 10000;
        background-color: #222;
        width: 100%;
    }

    .navul1 {
        display: flex;
        align-items: center;
        gap: 1rem;
        list-style: none;
        padding: 4px;
        justify-content: center;
    }

    .navli1 {
        /* border-right: 1px solid grey; */
        /* padding: 0 8px 0 0; */
        /* font-size: 13px; */
        border-right: 1px solid grey;
        padding-right: 9px;
        padding-left: 0px;
        font-size: 13px;
    }

    .navli1 a {
        color: grey;
    }

    .navli1 a:hover {
        color: #fff;
    }

    .navli1:last-child {
        border-right: unset;
    }

    .res_toggle {
        display: none;
        cursor: pointer;
    }

    .toggleshow {
        display: flex !important;
    }

    .nav_active {
        font-weight: 900;
        border-bottom: 3px solid;
        color: #fff !important;
    }

    @media only screen and (max-width:1310px) {
        .res_toggle {
            display: block;
        }

        .navbar {
            padding: 7px 10px;
        }

        .navul1 {

            flex-direction: column;
            position: absolute;
            z-index: 1000;
            background: #000000f2;
            width: 100%;
            top: 46px;
            left: 0;
            display: none;
        }

        .navli1 {
            border-right: unset;

        }

        header#ashade-header {
            position: fixed;
            left: 0;
            top: 45px;
            width: 100%;
            z-index: 181;
            transition: z-index 0.5s, transform 0.5s;
        }


    }
</style>
</head>
<body class="has-spotlight ashade-smooth-scroll">
        <!-- nav1 -->
        <div class="nav1">
        <div class="container-fluid text-center">
            <div class="navbar">
                <img src="" alt="Logo" class="logo">
                <!-- toggle img start -->
                <img src="<?= base_url() ?>assets/icons/toggle.jpg" style="width:34px;" alt="toggle" class="res_toggle" id="res_toggle">
                <!-- toggle img end -->
                <ul class="navul1 " id="navul1">
                    <li class="navli1"><a href="<?= base_url() ?>">Virtual Link</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_mall">Virtual Mall</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_property">Virtual Property</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Virtual Education</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_studio" class="nav_active">VR Studio</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_tour">VR Tour</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_dharisnam">VR Dharisanam</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Buy Sharing</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">CP Hub</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Live Stream</a></li>
                    <li class="navli1 navrmborder"><a href="#">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Header -->
        <!-- Header -->
        <header id="ashade-header">


            <div class="ashade-header-inner">
                <div class="ashade-logo-block">
                    <a href="<?= base_url() ?>/vr_studio" class="ashade-logo is-retina">
                        <img src="https://demo.shadow-themes.com/html/ashade/img/logo.jpg" alt="Ashade Logo" width="128" height="110">
                    </a>
                </div>
                <div class="ashade-nav-block">
                    <nav class="ashade-nav">
                        <ul class="main-menu">
                            <li class="menu-item-has-children">
                                <a href="#">Home</a>
                             
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Works</a>
                                <ul class="sub-menu">
                                
                                    <li class="menu-item-has-children">
                                        <a href="#">Slider</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?= base_url() ?>vr_studio/works_slider_parallax">Parallax Slider</a></li>
                                           
                                        </ul>
                                    </li>
                                   
                                    <li class="menu-item-has-children">
                                        <a href="#">Adjusted</a>
                                        <ul class="sub-menu">
                                           
                                            <li><a href="<?= base_url() ?>vr_studio/work_adjusted">4 Columns</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Showcase</a>
                             
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Rental</a>
                                <ul class="sub-menu">
                                    <li><a href="about.html">About Me</a></li>
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="testimonials.html">Testimonials</a></li>
                                    <li><a href="<?= base_url() ?>vr_studio/gallery_bricks">For rental</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">VR-live</a>
                                <ul class="sub-menu">
                                    <li><a href="about.html">About Me</a></li>
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="testimonials.html">Testimonials</a></li>
                                    <li><a href="<?= base_url() ?>vr_studio/gallery_bricks">For rental</a></li>
                                </ul>
                            </li>
                            <li><a href="<?= base_url() ?>vr_studio/contacts">Contacts</a></li>
                            <li>
                                <a href="#" class="ashade-aside-toggler">
                                    <span class="ashade-aside-toggler__icon01"></span>
                                    <span class="ashade-aside-toggler__icon02"></span>
                                    <span class="ashade-aside-toggler__icon03"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>

    <!-- Content -->
    <div class="ashade-page-title-wrap">
        <h1 class="ashade-page-title">
            <span>Wedding Photos</span>
            New Family
        </h1>
    </div>

    <main class="ashade-content-wrap">
		<div class="ashade-content-scroll">
			<div class="ashade-content">
				<section class="ashade-section">
					<div class="ashade-row">
						<div class="ashade-col col-12">
							<p class="ashade-intro">The birth of a new family is truly an exciting and touching moment. It was a great pleasure for me to help these beautiful people capture these wonderful moments of their lives.</p>
						</div>
					</div>
				</section>
				<section class="ashade-section">
					<div class="ashade-gallery-bricks is-2x3">
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/01.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/01.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/02.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/02.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/03.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/03.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/04.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/04.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/05.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/05.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/06.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/06.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/07.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/07.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/08.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/08.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/09.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/09.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
						<div class="ashade-gallery-item">
							<a href="img/gallery/bricks2x3/10.jpg" class="ashade-lightbox-link" data-size="1800x1200">
								<img src="https://demo.shadow-themes.com/html/ashade/img/null.jpg" data-src="https://demo.shadow-themes.com/html/ashade/img/gallery/bricks2x3/10.jpg" alt="New Family" class="lazy" width="1800" height="1200">
							</a>
						</div><!-- .ashade-gallery-item -->
					</div>
				</section>
			</div><!-- .ashade-content -->
			
			<!-- Footer -->
			<footer id="ashade-footer">
				<div class="ashade-footer-inner">
					<div class="ashade-footer__socials">
						<ul class="ashade-socials">
							<li><a href="#">Fb</a></li>
							<li><a href="#">Tw</a></li>
							<li><a href="#">In</a></li>
							<li><a href="#">Yt</a></li>
						</ul>
					</div>
					<div class="ashade-footer__copyright">
						Copyright &copy; 2023. All Rights Reserved.
					</div>
				</div>
			</footer>
		</div><!-- .ashade-content-scroll -->
	</main>
    
    <div class="ashade-to-top-wrap ashade-back-wrap">
        <div class="ashade-back is-to-top">
            <span>Back to</span>
            <span>Top</span>
        </div>
    </div>

    <!-- Aside Bar -->
    <aside id="ashade-aside">
       	<a href="#" class="ashade-aside-close">Close Sidebar</a>
        <div class="ashade-aside-inner">
        	<div class="ashade-aside-content">
				<div class="ashade-widget ashade-widget--about">
					<div class="ashade-widget--about__head">
						<img src="https://demo.shadow-themes.com/html/ashade/img/general/owner-avatar.jpg" alt="Andrew Shade">
						<h5>
							<span>Photographer</span>
							Andrew Shade
						</h5>
					</div>
					<p>Nice to meet you, friend! My name is Andrew Shade. I am from Denver. Photography is my passion. Through the lens the world looks different and I would like to show you this difference.</p>
					<p class="align-right">
						<a href="about.html" class="ashade-learn-more">Learn More</a>
					</p>
				</div><!-- .ashade-widget -->
       			
				<div class="ashade-widget ashade-widget--contacts">
					<h5 class="ashade-widget-title">
						<span>My contacts and socials</span>
						How to find me
					</h5>
					<ul class="ashade-contact-details__list">
						<li>
							<i class="ashade-contact-icon la la-map-marker"></i>
							1250 Welton St, Denver, CO 80204
						</li>
						<li>
							<i class="ashade-contact-icon la la-phone"></i>
							<a href="tel:+11234567890">+1 (123) 456 - 78 - 90</a>
						</li>
						<li>
							<i class="ashade-contact-icon la la-envelope"></i>
							<a href="mailto:a.shade@example.com">a.shade@example.com</a>
						</li>
						<li class="ashade-contact-socials">
							<i class="ashade-contact-icon la la-share-alt"></i>
							<a href="facebook.com" target="_blank">Fb</a>
							<a href="twitter.com" target="_blank">Tw</a>
							<a href="instagram.com" target="_blank">In</a>
							<a href="500px.com" target="_blank">Px</a>
						</li>
					</ul>
					<p class="align-right">
						<a href="contacts.html" class="ashade-learn-more">Get in touch</a>
					</p>
				</div><!-- .ashade-widget -->
       			
        	</div><!-- .ashade-aside-content -->
        </div><!-- .ashade-aside-inner -->
    </aside>

    <!-- UI Elements -->
    <div class="ashade-menu-overlay"></div>
    <div class="ashade-aside-overlay"></div>
    <div class="ashade-cursor is-inactive">
    	<span class="ashade-cursor-circle"></span>
    	<span class="ashade-cursor-slider"></span>
    	<span class="ashade-cursor-close ashade-cursor-label">Close</span>
    	<span class="ashade-cursor-zoom ashade-cursor-label">Zoom</span>
    </div>

    <script>
        document.getElementById('res_toggle').addEventListener('click', () => {
            document.getElementById('navul1').classList.toggle('toggleshow')
        })
    </script>
    <!-- SCRIPTS -->
    <script src="<?= base_url() ?>vl_assets/js/jquery.min.js"></script>
	<script src="<?= base_url() ?>vl_assets/js/masonry.min.js"></script>
    <script src="<?= base_url() ?>vl_assets/js/gsap.min.js"></script>
    <script src="<?= base_url() ?>vl_assets/js/jquery.lazy.min.js"></script>
	<script src="<?= base_url() ?>vl_assets/js/photoswipe.min.js"></script>
	<script src="<?= base_url() ?>vl_assets/js/photoswipe-ui-default.min.js"></script>
    <script src="<?= base_url() ?>vl_assets/js/core.js"></script>
</body>
</html>