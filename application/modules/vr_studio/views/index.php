<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ashade | Home with Video</title>

    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat%3A500%2C700%7CRoboto+Condensed:700%7CRoboto%3A700&display=swap" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="<?= base_url() ?>vl_assets/css/line-awesome.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url() ?>vl_assets/css/style.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url() ?>vl_assets/css/responsive.css">

    <!-- Icon -->
    <link rel="icon" href="<?= base_url() ?>vl_assets/img/favicon.png" sizes="32x32" />
    <!-- <link rel="stylesheet" href="<?= base_url() ?>vrmall_assets/css/vr_mall.css"> -->
</head>
<style>
    .navbar {
        display: flex;
        align-items: baseline;
        padding: 7px 0px 0px;
        justify-content: space-between;
    }

    .logo {
        width: 90px;
        height: 40px;
        object-fit: cover;
    }

    .nav1 {
        position: absolute;
        /* z-index: 200; */
        z-index: 10000;
        background-color: #222;
        width: 100%;
    }

    .navul1 {
        display: flex;
        align-items: center;
        gap: 1rem;
        list-style: none;
        padding: 4px;
        justify-content: center;
    }

    .navli1 {
        /* border-right: 1px solid grey; */
        /* padding: 0 8px 0 0; */
        /* font-size: 13px; */
        border-right: 1px solid grey;
        padding-right: 9px;
        padding-left: 0px;
        font-size: 13px;
    }

    .navli1 a {
        color: grey;
    }

    .navli1 a:hover {
        color: #fff;
    }

    .navli1:last-child {
        border-right: unset;
    }

    .res_toggle {
        display: none;
        cursor: pointer;
    }

    .toggleshow {
        display: flex !important;
    }

    .nav_active {
        font-weight: 900;
        border-bottom: 3px solid;
        color: #fff !important;
    }

    @media only screen and (max-width:1310px) {
        .res_toggle {
            display: block;
        }

        .navbar {
            padding: 7px 10px;
        }

        .navul1 {

            flex-direction: column;
            position: absolute;
            z-index: 1000;
            background: #000000f2;
            width: 100%;
            top: 46px;
            left: 0;
            display: none;
        }

        .navli1 {
            border-right: unset;

        }

        header#ashade-header {
            position: fixed;
            left: 0;
            top: 45px;
            width: 100%;
            z-index: 181;
            transition: z-index 0.5s, transform 0.5s;
        }


    }
</style>

<body class="ashade-home-template has-spotlight ashade-smooth-scroll is-init is-loaded">


    <!-- nav1 -->
    <!-- nav1 -->
    <div class="nav1">
        <div class="container-fluid text-center">
            <div class="navbar">
                <img src="" alt="Logo" class="logo">
                <!-- toggle img start -->
                <img src="<?= base_url() ?>assets/icons/toggle.jpg" style="width:34px;" alt="toggle" class="res_toggle" id="res_toggle">
                <!-- toggle img end -->
                <ul class="navul1 " id="navul1">
                    <li class="navli1"><a href="<?= base_url() ?>">Virtual Link</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_mall">Virtual Mall</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_property">Virtual Property</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Virtual Education</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_studio" class="nav_active">VR Studio</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_tour">VR Tour</a></li>
                    <li class="navli1"><a href="<?= base_url() ?>vr_dharisnam">VR Dharisanam</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Buy Sharing</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">CP Hub</a></li>
                    <li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Live Stream</a></li>
                    <li class="navli1 navrmborder"><a href="#">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div> 
    <div class="ashade-home-template has-spotlight ashade-smooth-scroll">
        <!-- Header -->
        <header id="ashade-header">


            <div class="ashade-header-inner">
                <div class="ashade-logo-block">
                    <a href="<?= base_url() ?>/vr_studio" class="ashade-logo is-retina">
                        <img src="<?= base_url() ?>vl_assets/img/logo.png" alt="Ashade Logo" width="128" height="110">
                    </a>
                </div>
                <div class="ashade-nav-block">
                    <nav class="ashade-nav">
                        <ul class="main-menu">
                            <li class="menu-item-has-children">
                                <a href="#">Home</a>
                             
                            </li>
                            <li class="menu-item-has-children">
                                <a href="<?= base_url() ?>vr_studio/work_adjusted"">Works</a>
                                
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Showcase</a>
                             
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Pages</a>
                                <ul class="sub-menu">
                                    <li><a href="about.html">About Me</a></li>
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="testimonials.html">Testimonials</a></li>
                                    <li><a href="<?= base_url() ?>vr_studio/gallery_bricks">For rental</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">VR-live</a>
                                
                            </li>
                            <li><a href="<?= base_url() ?>vr_studio/contacts">Contacts</a></li>
                            <li>
                                <a href="#" class="ashade-aside-toggler">
                                    <span class="ashade-aside-toggler__icon01"></span>
                                    <span class="ashade-aside-toggler__icon02"></span>
                                    <span class="ashade-aside-toggler__icon03"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>

        <!-- Home Background -->
        <div class="ashade-home-background ashade-page-background is-video">
            <video src="<?= base_url() ?>vrmall_assets/video/home-bg.mp4" poster="<?= base_url() ?>vl_assets/img/bgs/bg-home01.png" muted autoplay loop></video>
        </div>

        <!-- Home Links and Titles -->
        <div class="ashade-home-link--works ashade-home-link-wrap">
            <div class="ashade-home-link is-link">
                <span>My Photo Portfolio</span>
                <span>Explore Works</span>
            </div>
        </div>
        <!-- .ashade-home-link-wrap -->
        <div class="ashade-home-link--contacts ashade-home-link-wrap">
            <div class="ashade-home-link is-link">
                <span>How to find me</span>
                <span>Contact Me</span>
            </div>
        </div>
        <!-- .ashade-home-link-wrap -->

        <!-- Home Title and Back Button -->
        <div class="ashade-page-title-wrap is-inactive ">
            <h1 class="ashade-page-title">&nbsp;</h1>
        </div>
        <!-- .ashade-page-title-wrap -->

        <div class="ashade-home-return ashade-back-wrap is-inactive">
            <div class="ashade-back is-home-return">
                <span>Return</span>
                <span>Back</span>
            </div>
        </div>
        <!-- .ashade-to-top-wrap -->

        <!-- Home Contacts Block -->
        <div id="ashade-home-contacts">
            <div class="ashade-row">
                <div class="ashade-col col-12">
                    <p class="ashade-intro">Nice to meet you, friend! My name is Adrew Shade. I’m a professional photographer from Denver, Colorado. If you have any questions, suggestions or you just want to book a photo session feel free to use the contact form below. Lets make
                        something great together!</p>
                </div>
            </div>
            <!-- .ashade-row -->
            <div class="ashade-row">
                <div class="ashade-col col-4">
                    <div class="ashade-contact-details">
                        <h4 class="ashade-contact-details__title">
                            <span>My Contacts and Socials</span> How to Find Me
                        </h4>
                        <ul class="ashade-contact-details__list">
                            <li>
                                <i class="ashade-contact-icon la la-map-marker"></i> 1250 Welton St, Denver, CO 80204
                            </li>
                            <li>
                                <i class="ashade-contact-icon la la-phone"></i>
                                <a href="tel:+11234567890">+1 (123) 456 - 78 - 90</a>
                            </li>
                            <li>
                                <i class="ashade-contact-icon la la-envelope"></i>
                                <a href="mailto:a.shade@example.com">a.shade@example.com</a>
                            </li>
                            <li class="ashade-contact-socials">
                                <i class="ashade-contact-icon la la-share-alt"></i>
                                <a href="facebook.com" target="_blank">Fb</a>
                                <a href="twitter.com" target="_blank">Tw</a>
                                <a href="instagram.com" target="_blank">In</a>
                                <a href="500px.com" target="_blank">Px</a>
                            </li>
                        </ul>
                    </div>
                    <!-- .ashade-contact-details -->
                </div>
                <div class="ashade-col col-8">
                    <form action="mail.php" method="post" class="ashade-contact-form">
                        <div class="ashade-row ashade-small-gap">
                            <div class="ashade-col col-4">
                                <input type="text" id="name" name="name" placeholder="Your Name" required>
                            </div>
                            <div class="ashade-col col-4">
                                <input type="email" id="email" name="email" placeholder="Your Email" required>
                            </div>
                            <div class="ashade-col col-4">
                                <input type="tel" id="phone" name="phone" placeholder="Your Phone" required>
                            </div>
                        </div>
                        <textarea name="message" id="message" placeholder="Your Message" required></textarea>
                        <div class="ashade-contact-form__footer">
                            <div class="ashade-contact-form__response"></div>
                            <div class="ashade-contact-form__submit">
                                <input type="submit" value="Send Message">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- #ashade-home-contacts -->

        <!-- Home Works Block -->
        <div id="ashade-home-works">
            <div class="ashade-row">
                <div class="ashade-col col-12">
                    <p class="ashade-intro">Photography is my passion. Through the lens the world looks different and i would like to show you this difference. You can see it in my albums that are presented here.</p>
                    <div class="ashade-albums-grid ashade-grid ashade-grid-3cols is-masonry">
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album01.png" alt="My Special Day">
                            </div>
                            <h5>
                                <span>Wedding Photos</span> My Special Day
                            </h5>
                            <a href="gallery-slider-parallax.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album02.png" alt="Harley Davidson">
                            </div>
                            <h5>
                                <span>Commercial Photos</span> Harley Davidson
                            </h5>
                            <a href="gallery-bricks-1x2.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album03.png" alt="Together Forever">
                            </div>
                            <h5>
                                <span>Love Story</span> Together Forever
                            </h5>
                            <a href="gallery-grid-3columns.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album05.png" alt="Sunset Nature">
                            </div>
                            <h5>
                                <span>Nature Photos</span> Sunset Nature
                            </h5>
                            <a href="gallery-masonry-4columns.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album04.png" alt="Girl on Farm">
                            </div>
                            <h5>
                                <span>Personal Session</span> Girl on Farm
                            </h5>
                            <a href="gallery-ribbon-large.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album06.png" alt="City Tour">
                            </div>
                            <h5>
                                <span>Travel Photos</span> City Tour
                            </h5>
                            <a href="gallery-masonry-3columns.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album08.png" alt="Colors of Nature">
                            </div>
                            <h5>
                                <span>Nature Photos</span> Colors of Nature
                            </h5>
                            <a href="gallery-justified.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album07.png" alt="Auto Showroom">
                            </div>
                            <h5>
                                <span>Commercial Photo</span> Auto Showroom
                            </h5>
                            <a href="gallery-adjusted-3columns.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album09.png" alt="Abandoned">
                            </div>
                            <h5>
                                <span>Travel Photos</span> Abandoned
                            </h5>
                            <a href="gallery-ribbon-medium.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album11.png" alt="Let's Play">
                            </div>
                            <h5>
                                <span>Personal Session</span> Let's Play
                            </h5>
                            <a href="gallery-ribbon-vertical.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album10.png" alt="True Love">
                            </div>
                            <h5>
                                <span>Love Story</span> True Love
                            </h5>
                            <a href="gallery-slider-fade.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                        <div class="ashade-album-item ashade-grid-item">
                            <div class="ashade-album-item__image">
                                <img src="<?= base_url() ?>vl_assets/img/albums/masonry/album12.png" alt="New Family">
                            </div>
                            <h5>
                                <span>Wedding Photos</span> New Family
                            </h5>
                            <a href="gallery-bricks-2x3.html" class="ashade-album-item__link"></a>
                        </div>
                        <!-- .ashade-album-item -->
                    </div>
                    <!-- .ashade-albums-grid -->
                </div>
                <!-- .ashade-col -->
            </div>
            <!-- .ashade-row -->
        </div>
        <!-- #ashade-home-works -->

        <!-- Footer -->
        <footer id="ashade-footer">
            <div class="ashade-footer-inner">
                <div class="ashade-footer__socials">
                    <ul class="ashade-socials">
                        <li><a href="#">Fb</a></li>
                        <li><a href="#">Tw</a></li>
                        <li><a href="#">In</a></li>
                        <li><a href="#">Yt</a></li>
                    </ul>
                </div>
                <div class="ashade-footer__copyright">
                    Copyright &copy; 2023. All Rights Reserved.
                </div>
            </div>
        </footer>

        <!-- Aside Bar -->
        <aside id="ashade-aside">
            <a href="#" class="ashade-aside-close">Close Sidebar</a>
            <div class="ashade-aside-inner">
                <div class="ashade-aside-content">
                    <div class="ashade-widget ashade-widget--about">
                        <div class="ashade-widget--about__head">
                            <img src="<?= base_url() ?>vl_assets/img/general/owner-avatar.png" alt="Andrew Shade">
                            <h5>
                                <span>Photographer</span> Andrew Shade
                            </h5>
                        </div>
                        <p>Nice to meet you, friend! My name is Andrew Shade. I am from Denver. Photography is my passion. Through the lens the world looks different and I would like to show you this difference.</p>
                        <p class="align-right">
                            <a href="about.html" class="ashade-learn-more">Learn More</a>
                        </p>
                    </div>
                    <!-- .ashade-widget -->

                    <div class="ashade-widget ashade-widget--contacts">
                        <h5 class="ashade-widget-title">
                            <span>My contacts and socials</span> How to find me
                        </h5>
                        <ul class="ashade-contact-details__list">
                            <li>
                                <i class="ashade-contact-icon la la-map-marker"></i> 1250 Welton St, Denver, CO 80204
                            </li>
                            <li>
                                <i class="ashade-contact-icon la la-phone"></i>
                                <a href="tel:+11234567890">+1 (123) 456 - 78 - 90</a>
                            </li>
                            <li>
                                <i class="ashade-contact-icon la la-envelope"></i>
                                <a href="mailto:a.shade@example.com">a.shade@example.com</a>
                            </li>
                            <li class="ashade-contact-socials">
                                <i class="ashade-contact-icon la la-share-alt"></i>
                                <a href="facebook.com" target="_blank">Fb</a>
                                <a href="twitter.com" target="_blank">Tw</a>
                                <a href="instagram.com" target="_blank">In</a>
                                <a href="500px.com" target="_blank">Px</a>
                            </li>
                        </ul>
                        <p class="align-right">
                            <a href="contacts.html" class="ashade-learn-more">Get in touch</a>
                        </p>
                    </div>
                    <!-- .ashade-widget -->

                </div>
                <!-- .ashade-aside-content -->
            </div>
            <!-- .ashade-aside-inner -->
        </aside>

        <!-- UI Elements -->
        <div class="ashade-home-block-overlay"></div>
        <div class="ashade-menu-overlay"></div>
        <div class="ashade-aside-overlay"></div>
        <div class="ashade-cursor is-inactive">
            <span class="ashade-cursor-circle"></span>
            <span class="ashade-cursor-slider"></span>
            <span class="ashade-cursor-close ashade-cursor-label">Close</span>
            <span class="ashade-cursor-zoom ashade-cursor-label">Zoom</span>
        </div>

        <!-- end div -->
    </div>

    <script>
        document.getElementById('res_toggle').addEventListener('click', () => {
            document.getElementById('navul1').classList.toggle('toggleshow')
        })
    </script>
    <!-- SCRIPTS -->
    <script src="<?= base_url() ?>vl_assets/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>vl_assets/js/gsap.min.js"></script>
    <script src="<?= base_url() ?>vl_assets/js/masonry.min.js"></script>
    <script src="<?= base_url() ?>vl_assets/js/core.js"></script>
</body>

</html>