<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vr_tour extends MY_Controller{

    //keep controller name same as filename
    public function index()
    {
        $this->load->view('index');        
    }

    public function error_404()
    {
        $this->load->view('404');        
    }

    public function blank()
    {
        $this->load->view('blank');        
    }

    public function events_detail()
    {
        $this->load->view('events-detail');        
    }

    public function events()
    {
        $this->load->view('events');        
    }

    public function forget_password()
    {
        $this->load->view('forget-password');        
    }

    public function login()
    {
        $this->load->view('login');        
    }

    public function movies_detail()
    {
        $this->load->view('movies-detail');        
    }

    public function movies()
    {
        $this->load->view('movies');        
    }

    public function offers()
    {
        $this->load->view('offers');        
    }
    public function people_detail()
    {
        $this->load->view('people-detail');        
    }
    public function people()
    {
        $this->load->view('people');        
    }
    public function profile()
    {
        $this->load->view('profile');        
    }
    public function register()
    {
        $this->load->view('register');        
    }
    public function sports_detail()
    {
        $this->load->view('sports-detail');        
    }
    public function sports()
    {
        $this->load->view('sports');        
    }
}