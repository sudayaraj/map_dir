<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Attributes_Model extends CI_Model
{

    public function insert_adpost($insert_data)
    {
        $this->db->insert('adposts', $insert_data);
        $insert_id = $this->db->insert_id();
        return $insert_id;       
        
    }

    public function upload_images($insert_data, $id)
    {        
        $this->db->insert('adpost_img', $insert_data);
        return true;        
    }

    public function getAllPost(){
        $this->db->select('*');
        $this->db->where('isactive', 1);
        $q = $this->db->get('adposts');
        if($q->num_rows() > 0){
            return $q->result_array();
        }
        
        
    }
}

/* End of file Attributes_Model.php */
