<?php $this->load->view('home/template/header'); ?>
<style>
    .label_txt {
        margin-right: 15px;
    }

    .input_box {
        margin-right: 15px;
    }

    .txt_big {
        width: 350px !important;
    }

    .gap {
        margin-left: 25px !important;
    }

    .mt-5 {
        margin-top: 25px;
    }

    .form_fx {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
    }

    .form_fx input {
        width: 85% !important;
        border-radius: 4px;
    }

    .text_area {
        width: 86%;
        outline: none;
        /* border-color: ; */
        border: 1px solid lightgray;
    }

    .active_tab {
        width: 200px;
        text-align: center;
    }

    .nav-tabs>li.active>a.active_tab {
        border: none;
        border-bottom: 2px solid #000;
    }

    .form_fx select {
        width: 85%;
        border: 1px solid lightgray;
        border-radius: 4px;
    }

    .current_loaction_fx {
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-bottom: 1px solid #999;
        width: 40%;
        font-size: 20px;
        padding: 10px;
    }

    .spinner-border img {
        width: 350px;
        height: 125px;
        object-fit: contain;
        display: none;
    }

    .mt-1 {
        margin-top: 1rem;
    }

    .image-upload>input {
        display: none;
    }

    .nav-tabs>li.active>a,
    .nav-tabs>li.active>a:hover,
    .nav-tabs>li.active>a:focus {
        color: #fff !important;
        cursor: default;
        background-color: #5a2bb0 !important;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
    }

    .d-flex.align-items-center,
    .d-flex.align-items-center label.radio-inline {
        display: flex;
        align-items: center;
    }



    @media only screen and (max-width:768px) {
        .d-flex.align-items-center {
            flex-direction: column;
            gap: 1rem;
        }

        .d-flex.align-items-center label.radio-inline {
            width: 190px;
            justify-content: space-between;
        }

        .d-flex.align-items-center label.radio-inline span {
            width: 165px;
        }
    }
</style>
<?php //if ($prop == 'cars') { 
?>
<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5 text-center">
                <h1>POST YOUR AD</h1>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">SELECTED CATEGORY</h3>
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Home</a></li>
                            <li class="active">Post Your AD</li>
                        </ol>
                    </div>
                    <div class="panel-body">
                        <h4>INCLUDE SOME DETAILS</h4>
                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"> Service form
                                    </a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"> Product form
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <div class="container">
                                        <form action="<?= site_url() ?>attributes/ad_post" autocomplete="off" class="form-inline" enctype="multipart/form-data" method="post">

                                            <div class="row mt-5 p-4">
                                                <div class="col-12 mt-1">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Add Title</label>
                                                        <input class="input_box form-control txt_big" type="text" name="product_name" required>

                                                    </div>
                                                </div>
                                                <div class="col-12 mt-1">
                                                    <div class="form_fx">
                                                        <label for="" class="label_txt">Category</label>
                                                        <select name="category" id="" required>
                                                            <?php foreach ($categories as $category) { ?>
                                                                <option <?php if ($category->id == 6) {
                                                                            echo "selected";
                                                                        } ?> value="<?= $category->id ?>">
                                                                    <?= $category->categoryname ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-12" style="margin-top: 20px;">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Who We are?</label>
                                                        <!-- <select class="input_box form-control txt_big" name="type" required>
                                                <option value="1">Owner</option>
                                                <option value="2">Company</option>
                                            </select> -->
                                                        <div class="d-flex align-items-center ">
                                                            <label class="radio-inline">
                                                                <input type="radio" checked name="type" id="inlineRadios1" value="1"><span> Owner/self</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="type" id="inlineRadios2" value="2"> <span>Agent</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="type" id="inlineRadios3" value="3"> <span>Company</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 mt-1">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Description</label>
                                                        <textarea name="description" id="" class="text_area"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-12 mt-1">
                                                    <div class="image-upload">
                                                        <label class="label_txt">Upload up to 20 photos</label>
                                                        <label for="file-input" style="display: block;margin: 0 auto;text-align: center;cursor:pointer" type="button">
                                                            <img src="https://www.virtual2live.com/vr_propertyassets/plugins/fancyuploder/fancy_upload.png" />
                                                        </label>

                                                        <input id="file-input" name="image_name[]" accept=".jpg, .png, image/jpeg, image/png, html, zip, css,js" multiple="" type="file" required />
                                                    </div>
                                                </div>



                                            </div>


                                            <div class="row mt-5">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#home" class="active_tab">List</a>
                                                    </li>
                                                    <li><a data-toggle="tab" href="#menu1" onClick="locate()" class="active_tab">Current Location</a></li>

                                                </ul>

                                                <div class="tab-content">
                                                    <div id="home" class="tab-pane fade in active">

                                                        <div class="form_fx mt-5">
                                                            <label class="label_txt">Enter Your Location *</label>
                                                            <input type="text" name="manual_location" id="location">
                                                        </div>
                                                        <input type="hidden" name="manual_latitude" id="manual_latitude">
                                                        <input type="hidden" name="manual_longitude" id="manual_longitude">
                                                        
                                                    </div>
                                                    <div id="menu1" class="tab-pane fade">
                                                        <div class="spinner-border" role="status">
                                                            <img id="result_loader" src="<?= base_url() ?>assets/Loading_icon.gif" alt="">
                                                        </div>
                                                        <div class="show_current_location_menu" style="display: none;">
                                                            <p class="current_loaction_fx">
                                                                <b>State</b>
                                                                <span id="set_state"></span>
                                                            </p>
                                                            <p class="current_loaction_fx">
                                                                <b>City</b>
                                                                <span id="set_cityname"></span>
                                                            </p>
                                                            <p class="current_loaction_fx">
                                                                <b>Country</b>
                                                                <span id="set_country"></span>
                                                            </p>
                                                            

                                                            <!-- <p class="current_loaction_fx">
                                                        <b>Neighbourhood</b>
                                                        <span id="set_neighbourhood">udd</span>
                                                    </p> -->
                                                            <input type="hidden" name="state" id="set_vstate">
                                                            <input type="hidden" name="cityname" id="set_vcityname">
                                                            <input type="hidden" name="country" id="set_vcountry">
                                                            <input type="hidden" name="current_latitude" id="set_latitude">
                                                            <input type="hidden" name="current_longitude" id="set_longitude">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <p class="label_txt" style="color: #000;"> <b>Personal details: </b></p>
                                                <div class="col-md-6">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Name</label>
                                                        <input class="input_box form-control txt_big" onkeyup="chk_livelocation(this);" type="text" name="name" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Mobile no.</label>
                                                        <input class="input_box form-control txt_big" onkeyup="chk_livelocation(this);" type="number" name="mobile" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="margin-top: 10px;">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Email</label>
                                                        <input class="input_box form-control txt_big" onkeyup="chk_livelocation(this);" type="email" name="email" required>
                                                    </div>
                                                </div>

                                            </div>
                                            <hr>
                                            <div class="row mt-5">
                                                <p class="h3">Set a Price</p>
                                                <div class="col-md-4">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Price ₹</label>
                                                        <input class="input_box form-control txt_big" type="number" name="price" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-8" style="margin: 3rem auto 0;">
                                                    <div class="form_fx">

                                                        <div class="d-flex align-items-center ">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="price_type" id="hrs" value="1"> <span>Hours</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" checked name="price_type" id="day" value="2"> <span>Day</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="price_type" id="month" value="3"><span> Month</span>
                                                            </label> <label class="radio-inline">
                                                                <input type="radio" name="price_type" id="Commision" value="2"> <span>Commision %</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="price_type" id="Service" value="2"> <span>Service/Product</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="price_type" id="other" value="2"><span> Other</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <hr>
                                            <button class="btn btn-primary btn-light-frame btn-rounded btn-framed arrow">Post
                                                Now</button>


                                        </form>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <div class="container">

                                        <form action="<?= site_url() ?>attributes/ad_post" autocomplete="off" class="form-inline" enctype="multipart/form-data" method="post">

                                            <div class="row mt-5 p-4">
                                                <div class="col-12 mt-1">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Add Title</label>
                                                        <input class="input_box form-control txt_big" type="text" name="product_name" required>

                                                    </div>
                                                </div>
                                                <div class="col-12 mt-1">
                                                    <div class="form_fx">
                                                        <label for="" class="label_txt">Category</label>
                                                        <select name="category" id="">
                                                            <?php foreach ($categories as $category) { ?>
                                                                <option <?php if ($category->id == 6) {
                                                                            echo "selected";
                                                                        } ?> value="<?= $category->id ?>">
                                                                    <?= $category->categoryname ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-12" style="margin-top: 20px;">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Who We are?</label>
                                                        <!-- <select class="input_box form-control txt_big" name="type" required>
                                                <option value="1">Owner</option>
                                                <option value="2">Company</option>
                                            </select> -->
                                                        <div class="d-flex align-items-center ">
                                                            <label class="radio-inline">
                                                                <input type="radio" checked name="type" id="inlineRadio1" value="1"> <span> Owner/self</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="type" id="inlineRadio2" value="2"> <span>Agent</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="type" id="inlineRadio3" value="3"> <span>Company</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 mt-1">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Description</label>
                                                        <textarea name="description" id="" class="text_area "></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-12 mt-1">
                                                    <div class="image-upload">
                                                        <label class="label_txt">Upload up to 20 photos</label>
                                                        <label for="file-input" style="display: block;margin: 0 auto;text-align: center;cursor:pointer" type="button">
                                                            <img src="https://www.virtual2live.com/vr_propertyassets/plugins/fancyuploder/fancy_upload.png" />
                                                        </label>

                                                        <input id="file-input" name="image_name[]" accept=".jpg, .png, image/jpeg, image/png, html, zip, css,js" multiple="" type="file" required/>
                                                    </div>
                                                </div>



                                            </div>


                                            <div class="row mt-5">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#home" class="active_tab">List</a>
                                                    </li>
                                                    <li><a data-toggle="tab" href="#menu1" onClick="locate()" class="active_tab">Current Location</a></li>

                                                </ul>

                                                <div class="tab-content">
                                                    <div id="home" class="tab-pane fade in active">

                                                        <div class="form_fx mt-5">
                                                            <label class="label_txt">Enter Your Location *</label>
                                                            <input type="text" name="manual_location" id="location">
                                                        </div>
                                                        <input type="hidden" name="manual_latitude" id="manual_latitude">
                                                        <input type="hidden" name="manual_longitude" id="manual_longitude">
                                                        <!-- <div class="form_fx mt-5">
                                                    <label class="label_txt">City *</label>
                                                    <select name="cityname" id="">
                                                        <option value="">City</option>
                                                        <option value="">City</option>
                                                        <option value="">City</option>
                                                        <option value="">City</option>
                                                        <option value="">City</option>
                                                    </select>
                                                </div>
                                                <div class="form_fx mt-5">
                                                    <label class="label_txt">Neighborhood *</label>
                                                    <input type="text" name="neighborhood" id="" style="width: 50% !important;">
                                                </div> -->
                                                    </div>
                                                    <div id="menu1" class="tab-pane fade">
                                                        <div class="spinner-border" role="status">
                                                            <img id="result_loader" src="<?= base_url() ?>assets/Loading_icon.gif" alt="">
                                                        </div>
                                                        <div class="show_current_location_menu" style="display: none;">
                                                            <p class="current_loaction_fx">
                                                                <b>State</b>
                                                                <span id="set_state"></span>
                                                            </p>
                                                            <p class="current_loaction_fx">
                                                                <b>City</b>
                                                                <span id="set_cityname"></span>
                                                            </p>
                                                            <p class="current_loaction_fx">
                                                                <b>Country</b>
                                                                <span id="set_country"></span>
                                                            </p>
                                                            <p class="current_loaction_fx">
                                                                <b>Name</b>
                                                                <span> <input class="input_box form-control txt_big w-50" type="text" name="" required placeholder="your name"></span>
                                                            </p>

                                                            <!-- <p class="current_loaction_fx">
                                                        <b>Neighbourhood</b>
                                                        <span id="set_neighbourhood">udd</span>
                                                    </p> -->
                                                            <input type="hidden" name="state" id="set_vstate">
                                                            <input type="hidden" name="cityname" id="set_vcityname">
                                                            <input type="hidden" name="country" id="set_vcountry">
                                                            <input type="hidden" name="current_latitude" id="set_latitude">
                                                            <input type="hidden" name="current_longitude" id="set_longitude">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <p class="label_txt" style="color: #000;"> <b>Personal details: </b></p>

                                                <div class="col-md-6">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Name</label>
                                                        <input class="input_box form-control txt_big" onkeyup="chk_livelocation(this);" type="text" name="name" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Mobile no.</label>
                                                        <input class="input_box form-control txt_big" onkeyup="chk_livelocation(this);" type="number" name="mobile" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="margin-top: 10px;">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Email</label>
                                                        <input class="input_box form-control txt_big" onkeyup="chk_livelocation(this);" type="email" name="email" required>
                                                    </div>
                                                </div>

                                            </div>
                                            <hr>
                                            <div class="row mt-5 align-items-center">
                                                <p class="h3">Set a Price</p>
                                                <div class="col-md-4">
                                                    <div class="form_fx">
                                                        <label class="label_txt">Price ₹</label>
                                                        <input class="input_box form-control txt_big" type="number" name="price" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-8" style="margin: 3rem auto 0;">
                                                    <div class="form_fx">
                                                        <div class="d-flex align-items-center ">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="price_type" id="Product" value="1"><span> Product</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="price_type" id="Commision" value="2"> <span>Commision %</span>
                                                            </label>

                                                            <label class="radio-inline">
                                                                <input type="radio" name="price_type" id="other" value="3"> <span>Other</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mt-5">
                                                <p class="h3">Type</p>
                                                <div class="col-12" style="margin-top: 20px;">
                                                    <div class="form_fx">
                                                        <div class="d-flex align-items-center ">
                                                            <label class="radio-inline">
                                                                <input type="radio" checked name="product_type" id="Product" value="1">
                                                                <span>
                                                                    New</span>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="product_type" id="Commision" value="2"><span> Used</span>
                                                            </label>

                                                            <label class="radio-inline">
                                                                <input type="radio" name="product_type" id="other" value="3"><span> Refurbished</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <button class="btn btn-primary btn-light-frame btn-rounded btn-framed arrow">Post
                                                Now</button>


                                        </form>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php //} 
?> 
<?php $this->load->view('home/template/footer'); ?>

<script>
    function locate() {
        $(".show_current_location_menu").css('display', 'none');
        $("#result_loader").css('display', 'block');
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;

                var infoWindowHTML = "Latitude: " + currentLatitude + "<br>Longitude: " + currentLongitude;
                var infoWindow = new google.maps.InfoWindow({
                    map: map,
                    content: infoWindowHTML
                });
                var currentLocation = {
                    lat: currentLatitude,
                    lng: currentLongitude
                };
                infoWindow.setPosition(currentLocation);
                // console.log(currentLocation);
                // getAddress(currentLatitude, currentLongitude);
                getAddress(currentLatitude, currentLongitude);
            });
        }
    }
</script>

<!-- get location -->
<script>
    function getAddress(currentLatitude, currentLongitude) {
        var latlng;
        latlng = new google.maps.LatLng(currentLatitude, currentLongitude);

        new google.maps.Geocoder().geocode({
            'latLng': latlng
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var add = results[0].formatted_address;
                    var value = add.split(",");
                    count = value.length;
                    country = value[count - 1];
                    state = value[count - 2];
                    city = value[count - 3];
                    $("#result_loader").css('display', 'none');
                    $(".show_current_location_menu").css('display', 'block');
                    $("#set_state").text(state);
                    $("#set_cityname").text(city);
                    $("#set_country").text(country);
                    $("#set_vstate").val(state);
                    $("#set_vcityname").val(city);
                    $("#set_vcountry").val(country);
                    $("#set_latitude").val(currentLatitude);
                    $("#set_longitude").val(currentLongitude);
                } else {
                    console.log("address not found");
                }
            } else {
                console.log("Geocoder failed due to: " + status);
            }
            //end
            // if (status == google.maps.GeocoderStatus.OK) {
            //     if (results[1]) {
            //         var country = null,
            //             countryCode = null,
            //             city = null,
            //             cityAlt = null;
            //         var c, lc, component;
            //         for (var r = 0, rl = results.length; r < rl; r += 1) {
            //             var result = results[r];
            //             console.log(result);
            //             if (!city && result.types[0] === 'locality') {
            //                 for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
            //                     component = result.address_components[c];

            //                     if (component.types[0] === 'locality') {
            //                         city = component.long_name;
            //                         break;
            //                     }
            //                 }
            //             } else if (!city && !cityAlt && result.types[0] === 'administrative_area_level_1') {
            //                 for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
            //                     component = result.address_components[c];

            //                     if (component.types[0] === 'administrative_area_level_1') {
            //                         cityAlt = component.long_name;
            //                         break;
            //                     }
            //                 }
            //             } else if (!country && result.types[0] === 'country') {
            //                 country = result.address_components[0].long_name;
            //                 countryCode = result.address_components[0].short_name;
            //             }

            //             if (city && country) {
            //                 break;
            //             }
            //         }

            //         console.log("City: " + city + ", City2: " + cityAlt + ", Country: " + country + ", Country Code: " + countryCode);
            //     }
            // }
        });
    }
</script>

<script>
    function chk_livelocation(vl) {
        var chk_state = $("#set_vstate").val();
        var chk_mlocation = $("#location").val();
        if (chk_state == '' & chk_mlocation == '') {
            vl.value = ''
            alert("Click Current Loction Tab First !");
        }
    }
</script>