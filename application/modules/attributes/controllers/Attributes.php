<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Attributes extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Attributes_Model');
        $this->load->model('home/Home_Model');

        if ($this->session->userdata('user_logged_in') != 1) {

            redirect('home', 'refresh');
        }
    }

    public function index()
    {
        // $data['prop'] = $prop;
        $data['categories'] = $this->Home_Model->getCategories();
        $this->load->view('index', $data);
    }

    public function ad_post()
    {
        $username = $this->session->userdata('username');
        $userid = $this->session->userdata('id');

        $product_name = $this->input->post('product_name');
        $type = $this->input->post('type');
        $image_name = $this->input->post('image_name');
        $description = $this->input->post('description');
        $category = $this->input->post('category');
        if (empty($this->input->post('manual_latitude'))) {
            $state = $this->input->post('state');
            $cityname = $this->input->post('cityname');
            $country = $this->input->post('country');
            $current_latitude = $this->input->post('current_latitude');
            $current_longitude = $this->input->post('current_longitude');
        } else {
            $manual_location = $this->input->post('manual_location');
            $mlocation = explode(',', $manual_location);
            $cityname = $mlocation[0];
            $state = $mlocation[1];
            $country = $mlocation[2];
            $current_latitude = $this->input->post('manual_latitude');
            $current_longitude = $this->input->post('manual_longitude');
        }
        $name = $this->input->post('name');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $price = $this->input->post('price');
        $price_type = $this->input->post('price_type');

        $insert_data = array(
            "product_name" => $product_name,
            "type" => $type,
            "description" => $description,
            "category" => $category,
            "category_id" => $category,
            "state" => $state,
            "city" => $cityname,
            "country" => $country,
            "current_latitude" => $current_latitude,
            "current_longitude" => $current_longitude,
            "name" => $name,
            "mobile" => $mobile,
            "email" => $email,
            "price" => $price,
            "price_type" => $price_type,
            "username" => $username,
            "userid" => $userid,
            "isactive" => 1
        );

        $inserted_id = $this->Attributes_Model->insert_adpost($insert_data);
        $this->multiple_files($inserted_id);
    }

    public function multiple_files($id)
    {
        //Image Size	
        $image_size = $this->config->item('image_sizes');

        //Upload Configuration Image
        $config['upload_path'] = './adpostimages/' . $id . '/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['rename'] = true;
        $config['image_sizes'] = $image_size;
        $files = $_FILES;


        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0755, TRUE);
        }
        $mum_files = count($files['image_name']);
        $dataInfo = array();
        for ($i = 0; $i < $mum_files; $i++) {
            if (isset($files['image_name']['name'][$i])) {
                $config['file_name'] = time() . '-' . $files['image_name']['name'][$i];
                $this->load->library("upload", $config);
                $_FILES['image_name']['name'] = $files['image_name']['name']["$i"];
                $_FILES['image_name']['type'] = $files['image_name']['type']["$i"];
                $_FILES['image_name']['tmp_name'] = $files['image_name']['tmp_name']["$i"];
                $_FILES['image_name']['error'] = $files['image_name']['error']["$i"];
                $_FILES['image_name']['size'] = $files['image_name']['size']["$i"];
                $filename = rand() . '-' . $_FILES['image_name']['name'];

                if (!$this->upload->do_upload('image_name')) {
                    $error_message = $this->upload->display_errors();

                    $this->session->set_flashdata('status', 'error');
                    $this->session->set_flashdata('message', "$error_message");
                } else {
                    $this->session->set_flashdata('status', 'success');
                    $this->session->set_flashdata('message', "AD Posted Successfully");
                }

                $dataInfo[] = $this->upload->data();
            }
        }

        $all_imgs = '';

        if (count($dataInfo) > 0) {
            foreach ($dataInfo as $info) {
                $all_imgs .= $info['file_name'];
                $all_imgs .= ',';
            }
        }

        $insert_data = array(
            'images' => rtrim($all_imgs, ","),
            'adposts_id' => $id,
            'status' => 1

        );
        if (!$this->upload->do_upload('image_name')) {
            $this->db->where('id', $id);
            $this->db->delete('adposts');
        } else {
            $this->Attributes_Model->upload_images($insert_data, $id);
        }

        redirect('home', 'refresh');
    }

    public function getAllPost()
    {

        $adposts = $this->Attributes_Model->getAllPost();

        // echo "<pre>";
        // print_r ($adposts);
        // echo "</pre>";
        // echo json_encode($adposts);
        // exit;

        $data = array();
        foreach ($adposts as $adpost) {

            $data[] =
                [
                    "id" => $adpost['id'],
                    "latitude" => $adpost['current_latitude'],
                    "longitude" => $adpost['current_longitude'],
                    'featured' => 1,
                    'title' => $adpost['product_name'],
                    'location' => $adpost['city'],
                    'city' => 1,
                    'phone' => $adpost['mobile'],
                    'category' => "AD Post",
                    'rating' => "",
                    'reviews_number' => "",
                    'marker_image' => "assets/img/items/29.jpg",
                    'gallery' => array(
                        "assets/img/items/29.jpg",
                        "assets/img/items/11.jpg",
                        "assets/img/items/12.jpg"
                    ),
                    'price' => $adpost['price'],
                    'tags' => array(
                        "Diesel",
                        "First Owner",
                        "4x4",
                        "Air Conditioning"
                    ),
                    'additional_info' => "",
                    'url' => "detail.html",
                    'description' => $adpost['description'],
                    'description_list' => array(
                        [
                            'title' => "Engine",
                            'value' => "Diesel",
                        ],
                        [
                            'title' => "Mileage",
                            'value' => 14500,
                        ],
                        [
                            'title' => "Max Speed",
                            'value' => "220 Mph",
                        ],
                        [
                            'title' => "marker_color",
                            'value' => "Dark Brown",
                        ],
                        [
                            'title' => "Status",
                            'value' => "Sale",
                        ],
                    ),
                    'marker_color' => "#45ad00"
                ];
        }
        echo json_encode($data);
    }
}
