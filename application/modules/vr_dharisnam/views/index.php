<!doctype html>
<html lang="en" dir="ltr">

<head>
	<!-- Meta data -->
	<meta charset="UTF-8">
	<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Reallist- Bootstrap Responsive Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose HTML Template" name="description">
	<meta content="Spruko Technologies Private Limited" name="author">
	<meta name="keywords" content="html template, real estate websites, real estate html template, property websites, premium html templates, real estate company website, bootstrap real estate template, real estate marketplace html template, listing website template, property listing html template, real estate bootstrap template, real estate html5 template, real estate listing template, property template, property dealer website" />

	<!-- Favicon -->
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

	<!-- Title -->
	<title>Reallist- Bootstrap Responsive Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose HTML
		Template</title>

	<!-- Bootstrap Css -->
	<link id="style" href="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/css/bootstrap.min.css" rel="stylesheet" />

	<!-- Dashboard Css -->
	<link href="<?= base_url() ?>vr_propertyassets/css/style.css" rel="stylesheet" />

	<!-- Font-awesome  Css -->
	<link href="<?= base_url() ?>vr_propertyassets/css/icons.css" rel="stylesheet" />

	<!--Select2 Plugin -->
	<link href="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.min.css" rel="stylesheet" />

	<!-- Owl Theme css-->
	<link href="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" />

	<!-- P-scroll bar css-->
	<link href="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= base_url() ?>vrmall_assets/css/vr_mall.css">
	<style>
		.vr_prop_cat_ht {
			height: 155px;
		}
	</style>
</head>

<body class="main-body">

	<!--Loader-->
	<div id="global-loader">
		<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/loader.svg" class="loader-img" alt="">
	</div>
	<div class="nav1">
		<div class="container-fluid text-center">
			<div class="navbar">
				<img src="" alt="Logo" class="logo">
				<!-- toggle img start -->
				<img src="<?= base_url() ?>assets/icons/toggle.jpg" style="width:34px;" alt="toggle" class="res_toggle" id="res_toggle">
				<!-- toggle img end -->
				<ul class="navul1 " id="navul1">
					<li class="navli1"><a href="<?= base_url() ?>">Virtual Link</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_mall">Virtual Mall</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_property">Virtual Property</a></li>
					<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Virtual Education</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_studio">VR Studio</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_tour">VR Tour</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_dharisnam" class="nav_active">VR Dharisanam</a></li>
					<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Buy Sharing</a></li>
					<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">CP Hub</a></li>
					<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Live Stream</a></li>
					<li class="navli1 navrmborder"><a href="#">Contact Us</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!--Topbar-->
	<div class="header-main pt_3">
		<div class="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-xl-8 col-lg-8 col-sm-4 col-7">
						<div class="top-bar-left d-flex">
							<div class="clearfix">
								<ul class="socials">
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
									</li>
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
									</li>
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-linkedin"></i></a>
									</li>
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-google-plus"></i></a>
									</li>
								</ul>
							</div>
							<div class="clearfix">
								<ul class="contact">
									<li class="me-5 d-lg-none">
										<a href="javascript:void(0);" class="callnumber text-dark"><span><i class="fa fa-phone me-1"></i>: +425 345 8765</span></a>
									</li>
									<li class="select-country me-5">
										<select class="form-control select2-flag-search" data-placeholder="Select Country">
											<option value="UM">United States of America</option>
											<option value="AF">Afghanistan</option>
											<option value="AL">Albania</option>
											<option value="AD">Andorra</option>
											<option value="AG">Antigua and Barbuda</option>
											<option value="AU">Australia</option>
											<option value="AM">Armenia</option>
											<option value="AO">Angola</option>
											<option value="AR">Argentina</option>
											<option value="AT">Austria</option>
											<option value="AZ">Azerbaijan</option>
											<option value="BA">Bosnia and Herzegovina</option>
											<option value="BB">Barbados</option>
											<option value="BD">Bangladesh</option>
											<option value="BE">Belgium</option>
											<option value="BF">Burkina Faso</option>
											<option value="BG">Bulgaria</option>
											<option value="BH">Bahrain</option>
											<option value="BJ">Benin</option>
											<option value="BN">Brunei</option>
											<option value="BO">Bolivia</option>
											<option value="BT">Bhutan</option>
											<option value="BY">Belarus</option>
											<option value="CD">Congo</option>
											<option value="CA">Canada</option>
											<option value="CF">Central African Republic</option>
											<option value="CI">Cote d'Ivoire</option>
											<option value="CL">Chile</option>
											<option value="CM">Cameroon</option>
											<option value="CN">China</option>
											<option value="CO">Colombia</option>
											<option value="CU">Cuba</option>
											<option value="CV">Cabo Verde</option>
											<option value="CY">Cyprus</option>
											<option value="DJ">Djibouti</option>
											<option value="DK">Denmark</option>
											<option value="DM">Dominica</option>
											<option value="DO">Dominican Republic</option>
											<option value="EC">Ecuador</option>
											<option value="EE">Estonia</option>
											<option value="ER">Eritrea</option>
											<option value="ET">Ethiopia</option>
											<option value="FI">Finland</option>
											<option value="FJ">Fiji</option>
											<option value="FR">France</option>
											<option value="GA">Gabon</option>
											<option value="GD">Grenada</option>
											<option value="GE">Georgia</option>
											<option value="GH">Ghana</option>
											<option value="GH">Ghana</option>
											<option value="HN">Honduras</option>
											<option value="HT">Haiti</option>
											<option value="HU">Hungary</option>
											<option value="ID">Indonesia</option>
											<option value="IE">Ireland</option>
											<option value="IL">Israel</option>
											<option value="IN">India</option>
											<option value="IQ">Iraq</option>
											<option value="IR">Iran</option>
											<option value="IS">Iceland</option>
											<option value="IT">Italy</option>
											<option value="JM">Jamaica</option>
											<option value="JO">Jordan</option>
											<option value="JP">Japan</option>
											<option value="KE">Kenya</option>
											<option value="KG">Kyrgyzstan</option>
											<option value="KI">Kiribati</option>
											<option value="KW">Kuwait</option>
											<option value="KZ">Kazakhstan</option>
											<option value="LA">Laos</option>
											<option value="LB">Lebanons</option>
											<option value="LI">Liechtenstein</option>
											<option value="LR">Liberia</option>
											<option value="LS">Lesotho</option>
											<option value="LT">Lithuania</option>
											<option value="LU">Luxembourg</option>
											<option value="LV">Latvia</option>
											<option value="LY">Libya</option>
											<option value="MA">Morocco</option>
											<option value="MC">Monaco</option>
											<option value="MD">Moldova</option>
											<option value="ME">Montenegro</option>
											<option value="MG">Madagascar</option>
											<option value="MH">Marshall Islands</option>
											<option value="MK">Macedonia (FYROM)</option>
											<option value="ML">Mali</option>
											<option value="MM">Myanmar (formerly Burma)</option>
											<option value="MN">Mongolia</option>
											<option value="MR">Mauritania</option>
											<option value="MT">Malta</option>
											<option value="MV">Maldives</option>
											<option value="MW">Malawi</option>
											<option value="MX">Mexico</option>
											<option value="MZ">Mozambique</option>
											<option value="NA">Namibia</option>
											<option value="NG">Nigeria</option>
											<option value="NO">Norway</option>
											<option value="NP">Nepal</option>
											<option value="NR">Nauru</option>
											<option value="NZ">New Zealand</option>
											<option value="OM">Oman</option>
											<option value="PA">Panama</option>
											<option value="PF">Paraguay</option>
											<option value="PG">Papua New Guinea</option>
											<option value="PH">Philippines</option>
											<option value="PK">Pakistan</option>
											<option value="PL">Poland</option>
											<option value="QA">Qatar</option>
											<option value="RO">Romania</option>
											<option value="RU">Russia</option>
											<option value="RW">Rwanda</option>
											<option value="SA">Saudi Arabia</option>
											<option value="SB">Solomon Islands</option>
											<option value="SC">Seychelles</option>
											<option value="SD">Sudan</option>
											<option value="SE">Sweden</option>
											<option value="SG">Singapore</option>
											<option value="TG">Togo</option>
											<option value="TH">Thailand</option>
											<option value="TJ">Tajikistan</option>
											<option value="TL">Timor-Leste</option>
											<option value="TM">Turkmenistan</option>
											<option value="TN">Tunisia</option>
											<option value="TO">Tonga</option>
											<option value="TR">Turkey</option>
											<option value="TT">Trinidad and Tobago</option>
											<option value="TW">Taiwan</option>
											<option value="UA">Ukraine</option>
											<option value="UG">Uganda</option>
											<option value="UY">Uruguay</option>
											<option value="UZ">Uzbekistan</option>
											<option value="VA">Vatican City (Holy See)</option>
											<option value="VE">Venezuela</option>
											<option value="VN">Vietnam</option>
											<option value="VU">Vanuatu</option>
											<option value="YE">Yemen</option>
											<option value="ZM">Zambia</option>
											<option value="ZW">Zimbabwe</option>
										</select>
									</li>
									<li class="dropdown me-5">
										<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><span>
												Language <i class="fa fa-caret-down text-muted"></i></span> </a>
										<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
											<a href="javascript:void(0);" class="dropdown-item">
												English
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												Arabic
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												German
											</a>
											<a href="javascript:void(0);" class="dropdown-item">
												Greek
											</a>
											<a href="javascript:void(0);" class="dropdown-item">
												Japanese
											</a>
										</div>
									</li>
									<li class="dropdown">
										<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><span>Currency <i class="fa fa-caret-down text-muted"></i></span></a>
										<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
											<a href="javascript:void(0);" class="dropdown-item">
												USD
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												EUR
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												INR
											</a>
											<a href="javascript:void(0);" class="dropdown-item">
												GBP
											</a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-4 col-sm-8 col-5">
						<div class="top-bar-right">
							<ul class="custom">
								<li>
									<a href="register.html" class="text-dark"><i class="fa fa-user me-1"></i>
										<span>Register</span></a>
								</li>
								<li>
									<a href="login.html" class="text-dark"><i class="fa fa-sign-in me-1"></i>
										<span>Login</span></a>
								</li>
								<li class="dropdown">
									<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><i class="fa fa-home me-1"></i><span> My Dashboard</span></a>
									<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
										<a href="mydash.html" class="dropdown-item">
											<i class="dropdown-icon icon icon-user"></i> My Profile
										</a>
										<a href="ad-list.html" class="dropdown-item">
											<i class="dropdown-icon icon icon-speech"></i> Ads
										</a>
										<a href="settings.html" class="dropdown-item">
											<i class="dropdown-icon icon icon-bell"></i> Notifications
										</a>
										<a href="mydash.html" class="dropdown-item">
											<i class="dropdown-icon  icon icon-settings"></i> Account Settings
										</a>
										<a href="login.html" class="dropdown-item">
											<i class="dropdown-icon icon icon-power"></i> Log out
										</a>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="cover-image sptb-tab bg-background2 mb-lg-8 pb-lg-0 pb-8" data-bs-image-src="https://www.spruko.com/demo/reallist/Reallist/assets/images/banners/banner1.jpg">

			<!-- Duplex Houses Header -->
			<div class="sticky">
				<div class="horizontal-header clearfix ">
					<div class="container">
						<a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
						<span class="smllogo">
							<a href="<?= base_url() ?>vr_dharisnam/index">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo.png" class="mobile-light-logo" width="120" alt="" />
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" class="mobile-dark-logo" width="120" alt="" />
							</a>
						</span>
						<a href="tel:245-6325-3256" class="callusbtn"><i class="fa fa-phone" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
			<!-- /Duplex Houses Header -->

			<div class="horizontal-main clearfix">
				<div class="horizontal-mainwrapper container clearfix">
					<div class="desktoplogo">
						<a href="<?= base_url() ?>vr_dharisnam/index"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<div class="desktoplogo-1">
						<a href="<?= base_url() ?>vr_dharisnam/index"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<!--Nav-->
					<nav class="horizontalMenu clearfix d-md-flex">
						<ul class="horizontalMenu-list">
							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/" class="active">Home </a>

							</li>
							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/about">About Us </a></li>

							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/page_list_map2">Pages </a>

							</li>
							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/blog_list">Blog </a>
							</li>
							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/col_left">Add details</a>

							</li>
							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/contact"> Contact Us <span class="hmarrow"></span></a></li>
							<li aria-haspopup="true">
								<a  href="<?= base_url() ?>vr_dharisnam/ad_posts">Post Property Ad</a>
							</li>
							
						</ul>
						
					</nav>
					<!--Nav-->
				</div>
			</div>
		</div>
	</div>

	<!--Map-->
	<div class="relative overflow-hidden">
		<div class="axgmap" data-latlng="36.261992,-451.757813" data-zoom="10">
			<div class="axgmap-img" data-latlng="36.261497,-451.756622" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/1.png" data-title="Louvre Museum">
				<h4>House for Rent</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div>Price: <a class="h4"> $ 150</a></div>
				<div class="rating-stars block mt-2 mb-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
			<div class="axgmap-img" data-latlng="36.286021,-451.851540" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/2.png" data-title="Louvre Museum">
				<h4>Flat for Rent</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v2.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div>Price: <a class="h4"> $ 15</a></div>
				<div class="rating-stars block mt-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
			<div class="axgmap-img" data-latlng="36.341350,-452.018394" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/3.png" data-title="Louvre Museum">
				<h4>RealEstate Agent1</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div><a class="h6">Agent Reallists</a></div>
				<div class="rating-stars block mt-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
			<div class="axgmap-img" data-latlng="36.299857,-451.605034" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/1.png" data-title="Louvre Museum">
				<h4>House for sale</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v3.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div>Price: <a class="h4"> $ 180</a></div>
				<div class="rating-stars block mt-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
			<div class="axgmap-img" data-latlng="36.217358,-451.986122" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/2.png" data-title="Louvre Museum">
				<h4>Flat for Rent</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div>Price: <a class="h4"> $ 21</a></div>
				<div class="rating-stars block mt-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
			<div class="axgmap-img" data-latlng="36.152742,-451.785965" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/3.png" data-title="Louvre Museum">
				<h4>RealEstate Agent1</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div><a class="h6"> Agent Reallist</a></div>
				<div class="rating-stars block mt-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
			<div class="axgmap-img" data-latlng="37.347507,-453.718872" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/4.png" data-title="Louvre Museum">
				<h4>BestRealEstate</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f1.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div><a class="h6"> Offer : 10am-11pm</a></div>
				<div class="rating-stars block mt-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
			<div class="axgmap-img" data-latlng="35.865961,-453.740845" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/5.png" data-title="Louvre Museum">
				<h4>Shop for Rent</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div><a class="h6"> Offer : 10am-11pm</a></div>
				<div class="rating-stars block mt-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p><i class="fa fa-phone-square" aria-hidden="true"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
			<div class="axgmap-img" data-latlng="38.630818,-456.394043" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/6.png" data-title="Louvre Museum">
				<h4>2BK House For Sale</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h3.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div>Price: <a class="h4"> $ 201</a></div>
				<div class="rating-stars block mt-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
			<div class="axgmap-img" data-latlng="36.962237,-447.599487" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/7.png" data-title="Louvre Museum">
				<h4>Office Houses For Rent</h4>
				<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b2.png" alt="img" class="w-150 h100 mb-3 mt-2">
				<div><a class="h6"> Offer : 10am-11pm</a></div>
				<div class="rating-stars block mt-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
			</div>
		</div>
		<!--Sliders Section-->
		<div class="row p-0">
			<div class="col-xl-6 col-lg-12 col-md-12 d-block mx-auto p-0">
				<div class="map-absolute">
					<div class="py-5 px-3 br-3 bg-gradient-primary">
						<div class="header-text1 mb-0">
							<div class="container">
								<div class="search-background bg-transparent">
									<div class="form row no-gutters ">
										<div class="form-group  col-xl-4 col-lg-3 col-md-12 mb-0">
											<input type="text" class="form-control input-lg br-tr-md-0 br-br-md-0" id="text4" placeholder="Find Your Property">
										</div>
										<div class="form-group  col-xl-3 col-lg-3 col-md-12 mb-0">
											<input type="text" class="form-control input-lg br-md-0" id="text5" placeholder="Enter Location">
											<span><i class="fa fa-map-marker location-gps me-1"></i> </span>
										</div>
										<div class="form-group col-xl-3 col-lg-3 col-md-12 select2-lg mb-0">
											<select class="form-control select2-show-search  border-bottom-0" data-placeholder="Property Type">
												<optgroup label="Categories">
													<option>Property Type</option>
													<option value="1">Deluxe Houses</option>
													<option value="2">Modren Flats</option>
													<option value="3">Apartments</option>
													<option value="4">Stylish Houses</option>
													<option value="5">Offices Houses</option>
													<option value="6">Luxury Houses</option>
													<option value="7">Nature Houses</option>
												</optgroup>
											</select>
										</div>
										<div class="col-xl-2 col-lg-3 col-md-12 mb-0">
											<a href="javascript:void(0);" class="btn btn-lg btn-block btn-primary br-tl-md-0 br-bl-md-0">Search
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- /header-text -->
			</div>
		</div>
		<!--/Sliders Section-->
	</div>
	<!--/Map-->

	<!-- Categories-->
	<section class="sptb bg-white">
		<div class="container">
			<div class="section-title center-block text-center">
				<h2>Categories</h2>
				<p class="invisble">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
			</div>
			<div class="row">
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">


						<div class="card bg-primary text-white mb-xl-0" type="button" id="categorydd1" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="#"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/1.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Virtual Reality Meditation </h5>
									</div>
								</div>
							</div>
						</div>

						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd1">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card bg-secondary text-white mb-xl-0" type="button" id="categorydd2" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/2.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">District Wise Temple list</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd2">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>

				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card  bg-warning text-white mb-xl-0" type="button" id="categorydd3" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/3.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Hindu Gods</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd3">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card bg-success text-white mb-md-0" type="button" id="categorydd4" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/4.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Navagraha temple</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd4">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card bg-purple text-white mb-sm-0" type="button" id="categorydd5" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/5.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Divya Desam</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd5">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card bg-cyan text-white mb-0" type="button" id="categorydd6" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/6.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Siddhar Temples</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd6">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>

				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">


						<div class="card bg-primary text-white mb-xl-0" type="button" id="categorydd7" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="#"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/1.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Sai Baba Temple </h5>
									</div>
								</div>
							</div>
						</div>

						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd7">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card bg-secondary text-white mb-xl-0" type="button" id="categorydd8" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/2.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Iskcon</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd8">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>

				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card  bg-warning text-white mb-xl-0" type="button" id="categorydd9" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/3.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Yamadharmaraj and Chitragupta</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd9">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card bg-success text-white mb-md-0" type="button" id="categorydd10" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/4.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Temple News</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd10">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card bg-purple text-white mb-sm-0" type="button" id="categorydd11" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/5.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Upcoming Temple Event and Functions</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd11">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 my-2">
					<div class="dropdown">
						<div class="card bg-cyan text-white mb-0" type="button" id="categorydd12" data-bs-toggle="dropdown" aria-expanded="false">
							<div class="card-body vr_prop_cat_ht">
								<div class="cat-item text-center">
									<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
									<div class="cat-img text-shadow1">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/6.png" alt="img" class="cover-image">
									</div>
									<div class="cat-desc text-white">
										<h5 class="mb-1">Live Dharisanam</h5>
									</div>
								</div>
							</div>
						</div>
						<ul class="dropdown-menu shadow-lg" aria-labelledby="categorydd12">
							<li><a class="dropdown-item" href="#">Action</a></li>
							<li><a class="dropdown-item" href="#">Another action</a></li>
							<li><a class="dropdown-item" href="#">Something else here</a></li>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!--Categories-->

	<!--Latest Ads-->
	<section class="sptb ">
		<div class="container">
			<div class="section-title center-block text-center">
				<h2>Virtual Reality Dhyanam & Prayers</h2>
				<p class="invisble">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
			</div>
			<div id="myCarousel1" class="owl-carousel owl-carousel-icons2">
				<div class="item">
					<div class="card mb-0">
						<div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
						<div class="item-card2-img">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/f1.jpg" alt="img" class="cover-image">
							<div class="tag-text">
								<span class="bg-danger tag-option">For Sale </span>
								<span class="bg-pink tag-option">Open</span>
							</div>
						</div>
						<div class="item-card2-icons">
							<a href="<?= base_url() ?>vr_dharisnam/col_left" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
							<a href="javascript:void(0);" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="card-body">
							<div class="item-card2">
								<div class="item-card2-text">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">Deluxe Houses</h4>
									</a>
									<p class="mb-2"><i class="fa fa-map-marker text-danger me-1"></i> Preston Street
										Wichita , USA </p>
									<h5 class="font-weight-bold mb-3">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
								</div>
								<ul class="item-card2-list">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											256 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 3 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 2 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
							</div>
						</div>
						<div class="card-footer">
							<div class="footerimg d-flex mt-0 mb-0">
								<div class="d-flex footerimg-l mb-0">
									<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/18.jpg" alt="image" class="avatar brround  me-2">
									<h5 class="time-title text-muted p-0 leading-normal my-auto">Wendy Peake<i class="si si-check text-success fs-12 ms-1" data-bs-toggle="tooltip" data-bs-placement="top" title="verified"></i></h5>
								</div>
								<div class="my-auto footerimg-r ms-auto">
									<small class="text-muted">1 day ago</small>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="card mb-0">
						<div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
						<div class="item-card2-img">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h4.jpg" alt="img" class="cover-image">
							<div class="tag-text"><span class="bg-danger tag-option">For Rent </span></div>
						</div>
						<div class="item-card2-icons">
							<a href="javascript:void(0);" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
							<a href="javascript:void(0);" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="card-body">
							<div class="item-card2">
								<div class="item-card2-text">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">2BK Houses</h4>
									</a>
									<p class="mb-2"><i class="fa fa-map-marker text-danger me-1"></i> Preston Street
										Wichita , USA </p>
									<h5 class="font-weight-bold mb-3">$12,890 <span class="fs-12  font-weight-normal">Per Month</span></h5>
								</div>
								<ul class="item-card2-list">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											150 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 2 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
							</div>
						</div>
						<div class="card-footer">
							<div class="footerimg d-flex mt-0 mb-0">
								<div class="d-flex footerimg-l mb-0">
									<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/female/12.jpg" alt="image" class="avatar brround  me-2">
									<h5 class="time-title text-muted p-0 leading-normal my-auto">Ryan Lyman<i class="si si-check text-success fs-12 ms-1" data-bs-toggle="tooltip" data-bs-placement="top" title="verified"></i></h5>
								</div>
								<div class="my-auto footerimg-r ms-auto">
									<small class="text-muted">55 mins ago</small>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="card mb-0">
						<div class="item-card2-img">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/b1.jpg" alt="img" class="cover-image">
							<div class="tag-text">
								<span class="bg-danger tag-option">For Rent </span>
								<span class="bg-pink tag-option">Hot</span>
							</div>
						</div>
						<div class="item-card2-icons">
							<a href="javascript:void(0);" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
							<a href="javascript:void(0);" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="card-body">
							<div class="item-card2">
								<div class="item-card2-text">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">Office Rooms</h4>
									</a>
									<p class="mb-2"><i class="fa fa-map-marker text-danger me-1"></i> Preston Street
										Wichita , USA </p>
									<h5 class="font-weight-bold mb-3">$25,784 <span class="fs-12  font-weight-normal">Per Month</span></h5>
								</div>
								<ul class="item-card2-list">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											256 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 8 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 4 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 4 Car</a></li>
								</ul>
							</div>
						</div>
						<div class="card-footer">
							<div class="footerimg d-flex mt-0 mb-0">
								<div class="d-flex footerimg-l mb-0">
									<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/8.jpg" alt="image" class="avatar brround  me-2">
									<h5 class="time-title text-muted p-0 leading-normal my-auto">Joan Hunter<i class="si si-check text-success fs-12 ms-1" data-bs-toggle="tooltip" data-bs-placement="top" title="verified"></i></h5>
								</div>
								<div class="my-auto footerimg-r ms-auto">
									<small class="text-muted">2 day ago</small>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="card mb-0">
						<div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
						<div class="item-card2-img">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/v1.jpg" alt="img" class="cover-image">
							<div class="tag-text"><span class="bg-danger tag-option">For Sale </span></div>
						</div>
						<div class="item-card2-icons">
							<a href="javascript:void(0);" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
							<a href="javascript:void(0);" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="card-body">
							<div class="item-card2">
								<div class="item-card2-text">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">Apartments</h4>
									</a>
									<p class="mb-2"><i class="fa fa-map-marker text-danger me-1"></i> Preston Street
										Wichita , USA </p>
									<h5 class="font-weight-bold mb-3">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
								</div>
								<ul class="item-card2-list">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											700 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 20 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 10 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 10 Car</a></li>
								</ul>
							</div>
						</div>
						<div class="card-footer">
							<div class="footerimg d-flex mt-0 mb-0">
								<div class="d-flex footerimg-l mb-0">
									<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/female/19.jpg" alt="image" class="avatar brround  me-2">
									<h5 class="time-title text-muted p-0 leading-normal my-auto">Elizabeth<i class="si si-check text-success fs-12 ms-1" data-bs-toggle="tooltip" data-bs-placement="top" title="verified"></i></h5>
								</div>
								<div class="my-auto footerimg-r ms-auto">
									<small class="text-muted">50 mins ago</small>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item sold-out">
					<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span>
					</div>
					<div class="card mb-0">
						<div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
						<div class="item-card2-img">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/f3.jpg" alt="img" class="cover-image">
							<div class="tag-text">
								<span class="bg-danger tag-option">For Sale </span>
								<span class="bg-pink tag-option">New</span>
							</div>
						</div>
						<div class="item-card2-icons">
							<a href="javascript:void(0);" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
							<a href="javascript:void(0);" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="card-body">
							<div class="item-card2">
								<div class="item-card2-text">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">Duplex House</h4>
									</a>
									<p class="mb-2"><i class="fa fa-map-marker text-danger me-1"></i> Preston Street
										Wichita , USA </p>
									<h5 class="font-weight-bold mb-3">$23,789 <span class="fs-12  font-weight-normal">Per Month</span></h5>
								</div>
								<ul class="item-card2-list">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											300 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
							</div>
						</div>
						<div class="card-footer">
							<div class="footerimg d-flex mt-0 mb-0">
								<div class="d-flex footerimg-l mb-0">
									<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/female/18.jpg" alt="image" class="avatar brround  me-2">
									<h5 class="time-title text-muted p-0 leading-normal my-auto">Boris Nash<i class="si si-check text-success fs-12 ms-1" data-bs-toggle="tooltip" data-bs-placement="top" title="verified"></i></h5>
								</div>
								<div class="my-auto footerimg-r ms-auto">
									<small class="text-muted">12 mins ago</small>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Latest Ads-->

	<!--post section-->
	<section>
		<div class="cover-image sptb bg-background-color" data-bs-image-src="https://www.spruko.com/demo/reallist/Reallist/assets/images/banners/banner4.jpg">
			<div class="content-text mb-0">
				<div class="content-text mb-0">
					<div class="container">
						<div class="text-center text-white ">
							<h1 class="mb-2">Subscribe</h1>
							<p class="fs-16">It is a long established fact that a reader will be distracted by the
								readable.</p>
							<div class="row">
								<div class="col-lg-8 mx-auto d-block">
									<div class="mt-5">
										<div class="input-group sub-input mt-1">
											<input type="text" class="form-control input-lg " placeholder="Enter your Email">
											<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
												Subscribe
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/post section-->

	<!--Featured Ads-->
	<section class="sptb bg-patterns">
		<div class="container">
			<div class="section-title center-block text-center">
				<h2>Live Dharishnam & Event</h2>
				<p class="invisble">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
			</div>
			<div class="row">
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="card">
						<div class="arrow-ribbon bg-primary">For Sale</div>
						<div class="item-card7-imgs">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/b3.jpg" alt="img" class="cover-image">
						</div>
						<div class="item-card7-overlaytext">
							<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-white"> Featured </a>
							<span class="mb-0 fs-13"><i class="fa fa fa-heart-o"></i></span>
						</div>
						<div class="card-body">
							<div class="item-card7-desc">
								<div class="item-card7-text">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">Deluxe Flat</h4>
									</a>
									<p class=""><i class="icon icon-location-pin text-muted me-1"></i> New York, NY
										10012, US </p>
								</div>
								<ul class="item-cards7-ic mb-0">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											300 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
								<h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per
										Month</span></h5>
							</div>
						</div>
						<div class="card-footer">
							<div class="d-flex mb-0">
								<span class="fs-12"><i class="icon icon-event me-2 mt-1"></i>Jul 10 2019 </span>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="" data-bs-toggle="tooltip" data-bs-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="card mb-0">
						<div class="arrow-ribbon bg-purple">For Buy</div>
						<div class="item-card7-imgs">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h3.jpg" alt="img" class="cover-image">
						</div>
						<div class="item-card7-overlaytext">
							<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-white">Featured</a>
							<span class="mb-0 fs-13 active"><i class="fa fa fa-heart"></i></span>
						</div>
						<div class="card-body">
							<div class="item-card7-desc">
								<div class="item-card7-text">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">2BHk Deluxe Flat</h4>
									</a>
									<p class=""><i class="icon icon-location-pin text-muted me-1"></i> New York, NY
										10012, US </p>
								</div>
								<ul class="item-cards7-ic mb-0">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											300 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
								<h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per
										Month</span></h5>
							</div>
						</div>
						<div class="card-footer">
							<div class="d-flex mb-0">
								<span class="fs-12"><i class="icon icon-event me-2 mt-1"></i>Jul 05 2019 </span>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="" data-bs-toggle="tooltip" data-bs-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="card mb-0">
						<div class="arrow-ribbon bg-secondary">For Rent</div>
						<div class="item-card7-imgs">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/v4.jpg" alt="img" class="cover-image">
						</div>
						<div class="item-card7-overlaytext">
							<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-white">Featured</a>
							<span class="mb-0 fs-13"><i class="fa fa fa-heart-o"></i></span>
						</div>
						<div class="card-body">
							<div class="item-card7-desc">
								<div class="item-card7-text">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">House For Sale</h4>
									</a>
									<p class=""><i class="icon icon-location-pin text-muted me-1"></i> New York, NY
										10012, US </p>
								</div>
								<ul class="item-cards7-ic mb-0">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											300 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
								<h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per
										Month</span></h5>
							</div>
						</div>
						<div class="card-footer">
							<div class="d-flex mb-0">
								<span class="fs-12"><i class="icon icon-event me-2 mt-1"></i>June 29 2019 </span>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="" data-bs-toggle="tooltip" data-bs-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="card mb-0">
						<div class="arrow-ribbon bg-primary">For Sale</div>
						<div class="item-card7-imgs">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/f2.jpg" alt="img" class="cover-image">
						</div>
						<div class="item-card7-overlaytext">
							<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-white"> Featured</a>
							<span class="mb-0 fs-13 active"><i class="fa fa fa-heart"></i></span>
						</div>
						<div class="card-body">
							<div class="item-card7-desc">
								<div class="item-card7-text">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">Office For Rent</h4>
									</a>
									<p class=""><i class="icon icon-location-pin text-muted me-1"></i> New York, NY
										10012, US </p>
								</div>
								<ul class="item-cards7-ic mb-0">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											300 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
								<h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per
										Month</span></h5>
							</div>
						</div>
						<div class="card-footer">
							<div class="d-flex mb-0">
								<span class="fs-12"><i class="icon icon-event me-2 mt-1"></i>June 25 2019 </span>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="" data-bs-toggle="tooltip" data-bs-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="card mb-0">
						<div class="arrow-ribbon bg-secondary">For Rent</div>
						<div class="item-card7-imgs">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/j3.jpg" alt="img" class="cover-image">
						</div>
						<div class="item-card7-overlaytext">
							<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-white">Featured</a>
							<span class="mb-0 fs-13"><i class="fa fa fa-heart-o"></i></span>
						</div>
						<div class="card-body">
							<div class="item-card7-desc">
								<div class="item-card7-text ">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">Modern House For Sale</h4>
									</a>
									<p class=""><i class="icon icon-location-pin text-muted me-1"></i> New York, NY
										10012, US </p>
								</div>
								<ul class="item-cards7-ic mb-0">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											300 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
								<h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per
										Month</span></h5>
							</div>
						</div>
						<div class="card-footer">
							<div class="d-flex mb-0">
								<span class="fs-12"><i class="icon icon-event me-2 mt-1"></i>June 19 2019 </span>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="" data-bs-toggle="tooltip" data-bs-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="card mb-0">
						<div class="arrow-ribbon bg-primary">For Sale</div>
						<div class="item-card7-imgs">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h2.jpg" alt="img" class="cover-image">
						</div>
						<div class="item-card7-overlaytext">
							<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-white">Featured</a>
							<span class="mb-0 fs-13"><i class="fa fa fa-heart-o"></i></span>
						</div>
						<div class="card-body">
							<div class="item-card7-desc">
								<div class="item-card7-text ">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">Modern House For Sale</h4>
									</a>
									<p class=""><i class="icon icon-location-pin text-muted me-1"></i> New York, NY
										10012, US </p>
								</div>
								<ul class="item-cards7-ic mb-0">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											300 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
								<h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per
										Month</span></h5>
							</div>
						</div>
						<div class="card-footer">
							<div class="d-flex mb-0">
								<span class="fs-12"><i class="icon icon-event me-2 mt-1"></i>June 19 2019 </span>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="" data-bs-toggle="tooltip" data-bs-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="sold-out1">
						<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold
								Out</span></div>
						<div class="card mb-0">
							<div class="arrow-ribbon bg-purple">For Buy</div>
							<div class="item-card7-imgs">
								<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/f1.jpg" alt="img" class="cover-image">
							</div>
							<div class="item-card7-overlaytext">
								<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-white">Featured</a>
								<span class="mb-0 fs-13"><i class="fa fa fa-heart-o"></i></span>
							</div>
							<div class="card-body">
								<div class="item-card7-desc">
									<div class="item-card7-text ">
										<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
											<h4 class="">Modern House For Sale</h4>
										</a>
										<p class=""><i class="icon icon-location-pin text-muted me-1"></i> New York, NY
											10012, US </p>
									</div>
									<ul class="item-cards7-ic mb-0">
										<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i> 300 Sqft</a></li>
										<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
										<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
										<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
									</ul>
									<h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
								</div>
							</div>
							<div class="card-footer">
								<div class="d-flex mb-0">
									<span class="fs-12"><i class="icon icon-event me-2 mt-1"></i>June 19 2019 </span>
									<div class="ms-auto">
										<a href="javascript:void(0);" class="" data-bs-toggle="tooltip" data-bs-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="card mb-0">
						<div class="arrow-ribbon bg-primary">For Sale</div>
						<div class="item-card7-imgs">
							<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/v3.jpg" alt="img" class="cover-image">
						</div>
						<div class="item-card7-overlaytext">
							<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-white">Featured</a>
							<span class="mb-0 fs-13"><i class="fa fa fa-heart-o"></i></span>
						</div>
						<div class="card-body">
							<div class="item-card7-desc">
								<div class="item-card7-text ">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="">Modern House For Sale</h4>
									</a>
									<p class=""><i class="icon icon-location-pin text-muted me-1"></i> New York, NY
										10012, US </p>
								</div>
								<ul class="item-cards7-ic mb-0">
									<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
											300 Sqft</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
									<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
								</ul>
								<h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per
										Month</span></h5>
							</div>
						</div>
						<div class="card-footer">
							<div class="d-flex mb-0">
								<span class="fs-12"><i class="icon icon-event me-2 mt-1"></i>June 19 2019 </span>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="" data-bs-toggle="tooltip" data-bs-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/Featured Ads-->

	<!--Download -->
	<section class="sptb bg-white">
		<div class="container">
			<div class="section-title center-block text-center">
				<h2>Download</h2>
				<p class="invisble">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="text-center text-wrap">
						<div class="btn-list">
							<a href="javascript:void(0);" class="btn btn-primary btn-lg"><i class="fa fa-apple fa-1x me-2"></i> App Store</a>
							<a href="javascript:void(0);" class="btn btn-secondary btn-lg"><i class="fa fa-android fa-1x me-2"></i> Google Play</a>
							<a href="javascript:void(0);" class="btn btn-info btn-lg"><i class="fa fa-windows fa-1x me-2"></i> Windows</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Download -->

	<!--Location-->
	<section class="sptb">
		<div class="container">
			<div class="section-title center-block text-center">
				<h2>Important Temples & Prayer</h2>
				<p class="invisble">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
			</div>
			<div class="row">
				<div class="col-12 col-md-12 col-lg-12 col-xl-6">
					<div class="row">
						<div class="col-sm-12 col-lg-6 col-md-6 ">
							<div class="item-card overflow-hidden">
								<div class="item-card-desc">
									<div class="card text-center overflow-hidden">
										<div class="card-img">
											<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/germany.jpg" alt="img" class="cover-image">
										</div>
										<div class="item-tags">
											<div class="bg-primary tag-option"><i class="fa fa fa-heart-o me-1"></i> 786
											</div>
										</div>
										<div class="item-card-text">
											<h4 class="mb-0">44,327<span><i class="fa fa-map-marker me-1 text-primary"></i>GERMANY</span>
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-lg-6 col-md-6 ">
							<div class="item-card overflow-hidden">
								<div class="item-card-desc">
									<div class="card text-center overflow-hidden">
										<div class="card-img">
											<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/london.jpg" alt="img" class="cover-image">
										</div>
										<div class="item-tags">
											<div class="bg-secondary tag-option"><i class="fa fa fa-heart-o me-1"></i>
												89</div>
										</div>
										<div class="item-card-text">
											<h4 class="mb-0">52,145<span><i class="fa fa-map-marker me-1 text-primary"></i> LONDON</span>
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-lg-6 col-md-6 ">
							<div class="item-card overflow-hidden">
								<div class="item-card-desc">
									<div class="card text-center overflow-hidden mb-lg-0">
										<div class="card-img">
											<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/austerlia.jpg" alt="img" class="cover-image">
										</div>
										<div class="item-tags">
											<div class="bg-primary tag-option"><i class="fa fa fa-heart-o me-1"></i> 894
											</div>
										</div>
										<div class="item-card-text">
											<h4 class="mb-0">63,263<span><i class="fa fa-map-marker text-primary me-1"></i>AUSTERLIA</span>
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-lg-6 col-md-6 ">
							<div class="item-card overflow-hidden">
								<div class="item-card-desc">
									<div class="card text-center overflow-hidden mb-lg-0">
										<div class="card-img">
											<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/chicago.jpg" alt="img" class="cover-image">
										</div>
										<div class="item-tags">
											<div class="bg-secondary tag-option"><i class="fa fa fa-heart-o me-1"></i>
												123 </div>
										</div>
										<div class="item-card-text">
											<h4 class="mb-0">36,485<span><i class="fa fa-map-marker text-primary me-1"></i>CHICAGO</span>
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-xl-6 col-sm-12">
					<div class="item-card overflow-hidden">
						<div class="item-card-desc">
							<div class="card overflow-hidden mb-0">
								<div class="card-img">
									<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/losangels-1.jpg" alt="img" class="cover-image">
								</div>
								<div class="item-tags">
									<div class="bg-primary tag-option"><i class="fa fa fa-heart-o me-1"></i> 567 </div>
								</div>
								<div class="item-card-text">
									<h4 class="mb-0">64,825<span><i class="fa fa-map-marker text-primary me-1"></i>WASHINGTON</span></h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/Location-->

	<!-- News -->
	<section class="sptb bg-white">
		<div class="container">
			<div class="section-title center-block text-center">
				<h2>Recent Posts</h2>
				<p class="invisble">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
			</div>
			<div id="defaultCarousel" class="owl-carousel Card-owlcarousel owl-carousel-icons">
				<div class="item">
					<div class="card mb-0">
						<div class="item7-card-img">
							<a href="blog-details.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/ed1.jpg" alt="img" class="cover-image">
							<div class="item7-card-text">
								<span class="badge badge-info">Farm House</span>
							</div>
						</div>
						<div class="card-body p-4">
							<div class="item7-card-desc d-flex mb-2">
								<a href="javascript:void(0);" class="text-muted"><i class="fa fa-calendar-o text-muted me-2"></i>July-03-2019</a>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="text-muted"><i class="fa fa-comment-o text-muted me-2"></i>4 Comments</a>
								</div>
							</div>
							<a href="blog-details.html" class="text-dark">
								<h4 class="fs-20">Luxury Flat For Rent</h4>
							</a>
							<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
								commodo consequat </p>
							<div class="d-flex align-items-center pt-2 mt-auto">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/5.jpg" class="avatar brround avatar-md me-3" alt="avatar-img">
								<div>
									<a href="user<?= base_url() ?>vr_tour/profile" class="text-default">Joanne Nash</a>
									<small class="d-block text-muted">1 day ago</small>
								</div>
								<div class="ms-auto text-muted">
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fe fe-heart me-1"></i></a>
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fa fa-thumbs-o-up"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="card mb-0">
						<div class="item7-card-img">
							<a href="blog-details.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/j2.jpg" alt="img" class="cover-image">
							<div class="item7-card-text">
								<span class="badge badge-primary">Luxury Room</span>
							</div>
						</div>
						<div class="card-body p-4">
							<div class="item7-card-desc d-flex mb-2">
								<a href="javascript:void(0);" class="text-muted"><i class="fa fa-calendar-o text-muted me-2"></i>June-03-2019</a>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="text-muted"><i class="fa fa-comment-o text-muted me-2"></i>2 Comments</a>
								</div>
							</div>
							<a href="blog-details.html" class="text-dark">
								<h4 class="fs-20">Deluxe Flat For Sale</h4>
							</a>
							<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
								commodo consequat </p>
							<div class="d-flex align-items-center pt-2 mt-auto">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/7.jpg" class="avatar brround avatar-md me-3" alt="avatar-img">
								<div>
									<a href="user<?= base_url() ?>vr_tour/profile" class="text-default">Tanner Mallari</a>
									<small class="d-block text-muted">2 days ago</small>
								</div>
								<div class="ms-auto text-muted">
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fe fe-heart me-1"></i></a>
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fa fa-thumbs-o-up"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="card mb-0">
						<div class="item7-card-img">
							<a href="blog-details.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/ed2.jpg" alt="img" class="cover-image">
							<div class="item7-card-text">
								<span class="badge badge-success">2BHK Flat</span>
							</div>
						</div>
						<div class="card-body p-4">
							<div class="item7-card-desc d-flex mb-2">
								<a href="javascript:void(0);" class="text-muted"><i class="fa fa-calendar-o text-muted me-2"></i>June-19-2019</a>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="text-muted"><i class="fa fa-comment-o text-muted me-2"></i>8 Comments</a>
								</div>
							</div>
							<a href="blog-details.html" class="text-dark">
								<h4 class="fs-20">Luxury Home For Sale</h4>
							</a>
							<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
								commodo consequat </p>
							<div class="d-flex align-items-center pt-2 mt-auto">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/female/15.jpg" class="avatar brround avatar-md me-3" alt="avatar-img">
								<div>
									<a href="user<?= base_url() ?>vr_tour/profile" class="text-default">Aracely Bashore</a>
									<small class="d-block text-muted">5 days ago</small>
								</div>
								<div class="ms-auto text-muted">
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fe fe-heart me-1"></i></a>
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fa fa-thumbs-o-up"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="card mb-0">
						<div class="item7-card-img">
							<a href="blog-details.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h1.jpg" alt="img" class="cover-image">
							<div class="item7-card-text">
								<span class="badge badge-primary">Duplex House</span>
							</div>
						</div>
						<div class="card-body p-4">
							<div class="item7-card-desc d-flex mb-2">
								<a href="javascript:void(0);" class="text-muted"><i class="fa fa-calendar-o text-muted me-2"></i>June-03-2019</a>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="text-muted"><i class="fa fa-comment-o text-muted me-2"></i>4 Comments</a>
								</div>
							</div>
							<a href="blog-details.html" class="text-dark">
								<h4 class="font-weight-semibold">Luxury Flat For Rent</h4>
							</a>
							<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
								commodo consequat </p>
							<div class="d-flex align-items-center pt-2 mt-auto">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/15.jpg" class="avatar brround avatar-md me-3" alt="avatar-img">
								<div>
									<a href="user<?= base_url() ?>vr_tour/profile" class="text-default">Royal Hamblin</a>
									<small class="d-block text-muted">10 days ago</small>
								</div>
								<div class="ms-auto text-muted">
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fe fe-heart me-1"></i></a>
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fa fa-thumbs-o-up"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="card mb-0">
						<div class="item7-card-img">
							<a href="blog-details.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/j3.jpg" alt="img" class="cover-image">
							<div class="item7-card-text">
								<span class="badge badge-pink">Budget House</span>
							</div>
						</div>
						<div class="card-body p-4">
							<div class="item7-card-desc d-flex mb-2">
								<a href="javascript:void(0);" class="text-muted"><i class="fa fa-calendar-o text-muted me-2"></i>May-28-2019</a>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="text-muted"><i class="fa fa-comment-o text-muted me-2"></i>2 Comments</a>
								</div>
							</div>
							<a href="blog-details.html" class="text-dark">
								<h4 class="font-weight-semibold">Home For Sale</h4>
							</a>
							<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
								commodo consequat </p>
							<div class="d-flex align-items-center pt-2 mt-auto">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/female/5.jpg" class="avatar brround avatar-md me-3" alt="avatar-img">
								<div>
									<a href="user<?= base_url() ?>vr_tour/profile" class="text-default">Rosita Chatmon</a>
									<small class="d-block text-muted">10 days ago</small>
								</div>
								<div class="ms-auto text-muted">
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fe fe-heart me-1"></i></a>
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fa fa-thumbs-o-up"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="card mb-0">
						<div class="item7-card-img">
							<a href="blog-details.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/ed4.jpg" alt="img" class="cover-image">
							<div class="item7-card-text">
								<span class="badge badge-success">3BHK Flats</span>
							</div>
						</div>
						<div class="card-body p-4">
							<div class="item7-card-desc d-flex mb-2">
								<a href="javascript:void(0);" class="text-muted"><i class="fa fa-calendar-o text-muted me-2"></i>May-19-2019</a>
								<div class="ms-auto">
									<a href="javascript:void(0);" class="text-muted"><i class="fa fa-comment-o text-muted me-2"></i>8 Comments</a>
								</div>
							</div>
							<a href="blog-details.html" class="text-dark">
								<h4 class="font-weight-semibold">Luxury Home For Sale</h4>
							</a>
							<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
								commodo consequat </p>
							<div class="d-flex align-items-center pt-2 mt-auto">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/6.jpg" class="avatar brround avatar-md me-3" alt="avatar-img">
								<div>
									<a href="user<?= base_url() ?>vr_tour/profile" class="text-default">Loyd Nolf</a>
									<small class="d-block text-muted">15 days ago</small>
								</div>
								<div class="ms-auto text-muted">
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fe fe-heart me-1"></i></a>
									<a href="javascript:void(0)" class="icon d-none d-md-inline-block ms-3"><i class="fa fa-thumbs-o-up"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--News -->

	<!--Footer Section-->
	<section class="main-footer">
		<footer class="bg-dark text-white">
			<div class="footer-main">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-12">
							<h6>About</h6>
							<hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
								ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis exercitation ullamco
								laboris </p>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum .</p>
						</div>
						<div class="col-lg-2 col-sm-6">
							<h6>Our Quick Links</h6>
							<hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
							<ul class="list-unstyled mb-0">
								<li><a href="javascript:;">Our Team</a></li>
								<li><a href="javascript:;">Contact US</a></li>
								<li><a href="javascript:;">About</a></li>
								<li><a href="javascript:;">Luxury Rooms</a></li>
								<li><a href="javascript:;">Blog</a></li>
								<li><a href="javascript:;">Terms</a></li>
							</ul>
						</div>

						<div class="col-lg-3 col-sm-6">
							<h6>Contact</h6>
							<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
							<ul class="list-unstyled mb-0">
								<li>
									<a href="javascript:void(0);"><i class="fa fa-home me-3 text-primary"></i> New York,
										NY 10012, US</a>
								</li>
								<li>
									<a href="javascript:void(0);"><i class="fa fa-envelope me-3 text-primary"></i>
										info12323@example.com</a>
								</li>
								<li>
									<a href="javascript:void(0);"><i class="fa fa-phone me-3 text-primary"></i> + 01 234
										567 88</a>
								</li>
								<li>
									<a href="javascript:void(0);"><i class="fa fa-print me-3 text-primary"></i> + 01 234
										567 89</a>
								</li>
							</ul>
							<ul class="list-unstyled list-inline mt-3">
								<li class="list-inline-item">
									<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-facebook bg-facebook"></i>
									</a>
								</li>
								<li class="list-inline-item">
									<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-twitter bg-info"></i>
									</a>
								</li>
								<li class="list-inline-item">
									<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-google-plus bg-danger"></i>
									</a>
								</li>
								<li class="list-inline-item">
									<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-linkedin bg-linkedin"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="col-lg-4 col-md-12">
							<h6>Subscribe</h6>
							<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
							<div class="clearfix"></div>
							<div class="input-group w-100">
								<input type="text" class="form-control br-tl-3  br-bl-3 " placeholder="Email">
								<button type="button" class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button>
							</div>
							<h6 class="mb-0 mt-5">Payments</h6>
							<hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
							<div class="clearfix"></div>
							<ul class="footer-payments">
								<li class="ps-0"><a href="javascript:;"><i class="fa fa-cc-amex text-muted" aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i class="fa fa-cc-visa text-muted" aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i class="fa fa-credit-card-alt text-muted" aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i class="fa fa-cc-mastercard text-muted" aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i class="fa fa-cc-paypal text-muted" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-dark text-white p-0">
				<div class="container">
					<div class="row d-flex">
						<div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center ">
							Copyright © 2022 <a href="javascript:void(0);" class="fs-14 text-primary">Reallist</a>.
							Designed with <i class="fa fa-heart text-danger"></i> by <a href="javascript:void(0);" class="fs-14 text-primary">Spruko</a> All rights reserved.
						</div>
					</div>
				</div>
			</div>
		</footer>
	</section>
	<!--Footer Section-->

	<!-- Back to top -->
	<a href="#top" id="back-to-top"><i class="fa fa-rocket"></i></a>
	<script>
		document.getElementById('res_toggle').addEventListener('click', () => {
			document.getElementById('navul1').classList.toggle('toggleshow')
		})
	</script>
	<!-- JQuery js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery-3.6.0.min.js"></script>

	<!-- Bootstrap js -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/popper.min.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/js/bootstrap.min.js"></script>

	<!--JQuery RealEstaterkline Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery.sparkline.min.js"></script>

	<!-- Circle Progress Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/vendors/circle-progress.min.js"></script>

	<!-- Star Rating Js-->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/rating/jquery.rating-stars.js"></script>

	<!--Owl Carousel js -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.js"></script>

	<!--Horizontal Menu-->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/horizontal-menu/horizontal.js"></script>

	<!--JQuery TouchSwipe js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/jquery.touchSwipe.min.js"></script>

	<!--Select2 js -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.full.min.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/js/select2.js"></script>

	<!--Counters -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/counters/counterup.min.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/plugins/counters/waypoints.min.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/plugins/counters/numeric-counter.js"></script>

	<!-- Cookie js -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/jquery.ihavecookies.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/cookie.js"></script>

	<!--Map js -->
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyCW16SmpzDNLsrP-npQii6_8vBu_EJvEjA"></script>
	<script src="<?= base_url() ?>vr_propertyassets/plugins/map/jquery.axgmap.js"></script>

	<!-- P-scroll bar Js-->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.js"></script>


	<!-- sticky Js-->
	<script src="<?= base_url() ?>vr_propertyassets//js/sticky.js"></script>

	<!-- Swipe Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/swipe.js"></script>

	<!-- Scripts Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/owl-carousel.js"></script>

	<!-- themecolor Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/themeColors.js"></script>

	<!-- Custom Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/custom.js"></script>

	<!-- Custom-switcher Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/custom-switcher.js"></script>

</body>

</html>