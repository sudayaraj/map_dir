<!doctype html>
<html lang="en" dir="ltr">

<head>
	<!-- Meta data -->
	<meta charset="UTF-8">
	<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Reallist- Bootstrap Responsive Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose HTML Template" name="description">
	<meta content="Spruko Technologies Private Limited" name="author">
	<meta name="keywords" content="html template, real estate websites, real estate html template, property websites, premium html templates, real estate company website, bootstrap real estate template, real estate marketplace html template, listing website template, property listing html template, real estate bootstrap template, real estate html5 template, real estate listing template, property template, property dealer website" />

	<!-- Favicon -->
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

	<!-- Title -->
	<title>Reallist- Bootstrap Responsive Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose HTML
		Template</title>

	<!-- Bootstrap Css -->
	<link id="style" href="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/css/bootstrap.min.css" rel="stylesheet" />

	<!-- Dashboard Css -->
	<link href="<?= base_url() ?>vr_propertyassets/css/style.css" rel="stylesheet" />

	<!-- Font-awesome  Css -->
	<link href="<?= base_url() ?>vr_propertyassets/css/icons.css" rel="stylesheet" />

	<!--Select2 Plugin -->
	<link href="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.min.css" rel="stylesheet" />

	<!-- jquery ui RangeSlider -->
	<link href="<?= base_url() ?>vr_propertyassets/plugins/jquery-uislider/jquery-ui.css" rel="stylesheet">

	<!-- P-scroll bar css-->
	<link href="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= base_url() ?>vrmall_assets/css/vr_mall.css">

</head>

<body class="main-body">

	<!--Loader-->
	<div id="global-loader">
		<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/loader.svg" class="loader-img" alt="">
	</div>
	<div class="nav1">
		<div class="container-fluid text-center">
			<div class="navbar">
				<img src="" alt="Logo" class="logo">
				<!-- toggle img start -->
				<img src="<?= base_url() ?>assets/icons/toggle.jpg" style="width:34px;" alt="toggle" class="res_toggle" id="res_toggle">
				<!-- toggle img end -->
				<ul class="navul1 " id="navul1">
					<li class="navli1"><a href="<?= base_url() ?>">Virtual Link</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_mall">Virtual Mall</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_property">Virtual Property</a></li>
					<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Virtual Education</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_studio">VR Studio</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_tour" >VR Tour</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_dharisnam" class="nav_active">VR Dharisanam</a></li>
					<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Buy Sharing</a></li>
					<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">CP Hub</a></li>
					<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Live Stream</a></li>
					<li class="navli1 navrmborder"><a href="#">Contact Us</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!--Topbar-->
	<div class="header-main pt_3">
		<div class="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-xl-8 col-lg-8 col-sm-4 col-7">
						<div class="top-bar-left d-flex">
							<div class="clearfix">
								<ul class="socials">
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
									</li>
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
									</li>
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-linkedin"></i></a>
									</li>
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-google-plus"></i></a>
									</li>
								</ul>
							</div>
							<div class="clearfix">
								<ul class="contact">
									<li class="me-5 d-lg-none">
										<a href="javascript:void(0);" class="callnumber text-dark"><span><i class="fa fa-phone me-1"></i>: +425 345 8765</span></a>
									</li>
									<li class="select-country me-5">
										<select class="form-control select2-flag-search" data-placeholder="Select Country">
											<option value="UM">United States of America</option>
											<option value="AF">Afghanistan</option>
											<option value="AL">Albania</option>
											<option value="AD">Andorra</option>
											<option value="AG">Antigua and Barbuda</option>
											<option value="AU">Australia</option>
											<option value="AM">Armenia</option>
											<option value="AO">Angola</option>
											<option value="AR">Argentina</option>
											<option value="AT">Austria</option>
											<option value="AZ">Azerbaijan</option>
											<option value="BA">Bosnia and Herzegovina</option>
											<option value="BB">Barbados</option>
											<option value="BD">Bangladesh</option>
											<option value="BE">Belgium</option>
											<option value="BF">Burkina Faso</option>
											<option value="BG">Bulgaria</option>
											<option value="BH">Bahrain</option>
											<option value="BJ">Benin</option>
											<option value="BN">Brunei</option>
											<option value="BO">Bolivia</option>
											<option value="BT">Bhutan</option>
											<option value="BY">Belarus</option>
											<option value="CD">Congo</option>
											<option value="CA">Canada</option>
											<option value="CF">Central African Republic</option>
											<option value="CI">Cote d'Ivoire</option>
											<option value="CL">Chile</option>
											<option value="CM">Cameroon</option>
											<option value="CN">China</option>
											<option value="CO">Colombia</option>
											<option value="CU">Cuba</option>
											<option value="CV">Cabo Verde</option>
											<option value="CY">Cyprus</option>
											<option value="DJ">Djibouti</option>
											<option value="DK">Denmark</option>
											<option value="DM">Dominica</option>
											<option value="DO">Dominican Republic</option>
											<option value="EC">Ecuador</option>
											<option value="EE">Estonia</option>
											<option value="ER">Eritrea</option>
											<option value="ET">Ethiopia</option>
											<option value="FI">Finland</option>
											<option value="FJ">Fiji</option>
											<option value="FR">France</option>
											<option value="GA">Gabon</option>
											<option value="GD">Grenada</option>
											<option value="GE">Georgia</option>
											<option value="GH">Ghana</option>
											<option value="GH">Ghana</option>
											<option value="HN">Honduras</option>
											<option value="HT">Haiti</option>
											<option value="HU">Hungary</option>
											<option value="ID">Indonesia</option>
											<option value="IE">Ireland</option>
											<option value="IL">Israel</option>
											<option value="IN">India</option>
											<option value="IQ">Iraq</option>
											<option value="IR">Iran</option>
											<option value="IS">Iceland</option>
											<option value="IT">Italy</option>
											<option value="JM">Jamaica</option>
											<option value="JO">Jordan</option>
											<option value="JP">Japan</option>
											<option value="KE">Kenya</option>
											<option value="KG">Kyrgyzstan</option>
											<option value="KI">Kiribati</option>
											<option value="KW">Kuwait</option>
											<option value="KZ">Kazakhstan</option>
											<option value="LA">Laos</option>
											<option value="LB">Lebanons</option>
											<option value="LI">Liechtenstein</option>
											<option value="LR">Liberia</option>
											<option value="LS">Lesotho</option>
											<option value="LT">Lithuania</option>
											<option value="LU">Luxembourg</option>
											<option value="LV">Latvia</option>
											<option value="LY">Libya</option>
											<option value="MA">Morocco</option>
											<option value="MC">Monaco</option>
											<option value="MD">Moldova</option>
											<option value="ME">Montenegro</option>
											<option value="MG">Madagascar</option>
											<option value="MH">Marshall Islands</option>
											<option value="MK">Macedonia (FYROM)</option>
											<option value="ML">Mali</option>
											<option value="MM">Myanmar (formerly Burma)</option>
											<option value="MN">Mongolia</option>
											<option value="MR">Mauritania</option>
											<option value="MT">Malta</option>
											<option value="MV">Maldives</option>
											<option value="MW">Malawi</option>
											<option value="MX">Mexico</option>
											<option value="MZ">Mozambique</option>
											<option value="NA">Namibia</option>
											<option value="NG">Nigeria</option>
											<option value="NO">Norway</option>
											<option value="NP">Nepal</option>
											<option value="NR">Nauru</option>
											<option value="NZ">New Zealand</option>
											<option value="OM">Oman</option>
											<option value="PA">Panama</option>
											<option value="PF">Paraguay</option>
											<option value="PG">Papua New Guinea</option>
											<option value="PH">Philippines</option>
											<option value="PK">Pakistan</option>
											<option value="PL">Poland</option>
											<option value="QA">Qatar</option>
											<option value="RO">Romania</option>
											<option value="RU">Russia</option>
											<option value="RW">Rwanda</option>
											<option value="SA">Saudi Arabia</option>
											<option value="SB">Solomon Islands</option>
											<option value="SC">Seychelles</option>
											<option value="SD">Sudan</option>
											<option value="SE">Sweden</option>
											<option value="SG">Singapore</option>
											<option value="TG">Togo</option>
											<option value="TH">Thailand</option>
											<option value="TJ">Tajikistan</option>
											<option value="TL">Timor-Leste</option>
											<option value="TM">Turkmenistan</option>
											<option value="TN">Tunisia</option>
											<option value="TO">Tonga</option>
											<option value="TR">Turkey</option>
											<option value="TT">Trinidad and Tobago</option>
											<option value="TW">Taiwan</option>
											<option value="UA">Ukraine</option>
											<option value="UG">Uganda</option>
											<option value="UY">Uruguay</option>
											<option value="UZ">Uzbekistan</option>
											<option value="VA">Vatican City (Holy See)</option>
											<option value="VE">Venezuela</option>
											<option value="VN">Vietnam</option>
											<option value="VU">Vanuatu</option>
											<option value="YE">Yemen</option>
											<option value="ZM">Zambia</option>
											<option value="ZW">Zimbabwe</option>
										</select>
									</li>
									<li class="dropdown me-5">
										<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><span>
												Language <i class="fa fa-caret-down text-muted"></i></span> </a>
										<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
											<a href="javascript:void(0);" class="dropdown-item">
												English
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												Arabic
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												German
											</a>
											<a href="javascript:void(0);" class="dropdown-item">
												Greek
											</a>
											<a href="javascript:void(0);" class="dropdown-item">
												Japanese
											</a>
										</div>
									</li>
									<li class="dropdown">
										<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><span>Currency <i class="fa fa-caret-down text-muted"></i></span></a>
										<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
											<a href="javascript:void(0);" class="dropdown-item">
												USD
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												EUR
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												INR
											</a>
											<a href="javascript:void(0);" class="dropdown-item">
												GBP
											</a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-4 col-sm-8 col-5">
						<div class="top-bar-right">
							<ul class="custom">
								<li>
									<a href="register.html" class="text-dark"><i class="fa fa-user me-1"></i>
										<span>Register</span></a>
								</li>
								<li>
									<a href="login.html" class="text-dark"><i class="fa fa-sign-in me-1"></i>
										<span>Login</span></a>
								</li>
								<li class="dropdown">
									<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><i class="fa fa-home me-1"></i><span> My Dashboard</span></a>
									<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
										<a href="mydash.html" class="dropdown-item">
											<i class="dropdown-icon icon icon-user"></i> My Profile
										</a>
										<a href="ad-list.html" class="dropdown-item">
											<i class="dropdown-icon icon icon-speech"></i> Ads
										</a>
										<a href="settings.html" class="dropdown-item">
											<i class="dropdown-icon icon icon-bell"></i> Notifications
										</a>
										<a href="mydash.html" class="dropdown-item">
											<i class="dropdown-icon  icon icon-settings"></i> Account Settings
										</a>
										<a href="login.html" class="dropdown-item">
											<i class="dropdown-icon icon icon-power"></i> Log out
										</a>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="cover-image sptb-1 pt-0 pb-8 bg-background" data-bs-image-src="https://www.spruko.com/demo/reallist/Reallist/assets/images/banners/banner1.jpg">

			<!-- Duplex Houses Header -->
			<div class="sticky">
				<div class="horizontal-header clearfix ">
					<div class="container">
						<a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
						<span class="smllogo">
							<a href="<?= base_url() ?>vr_dharisnam/index">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo.png" class="mobile-light-logo" width="120" alt="" />
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" class="mobile-dark-logo" width="120" alt="" />
							</a>
						</span>
						<a href="tel:245-6325-3256" class="callusbtn"><i class="fa fa-phone" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
			<!-- /Duplex Houses Header -->

			<div class="horizontal-main bg-dark-transparent clearfix">
				<div class="horizontal-mainwrapper container clearfix">
					<div class="desktoplogo">
						<a href="<?= base_url() ?>vr_dharisnam/index"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<div class="desktoplogo-1">
						<a href="<?= base_url() ?>vr_dharisnam/index"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<!--Nav-->
					<nav class="horizontalMenu clearfix d-md-flex">
					<ul class="horizontalMenu-list">
						<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/">Home </a>

						</li>
						<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/about">About Us </a></li>

						<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/page_list_map2">Pages </a>

						</li>
						<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/blog_list">Blog </a>
						</li>
						<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/col_left" class="">Add details</a>

						</li>
						<li aria-haspopup="true"><a href="<?= base_url() ?>vr_dharisnam/contact"> Contact Us <span class="hmarrow"></span></a></li>
						<li aria-haspopup="true">
								<a  href="<?= base_url() ?>vr_dharisnam/ad_posts">Post Property Ad</a>
							</li>
					</ul>
					
					</nav>
					<!--Nav-->
				</div>
			</div>
		</div>
	</div>

	<!--Map-->
	<div class="d-lg-flex">
		<div class="map-width">
			<div class="axgmap" data-latlng="36.261992,-451.757813" data-zoom="10">
				<div class="axgmap-img" data-latlng="36.261497,-451.756622" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/1.png" data-title="Louvre Museum">
					<h4>House For Sale </h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h1.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 150</a></div>
					<div class="rating-stars block mt-2 mb-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
				<div class="axgmap-img" data-latlng="36.286021,-451.851540" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/2.png" data-title="Louvre Museum">
					<h4>House For Rent</h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h2.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 150</a></div>
					<div class="rating-stars block mt-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
				<div class="axgmap-img" data-latlng="36.341350,-452.018394" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/3.png" data-title="Louvre Museum">
					<h4>House For Sale</h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h3.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 180</a></div>
					<div class="rating-stars block mt-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
				<div class="axgmap-img" data-latlng="36.299857,-451.605034" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/4.png" data-title="Louvre Museum">
					<h4>House For Rent</h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h4.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 15</a></div>
					<div class="rating-stars block mt-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
				<div class="axgmap-img" data-latlng="36.217358,-451.986122" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/5.png" data-title="Louvre Museum">
					<h4>Best Place</h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h1.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 150</a></div>
					<div class="rating-stars block mt-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
				<div class="axgmap-img" data-latlng="36.152742,-451.785965" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/6.png" data-title="Louvre Museum">
					<h4>Office Area</h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h2.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 180</a></div>
					<div class="rating-stars block mt-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
				<div class="axgmap-img" data-latlng="37.347507,-453.718872" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/7.png" data-title="Louvre Museum">
					<h4>House For Sale</h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h3.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 180</a></div>
					<div class="rating-stars block mt-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
				<div class="axgmap-img" data-latlng="35.865961,-453.740845" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/1.png" data-title="Louvre Museum">
					<h4>Best Hotel</h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h4.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 150</a></div>
					<div class="rating-stars block mt-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p><i class="fa fa-phone-square" aria-hidden="true"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
				<div class="axgmap-img" data-latlng="38.630818,-456.394043" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/2.png" data-title="Louvre Museum">
					<h4>House Rent</h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h1.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 150</a></div>
					<div class="rating-stars block mt-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
				<div class="axgmap-img" data-latlng="36.962237,-447.599487" data-marker-image="<?= base_url() ?>vr_propertyassets/images/map/3.png" data-title="Louvre Museum">
					<h4>House For Sale</h4>
					<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h2.png" alt="img" class="w-150 h100 mb-3 mt-2">
					<div>Price: <a class="h4"> $ 150</a></div>
					<div class="rating-stars block mt-2">
						<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
						<div class="rating-stars-container">
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
							<div class="rating-star sm">
								<i class="fa fa-star"></i>
							</div>
						</div>
					</div>
					<p class="fs-16"><i class="fa fa-phone me-2"></i> 456-965-3568</p>
					<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>
			</div>
		</div>
		<div class="map-content-width vscroll card mb-0 br-0 mh-500">
			<div class="p-4">
				<div class="card">
					<div class="card-body p-3">
						<div class="header-text mb-0 map-input-group">
							<div class="container">
								<div class="row">
									<div class="col-xl-12 col-lg-12 col-md-12 d-block mx-auto">
										<div class="bg-transparent">
											<div class="form row no-gutters ">
												<div class="form-group  col-xl-4 col-lg-3 col-md-12 mb-0">
													<input type="text" class="form-control input-lg br-tr-md-0 br-br-md-0" id="text4" placeholder="Find  Property">
												</div>
												<div class="form-group  col-xl-3 col-lg-3 col-md-12 mb-0">
													<input type="text" class="form-control input-lg br-md-0" id="text5" placeholder="Enter Location">
													<span><i class="fa fa-map-marker location-gps me-1"></i> </span>
												</div>
												<div class="form-group col-xl-3 col-lg-3 col-md-12 select2-lg mb-0">
													<select class="form-control select2-show-search  border-bottom-0" data-placeholder="Select Category">
														<optgroup label="Categories">
															<option>Select</option>
															<option value="1">Virtual Reality Meditation</option>
															<option value="2">District Wise Temple list</option>
															<option value="3">Hindu Gods</option>
															<option value="4">Navagraha temple</option>
															<option value="5">Divya Desam</option>
															<option value="6">Siddhar Temples</option>
															<option value="7">Sai Baba Temple</option>
															<option value="9">Iskcon</option>
															<option value="4">Yamadharmaraj and Chitragupta</option>
															<option value="5">Temple News</option>
															<option value="6">Upcoming Temple Event and Functions</option>
															
														</optgroup>
													</select>
												</div>
												<div class="col-xl-2 col-lg-3 col-md-12 mb-0">
													<a href="javascript:void(0);" class="btn btn-lg btn-block btn-primary br-tl-md-0 br-bl-md-0"><i class="fa fa-search"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- /header-text -->
					</div>
				</div>
				<div class="card overflow-hidden">
					<div class="d-md-flex">
						<div class="item-card9-img">
							<div class="arrow-ribbon bg-primary">$263.99</div>
							<div class="item-card9-imgs">
								<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h4.png" alt="img" class="cover-image products-1">
							</div>
							<div class="item-card9-icons">
								<a href="javascript:void(0);" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="item-tags">
								<div class="bg-success tag-option">For Sale </div>
							</div>
							<div class="item-trans-rating">
								<div class="rating-stars block">
									<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
									<div class="rating-stars-container">
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card border-0 mb-0">
							<div class="card-body ">
								<div class="item-card9">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> Villa</a>
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="font-weight-bold mt-1">2BHK flat </h4>
									</a>
									<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem
										ullam corporis suscipit</p>
								</div>
							</div>
							<div class="card-footer pt-4 pb-4">
								<div class="item-card9-footer d-flex">
									<div class="item-card9-cost">
										<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
									</div>
									<div class="ms-auto">
										<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 2 days ago</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card overflow-hidden">
					<div class="d-md-flex">
						<div class="item-card9-img">
							<div class="arrow-ribbon bg-secondary">$987.88</div>
							<div class="item-card9-imgs">
								<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j2.png" alt="img" class="cover-image products-1">
							</div>
							<div class="item-card9-icons">
								<a href="javascript:void(0);" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="item-tags">
								<div class="bg-success tag-option">For Sale </div>
							</div>
							<div class="item-trans-rating">
								<div class="rating-stars block">
									<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="2">
									<div class="rating-stars-container">
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card border-0 mb-0">
							<div class="card-body ">
								<div class="item-card9">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> Deluxe
										House</a>
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="font-weight-bold mt-1">Luxury Home For Sale</h4>
									</a>
									<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem
										ullam corporis suscipit</p>
								</div>
							</div>
							<div class="card-footer pt-4 pb-4">
								<div class="item-card9-footer d-flex">
									<div class="item-card9-cost">
										<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
									</div>
									<div class="ms-auto">
										<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 15 mins ago</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card overflow-hidden">
					<div class="d-md-flex">
						<div class="item-card9-img">
							<div class="arrow-ribbon bg-success">$567</div>
							<div class="item-card9-imgs">
								<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/pe1.png" alt="img" class="cover-image products-1">
							</div>
							<div class="item-card9-icons">
								<a href="javascript:void(0);" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="item-tags">
								<div class="bg-danger tag-option">For Buy </div>
							</div>
							<div class="item-trans-rating">
								<div class="rating-stars block">
									<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
									<div class="rating-stars-container">
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card border-0 mb-0">
							<div class="card-body ">
								<div class="item-card9">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> 3BHK
										Flats</a>
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="font-weight-bold mt-1">Apartment For Rent </h4>
									</a>
									<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem
										ullam corporis suscipit</p>
								</div>
							</div>
							<div class="card-footer pt-4 pb-4">
								<div class="item-card9-footer d-flex">
									<div class="item-card9-cost">
										<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
									</div>
									<div class="ms-auto">
										<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 1 days ago</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card overflow-hidden">
					<div class="d-md-flex">
						<div class="item-card9-img">
							<div class="arrow-ribbon bg-pink">$567</div>
							<div class="item-card9-imgs">
								<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image products-1">
							</div>
							<div class="item-card9-icons">
								<a href="javascript:void(0);" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="item-tags">
								<div class="bg-success tag-option">For Sale </div>
							</div>
							<div class="item-trans-rating">
								<div class="rating-stars block">
									<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
									<div class="rating-stars-container">
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card border-0 mb-0">
							<div class="card-body ">
								<div class="item-card9">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> Office</a>
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="font-weight-bold mt-1"> Office rooms.... </h4>
									</a>
									<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem
										ullam corporis suscipit</p>
								</div>
							</div>
							<div class="card-footer pt-4 pb-4">
								<div class="item-card9-footer d-flex">
									<div class="item-card9-cost">
										<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
									</div>
									<div class="ms-auto">
										<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 35 mins ago</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card overflow-hidden">
					<div class="d-md-flex">
						<div class="item-card9-img">
							<div class="arrow-ribbon bg-primary">$839</div>
							<div class="item-card9-imgs">
								<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image products-1">
							</div>
							<div class="item-card9-icons">
								<a href="javascript:void(0);" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="item-tags">
								<div class="bg-info tag-option">For Rent </div>
							</div>
							<div class="item-trans-rating">
								<div class="rating-stars block">
									<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
									<div class="rating-stars-container">
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card border-0 mb-0">
							<div class="card-body ">
								<div class="item-card9">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> Luxury
										rooms</a>
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="font-weight-bold mt-1">Apartment For Rent</h4>
									</a>
									<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem
										ullam corporis suscipit</p>
								</div>
							</div>
							<div class="card-footer pt-4 pb-4">
								<div class="item-card9-footer d-flex">
									<div class="item-card9-cost">
										<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
									</div>
									<div class="ms-auto">
										<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 5 days ago</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card overflow-hidden mb-0">
					<div class="d-md-flex">
						<div class="item-card9-img">
							<div class="arrow-ribbon bg-secondary">$289</div>
							<div class="item-card9-imgs">
								<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="cover-image products-1">
							</div>
							<div class="item-card9-icons">
								<a href="javascript:void(0);" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="item-tags">
								<div class="bg-success tag-option">For Sale </div>
							</div>
							<div class="item-trans-rating">
								<div class="rating-stars block">
									<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
									<div class="rating-stars-container">
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm  ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
										<div class="rating-star sm ">
											<i class="fa fa-star"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card border-0 mb-0">
							<div class="card-body ">
								<div class="item-card9">
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i>
										Apartments</a>
									<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
										<h4 class="font-weight-bold mt-1">Apartment For Rent </h4>
									</a>
									<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem
										ullam corporis suscipit</p>
								</div>
							</div>
							<div class="card-footer pt-4 pb-4">
								<div class="item-card9-footer d-flex">
									<div class="item-card9-cost">
										<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
									</div>
									<div class="ms-auto">
										<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 3 days ago</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/Map-->

	<!--Sliders Section-->

	<!--/Sliders Section-->

	<!--Breadcrumb-->
	<div class="border-bottom bg-white">
		<div class="container">
			<div class="page-header bg-transparent">
				<h4 class="page-title">RealEstate list</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0);">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">RealEstate list</li>
				</ol>
			</div>
		</div>
	</div>
	<!--/Breadcrumb-->

	<!--Add listing-->
	<section class="sptb">
		<div class="container">
			<div class="row">
				<div class="col-xl-9 col-lg-8 col-md-12">
					<!--Add lists-->
					<div class=" mb-lg-0">
						<div class="">
							<div class="item2-gl ">
								<div class=" mb-0">
									<div class="">
										<div class="p-5 bg-white item2-gl-nav d-flex border br-5">
											<h6 class="mb-0 mt-2">Showing 1 to 10 of 30 entries</h6>
											<ul class="nav item2-gl-menu ms-auto mt-2">
												<li class=""><a href="#tab-11" class="active show" data-bs-toggle="tab" title="List style"><i class="fa fa-list"></i></a></li>
												<li><a href="#tab-12" data-bs-toggle="tab" class="" title="Grid"><i class="fa fa-th"></i></a></li>
											</ul>
											<div class="d-flex">
												<label class="me-2 mt-1 mb-sm-1 pt-2">Sort By:</label>
												<select name="item" class="form-control select-sm w-75 select2">
													<option value="1">Latest</option>
													<option value="2">Oldest</option>
													<option value="3">Price:Low-to-High</option>
													<option value="5">Price:Hight-to-Low</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-content">
									<div class="tab-pane active" id="tab-11">
										<div class="card overflow-hidden">
											<div class="d-md-flex">
												<div class="item-card9-img">
													<div class="arrow-ribbon bg-primary">$263.99</div>
													<div class="item-card9-imgs">
														<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h4.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-success tag-option">For Sale </div>
														<div class="bg-pink tag-option">Open </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
															<div class="rating-stars-container">
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card border-0 mb-0">
													<div class="card-body ">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Villa</a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
																<h4 class="font-weight-bold mt-1">2BHK flat </h4>
															</a>
															<div class="mb-2">
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i>
																	950 Sqft</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 4
																	Beds</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 3
																	Bath</a>
																<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a>
															</div>
														</div>
													</div>
													<div class="card-footer pt-4 pb-4">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i>
																		Owner</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		2 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card overflow-hidden">
											<div class="d-md-flex">
												<div class="item-card9-img">
													<div class="arrow-ribbon bg-secondary">$987.88</div>
													<div class="item-card9-imgs">
														<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
														<div id="carousel-controls1" class="carousel slide property-slide" data-bs-ride="carousel" data-interval="false">
															<div class="carousel-inner br-0">
																<div class="carousel-item active">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
																<div class="carousel-item">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
																<div class="carousel-item">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
																<div class="carousel-item">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
																<div class="carousel-item">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/e1.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
															</div>
															<a class="carousel-control-prev" href="#carousel-controls1" role="button" data-bs-slide="prev">
																<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																<span class="sr-only">Previous</span>
															</a>
															<a class="carousel-control-next" href="#carousel-controls1" role="button" data-bs-slide="next">
																<span class="carousel-control-next-icon" aria-hidden="true"></span>
																<span class="sr-only">Next</span>
															</a>
														</div>
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-success tag-option">For Sale </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="2">
															<div class="rating-stars-container">
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card border-0 mb-0">
													<div class="card-body ">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> Deluxe House</a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
																<h4 class="font-weight-bold mt-1">Luxury Home For Sale
																</h4>
															</a>
															<div class="mb-2">
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i>
																	950 Sqft</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 4
																	Beds</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 3
																	Bath</a>
																<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a>
															</div>
														</div>
													</div>
													<div class="card-footer pt-4 pb-4">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i>
																		Agent</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		15 mins ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="sold-out1">
											<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span></div>
											<div class="card overflow-hidden">
												<div class="d-md-flex">
													<div class="item-card9-img">
														<div class="arrow-ribbon bg-success">$567</div>
														<div class="item-card9-imgs">
															<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
															<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/pe1.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-danger tag-option">For Buy </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
																<div class="rating-stars-container">
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card border-0 mb-0">
														<div class="card-body ">
															<div class="item-card9">
																<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> 3BHK Flats</a>
																<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
																	<h4 class="font-weight-bold mt-1">Apartment For Rent
																	</h4>
																</a>
																<div class="mb-2">
																	<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i>
																		400 Sqft</a>
																	<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 3
																		Beds</a>
																	<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 3
																		Bath</a>
																	<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1
																		Car</a>
																</div>
															</div>
														</div>
														<div class="card-footer pt-4 pb-4">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																			USA</span></a>
																	<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i>
																			Owner</span></a>
																</div>
																<div class="ms-auto">
																	<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																			1 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card overflow-hidden">
											<div class="d-md-flex">
												<div class="item-card9-img">
													<div class="arrow-ribbon bg-pink">$567</div>
													<div class="item-card9-imgs">
														<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-success tag-option">For Sale </div>
														<div class="bg-pink tag-option">Hot </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
															<div class="rating-stars-container">
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card border-0 mb-0">
													<div class="card-body ">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> Office</a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
																<h4 class="font-weight-bold mt-1"> Office rooms....
																</h4>
															</a>
															<div class="mb-2">
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i>
																	1500 Sqft</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 5
																	Beds</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 3
																	Bath</a>
																<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a>
															</div>
														</div>
													</div>
													<div class="card-footer pt-4 pb-4">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i>
																		Agent</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		35 mins ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card overflow-hidden">
											<div class="d-md-flex">
												<div class="item-card9-img">
													<div class="arrow-ribbon bg-primary">$839</div>
													<div class="item-card9-imgs">
														<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-info tag-option">For Rent </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
															<div class="rating-stars-container">
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card border-0 mb-0">
													<div class="card-body ">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> Luxury rooms</a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
																<h4 class="font-weight-bold mt-1">Apartment For Rent
																</h4>
															</a>
															<div class="mb-2">
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i>
																	300 Sqft</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 2
																	Beds</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 2
																	Bath</a>
																<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1 Car</a>
															</div>
														</div>
													</div>
													<div class="card-footer pt-4 pb-4">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i>
																		Agent</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		5 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card overflow-hidden">
											<div class="d-md-flex">
												<div class="item-card9-img">
													<div class="arrow-ribbon bg-secondary">$289</div>
													<div class="item-card9-imgs">
														<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-success tag-option">For Sale </div>
														<div class="bg-pink tag-option">New</div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
															<div class="rating-stars-container">
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card border-0 mb-0">
													<div class="card-body ">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted"><i class="fa fa-tag me-1"></i> Apartments</a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark">
																<h4 class="font-weight-bold mt-1">Apartment For Rent
																</h4>
															</a>
															<div class="mb-2">
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i>
																	2500 Sqft</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 20
																	Beds</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 15
																	Bath</a>
																<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 10
																	Car</a>
															</div>
														</div>
													</div>
													<div class="card-footer pt-4 pb-4">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i>
																		Owner</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		3 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="tab-12">
										<div class="row">
											<div class="col-lg-6 col-md-12 col-xl-4">
												<div class="card overflow-hidden">
													<div class="item-card9-img">
														<div class="arrow-ribbon bg-primary">$263.99</div>
														<div class="item-card9-imgs">
															<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
															<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h4.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-success tag-option">For Sale </div>
															<div class="bg-pink tag-option">Open </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
																<div class="rating-stars-container">
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Villa</a>
															<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i>
																	Owner</span></a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark mt-2">
																<h4 class="font-weight-bold mt-1">2BHK flat </h4>
															</a>
															<ul class="item-card2-list">
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i>
																		700 Sqft</a></li>
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 5
																		Beds</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 4
																		Bath</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2
																		Car</a></li>
															</ul>
															<p class="mb-0">Ut enim ad minima veniamq nostrum exerci
															</p>
														</div>
													</div>
													<div class="card-footer">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		2 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-12 col-xl-4">
												<div class="card overflow-hidden">
													<div class="arrow-ribbon bg-secondary">$987.88</div>
													<div class="item-card9-img">
														<div class="item-card9-imgs">
															<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
															<div id="carousel-controls" class="carousel slide property-slide" data-bs-ride="carousel" data-interval="false">
																<div class="carousel-inner br-0">
																	<div class="carousel-item active">
																		<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																	<div class="carousel-item">
																		<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																	<div class="carousel-item">
																		<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																	<div class="carousel-item">
																		<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																	<div class="carousel-item">
																		<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/e1.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																</div>
																<a class="carousel-control-prev" href="#carousel-controls" role="button" data-bs-slide="prev">
																	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																	<span class="sr-only">Previous</span>
																</a>
																<a class="carousel-control-next" href="#carousel-controls" role="button" data-bs-slide="next">
																	<span class="carousel-control-next-icon" aria-hidden="true"></span>
																	<span class="sr-only">Next</span>
																</a>
															</div>
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-success tag-option">For Sale </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="2">
																<div class="rating-stars-container">
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Deluxe House</a>
															<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i>
																	Agent</span></a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark mt-2">
																<h4 class="font-weight-bold mt-1">Luxury Home For Sale
																</h4>
															</a>
															<ul class="item-card2-list">
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i>
																		950 Sqft</a></li>
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 4
																		Beds</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 3
																		Bath</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2
																		Car</a></li>
															</ul>
															<p class="mb-0">Ut enim ad minima veniamq nostrum exerci
																ullam</p>
														</div>
													</div>
													<div class="card-footer">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		15 mins ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-12 col-xl-4">
												<div class="sold-out1">
													<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span></div>
													<div class="card overflow-hidden">
														<div class="arrow-ribbon bg-success">$567</div>
														<div class="item-card9-img">
															<div class="item-card9-imgs">
																<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
																<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/pe1.png" alt="img" class="cover-image">
															</div>
															<div class="item-card9-icons">
																<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-danger tag-option">For Buy </div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
																	<div class="rating-stars-container">
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="item-card9">
																<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> 3BHK Flats</a>
																<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i>
																		Owner</span></a>
																<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark mt-2">
																	<h4 class="font-weight-bold mt-1">Apartment For Rent
																	</h4>
																</a>
																<ul class="item-card2-list">
																	<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i>
																			400 Sqft</a></li>
																	<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 3
																			Beds</a></li>
																	<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i>
																			3 Bath</a></li>
																	<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1
																			Car</a></li>
																</ul>
																<p class="mb-0">Ut enim ad minima veniamq nostrum exerci
																</p>
															</div>
														</div>
														<div class="card-footer">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																			USA</span></a>
																</div>
																<div class="ms-auto">
																	<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																			1 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-12 col-xl-4">
												<div class="card overflow-hidden">
													<div class="arrow-ribbon bg-pink">$567</div>
													<div class="item-card9-img">
														<div class="item-card9-imgs">
															<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
															<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-success tag-option">For Sale</div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
																<div class="rating-stars-container">
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Office</a>
															<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i>
																	Agent</span></a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark mt-2">
																<h4 class="font-weight-bold mt-1">Office rooms..</h4>
															</a>
															<ul class="item-card2-list">
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i>
																		1500 Sqft</a></li>
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 5
																		Beds</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 3
																		Bath</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2
																		Car</a></li>
															</ul>
															<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
														</div>
													</div>
													<div class="card-footer">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		35 mins ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-12 col-xl-4">
												<div class="card overflow-hidden">
													<div class="arrow-ribbon bg-primary">$839</div>
													<div class="item-card9-img">
														<div class="item-card9-imgs">
															<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
															<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-info tag-option">For Rent </div>
															<div class="bg-pink tag-option">Hot </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
																<div class="rating-stars-container">
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Luxury rooms</a>
															<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i>
																	Agent</span></a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark mt-2">
																<h4 class="font-weight-bold mt-1">Apartment For Rent
																</h4>
															</a>
															<ul class="item-card2-list">
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i>
																		300 Sqft</a></li>
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 2
																		Beds</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 2
																		Bath</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1
																		Car</a></li>
															</ul>
															<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
														</div>
													</div>
													<div class="card-footer">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		5 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-12 col-xl-4">
												<div class="card overflow-hidden">
													<div class="arrow-ribbon bg-secondary">$289</div>
													<div class="item-card9-img">
														<div class="item-card9-imgs">
															<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
															<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-success tag-option">For Sale </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
																<div class="rating-stars-container">
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Apartments</a>
															<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i>
																	Owner</span></a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark mt-2">
																<h4 class="font-weight-bold mt-1">Apartment For Rent
																</h4>
															</a>
															<ul class="item-card2-list">
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i>
																		2500 Sqft</a></li>
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 20
																		Beds</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 15
																		Bath</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 10
																		Car</a></li>
															</ul>
															<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
														</div>
													</div>
													<div class="card-footer">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		3 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-12 col-xl-4">
												<div class="card overflow-hidden">
													<div class="arrow-ribbon bg-primary">$289</div>
													<div class="item-card9-img">
														<div class="item-card9-imgs">
															<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
															<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j2.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-info tag-option">For Rent </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="2">
																<div class="rating-stars-container">
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> 2BHK House</a>
															<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i>
																	Owner</span></a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark mt-2">
																<h4 class="font-weight-bold mt-1">2BHK house For Rent
																</h4>
															</a>
															<ul class="item-card2-list">
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i>
																		160 Sqft</a></li>
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 2
																		Beds</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 3
																		Bath</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1
																		Car</a></li>
															</ul>
															<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
														</div>
													</div>
													<div class="card-footer">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		2 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-12 col-xl-4">
												<div class="card overflow-hidden">
													<div class="arrow-ribbon bg-secondary">$729</div>
													<div class="item-card9-img">
														<div class="item-card9-imgs">
															<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
															<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h3.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-success tag-option">For Sale </div>
															<div class="bg-pink tag-option">New </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
																<div class="rating-stars-container">
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Duplex House</a>
															<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i>
																	Owner</span></a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark mt-2">
																<h4 class="font-weight-bold mt-1">Duplex House For Sale
																</h4>
															</a>
															<ul class="item-card2-list">
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i>
																		1786 Sqft</a></li>
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 4
																		Beds</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 5
																		Bath</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2
																		Car</a></li>
															</ul>
															<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
														</div>
													</div>
													<div class="card-footer">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		4 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-12 col-xl-4">
												<div class="card overflow-hidden">
													<div class="arrow-ribbon bg-secondary">$389</div>
													<div class="item-card9-img">
														<div class="item-card9-imgs">
															<a href="<?= base_url() ?>vr_dharisnam/col_left"></a>
															<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j1.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-danger tag-option">For Buy </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
																<div class="rating-stars-container">
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body">
														<div class="item-card9">
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Garden House</a>
															<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i>
																	Owner</span></a>
															<a href="<?= base_url() ?>vr_dharisnam/col_left" class="text-dark mt-2">
																<h4 class="font-weight-bold mt-1">Graden House</h4>
															</a>
															<ul class="item-card2-list">
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i>
																		489 Sqft</a></li>
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 5
																		Beds</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 3
																		Bath</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1
																		Car</a></li>
															</ul>
															<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
														</div>
													</div>
													<div class="card-footer">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i>
																		USA</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i>
																		15 mins ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="center-block text-center">
								<ul class="pagination mb-5 mb-lg-0">
									<li class="page-item page-prev disabled">
										<a class="page-link" href="javascript:void(0);" tabindex="-1">Prev</a>
									</li>
									<li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a>
									</li>
									<li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
									<li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
									<li class="page-item page-next">
										<a class="page-link" href="javascript:void(0);">Next</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!--/Add lists-->
				</div>

				<!--Right Side Content-->
				<div class="col-xl-3 col-lg-4 col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="input-group">
								<input type="text" class="form-control br-tl-3  br-bl-3" placeholder="Search">
								<button type="button" class="btn btn-primary br-tr-3  br-br-3">
									Search
								</button>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Categories</h3>
						</div>
						<div class="card-body">
							<div class="" id="container">
								<div class="filter-product-checkboxs">
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
										<span class="custom-control-label">
											<span class="text-dark"> Virtual Reality Meditation<span class="label label-secondary float-end">14</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
										<span class="custom-control-label">
											<span class="text-dark">District Wise Temple list<span class="label label-secondary float-end">22</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox3" value="option3">
										<span class="custom-control-label">
											<span class="text-dark">Hindu Gods<span class="label label-secondary float-end">78</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox4" value="option3">
										<span class="custom-control-label">
											<span class="text-dark">Navagraha temple<span class="label label-secondary float-end">35</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox5" value="option3">
										<span class="custom-control-label">
											<span class="text-dark">Divya Desam<span class="label label-secondary float-end">23</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox6" value="option3">
										<span class="custom-control-label">
											<span class="text-dark">Siddhar Temples<span class="label label-secondary float-end">14</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
										<span class="custom-control-label">
											<span class="text-dark">Sai Baba Temple<span class="label label-secondary float-end">45</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
										<span class="custom-control-label">
											<span class="text-dark">Iskcon<span class="label label-secondary float-end">34</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
										<span class="custom-control-label">
											<span class="text-dark">Yamadharmaraj & <br>
											Chitragupta<span class="label label-secondary float-end">12</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
										<span class="custom-control-label">
											<span class="text-dark">Temple News<span class="label label-secondary float-end">18</span></span>
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
										<span class="custom-control-label">
											<span class="text-dark">Upcoming Temple Event & Functions<span class="label label-secondary float-end">02</span></span>
										</span>
									</label>
								</div>
							</div>
						</div>
						<div class="card-header border-top">
							<h3 class="card-title">Price Range</h3>
						</div>
						<div class="card-body">
							<h6>
								<label for="price">Price Range:</label>
								<input type="text" id="price">
							</h6>
							<div id="mySlider"></div>
						</div>
						<div class="card-header border-top">
							<h3 class="card-title">Condition</h3>
						</div>
						<div class="card-body">
							<div class="filter-product-checkboxs">
								<label class="custom-control custom-checkbox mb-2">
									<input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
									<span class="custom-control-label">
										For Rent
									</span>
								</label>
								<label class="custom-control custom-checkbox mb-2">
									<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
									<span class="custom-control-label">
										For Sale
									</span>
								</label>
								<label class="custom-control custom-checkbox mb-0">
									<input type="checkbox" class="custom-control-input" name="checkbox3" value="option3">
									<span class="custom-control-label">
										For Buy
									</span>
								</label>
							</div>
						</div>
						<div class="card-header border-top">
							<h3 class="card-title">Posted By</h3>
						</div>
						<div class="card-body">
							<div class="filter-product-checkboxs">
								<label class="custom-control custom-checkbox mb-2">
									<input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
									<span class="custom-control-label">
										Owner
									</span>
								</label>
								<label class="custom-control custom-checkbox mb-2">
									<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
									<span class="custom-control-label">
										Agent
									</span>
								</label>
								<label class="custom-control custom-checkbox mb-0">
									<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
									<span class="custom-control-label">
										Builder
									</span>
								</label>
							</div>
						</div>
						<div class="card-footer">
							<a href="javascript:void(0);" class="btn btn-primary btn-block">Apply Filter</a>
						</div>
					</div>
					<div class="card mb-0">
						<div class="card-header">
							<h3 class="card-title">Shares</h3>
						</div>
						<div class="card-body product-filter-desc">
							<div class="product-filter-icons text-center">
								<a href="javascript:void(0);" class="facebook-bg"><i class="fa fa-facebook"></i></a>
								<a href="javascript:void(0);" class="twitter-bg"><i class="fa fa-twitter"></i></a>
								<a href="javascript:void(0);" class="google-bg"><i class="fa fa-google"></i></a>
								<a href="javascript:void(0);" class="dribbble-bg"><i class="fa fa-dribbble"></i></a>
								<a href="javascript:void(0);" class="pinterest-bg"><i class="fa fa-pinterest"></i></a>
							</div>
						</div>
					</div>
				</div>
				<!--/Right Side Content-->
			</div>
		</div>
	</section>
	<!--/Add Listings-->

	<!-- Newsletter-->
	<section class="sptb bg-white border-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-xl-6 col-md-12">
					<div class="sub-newsletter">
						<h3 class="mb-2"><i class="fa fa-paper-plane-o me-2"></i> Subscribe To Our Newsletter</h3>
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
						</p>
					</div>
				</div>
				<div class="col-lg-5 col-xl-6 col-md-12">
					<div class="input-group sub-input mt-1">
						<input type="text" class="form-control input-lg " placeholder="Enter your Email">
						<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
							Subscribe
						</button>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/Newsletter-->

	<!--Footer Section-->
	<section class="main-footer">
		<footer class="bg-dark text-white">
			<div class="footer-main">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-12">
							<h6>About</h6>
							<hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
								ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis exercitation ullamco
								laboris </p>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum .</p>
						</div>
						<div class="col-lg-2 col-sm-6">
							<h6>Our Quick Links</h6>
							<hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
							<ul class="list-unstyled mb-0">
								<li><a href="javascript:;">Our Team</a></li>
								<li><a href="javascript:;">Contact US</a></li>
								<li><a href="javascript:;">About</a></li>
								<li><a href="javascript:;">Luxury Rooms</a></li>
								<li><a href="javascript:;">Blog</a></li>
								<li><a href="javascript:;">Terms</a></li>
							</ul>
						</div>

						<div class="col-lg-3 col-sm-6">
							<h6>Contact</h6>
							<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
							<ul class="list-unstyled mb-0">
								<li>
									<a href="javascript:void(0);"><i class="fa fa-home me-3 text-primary"></i> New York,
										NY 10012, US</a>
								</li>
								<li>
									<a href="javascript:void(0);"><i class="fa fa-envelope me-3 text-primary"></i>
										info12323@example.com</a>
								</li>
								<li>
									<a href="javascript:void(0);"><i class="fa fa-phone me-3 text-primary"></i> + 01 234
										567 88</a>
								</li>
								<li>
									<a href="javascript:void(0);"><i class="fa fa-print me-3 text-primary"></i> + 01 234
										567 89</a>
								</li>
							</ul>
							<ul class="list-unstyled list-inline mt-3">
								<li class="list-inline-item">
									<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-facebook bg-facebook"></i>
									</a>
								</li>
								<li class="list-inline-item">
									<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-twitter bg-info"></i>
									</a>
								</li>
								<li class="list-inline-item">
									<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-google-plus bg-danger"></i>
									</a>
								</li>
								<li class="list-inline-item">
									<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-linkedin bg-linkedin"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="col-lg-4 col-md-12">
							<h6>Subscribe</h6>
							<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
							<div class="clearfix"></div>
							<div class="input-group w-100">
								<input type="text" class="form-control br-tl-3  br-bl-3 " placeholder="Email">
								<button type="button" class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button>
							</div>
							<h6 class="mb-0 mt-5">Payments</h6>
							<hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
							<div class="clearfix"></div>
							<ul class="footer-payments">
								<li class="ps-0"><a href="javascript:;"><i class="fa fa-cc-amex text-muted" aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i class="fa fa-cc-visa text-muted" aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i class="fa fa-credit-card-alt text-muted" aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i class="fa fa-cc-mastercard text-muted" aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i class="fa fa-cc-paypal text-muted" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-dark text-white p-0">
				<div class="container">
					<div class="row d-flex">
						<div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center ">
							Copyright © 2022 <a href="javascript:void(0);" class="fs-14 text-primary">Reallist</a>.
							Designed with <i class="fa fa-heart text-danger"></i> by <a href="javascript:void(0);" class="fs-14 text-primary">Spruko</a> All rights reserved.
						</div>
					</div>
				</div>
			</div>
		</footer>
	</section>
	<!--Footer Section-->

	<!-- Back to top -->
	<a href="#top" id="back-to-top"><i class="fa fa-rocket"></i></a>
	<script>
		document.getElementById('res_toggle').addEventListener('click', () => {
			document.getElementById('navul1').classList.toggle('toggleshow')
		})
	</script>
	<!-- JQuery js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery-3.6.0.min.js"></script>

	<!-- Bootstrap js -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/popper.min.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/js/bootstrap.min.js"></script>

	<!--JQuery RealEstaterkline Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery.sparkline.min.js"></script>

	<!-- Circle Progress Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/vendors/circle-progress.min.js"></script>

	<!-- Star Rating Js-->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/rating/jquery.rating-stars.js"></script>

	<!--Owl Carousel js -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.js"></script>

	<!--Horizontal Menu-->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/horizontal-menu/horizontal.js"></script>

	<!--JQuery TouchSwipe js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/jquery.touchSwipe.min.js"></script>

	<!--Select2 js -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.full.min.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/js/select2.js"></script>

	<!--Map js -->
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyCW16SmpzDNLsrP-npQii6_8vBu_EJvEjA"></script>
	<script src="<?= base_url() ?>vr_propertyassets/plugins/map/jquery.axgmap.js"></script>

	<!-- Cookie js -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/jquery.ihavecookies.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/cookie.js"></script>

	<!-- Ion.RangeSlider -->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/jquery-uislider/jquery-ui.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/js/uislider.js"></script>

	<!-- P-scroll bar Js-->
	<script src="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/mapscroll.js"></script>

	<!-- sticky Js-->
	<script src="<?= base_url() ?>vr_propertyassets//js/sticky.js"></script>

	<!--Showmore Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/jquery.showmore.js"></script>
	<script src="<?= base_url() ?>vr_propertyassets/js/showmore.js"></script>

	<!-- Scripts Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/owl-carousel.js"></script>

	<!-- themecolor Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/themeColors.js"></script>

	<!-- Custom Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/custom.js"></script>

	<!-- Custom-switcher Js-->
	<script src="<?= base_url() ?>vr_propertyassets/js/custom-switcher.js"></script>

</body>

</html>