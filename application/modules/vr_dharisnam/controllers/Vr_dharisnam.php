<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vr_dharisnam extends MY_Controller{

    //keep controller name same as filename
    public function index()
    {
        $this->load->view('index');        
    }

    public function about()
    {
        $this->load->view('about');        
    }

    public function ad_posts()
    {
        $this->load->view('ad-posts');        
    }

    public function contact()
    {
        $this->load->view('contact');        
    }

    public function customerreq()
    {
        $this->load->view('customerreq');        
    }

    public function blog_list()
    {
        $this->load->view('blog-list');        
    }

    public function page_list_map2()
    {
        $this->load->view('page-list-map2');        
    }

    public function col_left()
    {
        $this->load->view('col-left');        
    }
}