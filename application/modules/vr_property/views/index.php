<?php
 $this->load->view('template/header');
 if(!empty($post_data)){
	$category = $post_data['category'];
	$location = $post_data['location'];
	$manual_latitude = $post_data['manual_latitude'];
	$manual_longitude = $post_data['manual_longitude'];
	$type = $post_data['type'];
 }else{
	$category = '';
	$location = '';
	$manual_latitude = '';
	$manual_longitude = '';
	$type = '';
 }
?>
<style>
	.map-absolute {
		bottom: 10px !important;
	}

	.item-search-tabs {
		margin-top: 0rem !important;
	}

	.form-select,
	.form-control {
		font-size: 0.8rem;
		height: 40px !important;
	}
</style>
<!--Map-->

<div class="relative overflow-hidden">
	<?php if (empty($cor)) {$cor = "20.593683,78.962883";}else{$cor;} ?>
	<div class="axgmap" id="set_cord" data-latlng="<?=$cor?>" data-zoom="10">
		<?php
		if ($adposts != false) {
			// echo "<pre>";
			// print_r ($adposts);
			// echo "</pre>";die;				
			foreach ($adposts as $adpost) {
				echo '<div class="axgmap-img" data-latlng="' . $adpost->current_latitude . ',' . $adpost->current_longitude . '" data-marker-image="' . base_url() . 'vr_propertyassets/images/map/1.png" data-title="' . $adpost->product_name . '">
				<h4>' . $adpost->categoryname . '</h4>
				<img src="' . base_url() . "property_adpostimages/" . $adpost->id . "/images/" . $adpost->images . '" alt="img" class="w-150 h100 mb-3 mt-2">
				<div>Price: <a class="h4"> ₹' . $adpost->price . '</a></div>
				<div class="rating-stars block mt-2 mb-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> ' . $adpost->mobile . '</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>';
			}
		}
		?>
	</div>
	<!--Sliders Section-->
	<div class="row p-0">
		<div class="col-xl-8 col-lg-8 col-md-12 d-block mx-auto p-0">
			<div class="map-absolute">
				<div class="py-3 px-3 br-3 bg-gradient-primary">
					<div class="header-text1 mb-0">

						<div class="search-background bg-transparent">
							<div class="row">

								<div class="item-search-tabs">


									<div class="tab-content">
										<div class="tab-pane active" id="tab1">
											<form action="<?= site_url() ?>vr_property/search_ads" method="post">
												<div class="form row align-items-center">
													<div class="form-group  col-lg-4 col-md-12 mb-0 px-2">
														<select class="form-control select2-show-search w-100" name="category" data-placeholder="Select">
															<optgroup label="Categories">
																<option value="0" <?php if($category==0){echo "selected";} ?> selected>Property type</option>
																<option value="0">All</option>
																<option value="1" <?php if($category==1){echo "selected";} ?>>Houses, Apartment Flats, Shops, Commercial Lands</option>
																<option value="2" <?php if($category==2){echo "selected";} ?>>Plots, Agri Lands </option>
																<option value="3" <?php if($category==3){echo "selected";} ?>>Paying Guest, Hostel, Girls Hostel, Manson</option>
																<option value="4" <?php if($category==4){echo "selected";} ?>>Hotel, Resorts, Rooms / Lodge</option>
																<option value="5" <?php if($category==5){echo "selected";} ?>>Party Hall, Trekking</option>
																<option value="6" <?php if($category==6){echo "selected";} ?>>Other Services</option>

															</optgroup>
														</select>
													</div>
													<div class=" col-lg-3 col-md-12 mb-0 px-2">
														<input type="text" class="form-control input-lg br-tr-md-0 br-br-md-0" value="<?=$location?>" name="location" id="location" placeholder="Location">
														<input type="hidden" name="manual_latitude" id="manual_latitude" value="<?=$manual_latitude?>">
														<input type="hidden" name="manual_longitude" id="manual_longitude" value="<?=$manual_longitude?>">
													</div>
													<div class="form-group  col-lg-3 col-md-12 mb-0 px-2">
														<select class="form-control select2-show-search w-100" name="type" data-placeholder="Select">
															<optgroup label="Categories">
																<option value="0" <?php if($type==0){echo "selected";} ?>>Type of Ads</option>
																<option value="0">All</option>
																<option value="1" <?php if($type==1){echo "selected";} ?>>Rent</option>
																<option value="2" <?php if($type==2){echo "selected";} ?>>Buy</option>
																<option value="3" <?php if($type==3){echo "selected";} ?>>Sell</option>
																<option value="3" <?php if($type==4){echo "selected";} ?>>Lease</option>

															</optgroup>
														</select>
													</div>
													<div class=" col-lg-2 col-md-12 mb-0">
														<div class="input-group">

															<button class="btn btn-primary" type="submit">Search
																Now</button>
														</div>
													</div>
												</div>
											</form>
										</div>


									</div>
								</div>

							</div>
						</div>

					</div>
				</div>
			</div><!-- /header-text -->
		</div>
	</div>
	<!--/Sliders Section-->
</div>
<!--/Map-->

<!-- Categories-->
<section class="sptb bg-white">
	<div class="container">
		<div class="section-title center-block text-center">
			<h2>Categories</h2>
			<p class="d-none">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
		</div>
		<div class="row">
			<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
				<div class="card bg-primary text-white mb-xl-0">
					<div class="card-body vr_prop_cat_ht">
						<div class="cat-item text-center">
							<a href="col-left.html"></a>
							<div class="cat-img text-shadow1">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/1.png" alt="img" class="cover-image">
							</div>
							<div class="cat-desc text-white">
								<h5 class="mb-1">Houses, Apartment Flats, Shops, Commercial Lands</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
				<div class="card bg-secondary text-white mb-xl-0">
					<div class="card-body vr_prop_cat_ht">
						<div class="cat-item text-center">
							<a href="col-left.html"></a>
							<div class="cat-img text-shadow1">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/2.png" alt="img" class="cover-image">
							</div>
							<div class="cat-desc text-white">
								<h5 class="mb-1">Plots, Agri Lands </h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
				<div class="card  bg-warning text-white mb-xl-0">
					<div class="card-body vr_prop_cat_ht">
						<div class="cat-item text-center">
							<a href="col-left.html"></a>
							<div class="cat-img text-shadow1">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/3.png" alt="img" class="cover-image">
							</div>
							<div class="cat-desc text-white">
								<h5 class="mb-1">Paying Guest, Hostel, Girls Hostel, Manson</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
				<div class="card bg-success text-white mb-md-0">
					<div class="card-body vr_prop_cat_ht">
						<div class="cat-item text-center">
							<a href="col-left.html"></a>
							<div class="cat-img text-shadow1">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/4.png" alt="img" class="cover-image">
							</div>
							<div class="cat-desc text-white">
								<h5 class="mb-1">Hotel, Resorts, Rooms / Lodge</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
				<div class="card bg-purple text-white mb-sm-0">
					<div class="card-body vr_prop_cat_ht">
						<div class="cat-item text-center">
							<a href="col-left.html"></a>
							<div class="cat-img text-shadow1">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/5.png" alt="img" class="cover-image">
							</div>
							<div class="cat-desc text-white">
								<h5 class="mb-1">Party Hall, Trekking</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
				<div class="card bg-cyan text-white mb-0">
					<div class="card-body vr_prop_cat_ht">
						<div class="cat-item text-center">
							<a href="col-left.html"></a>
							<div class="cat-img text-shadow1">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/categories/6.png" alt="img" class="cover-image">
							</div>
							<div class="cat-desc text-white">
								<h5 class="mb-1">Others services</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--Categories-->

<!--Latest Ads-->
<section class="sptb ">
	<div class="container">
		<div class="section-title center-block text-center">
			<h2>Recent Properties</h2>
			<p class="d-none">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
		</div>
		<div id="myCarousel1" class="owl-carousel owl-carousel-icons2">
			<?php if (!empty($adposts)) {
				foreach ($adposts as $adpostss) { ?>
					<div class="item <?php if ($adpostss->sold == true) {
											echo "sold-out";
										} ?>">
						<?php if ($adpostss->sold == true) { ?>
							<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span>
							</div><?php } ?>
						<div class="card mb-0">
							<div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
							<div class="item-card2-img">
								<a href="col-left.html"></a>
								<img src="<?= base_url() ?>property_adpostimages/<?= $adpostss->id ?>/images/<?= $adpostss->images ?>" alt="img" class="cover-image">
								<div class="tag-text">
									<span class="bg-danger tag-option"><?php if ($adpostss->type == 1) {
																			echo "For Sale";
																		} else if ($adpostss->type == 3) {
																			echo "For Rent";
																		} ?> </span>
									<span class="bg-pink tag-option">Open</span>
								</div>
							</div>
							<div class="item-card2-icons">
								<a href="col-left.html" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
								<a href="javascript:void(0);" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="card-body">
								<div class="item-card2">
									<div class="item-card2-text">
										<a href="col-left.html" class="text-dark">
											<h4 class=""><?= $adpostss->categoryname ?></h4>
										</a>
										<p class="mb-2"><i class="fa fa-map-marker text-danger me-1"></i> <?= $adpostss->city . ',', $adpostss->state ?> </p>
										<h5 class="font-weight-bold mb-3"><?= 'INR ' . $adpostss->price ?> <span class="fs-12  font-weight-normal">Per Month</span></h5>
									</div>
									<ul class="item-card2-list">
										<li><a href="javascript:void(0);"><i class="fa fa-arrows-alt text-muted me-1"></i>
												256 Sqft</a></li>
										<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bed text-muted me-1"></i> 3 Beds</a></li>
										<li><a href="javascript:void(0);" class="icons"><i class="fa fa-bath text-muted me-1"></i> 2 Bath</a></li>
										<li><a href="javascript:void(0);" class="icons"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
									</ul>
								</div>
							</div>
							<div class="card-footer">
								<div class="footerimg d-flex mt-0 mb-0">
									<div class="d-flex footerimg-l mb-0">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/18.jpg" alt="image" class="avatar brround  me-2">
										<h5 class="time-title text-muted p-0 leading-normal my-auto"><?php if ($adpostss->userid == 0) {
																											echo $adpostss->name . " <small>(Guest User)</small>";
																										} else {
																											echo $adpostss->name;
																										} ?><i class="si si-check text-success fs-12 ms-1" data-bs-toggle="tooltip" data-bs-placement="top" title="verified"></i></h5>
									</div>
									<div class="my-auto footerimg-r ms-auto">
										<small class="text-muted">1 day ago</small>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php }
			} ?>

		</div>
	</div>
</section>
<!--Latest Ads-->

<!--post section-->
<section>
	<div class="cover-image sptb bg-background-color" data-bs-image-src="https://www.spruko.com/demo/reallist/Reallist/assets/images/banners/banner4.jpg">
		<div class="content-text mb-0">
			<div class="content-text mb-0">
				<div class="container">
					<div class="text-center text-white ">
						<h1 class="mb-2">Subscribe</h1>
						<p class="fs-16">It is a long established fact that a reader will be distracted by the
							readable.</p>
						<div class="row">
							<div class="col-lg-8 mx-auto d-block">
								<div class="mt-5">
									<div class="input-group sub-input mt-1">
										<input type="text" class="form-control input-lg " placeholder="Enter your Email">
										<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
											Subscribe
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/post section-->



<!--Location-->
<section class="sptb">
	<div class="container">
		<div class="section-title center-block text-center">
			<h2>Top Lisiting Property</h2>
			<p class="d-none">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
		</div>
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-6">
				<div class="row">
					<div class="col-sm-12 col-lg-6 col-md-6 ">
						<div class="item-card overflow-hidden">
							<div class="item-card-desc">
								<div class="card text-center overflow-hidden">
									<div class="card-img">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/germany.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-tags">
										<div class="bg-primary tag-option"><i class="fa fa fa-heart-o me-1"></i> 786
										</div>
									</div>
									<div class="item-card-text">
										<h4 class="mb-0">44,327<span><i class="fa fa-map-marker me-1 text-primary"></i>GERMANY</span>
										</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-lg-6 col-md-6 ">
						<div class="item-card overflow-hidden">
							<div class="item-card-desc">
								<div class="card text-center overflow-hidden">
									<div class="card-img">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/london.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-tags">
										<div class="bg-secondary tag-option"><i class="fa fa fa-heart-o me-1"></i>
											89</div>
									</div>
									<div class="item-card-text">
										<h4 class="mb-0">52,145<span><i class="fa fa-map-marker me-1 text-primary"></i> LONDON</span>
										</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-lg-6 col-md-6 ">
						<div class="item-card overflow-hidden">
							<div class="item-card-desc">
								<div class="card text-center overflow-hidden mb-lg-0">
									<div class="card-img">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/austerlia.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-tags">
										<div class="bg-primary tag-option"><i class="fa fa fa-heart-o me-1"></i> 894
										</div>
									</div>
									<div class="item-card-text">
										<h4 class="mb-0">63,263<span><i class="fa fa-map-marker text-primary me-1"></i>AUSTERLIA</span>
										</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-lg-6 col-md-6 ">
						<div class="item-card overflow-hidden">
							<div class="item-card-desc">
								<div class="card text-center overflow-hidden mb-lg-0">
									<div class="card-img">
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/chicago.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-tags">
										<div class="bg-secondary tag-option"><i class="fa fa fa-heart-o me-1"></i>
											123 </div>
									</div>
									<div class="item-card-text">
										<h4 class="mb-0">36,485<span><i class="fa fa-map-marker text-primary me-1"></i>CHICAGO</span>
										</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-xl-6 col-sm-12">
				<div class="item-card overflow-hidden">
					<div class="item-card-desc">
						<div class="card overflow-hidden mb-0">
							<div class="card-img">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/locations/losangels-1.jpg" alt="img" class="cover-image">
							</div>
							<div class="item-tags">
								<div class="bg-primary tag-option"><i class="fa fa fa-heart-o me-1"></i> 567 </div>
							</div>
							<div class="item-card-text">
								<h4 class="mb-0">64,825<span><i class="fa fa-map-marker text-primary me-1"></i>WASHINGTON</span></h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/Location-->



<?php $this->load->view('template/footer');
?>

<script>
	// $(document).ready(function(){
	// 	var siteUrl = "<?= site_url() ?>";
	// 	$.ajax({
	// 		url: siteUrl+'vr_property/ajax_fetchmapdata',
	// 		type:'GET',
	// 		success: function(data){
	// 			if(data.result!=null){
	// 				$('.axgmap').append(data.result);
	// 				$('.axgmap').data('latlng',currentLatitude+','+ currentLongitude);
	// 			}
	// 		}
	// 	})
	// })
</script>