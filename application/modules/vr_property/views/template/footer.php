<!-- Newsletter-->
<section class="sptb bg-white border-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-xl-6 col-md-12">
				<div class="sub-newsletter">
					<h3 class="mb-2"><i class="fa fa-paper-plane-o me-2"></i> Subscribe To Our Newsletter</h3>
					<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
				</div>
			</div>
			<div class="col-lg-5 col-xl-6 col-md-12">
				<div class="input-group sub-input mt-1">
					<input type="text" class="form-control input-lg " placeholder="Enter your Email">
					<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
						Subscribe
					</button>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/Newsletter-->

<!--Footer Section-->
<section class="main-footer">
	<footer class="bg-dark text-white">
		<div class="footer-main">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-12">
						<h6>About</h6>
						<hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
							ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis exercitation ullamco
							laboris </p>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum .</p>
					</div>
					<div class="col-lg-2 col-sm-6">
						<h6>Our Quick Links</h6>
						<hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<ul class="list-unstyled mb-0">
							<li><a href="javascript:;">Our Team</a></li>
							<li><a href="javascript:;">Contact US</a></li>
							<li><a href="javascript:;">About</a></li>
							<li><a href="javascript:;">Luxury Rooms</a></li>
							<li><a href="javascript:;">Blog</a></li>
							<li><a href="javascript:;">Terms</a></li>
						</ul>
					</div>

					<div class="col-lg-3 col-sm-6">
						<h6>Contact</h6>
						<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<ul class="list-unstyled mb-0">
							<li>
								<a href="javascript:void(0);"><i class="fa fa-home me-3 text-primary"></i> New York,
									NY 10012, US</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-envelope me-3 text-primary"></i>
									info12323@example.com</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-phone me-3 text-primary"></i> + 01 234
									567 88</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-print me-3 text-primary"></i> + 01 234
									567 89</a>
							</li>
						</ul>
						<ul class="list-unstyled list-inline mt-3">
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-facebook bg-facebook"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-twitter bg-info"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-google-plus bg-danger"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-linkedin bg-linkedin"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-12">
						<h6>Subscribe</h6>
						<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<div class="clearfix"></div>
						<div class="input-group w-100">
							<input type="text" class="form-control br-tl-3  br-bl-3 " placeholder="Email">
							<button type="button" class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button>
						</div>
						<h6 class="mb-0 mt-5">Payments</h6>
						<hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
						<div class="clearfix"></div>
						<ul class="footer-payments">
							<li class="ps-0"><a href="javascript:;"><i class="fa fa-cc-amex text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-visa text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-credit-card-alt text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-mastercard text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-paypal text-muted" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-dark text-white p-0">
			<div class="container">
				<div class="row d-flex">
					<div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center ">
						Copyright © 2023 <a href="javascript:void(0);" class="fs-14 text-primary">Reallist</a>.
						Developed with <i class="fa fa-heart text-danger"></i> by <a href="javascript:void(0);" class="fs-14 text-primary">Udhay</a> All rights reserved.
					</div>
				</div>
			</div>
		</div>
	</footer>
</section>
<!--Footer Section-->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-rocket"></i></a>
<script>
	document.getElementById('res_toggle').addEventListener('click', () => {
		document.getElementById('navul1').classList.toggle('toggleshow')
	})
</script>
<!-- JQuery js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery-3.6.0.min.js"></script>

<!-- Bootstrap js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/popper.min.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/js/bootstrap.min.js"></script>

<!--JQuery RealEstaterkline Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery.sparkline.min.js"></script>

<!-- Circle Progress Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/circle-progress.min.js"></script>

<!-- Star Rating Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/rating/jquery.rating-stars.js"></script>

<!--Owl Carousel js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.js"></script>

<!--Horizontal Menu-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/horizontal-menu/horizontal.js"></script>

<!--JQuery TouchSwipe js-->
<script src="<?= base_url() ?>vr_propertyassets/js/jquery.touchSwipe.min.js"></script>

<!--Select2 js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.full.min.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/js/select2.js"></script>

<!--Counters -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/counters/counterup.min.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/counters/waypoints.min.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/counters/numeric-counter.js"></script>

<!-- Cookie js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/jquery.ihavecookies.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/cookie.js"></script>

<!--Map js -->

<!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyAJXHqnGy3iKO5P7veP9_vo51A6zuP4ZiE"></script> -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAJXHqnGy3iKO5P7veP9_vo51A6zuP4ZiE&libraries=places"></script>

<script>
	// window.onload = getlivelocate;

	function initMap() {
		if ("geolocation" in navigator) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var currentLatitude = position.coords.latitude;
				var currentLongitude = position.coords.longitude;

				var infoWindowHTML = "Latitude: " + currentLatitude + "<br>Longitude: " + currentLongitude;
				// var infoWindow = new google.maps.InfoWindow({
				// 	map: map,
				// 	content: infoWindowHTML
				// });
				// var currentLocation = {
				// 	lat: currentLatitude,
				// 	lng: currentLongitude
				// };
				// infoWindow.setPosition(currentLocation);
				// getmap(currentLatitude, currentLongitude);
				// console.log(currentLatitude+','+ currentLongitude);
				$('.axgmap').attr('data-latlng', currentLatitude + ', ' + currentLongitude);
				// console.warn('currentLatitude');
				// console.warn(currentLatitude);
			});
		}
	}
</script>

<script>
	var geocoder;
	geocoder = new google.maps.Geocoder();

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			const latitude = position.coords.latitude;
			const longitude = position.coords.longitude;
			$('#set_cord').attr('data-latlng') = latitude+','+longitude;
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};

			// Use the latitude and longitude values as needed
			$('#manual_latitude').val(latitude);
			$('#manual_longitude').val(longitude);
			geocodeLatLng(pos);
		});
	} else {
		console.error("Geolocation is not supported by this browser.");
	}

	function geocodeLatLng(pos) {
		geocoder.geocode({
			'location': pos
		}, function(results, status) {
			if (status === 'OK') {
				if (results[0]) {
					var addressComponents = results[0].address_components;
					var city, state, country;

					for (var i = 0; i < addressComponents.length; i++) {
						var types = addressComponents[i].types;
						if (types.includes('locality')) {
							city = addressComponents[i].long_name;
						} else if (types.includes('administrative_area_level_1')) {
							state = addressComponents[i].long_name;
						} else if (types.includes('country')) {
							country = addressComponents[i].long_name;
						}
					}

					document.getElementById('location').value = city + ', ' + state + ', ' + country;
				} else {
					document.getElementById('location').value = '';
				}
			} else {
				document.getElementById('location').value = '';
				// document.getElementById('location').value = 'Geocoding failed due to: ' + status;
			}
		});
	}
</script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/map/jquery.axgmap.js"></script>

<!-- P-scroll bar Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.js"></script>


<!-- sticky Js-->
<script src="<?= base_url() ?>vr_propertyassets//js/sticky.js"></script>

<!-- Swipe Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/swipe.js"></script>

<!-- Scripts Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/owl-carousel.js"></script>

<!-- themecolor Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/themeColors.js"></script>

<!-- Custom Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/custom.js"></script>

<!-- Custom-switcher Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/custom-switcher.js"></script>


<script>
	$(document).ready(function() {
		var autocomplete;
		var id = 'location';

		autocomplete = new google.maps.places.Autocomplete((document.getElementById(id)), {
			types: ['geocode'],
		})

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			$("#manual_latitude").val(place.geometry.location.lat());
			$("#manual_longitude").val(place.geometry.location.lng());
		})
	});
</script>
</body>

</html>