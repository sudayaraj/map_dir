<!doctype html>
<html lang="en" dir="ltr">

<head>
	<!-- Meta data -->
	<meta charset="UTF-8">
	<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Reallist- Bootstrap Responsive Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose HTML Template" name="description">
	<meta content="Spruko Technologies Private Limited" name="author">
	<meta name="keywords" content="html template, real estate websites, real estate html template, property websites, premium html templates, real estate company website, bootstrap real estate template, real estate marketplace html template, listing website template, property listing html template, real estate bootstrap template, real estate html5 template, real estate listing template, property template, property dealer website" />

	<!-- Favicon -->
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

	<!-- Title -->
	<title>Reallist- Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose</title>

	<!-- Bootstrap Css -->
	<link id="style" href="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/css/bootstrap.min.css" rel="stylesheet" />

	<!-- Dashboard Css -->
	<link href="<?= base_url() ?>vr_propertyassets/css/style.css" rel="stylesheet" />

	<!-- Font-awesome  Css -->
	<link href="<?= base_url() ?>vr_propertyassets/css/icons.css" rel="stylesheet" />

	<!--Select2 Plugin -->
	<link href="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.min.css" rel="stylesheet" />

	<!-- Owl Theme css-->
	<link href="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" />

	<!-- P-scroll bar css-->
	<link href="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= base_url() ?>vrmall_assets/css/vr_mall.css">
	<style>
		.vr_prop_cat_ht {
			height: 220px;
		}
	</style>
</head>

<body class="main-body">

	<!--Loader-->
	<div id="global-loader">
		<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/loader.svg" class="loader-img" alt="">
	</div>
	<div class="nav1">
		<div class="container-fluid text-center">
			<div class="navbar">
				<img src="" alt="Logo" class="logo">
				<!-- toggle img start -->
				<img src="<?= base_url() ?>assets/icons/toggle.jpg" style="width:34px;" alt="toggle" class="res_toggle" id="res_toggle">
				<!-- toggle img end -->
				<ul class="navul1 " id="navul1">
					<li class="navli1"><a href="<?= base_url() ?>">Virtual Link</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_mall">Virtual Mall</a></li>
					<li class="navli1"><a href="#" class="nav_active">Virtual Property</a></li>
					<li class="navli1"><a href="<?= base_url() ?>comingsoon/" target="_blank">Virtual Education</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_studio">VR Studio</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_tour">VR Tour</a></li>
					<li class="navli1"><a href="<?= base_url() ?>vr_dharisnam">VR Dharisanam</a></li>
					<li class="navli1"><a href="<?= base_url() ?>comingsoon/" target="_blank">Buy Sharing</a></li>
					<li class="navli1"><a href="<?= base_url() ?>comingsoon/" target="_blank">CP Hub</a></li>
					<li class="navli1"><a href="<?= base_url() ?>comingsoon/" target="_blank">Live Stream</a></li>
					<li class="navli1 navrmborder"><a href="#">Contact Us</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!--Topbar-->
	<div class="header-main pt_3">
		<div class="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-xl-8 col-lg-8 col-sm-4 col-7">
						<div class="top-bar-left d-flex">
							<div class="clearfix">
								<ul class="socials">
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
									</li>
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
									</li>
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-linkedin"></i></a>
									</li>
									<li>
										<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-google-plus"></i></a>
									</li>
								</ul>
							</div>
							<div class="clearfix d-none">
								<ul class="contact">
									<li class="me-5 d-lg-none">
										<a href="javascript:void(0);" class="callnumber text-dark"><span><i class="fa fa-phone me-1"></i>: +425 345 8765</span></a>
									</li>
									<li class="select-country me-5">
										<select class="form-control select2-flag-search" data-placeholder="Select Country">
											<option value="UM">United States of America</option>
											<option value="AF">Afghanistan</option>
											<option value="AL">Albania</option>
											<option value="AD">Andorra</option>
											<option value="AG">Antigua and Barbuda</option>
											<option value="AU">Australia</option>
											<option value="AM">Armenia</option>
											<option value="AO">Angola</option>
											<option value="AR">Argentina</option>
											<option value="AT">Austria</option>
											<option value="AZ">Azerbaijan</option>
											<option value="BA">Bosnia and Herzegovina</option>
											<option value="BB">Barbados</option>
											<option value="BD">Bangladesh</option>
											<option value="BE">Belgium</option>
											<option value="BF">Burkina Faso</option>
											<option value="BG">Bulgaria</option>
											<option value="BH">Bahrain</option>
											<option value="BJ">Benin</option>
											<option value="BN">Brunei</option>
											<option value="BO">Bolivia</option>
											<option value="BT">Bhutan</option>
											<option value="BY">Belarus</option>
											<option value="CD">Congo</option>
											<option value="CA">Canada</option>
											<option value="CF">Central African Republic</option>
											<option value="CI">Cote d'Ivoire</option>
											<option value="CL">Chile</option>
											<option value="CM">Cameroon</option>
											<option value="CN">China</option>
											<option value="CO">Colombia</option>
											<option value="CU">Cuba</option>
											<option value="CV">Cabo Verde</option>
											<option value="CY">Cyprus</option>
											<option value="DJ">Djibouti</option>
											<option value="DK">Denmark</option>
											<option value="DM">Dominica</option>
											<option value="DO">Dominican Republic</option>
											<option value="EC">Ecuador</option>
											<option value="EE">Estonia</option>
											<option value="ER">Eritrea</option>
											<option value="ET">Ethiopia</option>
											<option value="FI">Finland</option>
											<option value="FJ">Fiji</option>
											<option value="FR">France</option>
											<option value="GA">Gabon</option>
											<option value="GD">Grenada</option>
											<option value="GE">Georgia</option>
											<option value="GH">Ghana</option>
											<option value="GH">Ghana</option>
											<option value="HN">Honduras</option>
											<option value="HT">Haiti</option>
											<option value="HU">Hungary</option>
											<option value="ID">Indonesia</option>
											<option value="IE">Ireland</option>
											<option value="IL">Israel</option>
											<option value="IN">India</option>
											<option value="IQ">Iraq</option>
											<option value="IR">Iran</option>
											<option value="IS">Iceland</option>
											<option value="IT">Italy</option>
											<option value="JM">Jamaica</option>
											<option value="JO">Jordan</option>
											<option value="JP">Japan</option>
											<option value="KE">Kenya</option>
											<option value="KG">Kyrgyzstan</option>
											<option value="KI">Kiribati</option>
											<option value="KW">Kuwait</option>
											<option value="KZ">Kazakhstan</option>
											<option value="LA">Laos</option>
											<option value="LB">Lebanons</option>
											<option value="LI">Liechtenstein</option>
											<option value="LR">Liberia</option>
											<option value="LS">Lesotho</option>
											<option value="LT">Lithuania</option>
											<option value="LU">Luxembourg</option>
											<option value="LV">Latvia</option>
											<option value="LY">Libya</option>
											<option value="MA">Morocco</option>
											<option value="MC">Monaco</option>
											<option value="MD">Moldova</option>
											<option value="ME">Montenegro</option>
											<option value="MG">Madagascar</option>
											<option value="MH">Marshall Islands</option>
											<option value="MK">Macedonia (FYROM)</option>
											<option value="ML">Mali</option>
											<option value="MM">Myanmar (formerly Burma)</option>
											<option value="MN">Mongolia</option>
											<option value="MR">Mauritania</option>
											<option value="MT">Malta</option>
											<option value="MV">Maldives</option>
											<option value="MW">Malawi</option>
											<option value="MX">Mexico</option>
											<option value="MZ">Mozambique</option>
											<option value="NA">Namibia</option>
											<option value="NG">Nigeria</option>
											<option value="NO">Norway</option>
											<option value="NP">Nepal</option>
											<option value="NR">Nauru</option>
											<option value="NZ">New Zealand</option>
											<option value="OM">Oman</option>
											<option value="PA">Panama</option>
											<option value="PF">Paraguay</option>
											<option value="PG">Papua New Guinea</option>
											<option value="PH">Philippines</option>
											<option value="PK">Pakistan</option>
											<option value="PL">Poland</option>
											<option value="QA">Qatar</option>
											<option value="RO">Romania</option>
											<option value="RU">Russia</option>
											<option value="RW">Rwanda</option>
											<option value="SA">Saudi Arabia</option>
											<option value="SB">Solomon Islands</option>
											<option value="SC">Seychelles</option>
											<option value="SD">Sudan</option>
											<option value="SE">Sweden</option>
											<option value="SG">Singapore</option>
											<option value="TG">Togo</option>
											<option value="TH">Thailand</option>
											<option value="TJ">Tajikistan</option>
											<option value="TL">Timor-Leste</option>
											<option value="TM">Turkmenistan</option>
											<option value="TN">Tunisia</option>
											<option value="TO">Tonga</option>
											<option value="TR">Turkey</option>
											<option value="TT">Trinidad and Tobago</option>
											<option value="TW">Taiwan</option>
											<option value="UA">Ukraine</option>
											<option value="UG">Uganda</option>
											<option value="UY">Uruguay</option>
											<option value="UZ">Uzbekistan</option>
											<option value="VA">Vatican City (Holy See)</option>
											<option value="VE">Venezuela</option>
											<option value="VN">Vietnam</option>
											<option value="VU">Vanuatu</option>
											<option value="YE">Yemen</option>
											<option value="ZM">Zambia</option>
											<option value="ZW">Zimbabwe</option>
										</select>
									</li>
									<li class="dropdown me-5">
										<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><span>
												Language <i class="fa fa-caret-down text-muted"></i></span> </a>
										<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
											<a href="javascript:void(0);" class="dropdown-item">
												English
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												Arabic
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												German
											</a>
											<a href="javascript:void(0);" class="dropdown-item">
												Greek
											</a>
											<a href="javascript:void(0);" class="dropdown-item">
												Japanese
											</a>
										</div>
									</li>
									<li class="dropdown">
										<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><span>Currency <i class="fa fa-caret-down text-muted"></i></span></a>
										<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
											<a href="javascript:void(0);" class="dropdown-item">
												USD
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												EUR
											</a>
											<a class="dropdown-item" href="javascript:void(0);">
												INR
											</a>
											<a href="javascript:void(0);" class="dropdown-item">
												GBP
											</a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-4 col-sm-8 col-5">
						<div class="top-bar-right">
							<ul class="custom">
								<?php if ($this->session->userdata('user_logged_in') != 1) { ?>
									<li>
										<a href="#" class="text-dark" data-bs-toggle="modal" data-bs-target="#Register"><i class="fa fa-Register me-1"></i>
											<span>Register</span>
										</a>
									</li>
									<!--Register modal start  -->
									<div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="RegisterLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title fw-bold" id="RegisterLabel">Register </h4>
													<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												</div>
												<div class="modal-body">
													<form action="<?= site_url() ?>home/register" method="POST">
														<div class="">
															<p>Please fill in this form to create an account.</p>

															<div class="mb-3">
																<label for="email" class="form-label"><b>Email</b></label>
																<input class="form-control" type="text" placeholder="Enter Email" autocomplete="username" name="email" id="email" required>

															</div>
															<div class="mb-3">
																<label for="psw" class="form-label"><b>Password</b></label>
																<input class="form-control" type="password" placeholder="Enter Password" autocomplete="new-password" name="pwd" id="pwd_reg" required>
															</div>
															<div class="mb-3">
																<label for="psw-repeat" class="form-label"><b>Repeat Password</b></label>
																<small class="d-none" id="pwd_mismatch" style="color:red;">Those passwords didn’t match. Try again.</small>
																<input class="form-control" type="password" placeholder="Repeat Password" autocomplete="new-password" name="pwd_repeat" id="pwd_repeat" required>
															</div>

															<button class="btn btn-primary" id="registerbtn" type="submit" class="registerbtn">Register</button>
														</div>

														<div class=" signin" class="mb-3">
															<p>Already have an account? <a href="#" data-bs-toggle="modal" data-bs-target="#sigin">Sign
																	in</a>.</p>
														</div>
													</form>
												</div>

											</div>
										</div>
									</div>
									<!--Register modal end -->

									<li>

										<a href="#" class="text-dark" data-bs-toggle="modal" data-bs-target="#sigin"><i class="fa fa-sign-in me-1"></i>
											<span>Login</span></a>
										<!--signin modal start  -->
										<div class="modal fade" id="sigin" tabindex="-1" role="dialog" aria-labelledby="siginLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title fw-bold" id="siginLabel">Sign In</h4>
														<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

													</div>
													<div class="modal-body">
														<form action="<?= site_url() ?>home/login" autocomplete="off" method="post">

															<div class="mb-3">
																<label for="uname" class="form-label"><b>Username</b></label>
																<input class="form-control" type="text" placeholder="Enter Username" autocomplete="username" name="uname" required>
															</div>
															<div class="mb-3">
																<label for="psw" class="form-label"><b>Password</b></label>
																<input class="form-control" type="password" placeholder="Enter Password" autocomplete="current-password" name="pwd" required>
															</div>
															<input type="hidden" name="callback" value="<?= site_url() ?>vr_property">
															<div class="form-group" class="mb-3">
																<button class="btn btn-primary" type="submit" class="mb-3">Login</button>
															</div>

															<div class="my-3" style="background-color:#f1f1f1">

																<span class="psw"> <a href="#" data-bs-toggle="modal" data-bs-target="#Forgot_password">Forgot Password?</a></span>
															</div>
														</form>
													</div>

												</div>
											</div>
										</div>
										<!--signin modal end -->

										<!--Forgot password modal start  -->
										<div class="modal fade" id="Forgot_password" tabindex="-1" role="dialog" aria-labelledby="Forgot_passwordLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title fw-bold" id="Forgot_passwordLabel">Forgot Password</h4>
														<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>
													<div class="modal-body">
														<form id="form_forgotpassword" method="POST" action="<?= site_url() ?>home/forgot_password">
															<div class="">
																<h4>Forgot Password</h4>
																<!-- <p>Please fill in this form to create an account.</p> -->

																<div class="mb-3">
																	<label for="email" class="form-label"><b>Email</b></label>
																	<input type="text" class="form-control" placeholder="Enter Email" name="email_id" id="email" required>
																</div>
																<button class="btn btn-primary" id="registerbtn" type="submit" class="registerbtn">Reset Password</button>
															</div>
														</form>

														<form action="<?= site_url() ?>home/verify_otp" id="form_otp" method="POST" style="display: none;">
															<div>
																<h1>Enter OTP</h1>
																<p class="text-success">Check your email for the OTP</p>
																<div class="mb-3">
																	<!-- <label for="email"><b>Email</b></label> -->
																	<input type="hidden" name="email" id="verify_email">
																	<input type="text" class="form-control" placeholder="One Time Password" name="otp" id="otp" required>
																</div>
																<button class="btn btn-primary" id="registerbtn" type="submit" class="registerbtn">Submit</button>
															</div>
														</form>

														<form action="<?= site_url() ?>home/setpassword" id="form_setpassword" method="POST" style="display: none;">
															<div>
																<h4>Reset Your Password</h4>
																<p class="text-secoundary">Set the new password for your account so you can login and access all the features.</p>
																<div class="mb-3">
																	<label for="password" class="form-label"><b>Password</b></label>
																	<input type="password" class="form-control" placeholder="" name="password" id="fpassword" required>
																	<input type="hidden" name="email" id="ref_email">
																</div>
																<div class="mb-3">
																	<label for="confirmpassword" class="form-label"><b>Password</b></label>
																	<input type="password" class="form-control" oninput="verfiy_confirmpwd(this)" placeholder="" name="confirmpassword" id="fconfirmpassword" required>
																	<span id="confirmpassword_msg" style="display: none;" class="text-danger">Password and Confirm Password does not match</span>
																</div>
																<button class="btn btn-primary" disabled id="resetpwd_btn" type="submit" class="registerbtn">Submit</button>
															</div>
														</form>
													</div>

												</div>
											</div>
										</div>
										<!--Forgot password modal end -->
									</li>
								<?php } else { ?>
									<li class="dropdown">
										<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><i class="fa fa-home me-1"></i><span> My Dashboard</span></a>
										<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">

											<a href="<?= site_url() ?>vr_property/page_list_map2" class="dropdown-item">
												<i class="dropdown-icon icon icon-speech"></i> Ads
											</a>



											<a href="<?= site_url() ?>home/logout?callbackid=<?= base64_encode('vr_property') ?>" class="dropdown-item text-dark">
												<i class="dropdown-icon icon icon-power"></i> Log out
											</a>

										</div>
									</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="cover-image sptb-tab bg-background2 mb-lg-8 pb-lg-0 pb-8" data-bs-image-src="https://www.spruko.com/demo/reallist/Reallist/assets/images/banners/banner1.jpg">

			<!-- Duplex Houses Header -->
			<div class="sticky">
				<div class="horizontal-header clearfix ">
					<div class="container">
						<a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
						<span class="smllogo">
							<a href="<?= base_url() ?>vr_dharisnam/index">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo.png" class="mobile-light-logo" width="120" alt="" />
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" class="mobile-dark-logo" width="120" alt="" />
							</a>
						</span>
						<a href="tel:245-6325-3256" class="callusbtn"><i class="fa fa-phone" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
			<!-- /Duplex Houses Header -->

			<div class="horizontal-main clearfix">
				<div class="horizontal-mainwrapper container clearfix">
					<div class="desktoplogo">
						<a href="<?= base_url() ?>vr_dharisnam/index"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<div class="desktoplogo-1">
						<a href="<?= base_url() ?>vr_dharisnam/index"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<!--Nav-->
					<nav class="horizontalMenu clearfix d-md-flex">
						<ul class="horizontalMenu-list"><?php $url_arr = explode('/', $_SERVER['REQUEST_URI']); ?>
							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_property" <?php if (end($url_arr) == "home") {
																								echo 'class="active"';
																							} ?>>Home </a>

							</li>
							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_property/about">About Us </a></li>

							<li><a href="<?= base_url() ?>vr_property/page_list_map2">Pages</a>

							</li>
							<li><a href="<?= base_url() ?>vr_property/blog_list">Customer Blog </a>

							</li>

							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_property/contact"> Contact Us <span class="hmarrow"></span></a></li>
							<li aria-haspopup="true">
							<?php if ($this->session->userdata('user_logged_in') == 1) { ?>
								<a href="<?= base_url() ?>vr_property/adposts">Post Property Ad</a>
								<?php }else{?>
									<a href="#" data-bs-toggle="modal" data-bs-target="#sigin">
									Post Property Ad</a>
								<?php } ?>
							</li>
							<li aria-haspopup="true">
							<?php if ($this->session->userdata('user_logged_in') == 1) { ?>
								<a href="<?= base_url() ?>vr_property/customerreq">Customer Request</a>
								<?php }else{?>
									<a href="#" data-bs-toggle="modal" data-bs-target="#sigin">
									Customer Request</a>
								<?php } ?>
							</li>
						</ul>

					</nav>
					<!--Nav-->
				</div>
			</div>
		</div>
	</div>