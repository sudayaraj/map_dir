<!doctype html>
<html lang="en" dir="ltr">
	<head>
		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="Reallist- Bootstrap Responsive Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose HTML Template" name="description">
		<meta content="Spruko Technologies Private Limited" name="author">
		<meta name="keywords" content="html template, real estate websites, real estate html template, property websites, premium html templates, real estate company website, bootstrap real estate template, real estate marketplace html template, listing website template, property listing html template, real estate bootstrap template, real estate html5 template, real estate listing template, property template, property dealer website"/>

		<!-- Favicon -->
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<!-- Title -->
		<title>Reallist- Bootstrap Responsive Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose HTML Template</title>

		<!-- Bootstrap Css -->
		<link id="style" href="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/css/bootstrap.min.css" rel="stylesheet" />

		<!-- Dashboard Css -->
		<link href="<?= base_url() ?>vr_propertyassets/css/style.css" rel="stylesheet" />

		<!-- Font-awesome  Css -->
		<link href="<?= base_url() ?>vr_propertyassets/css/icons.css" rel="stylesheet"/>

		<!-- Owl Theme css-->
		<link href="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" />

		<!--Select2 Plugin -->
		<link href="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.min.css" rel="stylesheet" />

		<!-- P-scroll bar css-->
		<link href="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= base_url() ?>vrmall_assets/css/vr_mall.css">

	</head>

	<body class="main-body">

		<!--Loader-->
		<div id="global-loader">
			<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/loader.svg" class="loader-img" alt="">
		</div>
		<div class="nav1">
			<div class="container-fluid text-center">
				<div class="navbar">
					<img src="" alt="Logo" class="logo">
					<!-- toggle img start -->
					<img src="<?= base_url() ?>assets/icons/toggle.jpg" style="width:34px;" alt="toggle" class="res_toggle" id="res_toggle">
					<!-- toggle img end -->
					<ul class="navul1 " id="navul1">
						<li class="navli1"><a href="<?= base_url() ?>">Virtual Link</a></li>
						<li class="navli1"><a href="<?= base_url() ?>vr_mall">Virtual Mall</a></li>
						<li class="navli1"><a href="#" class="nav_active">Virtual Property</a></li>
						<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Virtual Education</a></li>
						<li class="navli1"><a href="<?= base_url() ?>vr_studio">VR Studio</a></li>
						<li class="navli1"><a href="<?= base_url() ?>vr_tour" >VR Tour</a></li>
						<li class="navli1"><a href="<?= base_url() ?>vr_dharisnam">VR Dharisanam</a></li>
						<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Buy Sharing</a></li>
						<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">CP Hub</a></li>
						<li class="navli1"><a href="<?=base_url()?>comingsoon/" target="_blank">Live Stream</a></li>
						<li class="navli1 navrmborder"><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!--Topbar-->
		<div class="header-main pt_3">
			<div class="top-bar">
				<div class="container">
					<div class="row">
						<div class="col-xl-8 col-lg-8 col-sm-4 col-7">
							<div class="top-bar-left d-flex">
								<div class="clearfix">
									<ul class="socials">
										<li>
											<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-linkedin"></i></a>
										</li>
										<li>
											<a class="social-icon text-dark" href="javascript:void(0);"><i class="fa fa-google-plus"></i></a>
										</li>
									</ul>
								</div>
								<div class="clearfix">
									<ul class="contact">
										<li class="me-5 d-lg-none">
											<a href="javascript:void(0);" class="callnumber text-dark"><span><i class="fa fa-phone me-1"></i>: +425 345 8765</span></a>
										</li>
										<li class="select-country me-5">
											<select class="form-control select2-flag-search" data-placeholder="Select Country">
												<option value="UM">United States of America</option>
												<option value="AF">Afghanistan</option>
												<option value="AL">Albania</option>
												<option value="AD">Andorra</option>
												<option value="AG">Antigua and Barbuda</option>
												<option value="AU">Australia</option>
												<option value="AM">Armenia</option>
												<option value="AO">Angola</option>
												<option value="AR">Argentina</option>
												<option value="AT">Austria</option>
												<option value="AZ">Azerbaijan</option>
												<option value="BA">Bosnia and Herzegovina</option>
												<option value="BB">Barbados</option>
												<option value="BD">Bangladesh</option>
												<option value="BE">Belgium</option>
												<option value="BF">Burkina Faso</option>
												<option value="BG">Bulgaria</option>
												<option value="BH">Bahrain</option>
												<option value="BJ">Benin</option>
												<option value="BN">Brunei</option>
												<option value="BO">Bolivia</option>
												<option value="BT">Bhutan</option>
												<option value="BY">Belarus</option>
												<option value="CD">Congo</option>
												<option value="CA">Canada</option>
												<option value="CF">Central African Republic</option>
												<option value="CI">Cote d'Ivoire</option>
												<option value="CL">Chile</option>
												<option value="CM">Cameroon</option>
												<option value="CN">China</option>
												<option value="CO">Colombia</option>
												<option value="CU">Cuba</option>
												<option value="CV">Cabo Verde</option>
												<option value="CY">Cyprus</option>
												<option value="DJ">Djibouti</option>
												<option value="DK">Denmark</option>
												<option value="DM">Dominica</option>
												<option value="DO">Dominican Republic</option>
												<option value="EC">Ecuador</option>
												<option value="EE">Estonia</option>
												<option value="ER">Eritrea</option>
												<option value="ET">Ethiopia</option>
												<option value="FI">Finland</option>
												<option value="FJ">Fiji</option>
												<option value="FR">France</option>
												<option value="GA">Gabon</option>
												<option value="GD">Grenada</option>
												<option value="GE">Georgia</option>
												<option value="GH">Ghana</option>
												<option value="GH">Ghana</option>
												<option value="HN">Honduras</option>
												<option value="HT">Haiti</option>
												<option value="HU">Hungary</option>
												<option value="ID">Indonesia</option>
												<option value="IE">Ireland</option>
												<option value="IL">Israel</option>
												<option value="IN">India</option>
												<option value="IQ">Iraq</option>
												<option value="IR">Iran</option>
												<option value="IS">Iceland</option>
												<option value="IT">Italy</option>
												<option value="JM">Jamaica</option>
												<option value="JO">Jordan</option>
												<option value="JP">Japan</option>
												<option value="KE">Kenya</option>
												<option value="KG">Kyrgyzstan</option>
												<option value="KI">Kiribati</option>
												<option value="KW">Kuwait</option>
												<option value="KZ">Kazakhstan</option>
												<option value="LA">Laos</option>
												<option value="LB">Lebanons</option>
												<option value="LI">Liechtenstein</option>
												<option value="LR">Liberia</option>
												<option value="LS">Lesotho</option>
												<option value="LT">Lithuania</option>
												<option value="LU">Luxembourg</option>
												<option value="LV">Latvia</option>
												<option value="LY">Libya</option>
												<option value="MA">Morocco</option>
												<option value="MC">Monaco</option>
												<option value="MD">Moldova</option>
												<option value="ME">Montenegro</option>
												<option value="MG">Madagascar</option>
												<option value="MH">Marshall Islands</option>
												<option value="MK">Macedonia (FYROM)</option>
												<option value="ML">Mali</option>
												<option value="MM">Myanmar (formerly Burma)</option>
												<option value="MN">Mongolia</option>
												<option value="MR">Mauritania</option>
												<option value="MT">Malta</option>
												<option value="MV">Maldives</option>
												<option value="MW">Malawi</option>
												<option value="MX">Mexico</option>
												<option value="MZ">Mozambique</option>
												<option value="NA">Namibia</option>
												<option value="NG">Nigeria</option>
												<option value="NO">Norway</option>
												<option value="NP">Nepal</option>
												<option value="NR">Nauru</option>
												<option value="NZ">New Zealand</option>
												<option value="OM">Oman</option>
												<option value="PA">Panama</option>
												<option value="PF">Paraguay</option>
												<option value="PG">Papua New Guinea</option>
												<option value="PH">Philippines</option>
												<option value="PK">Pakistan</option>
												<option value="PL">Poland</option>
												<option value="QA">Qatar</option>
												<option value="RO">Romania</option>
												<option value="RU">Russia</option>
												<option value="RW">Rwanda</option>
												<option value="SA">Saudi Arabia</option>
												<option value="SB">Solomon Islands</option>
												<option value="SC">Seychelles</option>
												<option value="SD">Sudan</option>
												<option value="SE">Sweden</option>
												<option value="SG">Singapore</option>
												<option value="TG">Togo</option>
												<option value="TH">Thailand</option>
												<option value="TJ">Tajikistan</option>
												<option value="TL">Timor-Leste</option>
												<option value="TM">Turkmenistan</option>
												<option value="TN">Tunisia</option>
												<option value="TO">Tonga</option>
												<option value="TR">Turkey</option>
												<option value="TT">Trinidad and Tobago</option>
												<option value="TW">Taiwan</option>
												<option value="UA">Ukraine</option>
												<option value="UG">Uganda</option>
												<option value="UY">Uruguay</option>
												<option value="UZ">Uzbekistan</option>
												<option value="VA">Vatican City (Holy See)</option>
												<option value="VE">Venezuela</option>
												<option value="VN">Vietnam</option>
												<option value="VU">Vanuatu</option>
												<option value="YE">Yemen</option>
												<option value="ZM">Zambia</option>
												<option value="ZW">Zimbabwe</option>
											</select>
										</li>
										<li class="dropdown me-5">
											<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><span> Language <i class="fa fa-caret-down text-muted"></i></span> </a>
											<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
												<a href="javascript:void(0);" class="dropdown-item" >
													English
												</a>
												<a class="dropdown-item" href="javascript:void(0);">
													Arabic
												</a>
												<a class="dropdown-item" href="javascript:void(0);">
													German
												</a>
												<a href="javascript:void(0);" class="dropdown-item" >
													Greek
												</a>
												<a href="javascript:void(0);" class="dropdown-item" >
													Japanese
												</a>
											</div>
										</li>
										<li class="dropdown">
											<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><span>Currency <i class="fa fa-caret-down text-muted"></i></span></a>
											<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
												<a href="javascript:void(0);" class="dropdown-item" >
													USD
												</a>
												<a class="dropdown-item" href="javascript:void(0);">
													EUR
												</a>
												<a class="dropdown-item" href="javascript:void(0);">
													INR
												</a>
												<a href="javascript:void(0);" class="dropdown-item" >
													GBP
												</a>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-lg-4 col-sm-8 col-5">
							<div class="top-bar-right">
								<ul class="custom">
									<li>
										<a href="register.html" class="text-dark"><i class="fa fa-user me-1"></i> <span>Register</span></a>
									</li>
									<li>
										<a href="login.html" class="text-dark"><i class="fa fa-sign-in me-1"></i> <span>Login</span></a>
									</li>
									<li class="dropdown">
										<a href="javascript:void(0);" class="text-dark" data-bs-toggle="dropdown"><i class="fa fa-home me-1"></i><span> My Dashboard</span></a>
										<div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
											<a href="mydash.html" class="dropdown-item" >
												<i class="dropdown-icon icon icon-user"></i> My Profile
											</a>
											<a href="ad-list.html" class="dropdown-item">
												<i class="dropdown-icon icon icon-speech"></i> Ads
											</a>
											<a href="settings.html" class="dropdown-item">
												<i class="dropdown-icon icon icon-bell"></i> Notifications
											</a>
											<a href="mydash.html" class="dropdown-item" >
												<i class="dropdown-icon  icon icon-settings"></i> Account Settings
											</a>
											<a href="login.html" class="dropdown-item">
												<i class="dropdown-icon icon icon-power"></i> Log out
											</a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Duplex Houses Header -->
			<div class="sticky">
				<div class="horizontal-header clearfix ">
					<div class="container">
						<a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
						<span class="smllogo">
							<a href="<?=base_url()?>vr_dharisnam/index">
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo.png" class="mobile-light-logo" width="120" alt=""/>
								<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" class="mobile-dark-logo" width="120" alt=""/>
							</a>
						</span>
						<a href="tel:245-6325-3256" class="callusbtn"><i class="fa fa-phone" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
			<!-- /Duplex Houses Header -->

			<div class="horizontal-main bg-dark-transparent clearfix">
				<div class="horizontal-mainwrapper container clearfix">
					<div class="desktoplogo">
						<a href="<?=base_url()?>vr_dharisnam/index"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<div class="desktoplogo-1">
						<a href="<?=base_url()?>vr_dharisnam/index"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<!--Nav-->
					<nav class="horizontalMenu clearfix d-md-flex">
					<ul class="horizontalMenu-list">
							<li aria-haspopup="true"><a href="<?= base_url() ?>vr_property"  >Home </a>
						
							</li>
							<li aria-haspopup="true"><a href="<?=base_url()?>vr_property/about">About Us </a></li>
		
							<li><a href="<?=base_url()?>vr_property/page_list_map2">Pages</a>
							
							</li>
							<li ><a href="<?=base_url()?>vr_property/blog_list">Blog </a>
								
							</li>
							<li ><a href="<?=base_url()?>vr_property/col_left" class="active">Categories </a>
							
							</li>
							<li aria-haspopup="true"><a href="<?=base_url()?>vr_property/contact"> Contact Us <span
										class="hmarrow"></span></a></li>
										<li aria-haspopup="true" >
								<a  href="<?= base_url() ?>vr_property/adposts">Post Property Ad</a>
							</li>
							<li aria-haspopup="true" >
								<a href="<?= base_url() ?>vr_property/customerreq">Customer request</a>
							</li>
						</ul>
						
					</nav>
					<!--Nav-->
				</div>
			</div>
		</div>

		<!--Sliders Section-->
		<div>
			<div class="cover-image sptb-1 bg-background" data-bs-image-src="https://www.spruko.com/demo/reallist/Reallist/assets/images/banners/banner1.jpg">
				<div class="header-text1 mb-0">
					<div class="container">
						<div class="row">
							<div class="col-xl-8 col-lg-12 col-md-12 d-block mx-auto">
								<div class="text-center text-white text-property">
									<h1 ><span class="font-weight-bold">16,25,365</span> Properties Available</h1>
								</div>
								<div class=" search-background bg-transparent">
									<div class="form row no-gutters">
										<div class="form-group  col-xl-6 col-lg-5 col-md-12 mb-0">
											<input type="text" class="form-control input-lg br-tr-md-0 br-br-md-0" id="text" placeholder="Search Property">
										</div>
										<div class="form-group col-xl-4 col-lg-4 select2-lg col-md-12 mb-0">
											<select class="form-control select2-show-search w-100" data-placeholder="Select">
												<optgroup label="Categories">
													<option>Select</option>
													<option value="1">Deluxe Houses</option>
													<option value="2">2BHK Homes</option>
													<option value="3">Apartments</option>
													<option value="4">Modren Houses</option>
													<option value="5">Job</option>
													<option value="6">Luxury Rooms</option>
													<option value="7">Luxury Rooms</option>
													<option value="8">Duplex Houses</option>
													<option value="9">3BHK Flatss</option>
													<option value="10">3BHk Homes</option>
												</optgroup>
											</select>
										</div>
										<div class="col-xl-2 col-lg-3 col-md-12 mb-0">
											<a href="javascript:void(0);" class="btn btn-lg btn-block btn-primary br-bl-md-0 br-tl-md-0">Search</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- /header-text -->
			</div>
		</div>
		<!--/Sliders Section-->

		<!--BreadCrumb-->
		<div class="bg-white border-bottom">
			<div class="container">
				<div class="page-header">
					<h4 class="page-title">RealEstate</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
						<li class="breadcrumb-item"><a href="javascript:void(0);">Categories</a></li>
						<li class="breadcrumb-item active" aria-current="page">RealEstate</li>
					</ol>
				</div>
			</div>
		</div>
		<!--/BreadCrumb-->

		<!--Add listing-->
		<section class="sptb">
			<div class="container">
				<div class="row">
					<div class="col-xl-8 col-lg-8 col-md-12">

						<!--Classified Description-->
						<div class="card overflow-hidden">
							<div class="ribbon ribbon-top-right text-danger"><span class="bg-danger">Offer</span></div>
							<div class="card-body">
								<div class="item-det mb-4">
									<a href="javascript:void(0);" class="text-dark"><h3 class="">Affordable 2 BHK Flats Available in Gated Community</h3></a>
									<ul class="d-flex">
										<li class="me-5"><a href="javascript:void(0);" class="icons"><i class="icon icon-briefcase text-muted me-1"></i> RealEstate</a></li>
										<li class="me-5"><a href="javascript:void(0);" class="icons"><i class="icon icon-location-pin text-muted me-1"></i> USA</a></li>
										<li class="me-5"><a href="javascript:void(0);" class="icons"><i class="icon icon-calendar text-muted me-1"></i> 5 hours ago</a></li>
										<li class="me-5"><a href="javascript:void(0);" class="icons"><i class="icon icon-eye text-muted me-1"></i> 765</a></li>
										<li class=""><a href="javascript:void(0);" class="icons">
										<i class="fa fa-star text-warning"></i>
										<i class="fa fa-star text-warning"></i>
										<i class="fa fa-star text-warning"></i>
										<i class="fa fa-star text-warning"></i>
										<i class="fa fa-star-half-o text-warning me-1"></i>4.5</a></li>
									</ul>
								</div>
								<div class="product-slider carousel-slide-1">
									<div id="carouselFade" class="carousel slide carousel-fade" data-bs-ride="carousel"
										data-bs-loop="false" data-bs-thumb="true" data-bs-dots="false">
										<div class="arrow-ribbon2 bg-primary">$539</div>
										<div class="carousel-inner slide-show-image" id=full-gallery>
											<div class="carousel-item active"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h1.jpg" alt="img"> </div>
											<div class="carousel-item"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/b4.jpg" alt="img"> </div>
											<div class="carousel-item"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h3.jpg" alt="img"> </div>
											<div class="carousel-item"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h4.jpg" alt="img"> </div>
											<div class="carousel-item"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h5.jpg" alt="img"> </div>
											<div class="carousel-item"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h6.jpg" alt="img"> </div>
											<div class="carousel-item"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/b1.jpg" alt="img"> </div>
											<div class="carousel-item"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/b2.jpg" alt="img"> </div>
											<div class="carousel-item"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/b3.jpg" alt="img"> </div>
											<div class="carousel-item"><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/b4.jpg" alt="img"> </div>
											<div class="thumbcarousel">
												<a class="carousel-control-prev" href="#carouselFade" role="button"
													data-bs-slide="prev">
													<i class="fa fa-angle-left" aria-hidden="true"></i>
												</a>
												<a class="carousel-control-next" href="#carouselFade" role="button"
													data-bs-slide="next">
													<i class="fa fa-angle-right" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Description</h3>
							</div>
							<div class="card-body">
								<div class="mb-4">
									<p>Luxury Home  For Sale odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atcorrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
									<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoraliz the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble thena bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.</p>
								</div>
								<h4 class="mb-4">Specifications</h4>
								<div class="row">
									<div class="col-xl-6 col-md-12">
										<ul class="list-unstyled widget-spec mb-0">
											<li class="">
												<i class="fa fa-bed text-muted w-5"></i> 2 BedRooms
											</li>
											<li class="">
												<i class="fa fa-bath text-muted w-5"></i> 2 BathRooms
											</li>
											<li class="">
												<i class="fa fa-life-ring text-muted w-5"></i> Unfurnished
											</li>
											<li class="">
												<i class="fa fa-car text-muted w-5" ></i> 2 Car Parking
											</li>
											<li class="">
												<i class="fa fa-globe text-muted w-5"></i> East East face
											</li>
											<li class="mb-xl-0">
												<i class="fa fa-pagelines text-muted w-5"></i> Garden
											</li>
										</ul>
									</div>
									<div class="col-xl-6 col-md-12">
										<ul class="list-unstyled widget-spec mb-0">
											<li class="">
												<i class="fa fa-lock text-muted w-5"></i> Security
											</li>
											<li class="">
												<i class="fa fa-building-o text-muted w-5"></i> Lift
											</li>
											<li class="">
												<i class="fa fa-check text-muted w-5"></i> Swimming fool
											</li>
											<li class="">
												<i class="fa fa-gamepad text-muted w-5"></i> Play Area
											</li>
											<li class="">
												<i class="fa fa-futbol-o text-muted w-5"></i> football Court
											</li>
											<li class="mb-0">
												<i class="fa fa-trophy text-muted w-5"></i> Cricket Court
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="pt-4 pb-4 ps-5 pe-5 border-top border-top">
								<div class="list-id">
									<div class="row">
										<div class="col">
											<a class="mb-0">Classified ID : #8256358</a>
										</div>
										<div class="col col-auto">
											Posted By <a class="mb-0 font-weight-bold">Individual</a> / 21st Dec 2019
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="icons">
									<a href="javascript:void(0);" class="btn btn-info icons"><i class="icon icon-share me-1"></i> Share Ad</a>
									<a href="javascript:void(0);" class="btn btn-primary icons"><i class="icon icon-heart  me-1"></i> 678</a>
									<a href="javascript:void(0);" class="btn btn-secondary icons"><i class="icon icon-printer  me-1"></i> Print</a>
								</div>
							</div>
						</div>
						<!--/Classified Description-->

						<h3 class="mb-5 mt-4">Related Posts</h3>

						<!--Related Posts-->
						<div id="myCarousel5" class="owl-carousel owl-carousel-icons3">
							<!-- Wrapper for carousel items -->

							<div class="item">
								<div class="card">
									<div class="arrow-ribbon bg-primary">For Sale</div>
									<div class="item-card7-imgs">
										<a href="col-left.html"></a>
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/ed1.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-card7-overlaytext">
										<a href="col-left.html" class="text-white">Duplex House</a>
										<h4 class="mb-0">$389</h4>
									</div>
									<div class="card-body">
										<div class="item-card7-desc">
											<a href="col-left.html" class="text-dark"><h4 class="font-weight-semibold">House For Sale</h4></a>
										</div>
										<div class="item-card7-text">
											<ul class="icon-card mb-0">
												<li ><a href="javascript:void(0);" class="icons"><i class="icon icon-location-pin text-muted me-1"></i>  Los Angles</a></li>
												<li><a href="javascript:void(0);" class="icons"><i class="icon icon-event text-muted me-1"></i> 5 hours ago</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-user text-muted me-1"></i> Sally Peake</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-phone text-muted me-1"></i> 5-67987608</a></li>
											</ul>
											<p class="mb-0"> Built Up 1250 Sq.ft</p>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="card">
									<div class="arrow-ribbon bg-secondary">For Rent</div>
									<div class="item-card7-imgs">
										<a href="col-left.html"></a>
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/v1.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-card7-overlaytext">
										<a href="col-left.html" class="text-white">Luxury Room</a>
										<h4 class=" mb-0">$854</h4>
									</div>
									<div class="card-body">
										<div class="item-card7-desc">
											<a href="col-left.html" class="text-dark"><h4 class="font-weight-semibold">Luxury Flat For Sale</h4></a>
										</div>
										<div class="item-card7-text">
											<ul class="icon-card mb-0">
												<li ><a href="javascript:void(0);" class="icons"><i class="icon icon-location-pin text-muted me-1"></i>  Los Angles</a></li>
												<li><a href="javascript:void(0);" class="icons"><i class="icon icon-event text-muted me-1"></i> 5 hours ago</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-user text-muted me-1"></i> Sally Peake</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-phone text-muted me-1"></i> 5-67987608</a></li>
											</ul>
											<p class="mb-0">Built Up 650 Sq.ft</p>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="card">
									<div class="item-card7-imgs">
										<a href="col-left.html"></a>
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/j1.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-card7-overlaytext">
										<a href="col-left.html" class="text-white">3BHK House</a>
										<h4 class=" mb-0">$786</h4>
									</div>
									<div class="card-body">
										<div class="item-card7-desc">
											<a href="col-left.html" class="text-dark"><h4 class="font-weight-semibold">Home For Sale</h4></a>
										</div>
										<div class="item-card7-text">
											<ul class="icon-card mb-0">
												<li ><a href="javascript:void(0);" class="icons"><i class="icon icon-location-pin text-muted me-1"></i>  Los Angles</a></li>
												<li><a href="javascript:void(0);" class="icons"><i class="icon icon-event text-muted me-1"></i> 5 hours ago</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-user text-muted me-1"></i> Sally Peake</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-phone text-muted me-1"></i> 5-67987608</a></li>
											</ul>
											<p class="mb-0">Built Up 1100 Sq.ft</p>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="card">
									<div class="arrow-ribbon bg-primary">For Sale</div>
									<div class="item-card7-imgs">
										<a href="col-left.html"></a>
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/f4.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-card7-overlaytext">
										<a href="col-left.html" class="text-white">2BHK House</a>
										<h4 class="mb-0">$539</h4>
									</div>
									<div class="card-body">
										<div class="item-card7-desc">
											<a href="rcol-left.html" class="text-dark"><h4 class="font-weight-semibold">2BHK Flat For Rent</h4></a>
										</div>
										<div class="item-card7-text">
											<ul class="icon-card mb-0">
												<li ><a href="javascript:void(0);" class="icons"><i class="icon icon-location-pin text-muted me-1"></i>  Los Angles</a></li>
												<li><a href="javascript:void(0);" class="icons"><i class="icon icon-event text-muted me-1"></i> 5 hours ago</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-user text-muted me-1"></i> Sally Peake</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-phone text-muted me-1"></i> 5-67987608</a></li>
											</ul>
											<p class="mb-0">Built Up 500 Sq.ft</p>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="card">
									<div class="item-card7-imgs">
										<a href="col-left.html"></a>
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/pel.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-card7-overlaytext">
										<a href="col-left.html" class="text-white"> 2BHk House</a>
										<h4 class=" mb-0">$925</h4>
									</div>
									<div class="card-body">
										<div class="item-card7-desc">
											<a href="col-left.html" class="text-dark"><h4 class="font-weight-semibold">Flat For Rent</h4></a>
										</div>
										<div class="item-card7-text">
											<ul class="icon-card mb-0">
												<li ><a href="javascript:void(0);" class="icons"><i class="icon icon-location-pin text-muted me-1"></i>  Los Angles</a></li>
												<li><a href="javascript:void(0);" class="icons"><i class="icon icon-event text-muted me-1"></i> 5 hours ago</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-user text-muted me-1"></i> Sally Peake</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-phone text-muted me-1"></i> 5-67987608</a></li>
											</ul>
											<p class="mb-0">Built Up 1000 Sq.ft</p>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="card">
									<div class="arrow-ribbon bg-secondary">For Rent</div>
									<div class="item-card7-imgs">
										<a href="col-left.html"></a>
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/h5.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-card7-overlaytext">
										<a href="col-left.html" class="text-white"> Duplex House</a>
										<h4 class="mb-0">$925</h4>
									</div>
									<div class="card-body">
										<div class="item-card7-desc">
											<a href="col-left.html" class="text-dark"><h4 class="font-weight-semibold"> Single Flat House For Rent</h4></a>
										</div>
										<div class="item-card7-text">
											<ul class="icon-card mb-0">
												<li ><a href="javascript:void(0);" class="icons"><i class="icon icon-location-pin text-muted me-1"></i>  Los Angles</a></li>
												<li><a href="javascript:void(0);" class="icons"><i class="icon icon-event text-muted me-1"></i> 5 hours ago</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-user text-muted me-1"></i> Sally Peake</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-phone text-muted me-1"></i> 5-67987608</a></li>
											</ul>
											<p class="mb-0">Built Up 3450 Sq.ft</p>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="card">
									<div class="item-card7-imgs">
										<a href="col-left.html"></a>
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/ed2.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-card7-overlaytext">
										<a href="col-left.html" class="text-white">Luxury Room</a>
										<h4 class=" mb-0">$378</h4>
									</div>
									<div class="card-body">
										<div class="item-card7-desc">
											<a href="col-left.html" class="text-dark"><h4 class="font-weight-semibold">House For Sale</h4></a>
										</div>
										<div class="item-card7-text">
											<ul class="icon-card mb-0">
												<li ><a href="javascript:void(0);" class="icons"><i class="icon icon-location-pin text-muted me-1"></i>  Los Angles</a></li>
												<li><a href="javascript:void(0);" class="icons"><i class="icon icon-event text-muted me-1"></i> 5 hours ago</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-user text-muted me-1"></i> Sally Peake</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-phone text-muted me-1"></i> 5-67987608</a></li>
											</ul>
											<p class="mb-0">Built Up 900 Sq.ft</p>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="card">
									<div class="arrow-ribbon bg-primary">For Sale</div>
									<div class="item-card7-imgs">
										<a href="col-left.html"></a>
										<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/products/j3.jpg" alt="img" class="cover-image">
									</div>
									<div class="item-card7-overlaytext">
										<a href="col-left.html" class="text-white">Budget House</a>
										<h4 class=" mb-0">$836</h4>
									</div>
									<div class="card-body">
										<div class="item-card7-desc">
											<a href="col-left.html" class="text-dark"><h4 class="font-weight-semibold">2BHK House For Rent</h4></a>
										</div>
										<div class="item-card7-text">
											<ul class="icon-card mb-0">
												<li ><a href="javascript:void(0);" class="icons"><i class="icon icon-location-pin text-muted me-1"></i>  Los Angles</a></li>
												<li><a href="javascript:void(0);" class="icons"><i class="icon icon-event text-muted me-1"></i> 5 hours ago</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-user text-muted me-1"></i> Sally Peake</a></li>
												<li class="mb-0"><a href="javascript:void(0);" class="icons"><i class="icon icon-phone text-muted me-1"></i> 5-67987608</a></li>
											</ul>
											<p class="mb-0">Built Up 2000 Sq.ft</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--/Related Posts-->

						<!--Comments-->
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Rating And Reviews</h3>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-md-12">
										<div class="mb-4">
											<p class="mb-2">
												<span class="fs-14 ms-2"><i class="fa fa-star text-yellow me-2"></i>5</span>
											</p>
											<div class="progress progress-md mb-4 h-4">
												<div class="progress-bar bg-success w-100">9,232</div>
											</div>
										</div>
										<div class="mb-4">
											<p class="mb-2">
												<span class="fs-14 ms-2"><i class="fa fa-star text-yellow me-2"></i>4</span>
											</p>
											<div class="progress progress-md mb-4 h-4">
												<div class="progress-bar bg-info w-80">8,125</div>
											</div>
										</div>
										<div class="mb-4">
											<p class="mb-2">
												<span class="fs-14 ms-2"><i class="fa fa-star text-yellow me-2"></i>  3</span>
											</p>
											<div class="progress progress-md mb-4 h-4">
												<div class="progress-bar bg-primary w-60">6,263</div>
											</div>
										</div>
										<div class="mb-4">
											<p class="mb-2">
												<span class="fs-14 ms-2"><i class="fa fa-star text-yellow me-2"></i>  2</span>
											</p>
											<div class="progress progress-md mb-4 h-4">
												<div class="progress-bar bg-secondary w-30">3,463</div>
											</div>
										</div>
										<div class="mb-5">
											<p class="mb-2">
												<span class="fs-14 ms-2"><i class="fa fa-star text-yellow me-2"></i>  1</span>
											</p>
											<div class="progress progress-md mb-4 h-4">
												<div class="progress-bar bg-orange w-20">1,456</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-body p-0">
								<div class="media mt-0 p-5">
                                    <div class="d-flex me-3">
                                        <a href="javascript:void(0);"><img class="media-object brround" alt="64x64" src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/1.jpg"> </a>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="mt-0 mb-1 font-weight-semibold">Joanne Scott
											<span class="fs-14 ms-0" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span>
											<span class="fs-14 ms-2"> 4.5 <i class="fa fa-star text-yellow"></i></span>
										</h5>
										<small class="text-muted"><i class="fa fa-calendar"></i> Dec 21st  <i class=" ms-3 fa fa-clock-o"></i> 13.00  <i class=" ms-3 fa fa-map-marker"></i> Brezil</small>
                                        <p class="font-13  mb-2 mt-2">
                                           Ut enim ad minim veniam, quis Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et  nostrud exercitation ullamco laboris   commodo consequat.
                                        </p>
										<a href="javascript:void(0);" class="me-2 btn btn-primary btn-sm m-1"><span class="">Helpful</span></a>
										<a href="javascript:void(0);" class="me-2 btn btn-default btn-sm m-1" data-bs-toggle="modal" data-bs-target="#Comment"><span class="">Comment</span></a>
										<a href="javascript:void(0);" class="me-2 btn btn-default btn-sm m-1" data-bs-toggle="modal" data-bs-target="#report"><span class="">Report</span></a>
                                        <div class="media mt-5">
                                            <div class="d-flex me-3">
                                                <a href="javascript:void(0);"> <img class="media-object brround" alt="64x64" src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/female/2.jpg"> </a>
                                            </div>
											<div class="media-body">
												<h5 class="mt-0 mb-1 font-weight-semibold">Rose Slater <span class="fs-14 ms-0" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span></h5>
												<small class="text-muted"><i class="fa fa-calendar"></i> Dec 22st  <i class=" ms-3 fa fa-clock-o"></i> 6.00  <i class=" ms-3 fa fa-map-marker"></i> Brezil</small>
												<p class="font-13  mb-2 mt-2">
												   Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris   commodo At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium, nisi ut aliquid ex ea commodi consequatur consequat.
												</p>
												<a href="javascript:void(0);" class="btn btn-default btn-sm" data-bs-toggle="modal" data-bs-target="#Comment"><span class="">Comment</span></a>
											</div>
										</div>
									</div>
								</div>
								<div class="media p-5 border-top mt-0">
									<div class="d-flex me-3">
										<a href="javascript:void(0);"> <img class="media-object brround" alt="64x64" src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/3.jpg"> </a>
									</div>
									<div class="media-body">
										<h5 class="mt-0 mb-1 font-weight-semibold">Edward
										<span class="fs-14 ms-0" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span>
										<span class="fs-14 ms-2"> 4 <i class="fa fa-star text-yellow"></i></span>
										</h5>
										<small class="text-muted"><i class="fa fa-calendar"></i> Dec 21st  <i class=" ms-3 fa fa-clock-o"></i> 16.35  <i class=" ms-3 fa fa-map-marker"></i> UK</small>
                                        <p class="font-13  mb-2 mt-2">
                                           Ut enim ad minim veniam, quis Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et  nostrud exercitation ullamco laboris   commodo consequat.
                                        </p>
										<a href="javascript:void(0);" class="me-2 btn btn-primary btn-sm m-1"><span class="">Helpful</span></a>
										<a href="javascript:void(0);" class="me-2 btn btn-default btn-sm m-1" data-bs-toggle="modal" data-bs-target="#Comment"><span class="">Comment</span></a>
										<a href="javascript:void(0);" class="me-2 btn btn-default btn-sm m-1" data-bs-toggle="modal" data-bs-target="#report"><span class="">Report</span></a>
									</div>
								</div>
							</div>
						</div>
						<!--/Comments-->

						<div class="card mb-lg-0">
							<div class="card-header">
								<h3 class="card-title">Leave a reply</h3>
							</div>
							<div class="card-body">
								<div>
									<div class="form-group">
										<input type="text" class="form-control" id="name1" placeholder="Your Name">
									</div>
									<div class="form-group">
										<input type="email" class="form-control" id="email" placeholder="Email Address">
									</div>
									<div class="form-group">
										<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Comment"></textarea>
									</div>
									<a href="javascript:void(0);" class="btn btn-primary">Send Reply</a>
								</div>
							</div>
						</div>
					</div>

					<!--Right Side Content-->
					<div class="col-xl-4 col-lg-4 col-md-12">
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Posted By</h3>
							</div>
							<div class="card-body  item-user">
								<div class="profile-pic mb-0">
									<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/faces/male/25.jpg" class="brround avatar-xxl" alt="user">
									<div class="">
										<a href="userprofile2.html" class="text-dark"><h4 class="mt-3 mb-1 font-weight-semibold">Robert McLean</h4></a>
										<p class="mb-0">RealEstate Agent</p>
										<span class="text-muted">Member Since November 2008</span>
										<h6 class="mt-2 mb-0"><a href="userprofile2.html" class="btn btn-primary btn-sm">See All Ads</a></h6>
									</div>
								</div>
							</div>
							<div class="card-body item-user">
								<h4 class="mb-4">Contact Info</h4>
								<div>
									<h6><span class="font-weight-semibold"><i class="fa fa-map-marker me-2 mb-2"></i></span><a href="javascript:void(0);" class="text-body"> 7981 Aspen Ave. Hammonton,  USA</a></h6>
									<h6><span class="font-weight-semibold"><i class="fa fa-envelope me-2 mb-2"></i></span><a href="javascript:void(0);" class="text-body"> robert123@gmail.com</a></h6>
									<h6><span class="font-weight-semibold"><i class="fa fa-phone me-2  mb-2"></i></span><a href="javascript:void(0);" class="text-body"> 0-235-657-24587</a></h6>
									<h6><span class="font-weight-semibold"><i class="fa fa-link me-2 "></i></span><a href="javascript:void(0);" class="text-body">http://spruko.com/</a></h6>
								</div>
								<div class=" item-user-icons mt-4">
									<a href="javascript:void(0);" class="facebook-bg mt-0"><i class="fa fa-facebook"></i></a>
									<a href="javascript:void(0);" class="twitter-bg"><i class="fa fa-twitter"></i></a>
									<a href="javascript:void(0);" class="google-bg"><i class="fa fa-google"></i></a>
									<a href="javascript:void(0);" class="dribbble-bg"><i class="fa fa-dribbble"></i></a>
								</div>
							</div>
							<div class="card-footer">
								<div class="text-start">
									<a href="javascript:void(0);" class="btn  btn-success m-1"><i class="fa fa-envelope"></i> Chat</a>
									<a href="javascript:void(0);" class="btn btn-primary m-1" data-bs-toggle="modal" data-bs-target="#contact"><i class="fa fa-user"></i> Contact Me</a>
									<a href="javascript:void(0);" class="btn  btn-info m-1"><i class="fa fa-share"></i> Share</a>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Keywords</h3>
							</div>
							<div class="card-body product-filter-desc">
								<div class="product-tags clearfix">
									<ul class="list-unstyled mb-0">
										<li><a href="javascript:void(0);">Home</a></li>
										<li><a href="javascript:void(0);">Real estate</a></li>
										<li><a href="javascript:void(0);">2BHK</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Shares</h3>
							</div>
							<div class="card-body product-filter-desc">
								<div class="product-filter-icons text-center">
									<a href="javascript:void(0);" class="facebook-bg"><i class="fa fa-facebook"></i></a>
									<a href="javascript:void(0);" class="twitter-bg"><i class="fa fa-twitter"></i></a>
									<a href="javascript:void(0);" class="google-bg"><i class="fa fa-google"></i></a>
									<a href="javascript:void(0);" class="dribbble-bg"><i class="fa fa-dribbble"></i></a>
									<a href="javascript:void(0);" class="pinterest-bg"><i class="fa fa-pinterest"></i></a>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Latest Seller Ads</h3>
							</div>
							<div class="card-body pb-3">
								<div class="rated-products">
									<ul class="vertical-scroll">
										<li class="item">
											<div class="media m-0 mt-0 p-5">
												<img class="me-4" src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/toys.png" alt="img">
												<div class="media-body">
													<h4 class="mt-2 mb-1">Apartment</h4>
													<span class="rated-products-ratings">
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
													</span>
													<div class="h5 mb-0 font-weight-semibold mt-1">$17 - $29</div>
												</div>
											</div>
										</li>
										<li class="item">
											<div class="media p-5 mt-0">
												<img class="me-4" src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/1.png" alt="img">
												<div class="media-body">
													<h4 class="mt-2 mb-1">Modren Apartment</h4>
													<span class="rated-products-ratings">
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star-o text-warning"> </i>
													</span>
													<div class="h5 mb-0 font-weight-semibold mt-1">$22 - $45</div>
												</div>
											</div>
										</li>
										<li class="item">
											<div class="media p-5 mt-0">
												<img class=" me-4" src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/4.png" alt="img">
												<div class="media-body">
													<h4 class="mt-2 mb-1">3BHK Flat</h4>
													<span class="rated-products-ratings">
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star-half-o text-warning"> </i>
													</span>
													<div class="h5 mb-0 font-weight-semibold mt-1">$35 - $72</div>
												</div>
											</div>
										</li>
										<li class="item">
											<div class="media p-5 mt-0">
												<img class=" me-4" src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/6.png" alt="img">
												<div class="media-body">
													<h4 class="mt-2 mb-1">2BHK Flat</h4>
													<span class="rated-products-ratings">
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star-half-o text-warning"> </i>
														<i class="fa fa-star-o text-warning"> </i>
													</span>
													<div class="h5 mb-0 font-weight-semibold mt-1">$12 - $21</div>
												</div>
											</div>
										</li>
										<li class="item">
											<div class="media  mb-0 p-5 mt-0">
												<img class=" me-4" src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/8.png" alt="img">
												<div class="media-body">
													<h4 class="mt-2 mb-1">Luxury House</h4>
													<span class="rated-products-ratings">
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star text-warning"> </i>
														<i class="fa fa-star-o text-warning"> </i>
														<i class="fa fa-star-o text-warning"> </i>
													</span>
													<div class="h5 mb-0 font-weight-semibold mt-1">$89 - $97</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Map location</h3>
							</div>
							<div class="card-body">
								<div class="map-header">
									<div class="map-header-layer" id="map2"></div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Search Ads</h3>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input type="text" class="form-control" id="text2" placeholder="What are you looking for?">
								</div>
								<div class="form-group">
									<select name="country" id="select-countries" class="form-control form-select  select2-show-search">
										<option value="1" selected>All Categories</option>
										<option value="2">RealEstate</option>
										<option value="3">Apartments</option>
										<option value="4">3BHK Flat</option>
										<option value="5">Homes</option>
										<option value="6">Luxury Rooms</option>
										<option value="7">Deluxe Houses</option>
										<option value="8">Duplex House</option>
										<option value="9">Luxury Rooms</option>
										<option value="10">2BHk Homes</option>
										<option value="11">Apartments</option>
										<option value="12">Duplex Houses</option>
										<option value="13">3BHK Flatss</option>
										<option value="14">2BHK Flats</option>
										<option value="15">Modren Houses</option>
									</select>
								</div>
								<div class="">
									<a href="javascript:void(0);" class="btn  btn-primary">Search</a>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Popular Tags</h3>
							</div>
							<div class="card-body">
								<div class="product-tags clearfix">
									<ul class="list-unstyled mb-0">
										<li><a href="javascript:void(0);">RealEstate</a></li>
										<li><a href="javascript:void(0);">Homes</a></li>
										<li><a href="javascript:void(0);">3BHK Flatss</a></li>
										<li><a href="javascript:void(0);">2BHK Homes</a></li>
										<li><a href="javascript:void(0);">Luxury Rooms</a></li>
										<li><a href="javascript:void(0);">Apartments</a></li>
										<li><a href="javascript:void(0);">3BHK Flatss</a></li>
										<li><a href="javascript:void(0);">Homes</a></li>
										<li><a href="javascript:void(0);">Luxury House For Sale</a></li>
										<li><a href="javascript:void(0);">Apartments</a></li>
										<li><a href="javascript:void(0);" class="mb-0">Luxury Rooms</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Latest Properties</h3>
							</div>
							<div class="card-body pb-3">
								<ul class="vertical-scroll">
									<li class="news-item">
										<table>
											<tr>
												<td><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/1.png" alt="img" class="w-8 border"/></td>
												<td class="ps-4"><h5 class="mb-1 ">Best New Model Houses</h5><a href="javascript:void(0);" class="btn-link">View Details</a><span class="float-end font-weight-bold">$17</span></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table>
											<tr>
												<td><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/2.png" alt="img" class="w-8 border"/></td>
												<td class="ps-4"><h5 class="mb-1 ">Trending New Model Houses</h5><a href="javascript:void(0);" class="btn-link">View Details</a><span class="float-end font-weight-bold">$17</span></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table>
											<tr>
												<td><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/3.png" alt="img" class="w-8 border" /></td>
												<td class="ps-4"><h5 class="mb-1 ">Best New Model Houses</h5><a href="javascript:void(0);" class="btn-link">View Details</a><span class="float-end font-weight-bold">$17</span></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table>
											<tr>
												<td><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/4.png" alt="img" class="w-8 border" /></td>
												<td class="ps-4"><h5 class="mb-1 ">Trending New Model Houses</h5><a href="javascript:void(0);" class="btn-link">View Details</a><span class="float-end font-weight-bold">$17</span></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table>
											<tr>
												<td><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/5.png" alt="img" class="w-8 border" /></td>
												<td class="ps-4"><h5 class="mb-1 ">Best New Model Houses</h5><a href="javascript:void(0);" class="btn-link">View Details</a><span class="float-end font-weight-bold">$17</span></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table>
											<tr>
												<td><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/6.png" alt="img" class="w-8 border" /></td>
												<td class="ps-4"><h5 class="mb-1 ">Duplex House</h5><a href="javascript:void(0);" class="btn-link">View Details</a><span class="float-end font-weight-bold">$17</span></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table>
											<tr>
												<td><img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/7.png" alt="img" class="w-8 border" /></td>
												<td class="ps-4"><h5 class="mb-1 ">Modren Flats</h5><a href="javascript:void(0);" class="btn-link">View Details</a><span class="float-end font-weight-bold">$17</span></td>
											</tr>
										</table>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!--/Right Side Content-->
				</div>
			</div>
		</section>

		<!-- Newsletter-->
		<section class="sptb bg-white border-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-xl-6 col-md-12">
						<div class="sub-newsletter">
							<h3 class="mb-2"><i class="fa fa-paper-plane-o me-2"></i> Subscribe To Our Newsletter</h3>
							<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-lg-5 col-xl-6 col-md-12">
						<div class="input-group sub-input mt-1">
							<input type="text" class="form-control input-lg " placeholder="Enter your Email">
								<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
									Subscribe
								</button>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/Newsletter-->

		<!--Footer Section-->
		<section class="main-footer">
			<footer class="bg-dark text-white">
				<div class="footer-main">
					<div class="container">
						<div class="row">
							<div class="col-lg-3 col-md-12">
								<h6>About</h6>
								<hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis  exercitation ullamco laboris </p>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum .</p>
							</div>
							<div class="col-lg-2 col-sm-6">
								<h6>Our Quick Links</h6>
								 <hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
								<ul class="list-unstyled mb-0">
									<li><a href="javascript:;">Our Team</a></li>
									<li><a href="javascript:;">Contact US</a></li>
									<li><a href="javascript:;">About</a></li>
									<li><a href="javascript:;">Luxury Rooms</a></li>
									<li><a href="javascript:;">Blog</a></li>
									<li><a href="javascript:;">Terms</a></li>
								</ul>
							</div>

							<div class="col-lg-3 col-sm-6">
								<h6>Contact</h6>
								<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
								<ul class="list-unstyled mb-0">
									<li>
										<a href="javascript:void(0);"><i class="fa fa-home me-3 text-primary"></i> New York, NY 10012, US</a>
									</li>
									<li>
										<a href="javascript:void(0);"><i class="fa fa-envelope me-3 text-primary"></i> info12323@example.com</a></li>
									<li>
										<a href="javascript:void(0);"><i class="fa fa-phone me-3 text-primary"></i> + 01 234 567 88</a>
									</li>
									 <li>
										<a href="javascript:void(0);"><i class="fa fa-print me-3 text-primary"></i> + 01 234 567 89</a>
									</li>
								</ul>
								<ul class="list-unstyled list-inline mt-3">
									<li class="list-inline-item">
									  <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-facebook bg-facebook"></i>
									  </a>
									</li>
									<li class="list-inline-item">
									  <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-twitter bg-info"></i>
									  </a>
									</li>
									<li class="list-inline-item">
									  <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-google-plus bg-danger"></i>
									  </a>
									</li>
									<li class="list-inline-item">
									  <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-linkedin bg-linkedin"></i>
									  </a>
									</li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-12">
								<h6>Subscribe</h6>
								<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
								<div class="clearfix"></div>
								<div class="input-group w-100">
									<input type="text" class="form-control br-tl-3  br-bl-3 " placeholder="Email">
										<button type="button" class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button>
								</div>
								<h6 class="mb-0 mt-5">Payments</h6>
								<hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
								<div class="clearfix"></div>
								<ul class="footer-payments">
									<li class="ps-0"><a href="javascript:;"><i class="fa fa-cc-amex text-muted" aria-hidden="true"></i></a></li>
									<li><a href="javascript:;"><i class="fa fa-cc-visa text-muted" aria-hidden="true"></i></a></li>
									<li><a href="javascript:;"><i class="fa fa-credit-card-alt text-muted" aria-hidden="true"></i></a></li>
									<li><a href="javascript:;"><i class="fa fa-cc-mastercard text-muted" aria-hidden="true"></i></a></li>
									<li><a href="javascript:;"><i class="fa fa-cc-paypal text-muted" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="bg-dark text-white p-0">
					<div class="container">
						<div class="row d-flex">
							<div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center ">
								Copyright © 2022 <a href="javascript:void(0);" class="fs-14 text-primary">Reallist</a>. Designed with <i class="fa fa-heart text-danger"></i> by <a href="javascript:void(0);" class="fs-14 text-primary">Spruko</a> All rights reserved.
							</div>
						</div>
					</div>
				</div>
			</footer>
		</section>
		<!--/Footer Section-->

		<!-- Message Modal -->
		<div class="modal fade" id="contact" tabindex="-1" role="dialog"  aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="examplecontactLongTitle">Send Message</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<input type="text" class="form-control" id="contact-name" placeholder="Your Name">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="contact-email" placeholder="Email Address">
						</div>
						<div class="form-group mb-0">
							<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Message"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-success">Send</button>
					</div>
				</div>
			</div>
		</div>

		<!--Comment Modal -->
		<div class="modal fade" id="Comment" tabindex="-1" role="dialog"  aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleCommentLongTitle">Leave a Comment</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<input type="text" class="form-control" id="Comment-name" placeholder="Your Name">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="Comment-email" placeholder="Email Address">
						</div>
						<div class="form-group mb-0">
							<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Message"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-success">Send</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Report Modal -->
		<div class="modal fade" id="report" tabindex="-1" role="dialog"  aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="examplereportLongTitle">Report Abuse</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<input type="text" class="form-control" id="report-name" placeholder="Enter url">
						</div>
						<div class="form-group">
							<select name="country" id="select-countries2" class="form-control form-select select2">
								<option value="1" selected>Categories</option>
								<option value="2">RealEstatem</option>
								<option value="3">Identity Theft</option>
								<option value="4">Online Shopping Fraud</option>
								<option value="5">Service Providers</option>
								<option value="6">Phishing</option>
								<option value="7">Spyware</option>
							</select>
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="report-email" placeholder="Email Address">
						</div>
						<div class="form-group mb-0">
							<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Message"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-success">Submit</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top" ><i class="fa fa-rocket"></i></a>

		<!-- JQuery js-->
		<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery-3.6.0.min.js"></script>

		<!-- Bootstrap js -->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/popper.min.js"></script>
		<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/js/bootstrap.min.js"></script>

		<!--JQuery RealEstaterkline Js-->
		<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery.sparkline.min.js"></script>

		<!-- Circle Progress Js-->
		<script src="<?= base_url() ?>vr_propertyassets/js/vendors/circle-progress.min.js"></script>

		<!-- Star Rating Js-->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/rating/jquery.rating-stars.js"></script>

		<!--Owl Carousel js -->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.js"></script>

		<!--Horizontal Menu-->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/horizontal-menu/horizontal.js"></script>

		<!--JQuery TouchSwipe js-->
		<script src="<?= base_url() ?>vr_propertyassets/js/jquery.touchSwipe.min.js"></script>

		<!-- Google Maps Plugin -->
		<script src="https://maps.google.com/maps/api/js?key=AIzaSyCW16SmpzDNLsrP-npQii6_8vBu_EJvEjA"></script>
		<script src="<?= base_url() ?>vr_propertyassets/plugins/maps-google/jquery.googlemap.js"></script>
		<script src="<?= base_url() ?>vr_propertyassets/plugins/maps-google/map.js"></script>

		<!--Select2 js -->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.full.min.js"></script>
		<script src="<?= base_url() ?>vr_propertyassets/js/select2.js"></script>

		<!-- Cookie js -->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/jquery.ihavecookies.js"></script>
		<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/cookie.js"></script>

		<!-- Count Down-->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/count-down/jquery.lwtCountdown-1.0.js"></script>

		<!-- P-scroll bar Js-->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.js"></script>

		<!-- sticky Js-->
		<script src="<?= base_url() ?>vr_propertyassets//js/sticky.js"></script>

		<!-- Vertical scroll bar Js-->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/vertical-scroll/jquery.bootstrap.newsbox.js"></script>
		<script src="<?= base_url() ?>vr_propertyassets/plugins/vertical-scroll/vertical-scroll.js"></script>

		<!-- Swipe Js-->
		<script src="<?= base_url() ?>vr_propertyassets/js/swipe.js"></script>

		<!-- Cover-image Js-->
		<script src="<?= base_url() ?>vr_propertyassets/js/owl-carousel.js"></script>

		<!--JQuery Slider  js-->
		<script src="<?= base_url() ?>vr_propertyassets/plugins/boot-slider/boot-slider.min.js"></script>
		<script src="<?= base_url() ?>vr_propertyassets/js/boot-slider.js"></script>

		<!-- themecolor Js-->
		<script src="<?= base_url() ?>vr_propertyassets/js/themeColors.js"></script>

		<!-- Custom Js-->
		<script src="<?= base_url() ?>vr_propertyassets/js/custom.js"></script>

		<!-- Custom-switcher Js-->
		<script src="<?= base_url() ?>vr_propertyassets/js/custom-switcher.js"></script>

	</body>
</html>