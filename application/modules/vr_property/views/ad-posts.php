<?php $this->load->view('template/header');
?>
<style>
	.spinner-border img {
		width: 350px;
		height: 125px;
		object-fit: contain;
		display: none;
	}

	.nav-tabs>li.active>a.active_tab {
		border: none;
		border-bottom: 2px solid #000;
	}

	.active_tab {
		padding: 0.5rem 1rem;
		width: 200px;
		display: block;
		margin: 0 auto;
		text-align: center;
		height: 50px;
	}

	.nav-tabs>li.active>a,
	.nav-tabs>li.active>a:hover,
	.nav-tabs>li.active>a:focus,
	a.active_tab.active {
		color: #fff !important;
		cursor: default;
		background-color: #5a2bb0 !important;
		border: 1px solid #ddd;
		border-bottom-color: transparent;
		padding: 0.5rem 1rem;
		width: 200px;
		display: block;
		margin: 0 auto;
		text-align: center;
	}

	.form_fx {
		display: flex;
		flex-direction: column;
		align-items: flex-start;
	}

	.label_txt {
		margin-right: 15px;
	}

	.form_fx input {
		width: 85% !important;
		border-radius: 4px;
	}

	.ff_fileupload_wrap {
		visibility: hidden;
	}

	.current_loaction_fx {
		display: flex;
		align-items: center;
		justify-content: space-between;
		border-bottom: 1px solid #999;
		width: 60%;
		font-size: 16px;
		padding: 10px;
	}
</style>
<!--Sliders Section-->
<section>
	<div class="bannerimg cover-image bg-background3" data-bs-image-src="https://www.spruko.com/demo/reallist/Reallist/assets/images/banners/banner2.jpg">
		<div class="header-text mb-0">
			<div class="container">
				<div class="text-center text-white">
					<h1>Ad Post</h1>
					<ol class="breadcrumb text-center">
						<li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
						<li class="breadcrumb-item"><a href="javascript:void(0);">Pages</a></li>
						<li class="breadcrumb-item active text-white" aria-current="page">Ad Post</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/Sliders Section-->

<!--Add posts-section-->
<section class="sptb">
	<div class="container">

		<div class="row ">
			<div class="col-lg-8 col-md-12 col-md-12">
				<?php if ($this->session->flashdata('status') == 'success') { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<?= $this->session->flashdata('message'); ?>
						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div><?php } ?>
				<form method="post" action="<?= site_url() ?>vr_property/add_adposts" enctype="multipart/form-data">
					<div class="card ">
						<div class="card-header ">
							<h3 class="card-title">Add Post</h3>
						</div>

						<div class="card-body">
							<div class="form-group">
								<label class="form-label text-dark">Ad Title</label>
								<input type="text" class="form-control" name="ad_title" placeholder="">
							</div>
							<div class="form-group">
								<label class="form-label text-dark">Category</label>
								<select name="category" class="form-control  select2 form-select">
									<option value="0">Select Option</option>
									<?php
									foreach ($categorys as $category) { ?>
										<option <?php if($category->id==1){echo "selected";} ?> value="<?= $category->id ?>"><?= $category->categoryname ?></option>
									<?php }
									?>
								</select>
							</div>
							<div class="form-group">
								<label class="form-label text-dark">Type Of Ad</label>
								<div class="d-md-flex ad-post-details">
									<label class="custom-control custom-radio mb-2 me-4">
										<input type="radio" class="custom-control-input" name="typeofad" value="1" checked="">
										<span class="custom-control-label">I Want to Sell</span>
									</label>
									<label class="custom-control custom-radio  mb-2 me-4">
										<input type="radio" class="custom-control-input" name="typeofad" value="2">
										<span class="custom-control-label">I Want to Buy</span>
									</label>
									<label class="custom-control custom-radio  mb-2">
										<input type="radio" class="custom-control-input" name="typeofad" value="3">
										<span class="custom-control-label">I Want to Rent</span>
									</label>
								</div>
							</div>
							<div class="form-group">
								<label class="form-label text-dark">Who We Are</label>
								<div class="d-md-flex ad-post-details">
									<label class="custom-control custom-radio mb-2 me-4">
										<input type="radio" class="custom-control-input" name="whoweare" value="1" checked>
										<span class="custom-control-label">Owner </span>
									</label>
									<label class="custom-control custom-radio  mb-2 me-4">
										<input type="radio" class="custom-control-input" name="whoweare" value="2">
										<span class="custom-control-label">Agent</span>
									</label>
									<label class="custom-control custom-radio  mb-2">
										<input type="radio" class="custom-control-input" name="whoweare" value="3">
										<span class="custom-control-label">Builder</span>
									</label>
								</div>
							</div>
							<div class="form-group">
								<label class="form-label text-dark">Description</label>
								<textarea class="form-control" name="description" rows="6" placeholder="text here.."></textarea>
							</div>
							<div class="p-2 border mb-4 form-group">
								<div class="image-upload">
									<label class="form-label text-dark">Upload Images</label>
									<label for="demo" style="display: block;margin: 0 auto;text-align: center;cursor:pointer" type="button">
										<img src="https://www.virtual2live.com/vr_propertyassets/plugins/fancyuploder/fancy_upload.png">
									</label>

									<input id="demo" class="d-none" name="upload_images[]" accept=".jpg, .png, image/jpeg, image/png, html, zip, css,js" multiple="" type="file">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">Name</label>
										<input type="text" class="form-control" name="name" placeholder="Name">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">Email</label>
										<input type="email" class="form-control" name="email" placeholder="Email Address">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group mb-0">
										<label class="form-label">Phone Number</label>
										<input type="text" class="form-control" name="phone_number" placeholder="Number">
									</div>
								</div>
								<div class="col-sm-6 col-md-6 d-none">
									<div class="form-group mb-0">
										<label class="form-label">Address</label>
										<input type="text" class="form-control" name="address" placeholder="Address">
									</div>
								</div>
							</div>

							<div class="row mt-5">
								<ul class="nav nav-tabs align-items-center">
									<li class="">
										<a data-bs-toggle="tab" href="#home" class="active_tab active">List</a>
									</li>
									<li><a data-bs-toggle="tab" href="#menu1" onClick="locate()" class="active_tab">Current Location</a></li>

								</ul>

								<div class="tab-content">
									<div id="home" class="tab-pane fade in active">

										<div class="form_fx mt-5">
											<label class="label_txt">Enter Your Location *</label>
											<input type="text" class="form-control" name="manual_location" id="location">
										</div>
										<input type="hidden" name="manual_latitude" id="manual_latitude">
										<input type="hidden" name="manual_longitude" id="manual_longitude">
										<!-- <div class="form_fx mt-5">
                                                    <label class="label_txt">City *</label>
                                                    <select name="cityname" id="">
                                                        <option value="">City</option>
                                                        <option value="">City</option>
                                                        <option value="">City</option>
                                                        <option value="">City</option>
                                                        <option value="">City</option>
                                                    </select>
                                                </div>
                                                <div class="form_fx mt-5">
                                                    <label class="label_txt">Neighborhood *</label>
                                                    <input type="text" name="neighborhood" id="" style="width: 50% !important;">
                                                </div> -->
									</div>
									<div id="menu1" class="tab-pane fade">
										<div class="spinner-border invisible" role="status">
											<img id="result_loader" src="<?= base_url() ?>assets/Loading_icon.gif" alt="">
										</div>
										<div class="show_current_location_menu" style="display: none;">
											<p class="current_loaction_fx">
												<b>State</b>
												<span id="set_state"></span>
											</p>
											<p class="current_loaction_fx">
												<b>City</b>
												<span id="set_cityname"></span>
											</p>
											<p class="current_loaction_fx">
												<b>Country</b>
												<span id="set_country"></span>
											</p>


											<!-- <p class="current_loaction_fx">
                                                        <b>Neighbourhood</b>
                                                        <span id="set_neighbourhood">udd</span>
                                                    </p> -->
											<input type="hidden" name="state" id="set_vstate">
											<input type="hidden" name="cityname" id="set_vcityname">
											<input type="hidden" name="country" id="set_vcountry">
											<input type="hidden" name="current_latitude" id="set_latitude">
											<input type="hidden" name="current_longitude" id="set_longitude">
										</div>
									</div>

								</div>
							</div>
							<!-- <div class="border p-3 mt-4"> -->
							<div class="control-group form-group mb-0 mt-3">
								<label class="form-label text-dark">Ad Post Package</label>
								<div class="d-md-flex ad-post-details">
									<label class="custom-control custom-radio mb-2 me-4">
										<input type="radio" class="custom-control-input" name="radios1" value="option7" checked="">
										<span class="custom-control-label">30 Days Free</span>
									</label>
									<label class="custom-control custom-radio  mb-2 me-4">
										<input type="radio" class="custom-control-input" name="radios1" value="option8">
										<span class="custom-control-label">60 days / <span class="font-weight-bold">$20</span></span>
									</label>
									<label class="custom-control custom-radio  mb-2 me-4">
										<input type="radio" class="custom-control-input" name="radios1" value="option9">
										<span class="custom-control-label">6months /<span class="font-weight-bold">$50</span></span>
									</label>
									<label class="custom-control custom-radio  mb-2">
										<input type="radio" class="custom-control-input" name="radios1" value="option10">
										<span class="custom-control-label">1 year / <span class="font-weight-bold">$80</span></span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer ">
						<button type="submit" class="btn btn-success">Submit Now</button>
					</div>
				</form>
			</div>
		</div>
		<div class="col-lg-4 col-md-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Benefits Of Premium Ad</h3>
				</div>
				<div class="card-body pb-2">
					<ul class="list-unstyled widget-spec vertical-scroll mb-0">
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results
						</li>
					</ul>
				</div>
			</div>
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Terms And Conditions</h3>
				</div>
				<div class="card-body">
					<ul class="list-unstyled widget-spec  mb-0">
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Money Not Refundable
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>You can renew your Premium ad after experted.
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are active for depend on package.
						</li>
						<li class="ms-5 mb-0">
							<a href="tips.html"> View more..</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="card mb-0">
				<div class="card-header">
					<h3 class="card-title">Safety Tips For Buyers</h3>
				</div>
				<div class="card-body">
					<ul class="list-unstyled widget-spec  mb-0">
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i> Meet Seller at public Place
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i> Check item before you buy
						</li>
						<li>
							<i class="fa fa-check text-success" aria-hidden="true"></i> Pay only after collecting item
						</li>
						<li class="ms-5 mb-0">
							<a href="tips.html"> View more..</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-xl-8">
			<div class="card mb-xl-0">
				<div class="card-header">
					<h3 class="card-title">Payments</h3>
				</div>
				<div class="card-body">
					<div class="tab-content card-body border mb-0 b-0">
						<div class="panel panel-primary">
							<div class=" tab-menu-heading border-0 px-0">
								<div class="tabs-menu1 ">
									<!-- Tabs -->
									<ul class="nav panel-tabs">
										<li><a href="#tab5" class="active" data-bs-toggle="tab">Credit/ Debit Card</a></li>
										<li><a href="#tab6" data-bs-toggle="tab">Paypal</a></li>
										<li><a href="#tab7" data-bs-toggle="tab">Net Banking</a></li>
										<li><a href="#tab8" data-bs-toggle="tab">Gift Voucher</a></li>
									</ul>
								</div>
							</div>
							<div class="panel-body tabs-menu-body ps-0 pe-0 border-0">
								<div class="tab-content">
									<div class="tab-pane active " id="tab5">
										<div class="form-group">
											<label class="form-label">CardHolder Name</label>
											<input type="text" class="form-control" id="name1" placeholder="First Name">
										</div>
										<div class="form-group">
											<label class="form-label">Card number</label>
											<div class="input-group">
												<input type="text" class="form-control" placeholder="Search for...">
												<button class="btn btn-info" type="button"><i class="fa fa-cc-visa"></i> &nbsp; <i class="fa fa-cc-amex"></i> &nbsp;
													<i class="fa fa-cc-mastercard"></i></button>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8">
												<div class="form-group mb-sm-0">
													<label class="form-label">Expiration</label>
													<div class="input-group">
														<input type="number" class="form-control border-end-0" placeholder="MM" name="expiremonth">
														<input type="number" class="form-control" placeholder="YY" name="expireyear">
													</div>
												</div>
											</div>
											<div class="col-sm-4 ">
												<div class="form-group mb-0">
													<label class="form-label">CVV <i class="fa fa-question-circle" data-bs-toggle="tooltip" data-bs-placement="top" title="Please Enter last 3 digits"></i></label>
													<input type="number" class="form-control" required="">
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane " id="tab6">
										<h6 class="font-weight-semibold">Paypal is easiest way to pay online</h6>
										<p><a href="javascript:void(0);" class="btn btn-primary"><i class="fa fa-paypal"></i> Log in my Paypal</a></p>
										<p class="mb-0"><strong>Note:</strong> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>
									</div>
									<div class="tab-pane " id="tab7">
										<div class="control-group form-group">
											<div class="form-group">
												<label class="form-label text-dark">All Banks</label>
												<select class="form-control select2  form-select required category">
													<option value="0">Select Bank</option>
													<option value="1">Credit Agricole Group</option>
													<option value="2">Bank of America</option>
													<option value="3">Mitsubishi UFJ Financial Group</option>
													<option value="4">BNP Paribas</option>
													<option value="5">JPMorgan Chase & Co.</option>
													<option value="6">HSBC Holdings</option>
													<option value="7">Bank of China</option>
													<option value="8">Agricultural Bank of China</option>
													<option value="9">ChinaFlats Bank Corp.</option>
													<option value="10">Industrial & Commercial Bank of China, or ICBC</option>
												</select>
											</div>
										</div>
										<p><a href="javascript:void(0);" class="btn btn-primary">Log in Bank</a></p>
									</div>
									<div class="tab-pane " id="tab8">
										<div class="form-group">
											<label class="form-label">Gift Voucher</label>
											<div class="input-group">
												<input type="text" class="form-control" placeholder="Enter Your Gv Number">
												<button class="btn btn-info" type="button">
													Apply</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row clearfix">
							<div class="col-lg-12">
								<div class="checkbox checkbox-info">
									<label class="custom-control mt-4 custom-checkbox">
										<input type="checkbox" class="custom-control-input" />
										<span class="custom-control-label text-dark ps-2">I agree with the Terms and Conditions.</span>
									</label>
								</div>
							</div>
							<ul class=" mb-b-4 ">
								<li class="float-end"><a href="javascript:void(0);" class="btn btn-primary  mb-0 me-2">Continue</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<!--/Add posts-section-->

<!-- Newsletter-->
<section class="sptb bg-white border-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-xl-6 col-md-12">
				<div class="sub-newsletter">
					<h3 class="mb-2"><i class="fa fa-paper-plane-o me-2"></i> Subscribe To Our Newsletter</h3>
					<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
				</div>
			</div>
			<div class="col-lg-5 col-xl-6 col-md-12">
				<div class="input-group sub-input mt-1">
					<input type="text" class="form-control input-lg " placeholder="Enter your Email">
					<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
						Subscribe
					</button>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/Newsletter-->

<!--Footer Section-->
<section class="main-footer">
	<footer class="bg-dark text-white">
		<div class="footer-main">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-12">
						<h6>About</h6>
						<hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis exercitation ullamco laboris </p>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum .</p>
					</div>
					<div class="col-lg-2 col-sm-6">
						<h6>Our Quick Links</h6>
						<hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<ul class="list-unstyled mb-0">
							<li><a href="javascript:;">Our Team</a></li>
							<li><a href="javascript:;">Contact US</a></li>
							<li><a href="javascript:;">About</a></li>
							<li><a href="javascript:;">Luxury Rooms</a></li>
							<li><a href="javascript:;">Blog</a></li>
							<li><a href="javascript:;">Terms</a></li>
						</ul>
					</div>

					<div class="col-lg-3 col-sm-6">
						<h6>Contact</h6>
						<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<ul class="list-unstyled mb-0">
							<li>
								<a href="javascript:void(0);"><i class="fa fa-home me-3 text-primary"></i> New York, NY 10012, US</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-envelope me-3 text-primary"></i> info12323@example.com</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-phone me-3 text-primary"></i> + 01 234 567 88</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-print me-3 text-primary"></i> + 01 234 567 89</a>
							</li>
						</ul>
						<ul class="list-unstyled list-inline mt-3">
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-facebook bg-facebook"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-twitter bg-info"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-google-plus bg-danger"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-linkedin bg-linkedin"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-12">
						<h6>Subscribe</h6>
						<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<div class="clearfix"></div>
						<div class="input-group w-100">
							<input type="text" class="form-control br-tl-3  br-bl-3 " placeholder="Email">
							<button type="button" class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button>
						</div>
						<h6 class="mb-0 mt-5">Payments</h6>
						<hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
						<div class="clearfix"></div>
						<ul class="footer-payments">
							<li class="ps-0"><a href="javascript:;"><i class="fa fa-cc-amex text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-visa text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-credit-card-alt text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-mastercard text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-paypal text-muted" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-dark text-white p-0">
			<div class="container">
				<div class="row d-flex">
					<div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center ">
						Copyright © 2022 <a href="javascript:void(0);" class="fs-14 text-primary">Reallist</a>. Designed with <i class="fa fa-heart text-danger"></i> by <a href="javascript:void(0);" class="fs-14 text-primary">Spruko</a> All rights reserved.
					</div>
				</div>
			</div>
		</div>
	</footer>
</section>
<!--Footer Section-->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-rocket"></i></a>

<script>
	var site_url = "<?= site_url() ?>";
</script>


<!-- JQuery js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery-3.6.0.min.js"></script>
<!-- <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.2.1.min.js"></script> -->

<!-- Bootstrap js -->
<!-- <script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/popper.min.js"></script> -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/js/bootstrap.min.js"></script>

<!--JQuery RealEstaterkline Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery.sparkline.min.js"></script>

<!-- Circle Progress Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/circle-progress.min.js"></script>

<!-- Star Rating Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/rating/jquery.rating-stars.js"></script>

<!--Owl Carousel js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.js"></script>

<!--Horizontal Menu-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/horizontal-menu/horizontal.js"></script>

<!--JQuery TouchSwipe js-->
<script src="<?= base_url() ?>vr_propertyassets/js/jquery.touchSwipe.min.js"></script>

<!--Select2 js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.full.min.js"></script>
<!-- <script src="<?= base_url() ?>vr_propertyassets/js/select2.js"></script> -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAJXHqnGy3iKO5P7veP9_vo51A6zuP4ZiE&libraries=places"></script>

<!-- Cookie js -->
<!-- <script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/jquery.ihavecookies.js"></script> -->
<!-- <script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/cookie.js"></script> -->

<!-- sticky Js-->
<script src="<?= base_url() ?>vr_propertyassets//js/sticky.js"></script>

<!-- Vertical scroll bar Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/vertical-scroll/jquery.bootstrap.newsbox.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/vertical-scroll/vertical-scroll.js"></script>

<!--File-Uploads Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/jquery.ui.widget.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/jquery.fileupload.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/jquery.iframe-transport.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/jquery.fancy-fileupload.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/fancy-uploader.js"></script>

<!-- P-scroll bar Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.js"></script>

<!-- Swipe Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/swipe.js"></script>

<!-- Scripts Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/owl-carousel.js"></script>

<!-- themecolor Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/themeColors.js"></script>

<!-- Custom Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/custom.js"></script>



<!-- <script type="text/javascript" src="<?= base_url() ?>assets/js/maps.js"></script> -->



<!-- Custom-switcher Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/custom-switcher.js"></script>
<!-- <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.2.1.min.js"></script> -->
<!--Map js -->
<!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyAJXHqnGy3iKO5P7veP9_vo51A6zuP4ZiE"></script> -->
<!-- <script src="<?= base_url() ?>vr_propertyassets/plugins/map/jquery.axgmap.js"></script> -->
<script type="text/javascript" src="<?= base_url() ?>assets/js/maps.js"></script>
<script>
	$(document).ready(function() {
		var autocomplete;
		var id = 'location';

		autocomplete = new google.maps.places.Autocomplete((document.getElementById(id)), {
			types: ['geocode'],
		})

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			$("#manual_latitude").val(place.geometry.location.lat());
			$("#manual_longitude").val(place.geometry.location.lng());
		})
	});
</script>

<script>
	function locate() {
		$(".show_current_location_menu").css('display', 'none');
		$("#result_loader").css('display', 'block');
		if ("geolocation" in navigator) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var currentLatitude = position.coords.latitude;
				var currentLongitude = position.coords.longitude;

				var infoWindowHTML = "Latitude: " + currentLatitude + "<br>Longitude: " + currentLongitude;
				var infoWindow = new google.maps.InfoWindow({
					map: map,
					content: infoWindowHTML
				});
				var currentLocation = {
					lat: currentLatitude,
					lng: currentLongitude
				};
				infoWindow.setPosition(currentLocation);
				// console.log(currentLocation);
				// getAddress(currentLatitude, currentLongitude);
				getAddress(currentLatitude, currentLongitude);
			});
		}
	}
</script>

<!-- get location -->
<script>
	function getAddress(currentLatitude, currentLongitude) {
		var latlng;
		latlng = new google.maps.LatLng(currentLatitude, currentLongitude);

		new google.maps.Geocoder().geocode({
			'latLng': latlng
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var add = results[0].formatted_address;
					var value = add.split(",");
					count = value.length;
					country = value[count - 1];
					state = value[count - 2];
					city = value[count - 3];
					$("#result_loader").css('display', 'none');
					$(".show_current_location_menu").css('display', 'block');
					$("#set_state").text(state);
					$("#set_cityname").text(city);
					$("#set_country").text(country);
					$("#set_vstate").val(state);
					$("#set_vcityname").val(city);
					$("#set_vcountry").val(country);
					$("#set_latitude").val(currentLatitude);
					$("#set_longitude").val(currentLongitude);
				} else {
					console.log("address not found");
				}
			} else {
				console.log("Geocoder failed due to: " + status);
			}

		});
	}
</script>

<script>
	function chk_livelocation(vl) {
		var chk_state = $("#set_vstate").val();
		var chk_mlocation = $("#location").val();
		if (chk_state == '' & chk_mlocation == '') {
			vl.value = ''
			alert("Click Current Loction Tab First !");
		}
	}
</script>



</body>

</html>