<?php $this->load->view('template/header');
?>

<style>
	ul.list_group.mb-0 {
		display: flex;
		align-items: center;
		padding: 2rem;
		gap: 0.5rem;
	}

	li.list-group_item {
		width: 200px;
		height: 50px;
		display: flex;
		justify-content: center;
		align-items: center;
		border: 1px solid #ebdfdf;
		border-radius: 3px;
		text-align: center;
		font-size: 12px;
		padding: 1rem;
		box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
	}
</style>
<!--Map-->
<div class="d-lg-flex">
	<div class="map-width">
	<div class="axgmap" data-latlng="20.593683,78.962883" data-zoom="10">
		<?php
		if ($adposts != false) {
			// echo "<pre>";
			// print_r ($adposts);
			// echo "</pre>";die;				
			foreach ($adposts as $adpost) {
				echo '<div class="axgmap-img" data-latlng="' . $adpost->current_latitude . ',' . $adpost->current_longitude . '" data-marker-image="' . base_url() . 'vr_propertyassets/images/map/1.png" data-title="' . $adpost->product_name . '">
				<h4>' . $adpost->categoryname . '</h4>
				<img src="' . base_url() . "property_adpostimages/" . $adpost->id . "/images/" . $adpost->images . '" alt="img" class="w-150 h100 mb-3 mt-2">
				<div>Price: <a class="h4"> ₹' . $adpost->price . '</a></div>
				<div class="rating-stars block mt-2 mb-2">
					<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
					<div class="rating-stars-container">
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
						<div class="rating-star sm">
							<i class="fa fa-star"></i>
						</div>
					</div>
				</div>
				<p class="fs-16"><i class="fa fa-phone me-2"></i> ' . $adpost->mobile . '</p>
				<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
				</div>';
			}
		}
		?>
	</div>
	</div>
	<div class="map-content-width vscroll card mb-0 br-0 mh-500">
		<div class="p-4">
			<div class="card">
				<div class="card-body p-3">
					<div class="header-text mb-0 map-input-group">
						<div class="container">
							<div class="row">
								<div class="col-xl-12 col-lg-12 col-md-12 d-block mx-auto">
									<div class="bg-transparent">
										<div class="form row no-gutters ">
											<div class="form-group  col-xl-4 col-lg-3 col-md-12 mb-0">
												<select class="form-control select2-show-search w-100" data-placeholder="Select">
													<optgroup label="Categories">
														<option>Property type</option>
														<option value="1">Houses, Apartment Flats, Shops, Commercial Lands</option>
														<option value="2">Plots, Agri Lands </option>
														<option value="3">Paying Guest, Hostel, Girls Hostel, Manson</option>
														<option value="4">Hotel, Resorts, Rooms / Lodge</option>
														<option value="5">Party Hall, Trekking</option>
														<option value="6">Other Services</option>

													</optgroup>
												</select>
											</div>
											<div class="form-group  col-xl-3 col-lg-3 col-md-12 mb-0">
												<input type="text" class="form-control input-lg br-md-0" id="text5" placeholder="Enter Location">
												<span><i class="fa fa-map-marker location-gps me-1"></i> </span>
											</div>
											<div class="form-group col-xl-3 col-lg-3 col-md-12 select2-lg mb-0">
												<select class="form-control select2-show-search w-100" data-placeholder="Select">
													<optgroup label="Categories">

														<option value="1">Rent</option>
														<option value="2">Buy</option>
														<option value="3">Sell</option>
														<option value="3">Lease</option>

													</optgroup>
												</select>
											</div>
											<div class="col-xl-2 col-lg-3 col-md-12 mb-0">
												<a href="javascript:void(0);" class="btn btn-lg btn-block btn-primary br-tl-md-0 br-bl-md-0"><i class="fa fa-search"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!-- /header-text -->
				</div>
			</div>
			<div class="card overflow-hidden">
				<div class="d-md-flex">
					<div class="item-card9-img">
						<div class="arrow-ribbon bg-primary">$263.99</div>
						<div class="item-card9-imgs">
							<a href="col-left.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h4.png" alt="img" class="cover-image products-1">
						</div>
						<div class="item-card9-icons">
							<a href="javascript:void(0);" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="item-tags">
							<div class="bg-success tag-option">For Sale </div>
						</div>
						<div class="item-trans-rating">
							<div class="rating-stars block">
								<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
								<div class="rating-stars-container">
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card border-0 mb-0">
						<div class="card-body ">
							<div class="item-card9">
								<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> Villa</a>
								<a href="col-left.html" class="text-dark">
									<h4 class="font-weight-bold mt-1">2BHK flat </h4>
								</a>
								<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
							</div>
						</div>
						<div class="card-footer pt-4 pb-4">
							<div class="item-card9-footer d-flex">
								<div class="item-card9-cost">
									<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
								</div>
								<div class="ms-auto">
									<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 2 days ago</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card overflow-hidden">
				<div class="d-md-flex">
					<div class="item-card9-img">
						<div class="arrow-ribbon bg-secondary">$987.88</div>
						<div class="item-card9-imgs">
							<a href="col-left.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j2.png" alt="img" class="cover-image products-1">
						</div>
						<div class="item-card9-icons">
							<a href="javascript:void(0);" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="item-tags">
							<div class="bg-success tag-option">For Sale </div>
						</div>
						<div class="item-trans-rating">
							<div class="rating-stars block">
								<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="2">
								<div class="rating-stars-container">
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card border-0 mb-0">
						<div class="card-body ">
							<div class="item-card9">
								<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> Deluxe House</a>
								<a href="col-left.html" class="text-dark">
									<h4 class="font-weight-bold mt-1">Luxury Home For Sale</h4>
								</a>
								<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
							</div>
						</div>
						<div class="card-footer pt-4 pb-4">
							<div class="item-card9-footer d-flex">
								<div class="item-card9-cost">
									<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
								</div>
								<div class="ms-auto">
									<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 15 mins ago</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card overflow-hidden">
				<div class="d-md-flex">
					<div class="item-card9-img">
						<div class="arrow-ribbon bg-success">$567</div>
						<div class="item-card9-imgs">
							<a href="col-left.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/pe1.png" alt="img" class="cover-image products-1">
						</div>
						<div class="item-card9-icons">
							<a href="javascript:void(0);" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="item-tags">
							<div class="bg-danger tag-option">For Buy </div>
						</div>
						<div class="item-trans-rating">
							<div class="rating-stars block">
								<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
								<div class="rating-stars-container">
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card border-0 mb-0">
						<div class="card-body ">
							<div class="item-card9">
								<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> 3BHK Flats</a>
								<a href="col-left.html" class="text-dark">
									<h4 class="font-weight-bold mt-1">Apartment For Rent </h4>
								</a>
								<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
							</div>
						</div>
						<div class="card-footer pt-4 pb-4">
							<div class="item-card9-footer d-flex">
								<div class="item-card9-cost">
									<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
								</div>
								<div class="ms-auto">
									<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 1 days ago</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card overflow-hidden">
				<div class="d-md-flex">
					<div class="item-card9-img">
						<div class="arrow-ribbon bg-pink">$567</div>
						<div class="item-card9-imgs">
							<a href="col-left.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image products-1">
						</div>
						<div class="item-card9-icons">
							<a href="javascript:void(0);" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="item-tags">
							<div class="bg-success tag-option">For Sale </div>
						</div>
						<div class="item-trans-rating">
							<div class="rating-stars block">
								<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
								<div class="rating-stars-container">
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card border-0 mb-0">
						<div class="card-body ">
							<div class="item-card9">
								<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> Office</a>
								<a href="col-left.html" class="text-dark">
									<h4 class="font-weight-bold mt-1"> Office rooms.... </h4>
								</a>
								<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
							</div>
						</div>
						<div class="card-footer pt-4 pb-4">
							<div class="item-card9-footer d-flex">
								<div class="item-card9-cost">
									<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
								</div>
								<div class="ms-auto">
									<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 35 mins ago</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card overflow-hidden">
				<div class="d-md-flex">
					<div class="item-card9-img">
						<div class="arrow-ribbon bg-primary">$839</div>
						<div class="item-card9-imgs">
							<a href="col-left.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image products-1">
						</div>
						<div class="item-card9-icons">
							<a href="javascript:void(0);" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="item-tags">
							<div class="bg-info tag-option">For Rent </div>
						</div>
						<div class="item-trans-rating">
							<div class="rating-stars block">
								<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
								<div class="rating-stars-container">
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card border-0 mb-0">
						<div class="card-body ">
							<div class="item-card9">
								<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> Luxury rooms</a>
								<a href="col-left.html" class="text-dark">
									<h4 class="font-weight-bold mt-1">Apartment For Rent</h4>
								</a>
								<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
							</div>
						</div>
						<div class="card-footer pt-4 pb-4">
							<div class="item-card9-footer d-flex">
								<div class="item-card9-cost">
									<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
								</div>
								<div class="ms-auto">
									<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 5 days ago</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card overflow-hidden mb-0">
				<div class="d-md-flex">
					<div class="item-card9-img">
						<div class="arrow-ribbon bg-secondary">$289</div>
						<div class="item-card9-imgs">
							<a href="col-left.html"></a>
							<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="cover-image products-1">
						</div>
						<div class="item-card9-icons">
							<a href="javascript:void(0);" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a>
						</div>
						<div class="item-tags">
							<div class="bg-success tag-option">For Sale </div>
						</div>
						<div class="item-trans-rating">
							<div class="rating-stars block">
								<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
								<div class="rating-stars-container">
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm  ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
									<div class="rating-star sm ">
										<i class="fa fa-star"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card border-0 mb-0">
						<div class="card-body ">
							<div class="item-card9">
								<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> Apartments</a>
								<a href="col-left.html" class="text-dark">
									<h4 class="font-weight-bold mt-1">Apartment For Rent </h4>
								</a>
								<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
							</div>
						</div>
						<div class="card-footer pt-4 pb-4">
							<div class="item-card9-footer d-flex">
								<div class="item-card9-cost">
									<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
								</div>
								<div class="ms-auto">
									<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 3 days ago</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--/Map-->

<!--Sliders Section-->

<!--/Sliders Section-->

<!--Breadcrumb-->
<div class="border-bottom bg-white">
	<div class="container">
		<div class="page-header bg-transparent">
			<h4 class="page-title">RealEstate list</h4>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
				<li class="breadcrumb-item"><a href="javascript:void(0);">Pages</a></li>
				<li class="breadcrumb-item active" aria-current="page">RealEstate list</li>
			</ol>
		</div>
	</div>
</div>
<!--/Breadcrumb-->
<!-- category card start -->
<div class="container my-3">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Categories</h3>
		</div>
		<div class="card-body p-0">
			<div class="list-catergory">
				<div class="item-list">
					<ul class="list_group mb-0">
						<li class="list-group_item">
							<a href="javascript:void(0);" class="text-dark">

								<span>Houses, Apartment Flats, Shops, Commercial Lands</span>

							</a>
						</li>
						<li class="list-group_item">
							<a href="javascript:void(0);" class="text-dark">

								<span>Plots, Agri Lands</span>

							</a>
						</li>
						<li class="list-group_item">
							<a href="javascript:void(0);" class="text-dark">

								<span>Paying Guest, Hostel, Girls Hostel, Manson</span>

							</a>
						</li>
						<li class="list-group_item">
							<a href="javascript:void(0);" class="text-dark">

								<span>Hotel, Resorts, Rooms / Lodge</span>

							</a>
						</li>
						<li class="list-group_item">
							<a href="javascript:void(0);" class="text-dark">

								<span>Party Hall, Trekking</span>

							</a>
						</li>
						<li class="list-group_item">
							<a href="javascript:void(0);" class="text-dark">

								<span>Others services</span>

							</a>
						</li>

					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- category card end -->

<!--Add listing-->
<section class="sptb pt-0">
	<div class="container">
		<div class="row">
			<div class="col-xl-9 col-lg-8 col-md-12">
				<!--Add lists-->
				<div class=" mb-lg-0">
					<div class="">
						<div class="item2-gl ">
							<div class=" mb-0">
								<div class="">
									<div class="p-5 bg-white item2-gl-nav d-flex border br-5">
										<h6 class="mb-0 mt-2">Showing 1 to 10 of 30 entries</h6>
										<ul class="nav item2-gl-menu ms-auto mt-2">
											<li class=""><a href="#tab-11" class="active show" data-bs-toggle="tab" title="List style"><i class="fa fa-list"></i></a></li>
											<li><a href="#tab-12" data-bs-toggle="tab" class="" title="Grid"><i class="fa fa-th"></i></a></li>
										</ul>
										<div class="d-flex">
											<label class="me-2 mt-1 mb-sm-1 pt-2">Sort By:</label>
											<select name="item" class="form-control select-sm w-75 select2">
												<option value="1">Latest</option>
												<option value="2">Oldest</option>
												<option value="3">Price:Low-to-High</option>
												<option value="5">Price:Hight-to-Low</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-content">
								<div class="tab-pane active" id="tab-11">
									<div class="card overflow-hidden">
										<div class="d-md-flex">
											<div class="item-card9-img">
												<div class="arrow-ribbon bg-primary">$263.99</div>
												<div class="item-card9-imgs">
													<a href="col-left.html"></a>
													<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h4.png" alt="img" class="cover-image">
												</div>
												<div class="item-card9-icons">
													<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
													<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
												</div>
												<div class="item-tags">
													<div class="bg-success tag-option">For Sale </div>
													<div class="bg-pink tag-option">Open </div>
												</div>
												<div class="item-trans-rating">
													<div class="rating-stars block">
														<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
														<div class="rating-stars-container">
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card border-0 mb-0">
												<div class="card-body ">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Villa</a>
														<a href="col-left.html" class="text-dark">
															<h4 class="font-weight-bold mt-1">2BHK flat </h4>
														</a>
														<div class="mb-2">
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i> 950 Sqft</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a>
															<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a>
														</div>
													</div>
												</div>
												<div class="card-footer pt-4 pb-4">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i> Owner</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 2 days ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="card overflow-hidden">
										<div class="d-md-flex">
											<div class="item-card9-img">
												<div class="arrow-ribbon bg-secondary">$987.88</div>
												<div class="item-card9-imgs">
													<a href="col-left.html"></a>
													<div id="carousel-controls1" class="carousel slide property-slide" data-bs-ride="carousel" data-interval="false">
														<div class="carousel-inner br-0">
															<div class="carousel-item active">
																<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
															</div>
															<div class="carousel-item">
																<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
															</div>
															<div class="carousel-item">
																<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
															</div>
															<div class="carousel-item">
																<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
															</div>
															<div class="carousel-item">
																<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/e1.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
															</div>
														</div>
														<a class="carousel-control-prev" href="#carousel-controls1" role="button" data-bs-slide="prev">
															<span class="carousel-control-prev-icon" aria-hidden="true"></span>
															<span class="sr-only">Previous</span>
														</a>
														<a class="carousel-control-next" href="#carousel-controls1" role="button" data-bs-slide="next">
															<span class="carousel-control-next-icon" aria-hidden="true"></span>
															<span class="sr-only">Next</span>
														</a>
													</div>
												</div>
												<div class="item-card9-icons">
													<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
													<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
												</div>
												<div class="item-tags">
													<div class="bg-success tag-option">For Sale </div>
												</div>
												<div class="item-trans-rating">
													<div class="rating-stars block">
														<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="2">
														<div class="rating-stars-container">
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card border-0 mb-0">
												<div class="card-body ">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> Deluxe House</a>
														<a href="col-left.html" class="text-dark">
															<h4 class="font-weight-bold mt-1">Luxury Home For Sale</h4>
														</a>
														<div class="mb-2">
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i> 950 Sqft</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a>
															<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a>
														</div>
													</div>
												</div>
												<div class="card-footer pt-4 pb-4">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i> Agent</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 15 mins ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sold-out1">
										<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span></div>
										<div class="card overflow-hidden">
											<div class="d-md-flex">
												<div class="item-card9-img">
													<div class="arrow-ribbon bg-success">$567</div>
													<div class="item-card9-imgs">
														<a href="col-left.html"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/pe1.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-danger tag-option">For Buy </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
															<div class="rating-stars-container">
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm  ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm ">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card border-0 mb-0">
													<div class="card-body ">
														<div class="item-card9">
															<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> 3BHK Flats</a>
															<a href="col-left.html" class="text-dark">
																<h4 class="font-weight-bold mt-1">Apartment For Rent </h4>
															</a>
															<div class="mb-2">
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i> 400 Sqft</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 3 Beds</a>
																<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a>
																<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1 Car</a>
															</div>
														</div>
													</div>
													<div class="card-footer pt-4 pb-4">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i> Owner</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 1 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="card overflow-hidden">
										<div class="d-md-flex">
											<div class="item-card9-img">
												<div class="arrow-ribbon bg-pink">$567</div>
												<div class="item-card9-imgs">
													<a href="col-left.html"></a>
													<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image">
												</div>
												<div class="item-card9-icons">
													<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
													<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
												</div>
												<div class="item-tags">
													<div class="bg-success tag-option">For Sale </div>
													<div class="bg-pink tag-option">Hot </div>
												</div>
												<div class="item-trans-rating">
													<div class="rating-stars block">
														<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
														<div class="rating-stars-container">
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card border-0 mb-0">
												<div class="card-body ">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> Office</a>
														<a href="col-left.html" class="text-dark">
															<h4 class="font-weight-bold mt-1"> Office rooms.... </h4>
														</a>
														<div class="mb-2">
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i> 1500 Sqft</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 5 Beds</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a>
															<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a>
														</div>
													</div>
												</div>
												<div class="card-footer pt-4 pb-4">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i> Agent</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 35 mins ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="card overflow-hidden">
										<div class="d-md-flex">
											<div class="item-card9-img">
												<div class="arrow-ribbon bg-primary">$839</div>
												<div class="item-card9-imgs">
													<a href="col-left.html"></a>
													<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image">
												</div>
												<div class="item-card9-icons">
													<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
													<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
												</div>
												<div class="item-tags">
													<div class="bg-info tag-option">For Rent </div>
												</div>
												<div class="item-trans-rating">
													<div class="rating-stars block">
														<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
														<div class="rating-stars-container">
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card border-0 mb-0">
												<div class="card-body ">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> Luxury rooms</a>
														<a href="col-left.html" class="text-dark">
															<h4 class="font-weight-bold mt-1">Apartment For Rent</h4>
														</a>
														<div class="mb-2">
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i> 300 Sqft</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 2 Beds</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 2 Bath</a>
															<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1 Car</a>
														</div>
													</div>
												</div>
												<div class="card-footer pt-4 pb-4">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i> Agent</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 5 days ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="card overflow-hidden">
										<div class="d-md-flex">
											<div class="item-card9-img">
												<div class="arrow-ribbon bg-secondary">$289</div>
												<div class="item-card9-imgs">
													<a href="col-left.html"></a>
													<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="cover-image">
												</div>
												<div class="item-card9-icons">
													<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
													<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
												</div>
												<div class="item-tags">
													<div class="bg-success tag-option">For Sale </div>
													<div class="bg-pink tag-option">New</div>
												</div>
												<div class="item-trans-rating">
													<div class="rating-stars block">
														<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
														<div class="rating-stars-container">
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm  ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
															<div class="rating-star sm ">
																<i class="fa fa-star"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card border-0 mb-0">
												<div class="card-body ">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted"><i class="fa fa-tag me-1"></i> Apartments</a>
														<a href="col-left.html" class="text-dark">
															<h4 class="font-weight-bold mt-1">Apartment For Rent </h4>
														</a>
														<div class="mb-2">
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-arrows-alt text-muted me-1"></i> 2500 Sqft</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bed text-muted me-1"></i> 20 Beds</a>
															<a href="javascript:void(0);" class="icons text-muted me-4"><i class="fa fa-bath text-muted me-1"></i> 15 Bath</a>
															<a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 10 Car</a>
														</div>
													</div>
												</div>
												<div class="card-footer pt-4 pb-4">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-user text-muted me-1"></i> Owner</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 3 days ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab-12">
									<div class="row">
										<div class="col-lg-6 col-md-12 col-xl-4">
											<div class="card overflow-hidden">
												<div class="item-card9-img">
													<div class="arrow-ribbon bg-primary">$263.99</div>
													<div class="item-card9-imgs">
														<a href="col-left.html"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h4.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-success tag-option">For Sale </div>
														<div class="bg-pink tag-option">Open </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
															<div class="rating-stars-container">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card-body">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Villa</a>
														<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i> Owner</span></a>
														<a href="col-left.html" class="text-dark mt-2">
															<h4 class="font-weight-bold mt-1">2BHK flat </h4>
														</a>
														<ul class="item-card2-list">
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i> 700 Sqft</a></li>
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 5 Beds</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 4 Bath</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a></li>
														</ul>
														<p class="mb-0">Ut enim ad minima veniamq nostrum exerci </p>
													</div>
												</div>
												<div class="card-footer">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 2 days ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-12 col-xl-4">
											<div class="card overflow-hidden">
												<div class="arrow-ribbon bg-secondary">$987.88</div>
												<div class="item-card9-img">
													<div class="item-card9-imgs">
														<a href="col-left.html"></a>
														<div id="carousel-controls" class="carousel slide property-slide" data-bs-ride="carousel" data-interval="false">
															<div class="carousel-inner br-0">
																<div class="carousel-item active">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
																<div class="carousel-item">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
																<div class="carousel-item">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
																<div class="carousel-item">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
																<div class="carousel-item">
																	<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/e1.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																</div>
															</div>
															<a class="carousel-control-prev" href="#carousel-controls" role="button" data-bs-slide="prev">
																<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																<span class="sr-only">Previous</span>
															</a>
															<a class="carousel-control-next" href="#carousel-controls" role="button" data-bs-slide="next">
																<span class="carousel-control-next-icon" aria-hidden="true"></span>
																<span class="sr-only">Next</span>
															</a>
														</div>
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-success tag-option">For Sale </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="2">
															<div class="rating-stars-container">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card-body">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Deluxe House</a>
														<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i> Agent</span></a>
														<a href="col-left.html" class="text-dark mt-2">
															<h4 class="font-weight-bold mt-1">Luxury Home For Sale</h4>
														</a>
														<ul class="item-card2-list">
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i> 950 Sqft</a></li>
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a></li>
														</ul>
														<p class="mb-0">Ut enim ad minima veniamq nostrum exerci ullam</p>
													</div>
												</div>
												<div class="card-footer">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 15 mins ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-12 col-xl-4">
											<div class="sold-out1">
												<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span></div>
												<div class="card overflow-hidden">
													<div class="arrow-ribbon bg-success">$567</div>
													<div class="item-card9-img">
														<div class="item-card9-imgs">
															<a href="col-left.html"></a>
															<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/pe1.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-danger tag-option">For Buy </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
																<div class="rating-stars-container">
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body">
														<div class="item-card9">
															<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> 3BHK Flats</a>
															<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i> Owner</span></a>
															<a href="col-left.html" class="text-dark mt-2">
																<h4 class="font-weight-bold mt-1">Apartment For Rent</h4>
															</a>
															<ul class="item-card2-list">
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i> 400 Sqft</a></li>
																<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 3 Beds</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
																<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
															</ul>
															<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
														</div>
													</div>
													<div class="card-footer">
														<div class="item-card9-footer d-flex">
															<div class="item-card9-cost">
																<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
															</div>
															<div class="ms-auto">
																<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 1 days ago</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-12 col-xl-4">
											<div class="card overflow-hidden">
												<div class="arrow-ribbon bg-pink">$567</div>
												<div class="item-card9-img">
													<div class="item-card9-imgs">
														<a href="col-left.html"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/b3.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-success tag-option">For Sale</div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
															<div class="rating-stars-container">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card-body">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Office</a>
														<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i> Agent</span></a>
														<a href="col-left.html" class="text-dark mt-2">
															<h4 class="font-weight-bold mt-1">Office rooms..</h4>
														</a>
														<ul class="item-card2-list">
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i> 1500 Sqft</a></li>
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 5 Beds</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a></li>
														</ul>
														<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
													</div>
												</div>
												<div class="card-footer">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 35 mins ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-12 col-xl-4">
											<div class="card overflow-hidden">
												<div class="arrow-ribbon bg-primary">$839</div>
												<div class="item-card9-img">
													<div class="item-card9-imgs">
														<a href="col-left.html"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/f4.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-info tag-option">For Rent </div>
														<div class="bg-pink tag-option">Hot </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
															<div class="rating-stars-container">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card-body">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Luxury rooms</a>
														<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i> Agent</span></a>
														<a href="col-left.html" class="text-dark mt-2">
															<h4 class="font-weight-bold mt-1">Apartment For Rent</h4>
														</a>
														<ul class="item-card2-list">
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i> 300 Sqft</a></li>
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 2 Beds</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 2 Bath</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
														</ul>
														<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
													</div>
												</div>
												<div class="card-footer">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 5 days ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-12 col-xl-4">
											<div class="card overflow-hidden">
												<div class="arrow-ribbon bg-secondary">$289</div>
												<div class="item-card9-img">
													<div class="item-card9-imgs">
														<a href="col-left.html"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/v1.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-success tag-option">For Sale </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
															<div class="rating-stars-container">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card-body">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Apartments</a>
														<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i> Owner</span></a>
														<a href="col-left.html" class="text-dark mt-2">
															<h4 class="font-weight-bold mt-1">Apartment For Rent</h4>
														</a>
														<ul class="item-card2-list">
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i> 2500 Sqft</a></li>
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 20 Beds</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 15 Bath</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 10 Car</a></li>
														</ul>
														<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
													</div>
												</div>
												<div class="card-footer">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 3 days ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-12 col-xl-4">
											<div class="card overflow-hidden">
												<div class="arrow-ribbon bg-primary">$289</div>
												<div class="item-card9-img">
													<div class="item-card9-imgs">
														<a href="col-left.html"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j2.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-info tag-option">For Rent </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="2">
															<div class="rating-stars-container">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card-body">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> 2BHK House</a>
														<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i> Owner</span></a>
														<a href="col-left.html" class="text-dark mt-2">
															<h4 class="font-weight-bold mt-1">2BHK house For Rent</h4>
														</a>
														<ul class="item-card2-list">
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i> 160 Sqft</a></li>
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 2 Beds</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
														</ul>
														<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
													</div>
												</div>
												<div class="card-footer">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 2 days ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-12 col-xl-4">
											<div class="card overflow-hidden">
												<div class="arrow-ribbon bg-secondary">$729</div>
												<div class="item-card9-img">
													<div class="item-card9-imgs">
														<a href="col-left.html"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/h3.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-success tag-option">For Sale </div>
														<div class="bg-pink tag-option">New </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
															<div class="rating-stars-container">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card-body">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Duplex House</a>
														<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i> Owner</span></a>
														<a href="col-left.html" class="text-dark mt-2">
															<h4 class="font-weight-bold mt-1">Duplex House For Sale</h4>
														</a>
														<ul class="item-card2-list">
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i> 1786 Sqft</a></li>
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 4 Beds</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 5 Bath</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 2 Car</a></li>
														</ul>
														<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
													</div>
												</div>
												<div class="card-footer">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 4 days ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-12 col-xl-4">
											<div class="card overflow-hidden">
												<div class="arrow-ribbon bg-secondary">$389</div>
												<div class="item-card9-img">
													<div class="item-card9-imgs">
														<a href="col-left.html"></a>
														<img src="https://www.spruko.com/demo/reallist/Reallist/assets/images/products/j1.png" alt="img" class="cover-image">
													</div>
													<div class="item-card9-icons">
														<a href="javascript:void(0);" class="item-card9-icons1 wishlist active" data-bs-toggle="tooltip" data-bs-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
														<a href="javascript:void(0);" class="item-card9-icons1 bg-purple" data-bs-toggle="tooltip" data-bs-placement="top" title="Share"> <i class="icon icon-share"></i></a>
													</div>
													<div class="item-tags">
														<div class="bg-danger tag-option">For Buy </div>
													</div>
													<div class="item-trans-rating">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
															<div class="rating-stars-container">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="card-body">
													<div class="item-card9">
														<a href="col-left.html" class="text-muted me-4"><i class="fa fa-tag me-1"></i> Garden House</a>
														<a href="javascript:void(0);" class=""><span class="text-muted"><i class="fa fa-user text-muted me-1"></i> Owner</span></a>
														<a href="col-left.html" class="text-dark mt-2">
															<h4 class="font-weight-bold mt-1">Graden House</h4>
														</a>
														<ul class="item-card2-list">
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-arrows-alt text-muted me-1"></i> 489 Sqft</a></li>
															<li><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bed text-muted me-1"></i> 5 Beds</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-bath text-muted me-1"></i> 3 Bath</a></li>
															<li class="mb-3"><a href="javascript:void(0);" class="icons text-muted"><i class="fa fa-car text-muted me-1"></i> 1 Car</a></li>
														</ul>
														<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
													</div>
												</div>
												<div class="card-footer">
													<div class="item-card9-footer d-flex">
														<div class="item-card9-cost">
															<a href="javascript:void(0);" class="me-4"><span class=""><i class="fa fa-map-marker text-muted me-1"></i> USA</span></a>
														</div>
														<div class="ms-auto">
															<a href="javascript:void(0);" class=""><span class=""><i class="fa fa-calendar-o text-muted me-1"></i> 15 mins ago</span></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="center-block text-center">
							<ul class="pagination mb-5 mb-lg-0">
								<li class="page-item page-prev disabled">
									<a class="page-link" href="javascript:void(0);" tabindex="-1">Prev</a>
								</li>
								<li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
								<li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
								<li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
								<li class="page-item page-next">
									<a class="page-link" href="javascript:void(0);">Next</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!--/Add lists-->
			</div>

			<!--Right Side Content-->
			<div class="col-xl-3 col-lg-4 col-md-12">
				<div class="card">
					<div class="card-body">
						<div class="input-group">
							<input type="text" class="form-control br-tl-3  br-bl-3" placeholder="Search">
							<button type="button" class="btn btn-primary br-tr-3  br-br-3">
								Search
							</button>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Categories</h3>
					</div>
					<div class="card-body">
						<div class="" id="container">
							<div class="filter-product-checkboxs">
								<label class="custom-control custom-checkbox mb-3">
									<input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
									<span class="custom-control-label">
										<span class="text-dark"> Houses, Apartment Flats, Shops, Commercial Lands<span class="label label-secondary float-end">14</span></span>
									</span>
								</label>
								<label class="custom-control custom-checkbox mb-3">
									<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
									<span class="custom-control-label">
										<span class="text-dark">Plots, Agri Lands<span class="label label-secondary float-end">22</span></span>
									</span>
								</label>
								<label class="custom-control custom-checkbox mb-3">
									<input type="checkbox" class="custom-control-input" name="checkbox3" value="option3">
									<span class="custom-control-label">
										<span class="text-dark">Paying Guest, Hostel, Girls Hostel, Manson<span class="label label-secondary float-end">78</span></span>
									</span>
								</label>
								<label class="custom-control custom-checkbox mb-3">
									<input type="checkbox" class="custom-control-input" name="checkbox4" value="option3">
									<span class="custom-control-label">
										<span class="text-dark">Hotel, Resorts, Rooms / Lodge<span class="label label-secondary float-end">35</span></span>
									</span>
								</label>
								<label class="custom-control custom-checkbox mb-3">
									<input type="checkbox" class="custom-control-input" name="checkbox5" value="option3">
									<span class="custom-control-label">
										<span class="text-dark">Party Hall, Trekking,and camping <span class="label label-secondary float-end">23</span></span>
									</span>
								</label>
								<label class="custom-control custom-checkbox mb-3">
									<input type="checkbox" class="custom-control-input" name="checkbox6" value="option3">
									<span class="custom-control-label">
										<span class="text-dark">Others services<span class="label label-secondary float-end">14</span></span>
									</span>
								</label>

							</div>
						</div>
					</div>
					<div class="card-header border-top">
						<h3 class="card-title">Price Range</h3>
					</div>
					<div class="card-body">
						<h6>
							<label for="price">Price Range:</label>
							<input type="text" id="price">
						</h6>
						<div id="mySlider"></div>
					</div>
					<div class="card-header border-top">
						<h3 class="card-title">Condition</h3>
					</div>
					<div class="card-body">
						<div class="filter-product-checkboxs">
							<label class="custom-control custom-checkbox mb-2">
								<input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
								<span class="custom-control-label">
									For Rent
								</span>
							</label>
							<label class="custom-control custom-checkbox mb-2">
								<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
								<span class="custom-control-label">
									For Sale
								</span>
							</label>
							<label class="custom-control custom-checkbox mb-0">
								<input type="checkbox" class="custom-control-input" name="checkbox3" value="option3">
								<span class="custom-control-label">
									For Buy
								</span>
							</label>
						</div>
					</div>
					<div class="card-header border-top">
						<h3 class="card-title">Posted By</h3>
					</div>
					<div class="card-body">
						<div class="filter-product-checkboxs">
							<label class="custom-control custom-checkbox mb-2">
								<input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
								<span class="custom-control-label">
									Owner
								</span>
							</label>
							<label class="custom-control custom-checkbox mb-2">
								<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
								<span class="custom-control-label">
									Agent
								</span>
							</label>
							<label class="custom-control custom-checkbox mb-0">
								<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
								<span class="custom-control-label">
									Builder
								</span>
							</label>
						</div>
					</div>
					<div class="card-footer">
						<a href="javascript:void(0);" class="btn btn-primary btn-block">Apply Filter</a>
					</div>
				</div>
				<div class="card mb-0">
					<div class="card-header">
						<h3 class="card-title">Shares</h3>
					</div>
					<div class="card-body product-filter-desc">
						<div class="product-filter-icons text-center">
							<a href="javascript:void(0);" class="facebook-bg"><i class="fa fa-facebook"></i></a>
							<a href="javascript:void(0);" class="twitter-bg"><i class="fa fa-twitter"></i></a>
							<a href="javascript:void(0);" class="google-bg"><i class="fa fa-google"></i></a>
							<a href="javascript:void(0);" class="dribbble-bg"><i class="fa fa-dribbble"></i></a>
							<a href="javascript:void(0);" class="pinterest-bg"><i class="fa fa-pinterest"></i></a>
						</div>
					</div>
				</div>
			</div>
			<!--/Right Side Content-->
		</div>
	</div>
</section>
<!--/Add Listings-->

<!-- Newsletter-->
<section class="sptb bg-white border-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-xl-6 col-md-12">
				<div class="sub-newsletter">
					<h3 class="mb-2"><i class="fa fa-paper-plane-o me-2"></i> Subscribe To Our Newsletter</h3>
					<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
				</div>
			</div>
			<div class="col-lg-5 col-xl-6 col-md-12">
				<div class="input-group sub-input mt-1">
					<input type="text" class="form-control input-lg " placeholder="Enter your Email">
					<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
						Subscribe
					</button>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/Newsletter-->

<!--Footer Section-->
<section class="main-footer">
	<footer class="bg-dark text-white">
		<div class="footer-main">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-12">
						<h6>About</h6>
						<hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis exercitation ullamco laboris </p>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum .</p>
					</div>
					<div class="col-lg-2 col-sm-6">
						<h6>Our Quick Links</h6>
						<hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<ul class="list-unstyled mb-0">
							<li><a href="javascript:;">Our Team</a></li>
							<li><a href="javascript:;">Contact US</a></li>
							<li><a href="javascript:;">About</a></li>
							<li><a href="javascript:;">Luxury Rooms</a></li>
							<li><a href="javascript:;">Blog</a></li>
							<li><a href="javascript:;">Terms</a></li>
						</ul>
					</div>

					<div class="col-lg-3 col-sm-6">
						<h6>Contact</h6>
						<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<ul class="list-unstyled mb-0">
							<li>
								<a href="javascript:void(0);"><i class="fa fa-home me-3 text-primary"></i> New York, NY 10012, US</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-envelope me-3 text-primary"></i> info12323@example.com</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-phone me-3 text-primary"></i> + 01 234 567 88</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-print me-3 text-primary"></i> + 01 234 567 89</a>
							</li>
						</ul>
						<ul class="list-unstyled list-inline mt-3">
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-facebook bg-facebook"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-twitter bg-info"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-google-plus bg-danger"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-linkedin bg-linkedin"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-12">
						<h6>Subscribe</h6>
						<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<div class="clearfix"></div>
						<div class="input-group w-100">
							<input type="text" class="form-control br-tl-3  br-bl-3 " placeholder="Email">
							<button type="button" class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button>
						</div>
						<h6 class="mb-0 mt-5">Payments</h6>
						<hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
						<div class="clearfix"></div>
						<ul class="footer-payments">
							<li class="ps-0"><a href="javascript:;"><i class="fa fa-cc-amex text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-visa text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-credit-card-alt text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-mastercard text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-paypal text-muted" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-dark text-white p-0">
			<div class="container">
				<div class="row d-flex">
					<div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center ">
						Copyright © 2022 <a href="javascript:void(0);" class="fs-14 text-primary">Reallist</a>. Designed with <i class="fa fa-heart text-danger"></i> by <a href="javascript:void(0);" class="fs-14 text-primary">Spruko</a> All rights reserved.
					</div>
				</div>
			</div>
		</div>
	</footer>
</section>
<!--Footer Section-->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-rocket"></i></a>

<!-- JQuery js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery-3.6.0.min.js"></script>

<!-- Bootstrap js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/popper.min.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/js/bootstrap.min.js"></script>

<!--JQuery RealEstaterkline Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery.sparkline.min.js"></script>

<!-- Circle Progress Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/circle-progress.min.js"></script>

<!-- Star Rating Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/rating/jquery.rating-stars.js"></script>

<!--Owl Carousel js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.js"></script>

<!--Horizontal Menu-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/horizontal-menu/horizontal.js"></script>

<!--JQuery TouchSwipe js-->
<script src="<?= base_url() ?>vr_propertyassets/js/jquery.touchSwipe.min.js"></script>

<!--Select2 js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.full.min.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/js/select2.js"></script>

<!--Map js -->
<script src="https://maps.google.com/maps/api/js?key=AIzaSyAJXHqnGy3iKO5P7veP9_vo51A6zuP4ZiE"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/map/jquery.axgmap.js"></script>

<!-- Cookie js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/jquery.ihavecookies.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/cookie.js"></script>

<!-- Ion.RangeSlider -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/jquery-uislider/jquery-ui.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/js/uislider.js"></script>

<!-- P-scroll bar Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/mapscroll.js"></script>

<!-- sticky Js-->
<script src="<?= base_url() ?>vr_propertyassets//js/sticky.js"></script>

<!--Showmore Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/jquery.showmore.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/js/showmore.js"></script>

<!-- Scripts Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/owl-carousel.js"></script>

<!-- themecolor Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/themeColors.js"></script>

<!-- Custom Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/custom.js"></script>

<!-- Custom-switcher Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/custom-switcher.js"></script>

</body>

</html>