<?php $this->load->view('template/header');
?>
<style>
	.select2-container {
		width: 50% !important;
	}

	input[name="title"] {
		width: 50% !important;
	}
</style>
<!--Sliders Section-->
<section>
	<div class="bannerimg cover-image bg-background3" data-bs-image-src="https://www.spruko.com/demo/reallist/Reallist/assets/images/banners/banner2.jpg">
		<div class="header-text mb-0">
			<div class="container">
				<div class="text-center text-white">
					<h1>Customer request</h1>
					<ol class="breadcrumb text-center">
						<li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
						<li class="breadcrumb-item"><a href="javascript:void(0);">Pages</a></li>
						<li class="breadcrumb-item active text-white" aria-current="page">Customer request</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/Sliders Section-->

<!--Customer requests-section-->
<section class="sptb">
	<div class="container">
		<div class="row ">
			<div class=" col-md-12">
				<div class="card ">
					<div class="card-header ">
						<h3 class="card-title">Customer request</h3>
					</div>
					<?php if ($this->session->flashdata('status') == 'success') { ?>
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							<?= $this->session->flashdata('msg'); ?>
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div><?php } ?>
					<form action="<?= site_url() ?>vr_property/save_customerreq" method="post">
						<div class="card-body">
							<div class="form-group">
								<label class="form-label text-dark">Request Title</label>
								<input type="text" name="title" class="form-control" placeholder="Title" required>
							</div>
							<div class="form-group">
								<label class="form-label text-dark">Category</label>
								<select class="form-control  select2 form-select" name="category" required>
									<option value="0">Select Category</option>
									<?php foreach ($category as $categorys) : ?>
										<option value="<?= $categorys->id; ?>"><?= $categorys->categoryname; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<label class="form-label text-dark">Type Of Ad</label>
								<div class="d-md-flex ad-post-details">
									<label class="custom-control custom-radio mb-2 me-4">
										<input type="radio" class="custom-control-input" name="type" value="1" checked="">
										<span class="custom-control-label">I Want to Sell</span>
									</label>
									<label class="custom-control custom-radio  mb-2 me-4">
										<input type="radio" class="custom-control-input" name="type" value="2">
										<span class="custom-control-label">I Want to Buy</span>
									</label>
									<label class="custom-control custom-radio  mb-2">
										<input type="radio" class="custom-control-input" name="type" value="3">
										<span class="custom-control-label">I Want to Rent</span>
									</label>
								</div>
							</div>
							<div class="form-group">
								<label class="form-label text-dark">Who We Are</label>
								<div class="d-md-flex ad-post-details">
									<label class="custom-control custom-radio mb-2 me-4">
										<input type="radio" class="custom-control-input" name="whoweare" value="1" checked="">
										<span class="custom-control-label">Owner </span>
									</label>
									<label class="custom-control custom-radio  mb-2 me-4">
										<input type="radio" class="custom-control-input" name="whoweare" value="2">
										<span class="custom-control-label">Agent</span>
									</label>
									<label class="custom-control custom-radio  mb-2">
										<input type="radio" class="custom-control-input" name="whoweare" value="3">
										<span class="custom-control-label">Builder</span>
									</label>
								</div>
							</div>
							<div class="form-group">
								<label class="form-label text-dark">Description</label>
								<textarea class="form-control" name="description" rows="6" placeholder="Description" required></textarea>
							</div>
							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">Name</label>
										<input type="text" class="form-control" name="name" placeholder="Name" required>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">Email</label>
										<input type="email" class="form-control" name="email" placeholder="Email Address" required>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group mb-0">
										<label class="form-label">Phone Number</label>
										<input type="text" class="form-control" name="mobile" placeholder="Number" required>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group mb-0">
										<label class="form-label">Address</label>
										<input type="text" class="form-control" name="address" placeholder="Address" required>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer ">
							<button type="submit" class="btn btn-success">Submit Now</button>
						</div>
					</form>
				</div>
			</div>



		</div>
	</div>
</section>
<!--/Customer requests-section-->

<!-- Newsletter-->
<section class="sptb bg-white border-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-xl-6 col-md-12">
				<div class="sub-newsletter">
					<h3 class="mb-2"><i class="fa fa-paper-plane-o me-2"></i> Subscribe To Our Newsletter</h3>
					<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
					</p>
				</div>
			</div>
			<div class="col-lg-5 col-xl-6 col-md-12">
				<div class="input-group sub-input mt-1">
					<input type="text" class="form-control input-lg " placeholder="Enter your Email">
					<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
						Subscribe
					</button>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/Newsletter-->

<!--Footer Section-->
<section class="main-footer">
	<footer class="bg-dark text-white">
		<div class="footer-main">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-12">
						<h6>About</h6>
						<hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
							ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis exercitation ullamco
							laboris </p>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum .</p>
					</div>
					<div class="col-lg-2 col-sm-6">
						<h6>Our Quick Links</h6>
						<hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<ul class="list-unstyled mb-0">
							<li><a href="javascript:;">Our Team</a></li>
							<li><a href="javascript:;">Contact US</a></li>
							<li><a href="javascript:;">About</a></li>
							<li><a href="javascript:;">Luxury Rooms</a></li>
							<li><a href="javascript:;">Blog</a></li>
							<li><a href="javascript:;">Terms</a></li>
						</ul>
					</div>

					<div class="col-lg-3 col-sm-6">
						<h6>Contact</h6>
						<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<ul class="list-unstyled mb-0">
							<li>
								<a href="javascript:void(0);"><i class="fa fa-home me-3 text-primary"></i> New York,
									NY 10012, US</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-envelope me-3 text-primary"></i>
									info12323@example.com</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-phone me-3 text-primary"></i> + 01 234
									567 88</a>
							</li>
							<li>
								<a href="javascript:void(0);"><i class="fa fa-print me-3 text-primary"></i> + 01 234
									567 89</a>
							</li>
						</ul>
						<ul class="list-unstyled list-inline mt-3">
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-facebook bg-facebook"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-twitter bg-info"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-google-plus bg-danger"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
									<i class="fa fa-linkedin bg-linkedin"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-12">
						<h6>Subscribe</h6>
						<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<div class="clearfix"></div>
						<div class="input-group w-100">
							<input type="text" class="form-control br-tl-3  br-bl-3 " placeholder="Email">
							<button type="button" class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button>
						</div>
						<h6 class="mb-0 mt-5">Payments</h6>
						<hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
						<div class="clearfix"></div>
						<ul class="footer-payments">
							<li class="ps-0"><a href="javascript:;"><i class="fa fa-cc-amex text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-visa text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-credit-card-alt text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-mastercard text-muted" aria-hidden="true"></i></a></li>
							<li><a href="javascript:;"><i class="fa fa-cc-paypal text-muted" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-dark text-white p-0">
			<div class="container">
				<div class="row d-flex">
					<div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center ">
						Copyright © 2022 <a href="javascript:void(0);" class="fs-14 text-primary">Reallist</a>.
						Designed with <i class="fa fa-heart text-danger"></i> by <a href="javascript:void(0);" class="fs-14 text-primary">Spruko</a> All rights reserved.
					</div>
				</div>
			</div>
		</div>
	</footer>
</section>
<!--Footer Section-->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-rocket"></i></a>
<script>
	document.getElementById('res_toggle').addEventListener('click', () => {
		document.getElementById('navul1').classList.toggle('toggleshow')
	})
</script>
<!-- JQuery js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery-3.6.0.min.js"></script>

<!-- Bootstrap js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/popper.min.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/bootstrap-5/js/bootstrap.min.js"></script>

<!--JQuery RealEstaterkline Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/jquery.sparkline.min.js"></script>

<!-- Circle Progress Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/vendors/circle-progress.min.js"></script>

<!-- Star Rating Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/rating/jquery.rating-stars.js"></script>

<!--Owl Carousel js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/owl-carousel/owl.carousel.js"></script>

<!--Horizontal Menu-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/horizontal-menu/horizontal.js"></script>

<!--JQuery TouchSwipe js-->
<script src="<?= base_url() ?>vr_propertyassets/js/jquery.touchSwipe.min.js"></script>

<!--Select2 js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/select2/select2.full.min.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/js/select2.js"></script>

<!-- Cookie js -->
<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/jquery.ihavecookies.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/cookie/cookie.js"></script>

<!-- sticky Js-->
<script src="<?= base_url() ?>vr_propertyassets//js/sticky.js"></script>

<!-- Vertical scroll bar Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/vertical-scroll/jquery.bootstrap.newsbox.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/vertical-scroll/vertical-scroll.js"></script>

<!--File-Uploads Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/jquery.ui.widget.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/jquery.fileupload.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/jquery.iframe-transport.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/jquery.fancy-fileupload.js"></script>
<script src="<?= base_url() ?>vr_propertyassets/plugins/fancyuploder/fancy-uploader.js"></script>

<!-- P-scroll bar Js-->
<script src="<?= base_url() ?>vr_propertyassets/plugins/pscrollbar/perfect-scrollbar.js"></script>

<!-- Swipe Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/swipe.js"></script>

<!-- Scripts Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/owl-carousel.js"></script>

<!-- themecolor Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/themeColors.js"></script>

<!-- Custom Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/custom.js"></script>

<!-- Custom-switcher Js-->
<script src="<?= base_url() ?>vr_propertyassets/js/custom-switcher.js"></script>

</body>

</html>