<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vr_property extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vrproperty_Model');
    }

    //keep controller name same as filename
    public function index()
    {
        $data['adposts'] = $this->Vrproperty_Model->get_adspost();
        $this->load->view('index', $data);
    }

    public function search_ads()
    {
        $category = $this->input->post('category');
        $location = $this->input->post('location');
        $manual_latitude = $this->input->post('manual_latitude');
        $manual_longitude = $this->input->post('manual_longitude');
        $type = $this->input->post('type');


        $data['post_data'] = $_POST;

        $data['adposts'] = $this->Vrproperty_Model->get_adspostbysearch($_POST);
        // echo $this->db->last_query();die;
        $data['cor'] = $manual_latitude . ',' . $manual_longitude;
        $this->load->view('index', $data);
    }

    public function about()
    {
        $this->load->view('about');
    }

    public function adposts()
    {
        $data['categorys'] = $this->Vrproperty_Model->get_category();
        $this->load->view('ad-posts', $data);
    }

    public function contact()
    {
        $this->load->view('contact');
    }

    public function customerreq()
    {
        $data['category'] = $this->Vrproperty_Model->get_category();
        $this->load->view('customerreq', $data);
    }

    public function blog_list()
    {
        $this->load->view('blog-list');
    }

    public function page_list_map2()
    {
        $data['adposts'] = $this->Vrproperty_Model->get_adspost();
        $this->load->view('page-list-map2', $data);
    }

    public function col_left()
    {
        $this->load->view('col-left');
    }

    public function add_adposts()
    {

        // echo "<pre>";
        // print_r ($_POST);
        // echo "</pre>";die;
        if (!empty($this->session->userdata('id'))) {
            $userid = $this->session->userdata('id');
        } else {
            $userid = 0;
        }
        $ad_title = $this->input->post('ad_title');
        $category = $this->input->post('category');
        $typeofad = $this->input->post('typeofad');
        $whoweare = $this->input->post('whoweare');
        $description = $this->input->post('description');
        $upload_images = $this->input->post('upload_images');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone_number = $this->input->post('phone_number');
        $address = $this->input->post('address');

        if (empty($this->input->post('manual_location'))) {
            $state = $this->input->post('state');
            $cityname = $this->input->post('cityname');
            $country = $this->input->post('country');
            $current_latitude = $this->input->post('current_latitude');
            $current_longitude = $this->input->post('current_longitude');
        } else {
            $manual_location = $this->input->post('manual_location');
            $mlocation = explode(',', $manual_location);
            $cityname = $mlocation[0];
            $state = $mlocation[1];
            $country = $mlocation[2];
            $current_latitude = $this->input->post('manual_latitude');
            $current_longitude = $this->input->post('manual_longitude');
        }

        $insertdata = array(
            "product_name" => $ad_title,
            "category_id" => $category,
            "state" => $state,
            "city" => $cityname,
            "country" => $country,
            "current_latitude" => $current_latitude,
            "current_longitude" => $current_longitude,
            "type" => $typeofad,
            "description" => $description,
            "mobile" => $phone_number,
            "email" => $email,
            "name" => $name,
            "whoweare" => $whoweare,
            "address" => $address,
            "userid" => $userid
        );

        $inserted_id = $this->Vrproperty_Model->insert_adposts($insertdata);
        $this->multiple_files($inserted_id);
        $this->session->set_flashdata('status', 'success');
        $this->session->set_flashdata('msg', 'your post successfully posted');
        redirect(base_url('vr_property/adposts'), 'refresh');
    }

    public function multiple_files($id)
    {
        //Image Size	
        $image_size = $this->config->item('image_sizes');

        //Upload Configuration Image
        $config['upload_path'] = './property_adpostimages/' . $id . '/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['rename'] = true;
        $config['image_sizes'] = $image_size;
        $files = $_FILES;


        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0755, TRUE);
        }
        $mum_files = count($files['upload_images']);
        $dataInfo = array();
        for ($i = 0; $i < $mum_files; $i++) {
            if (isset($files['upload_images']['name'][$i])) {
                $config['file_name'] = time() . '-' . $files['upload_images']['name'][$i];
                $this->load->library("upload", $config);
                $_FILES['upload_images']['name'] = $files['upload_images']['name']["$i"];
                $_FILES['upload_images']['type'] = $files['upload_images']['type']["$i"];
                $_FILES['upload_images']['tmp_name'] = $files['upload_images']['tmp_name']["$i"];
                $_FILES['upload_images']['error'] = $files['upload_images']['error']["$i"];
                $_FILES['upload_images']['size'] = $files['upload_images']['size']["$i"];
                $filename = rand() . '-' . $_FILES['upload_images']['name'];

                if (!$this->upload->do_upload('upload_images')) {
                    $error_message = $this->upload->display_errors();

                    $this->session->set_flashdata('status', 'error');
                    $this->session->set_flashdata('message', "$error_message");
                } else {
                    $this->session->set_flashdata('status', 'success');
                    $this->session->set_flashdata('message', "AD Posted Successfully");
                }

                $dataInfo[] = $this->upload->data();
            }
        }

        $all_imgs = '';

        if (count($dataInfo) > 0) {
            foreach ($dataInfo as $info) {
                $all_imgs .= $info['file_name'];
                $all_imgs .= ',';
            }
        }

        $insert_data = array(
            'images' => rtrim($all_imgs, ","),
            'adposts_id' => $id,
            'status' => 1

        );

        $this->Vrproperty_Model->upload_images($insert_data, $id);

        redirect(base_url('vr_property/adposts'), 'refresh');
    }

    public function ajax_fetchmapdata()
    {
        $adposts = $this->Vrproperty_Model->get_adspost();
        if ($adposts == false) {
            echo json_encode(array('result' => null));
        } else {
            // echo "<pre>";
            // print_r ($adposts);
            // echo "</pre>";die;
            $result = '';
            foreach ($adposts as $adpost) {
                $result .= '<div class="axgmap-img" data-latlng="' . $adpost->current_latitude . ',' . $adpost->current_longitude . '" data-marker-image="' . base_url() . 'vr_propertyassets/images/map/1.png" data-title="' . $adpost->product_name . '">
            <h4>' . $adpost->categoryname . '</h4>
            <img src="' . base_url() . "property_adpostimages/" . $adpost->id . "/images/" . $adpost->images . '" alt="img" class="w-150 h100 mb-3 mt-2">
            <div>Price: <a class="h4"> ₹' . $adpost->price . '</a></div>
            <div class="rating-stars block mt-2 mb-2">
                <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
                <div class="rating-stars-container">
                    <div class="rating-star sm">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="rating-star sm">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="rating-star sm">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="rating-star sm">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="rating-star sm">
                        <i class="fa fa-star"></i>
                    </div>
                </div>
            </div>
            <p class="fs-16"><i class="fa fa-phone me-2"></i> ' . $adpost->mobile . '</p>
            <a href="javascript:void(0);" class="btn btn-sm btn-primary btn-pill">Get Directions</a>
            </div>';
            }

            echo json_encode(array('result' => $result));
        }
    }

    public function save_customerreq()
    {

        if (!empty($this->session->userdata('id'))) {
            $userid = $this->session->userdata('id');
        } else {
            $userid = 0;
        }

        $title = $this->input->post('title');
        $category = $this->input->post('category');
        $type = $this->input->post('type');
        $whoweare = $this->input->post('whoweare');
        $description = $this->input->post('description');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $address = $this->input->post('address');

        $insertdata = array(
            "product_name" => $title,
            "category_id" => $category,
            "type" => $type,
            "description" => $description,
            "mobile" => $mobile,
            "email" => $email,
            "name" => $name,
            "whoweare" => $whoweare,
            "address" => $address,
            "userid" => $userid
        );

        $inserted_id = $this->Vrproperty_Model->insert_customer_req($insertdata);
        // $this->multiple_files($inserted_id);
        $this->session->set_flashdata('status', 'success');
        $this->session->set_flashdata('msg', 'your post successfully posted');
        redirect(base_url('vr_property/customerreq'), 'refresh');
    }
}
