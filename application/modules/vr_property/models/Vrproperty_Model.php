<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Vrproperty_Model extends CI_Model
{

        public function get_ad_posts()
        {
                $q = $this->db->get('property_adposts');
                return $q->result();
        }

        public function get_category()
        {
                $this->db->where('isactive', 1);
                $q = $this->db->get('property_category');
                return $q->result();
        }
        public function insert_adposts($insertdata)
        {
                $this->db->insert('property_adposts', $insertdata);
                $lastid = $this->db->insert_id();

                return $lastid;
        }

        public function upload_images($insert_data, $id)
        {
                $this->db->insert('property_adpost_img', $insert_data);
                return true;
        }

        public function get_adspost()
        {
                $this->db->select('property_adposts.*, property_adpost_img.images as images, property_category.categoryname as categoryname, property_type.typename as typename');
                $this->db->from('property_adposts');
                $this->db->join('property_adpost_img', 'property_adposts.id = property_adpost_img.adposts_id');
                $this->db->join('property_category', 'property_adposts.category_id = property_category.id');
                $this->db->join('property_type', 'property_adposts.type = property_type.id');
                $this->db->where('property_adposts.isactive', 1);

                $q = $this->db->get();
                if ($q->num_rows() > 0) {
                        return $q->result();
                } else {
                        return false;
                }
        }

        public function insert_customer_req($insertdata)
        {
                // $this->db->query("INSERT INTO `property_customer_request` (`product_name`, `category_id`, `type`, `description`, `mobile`, `email`, `name`, `whoweare`, `address`, `userid`) VALUES ('".$insertdata['product_name']."', '".$insertdata['category_id']."', '".$insertdata['type']."', '".$insertdata['description']."', '".$insertdata['mobile']."', '".$insertdata['email']."', '".$insertdata['name']."', '".$insertdata['whoweare']."', '".$insertdata['address']."', '".$insertdata['userid']."')");
                $this->db->insert('property_customer_request', $insertdata);
                $lastid = $this->db->insert_id();
                return $lastid;
        }

        public function get_adspostbysearch()
        {
                $category = $this->input->post('category');
                $location = $this->input->post('location');
                $manual_latitude = $this->input->post('manual_latitude');
                $manual_longitude = $this->input->post('manual_longitude');
                $type = $this->input->post('type');

                $this->db->select('property_adposts.*, property_adpost_img.images as images, property_category.categoryname as categoryname, property_type.typename as typename');
                $this->db->from('property_adposts');
                $this->db->join('property_adpost_img', 'property_adposts.id = property_adpost_img.adposts_id');
                $this->db->join('property_category', 'property_adposts.category_id = property_category.id');
                $this->db->join('property_type', 'property_adposts.type = property_type.id');
                $this->db->where('property_adposts.isactive', 1);
                if ($category != 0) {
                        $this->db->where('category_id', $category);
                }
                if ($type != 0) {
                        $this->db->where('type', $type);
                }
                if (!empty($location)) {
                        $location_arr = explode(', ', $location);
                        $city = $location_arr[0];
                        $state = $location_arr[1];
                        $country = $location_arr[2];
                        if(!empty($city)){
                                $this->db->like('city', $city, 'both');
                        }
                        if(!empty($state)){
                                $this->db->or_like('state', $state, 'both');
                        }
                        if(!empty($country)){
                                $this->db->or_like('country', $country, 'both');
                        }
                }

                $q = $this->db->get();
                if ($q->num_rows() > 0) {
                        return $q->result();
                } else {
                        return false;
                }
        }
}
