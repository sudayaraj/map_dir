<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "map_directory";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM adposts";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
$data=array();
  while($row = $result->fetch_assoc()) {
    
        $data []= 
            [
                'id' => $row['id'],
                'latitude' => $row['current_latitude'],
                'longitude' => $row['current_longitude'],
                'featured' => 1,
                'title' => $row['product_name'],
                'location' => $row['city'],
                'city' => 1,
                'phone' => $row['mobile'],
                'category' => "Car Rental",
                'rating' => "",
                'reviews_number' => "",
                'marker_image' => "assets/img/items/29.jpg",
                'gallery' => array(
                    "assets/img/items/29.jpg",
                    "assets/img/items/11.jpg",
                    "assets/img/items/12.jpg"
                ),
                'price' => $row['price'],
                'tags' => array(
                    "Diesel",
                    "First Owner",
                    "4x4",
                    "Air Conditioning"
                ),
                'additional_info' => "",
                'url' => "detail.html",
                'description' => "Vivamus vitae lacus accumsan, gravida orci sit amet, convallis erat. Sed at porttitor quam. Proin faucibus lacus et massa tempus, sed mattis justo elementum. Proin mauris felis, laoreet quis lacus non, mattis venenatis massa. ",
                'description_list' => array(
                    [
                        'title' => "Engine",
                        'value' => "Diesel",
                    ],
                    [
                        'title' => "Mileage",
                        'value' => 14500,
                    ],
                    [
                        'title' => "Max Speed",
                        'value' => "220 Mph",
                    ],
                    [
                        'title' => "marker_color",
                        'value' => "Dark Brown",
                    ],
                    [
                        'title' => "Status",
                        'value' => "Sale",
                    ],
                ),
                'marker_color' => "#45ad00"
            ];
    
  }
  echo json_encode($data);
} else {
  echo "0 results";
}
$conn->close();



