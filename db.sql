-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2023 at 10:42 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `map_directory`
--

-- --------------------------------------------------------

--
-- Table structure for table `adposts`
--

CREATE TABLE `adposts` (
  `id` int(11) NOT NULL,
  `product_name` varchar(150) NOT NULL,
  `category_id` int(11) NOT NULL,
  `category` varchar(150) DEFAULT NULL,
  `type` varchar(50) NOT NULL,
  `description` varchar(600) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `current_latitude` varchar(50) NOT NULL,
  `current_longitude` varchar(50) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `price` varchar(100) NOT NULL,
  `username` varchar(150) NOT NULL,
  `userid` int(50) NOT NULL,
  `isactive` int(11) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `expiry_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `adposts`
--

INSERT INTO `adposts` (`id`, `product_name`, `category_id`, `category`, `type`, `description`, `state`, `city`, `country`, `current_latitude`, `current_longitude`, `mobile`, `email`, `price`, `username`, `userid`, `isactive`, `created_datetime`, `expiry_date`) VALUES
(1, 'i20', 6, 'car', 'owner', 'hyundai', ' Tamil Nadu 642126', ' Udumalaipettai', ' India', '10.6021942', '77.2584488', '9566970915', 'test@gmail.com', '650000', 'testuser', 2, 1, '2023-05-07 09:03:01', NULL),
(2, 'i20', 6, 'car', 'owner', 'test', ' Tamil Nadu', 'Coimbatore', ' India', '11.0168445', '76.9558321', '9566970915', 'test@gmail.com', '650000', 'testuser', 2, 1, '2023-05-07 09:03:07', NULL),
(3, 'Royal Enfield', 37, 'bike', 'owner', '', ' Coimbatore', 'R.S. Puram', ' Tamil Nadu', '11.0101698', '76.95043729999999', '9566123456', 'test@gmail.com', '150000', 'testuser', 2, 1, '2023-05-07 09:03:11', NULL),
(4, 'Honda Amaze', 0, 'Second hand car sale', '1', 'Honda Amaze with good condition', ' Tamil Nadu', 'Mettupalayam', ' India', '11.3027849', '76.9383385', '9566970915', 'test@gmail.com', '800000', 'test@gmail.com', 3, 1, '2023-05-07 09:38:21', NULL),
(5, 'Cars', 0, 'Second hand car sale', '1', 'test car', ' Tamil Nadu', 'Coimbatore', ' India', '11.0168445', '76.9558321', '9566970915', 'test@gmail.com', '560000', 'testuser', 2, 1, '2023-05-21 05:15:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `adpost_img`
--

CREATE TABLE `adpost_img` (
  `id` int(11) NOT NULL,
  `adposts_id` int(11) NOT NULL,
  `images` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `adpost_img`
--

INSERT INTO `adpost_img` (`id`, `adposts_id`, `images`, `status`, `created_datetime`) VALUES
(1, 1, '1681586002-925704919s.jpg,1681586002-925704919s1.jpg,1681586002-925704919s2.jpg', 1, '2023-04-15 19:13:23'),
(2, 2, '1681632314-925704919s.jpg', 1, '2023-04-16 08:05:15'),
(3, 3, '1682699491-Royal_Enfield_8.jpg', 1, '2023-04-28 16:31:32'),
(4, 4, '1683452301-honda_amaze.jpg', 1, '2023-05-07 09:38:22'),
(5, 5, '1684646116-car.jpg', 1, '2023-05-21 05:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `categoryname` varchar(150) NOT NULL,
  `isactive` int(11) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `categoryname`, `isactive`, `created_datetime`) VALUES
(1, 'Home services', 1, '2023-04-30 08:34:23'),
(2, 'Hotal & Restaurant', 1, '2023-04-30 08:34:23'),
(3, 'Property Buy/Rental/Lease', 1, '2023-04-30 08:34:23'),
(4, 'Computer service', 1, '2023-04-30 08:34:23'),
(5, 'Car Mechanic', 1, '2023-04-30 08:34:23'),
(6, 'Second hand car sale', 1, '2023-04-30 08:34:23'),
(7, 'Borewell', 1, '2023-04-30 08:34:23'),
(8, 'Carpender', 1, '2023-04-30 08:34:23'),
(9, 'Cladding', 1, '2023-04-30 08:34:23'),
(10, 'Construction', 1, '2023-04-30 08:34:23'),
(11, 'Electrical', 1, '2023-04-30 08:34:23'),
(12, 'Electrician', 1, '2023-04-30 08:34:23'),
(13, 'Home Nursing', 1, '2023-04-30 08:34:23'),
(14, 'House Cleaning', 1, '2023-04-30 08:34:23'),
(15, 'Manpower', 1, '2023-04-30 08:34:23'),
(16, 'Painter', 1, '2023-04-30 08:34:23'),
(17, 'Partition', 1, '2023-04-30 08:34:23'),
(18, 'Paver Blocks', 1, '2023-04-30 08:34:23'),
(19, 'Plumber', 1, '2023-04-30 08:34:23'),
(20, 'CCTV Camera', 1, '2023-04-30 08:34:23'),
(21, 'Home Cook', 1, '2023-04-30 08:34:23'),
(22, 'Auditor', 1, '2023-04-30 08:34:23'),
(23, 'Lawyer', 1, '2023-04-30 08:34:23'),
(24, 'Repair and Installation', 1, '2023-04-30 08:34:23'),
(25, 'System provider', 1, '2023-04-30 08:34:23'),
(26, 'Packers and Movers', 1, '2023-04-30 08:34:23'),
(27, 'AC services', 1, '2023-04-30 08:34:23'),
(28, 'Marriage Assembler', 1, '2023-04-30 08:34:23'),
(29, 'Delivery Services', 1, '2023-04-30 08:34:23'),
(30, 'Astrologer', 1, '2023-04-30 08:34:23'),
(31, 'Astro Numerology', 1, '2023-04-30 08:36:35'),
(32, 'Pandit', 1, '2023-04-30 08:36:35'),
(33, 'Catering Services', 1, '2023-04-30 08:36:35'),
(34, 'Man power supplier ', 1, '2023-04-30 08:36:35'),
(35, 'Security Guard', 1, '2023-04-30 08:36:35'),
(36, 'Yoga teacher and trainer', 1, '2023-04-30 08:36:35'),
(37, 'Second hand bike sale', 1, '2023-04-30 11:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `keyword`
--

CREATE TABLE `keyword` (
  `id` int(11) NOT NULL,
  `keyword` varchar(150) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `keyword`
--

INSERT INTO `keyword` (`id`, `keyword`, `category_id`, `created_datetime`) VALUES
(1, 'laptop', 4, '2023-04-30 09:49:01'),
(2, 'apple laptop', 4, '2023-04-30 09:49:01'),
(3, 'laptop i3', 4, '2023-04-30 09:49:01'),
(4, 'computer table', 4, '2023-04-30 09:51:21'),
(5, 'computer set', 4, '2023-04-30 09:51:21'),
(6, 'computer monitor', 4, '2023-04-30 09:51:21'),
(7, 'desktop computer', 4, '2023-04-30 09:51:21'),
(8, 'car', 6, '2023-04-30 09:53:39'),
(9, 'cars swift dizer vdi', 6, '2023-04-30 09:53:39'),
(10, 'cars maruti suzuki 800 ac', 6, '2023-04-30 09:53:39'),
(11, 'cars i20', 6, '2023-04-30 09:54:04'),
(12, 'cars i10', 6, '2023-04-30 09:54:04');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `typename` varchar(150) NOT NULL,
  `isactive` int(11) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `typename`, `isactive`, `created_datetime`) VALUES
(1, 'Owner / Self', 1, '2023-04-30 08:20:32'),
(2, 'Broker', 1, '2023-04-30 08:20:32'),
(3, 'Agency', 1, '2023-04-30 08:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(150) NOT NULL,
  `level` int(11) NOT NULL COMMENT '1=Admin, 2=Users',
  `status` int(11) NOT NULL,
  `created_datatime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `level`, `status`, `created_datatime`) VALUES
(1, 'admin', 'admin123', 1, 1, '2023-04-08 12:23:46'),
(2, 'testuser', 'test123', 2, 1, '2023-04-08 12:23:46'),
(3, 'test@gmail.com', '123', 2, 1, '2023-04-30 17:36:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adposts`
--
ALTER TABLE `adposts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adpost_img`
--
ALTER TABLE `adpost_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keyword`
--
ALTER TABLE `keyword`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adposts`
--
ALTER TABLE `adposts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `adpost_img`
--
ALTER TABLE `adpost_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `keyword`
--
ALTER TABLE `keyword`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;